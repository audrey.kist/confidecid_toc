  
%% CONFIDENC  E DECID RUNNER
clear all;
   
clearvars; 
close all;
clc;

sca;  

Screen('Preference', 'SkipSyncTests', 0);
opengl('save','software');

%% Pathways
runner.pathway = fileparts(mfilename('fullpath')); % Dossier 'ConfiDecid'
runner.resultdir =  fullfile(runner.pathway, 'Sujets'); % Dossier 'Sujets' � l'int�rieur du dossier 'ConfiDecid'
runner.stimulidir = fullfile(runner.pathway, 'Stimuli'); % Dossier 'Stimuli' � l'int�rieur du dossier 'ConfiDecid'
runner.instructdir = fullfile(runner.pathway, 'Stimuli', 'instructions_confidecid'); % Dossier 'instructions_confidecid' � l'int�rieur du dossier 'Stimuli'
cd(runner.pathway);
addpath(genpath(runner.pathway)); % Add folder and its subfolders to search path

%% Participant infos

% Affichage d'une bo�te de dialogue pour demander les infos de la session
try
    UIoutput = ConfiDecid_gui(runner.pathway); % pathway = chemin du dossier 'Decid_Matlab'
catch
    return
end

global participant
participant = [];

% Identifiant du participant
participant.identifier = sprintf('%s_%d_%s', UIoutput.city, year(datetime), UIoutput.identifier);

% Infos g�n�rales sur l'exp�rience
participant.date = datestr(now,'yyyy/mm/dd - HH:MM:SS'); % Date
[~,h] = system('hostname'); % Machine utilis�e lors de l'exp�rience
participant.hostname = deblank(h); % Stock�e dans hostname
participant.city = UIoutput.city; % Ville o� de d�roule l'exp�rience
participant.task = 'ConfiDecid';
participant.protocol = UIoutput.task; % Cognition, Stim...
participant.info.nameid = UIoutput.identifier; % Name in the form: NOMp
participant.info.birth = UIoutput.participant.birth; % Date de naissance
participant.info.age = UIoutput.participant.age; % �ge
participant.info.sex = UIoutput.participant.sex; % H = homme ; F = femme
participant.info.laterality = UIoutput.participant.laterality; % L = left-handed ; R = right-handed
participant.eyetracker = UIoutput.eye{2,:}; % Eye-tracker
participant.dataFolder = fullfile(runner.resultdir, participant.identifier); % Dossier du participant (cr�� lors du loca)

%% Save task dependent informations

if strcmp(UIoutput.task, 'cognition')
    participant.session = UIoutput.session; % 0 = Loca (training), 1,2,3 = DECID experiment
    participant.sess_name = sprintf('%s%d_%s', participant.task, participant.session, participant.protocol); % i.e. ConfiDecid1_cognition...
    participant.trials = UIoutput.trials; % Number of trials/sess
elseif strcmp(UIoutput.task, 'stim') && isfield(UIoutput, 'elec') % Stim task
    participant.elec = UIoutput.elec;
    participant.intensity = UIoutput.intensity;
    participant.sess_name = sprintf('%s_%s_%s_%dmA', participant.task, participant.protocol, participant.elec, participant.intensity); % i.e ConfiDecid_stim_X3-4_2mA...
elseif strcmp(UIoutput.task, 'stim') && ~isfield(UIoutput, 'elec') % Stim training
    participant.sess_name = sprintf('%s_%s_training', participant.task, participant.protocol); % ConfiDecid_stim_training
elseif strcmp(UIoutput.task, 'realtime') && isfield(UIoutput, 'session') % Real time task
    participant.session = UIoutput.session;
    participant.elec = UIoutput.elec;
    participant.sess_name = sprintf('%s%d_%s_%s', participant.task, participant.session, participant.protocol, participant.elec); % i.e. ConfiDecid1_realtime_X3...
elseif strcmp(UIoutput.task, 'realtime') && ~isfield(UIoutput, 'session') % Real time training
    participant.session = 0;
    participant.elec = split(UIoutput.elec,',');
    participant.sess_name = sprintf('%s_%s_training_%s', participant.task, participant.protocol, strjoin(participant.elec,'-')); % i.e. ConfiDecid_realtime_training_X3...
end

%% Default Experiment flags

% Port used
if strcmp(UIoutput.port, 'serial') % Serial port
    flags.trigger_serial = 1;
    flags.trigger_LPT = 0;
elseif strcmp(UIoutput.port, 'parallel') % Parallel port
    flags.trigger_serial = 0;
    flags.trigger_LPT = 1;
else % No port (DEBUG)
    flags.trigger_serial = 0;
    flags.trigger_LPT = 0;
end

% Eye-tracker
flags.eyetracker = str2double(UIoutput.eye{1,:}); % 0 = no eye-tracker ; 1 = Tobii eye-tracker

% Photodiode
flags.photodiode = UIoutput.photodiode; % 0 = no photodiode ; 1 = photodiode

%% Check if everything is ok

if strcmp(participant.protocol, 'cognition')
    if (~exist(participant.dataFolder, 'dir') && participant.session ~= 0) || ... % Le dossier du participant n'existe pas et la session lanc�e n'est pas l'entrainement
            (exist(participant.dataFolder, 'dir') && participant.session ~= 0 && ~exist(fullfile(participant.dataFolder, sprintf('%s_ConfiDecid0_%s.mat', participant.identifier, participant.protocol)),'file')) % La session d'entra�nement n'existe pas
        
        answer = questdlg({'Warning: It is necessary to do the TRAINING before starting the task!','','What do you want to do?',''}, 'Training not found',...
            'Run training','Exit','Run training');
        % Handle response
        switch answer
            case 'Run training'
                participant.session = 0;
            case 'Exit'
                disp('Experiment cancelled !');
                return
        end
        
    elseif participant.session ~= 0 && exist(fullfile(participant.dataFolder, sprintf('%s_%s.mat', participant.identifier, participant.sess_name)),'file') % La session existe d�j�
        resp = 0;
        
        while resp == 0
            answer = questdlg(sprintf('Session %d already exists, what do you want to do?',participant.session),'', ...
                'Overwrite it',sprintf('Run session %d',participant.session+1),'Exit','Exit');
            % Handle response
            switch answer
                case 'Overwrite it'
                    resp = 1;
                case sprintf('Run session %d',participant.session+1)
                    participant.session = participant.session + 1;
                    participant.sess_name = sprintf('%s%d_%s', participant.task, participant.session, participant.protocol);
                case 'Exit'
                    disp('Experiment cancelled !');
                    return
            end
            
            if ~exist(fullfile(participant.dataFolder, sprintf('%s_%s.mat', participant.identifier, participant.sess_name)),'file')
                resp = 1;
            end
        end
        
    end
end

%% Run experiment and save data

try
    
    if strcmp(participant.protocol, 'realtime') % Start BCI2000 (= listen Micromed) before the Psychtoolbox (in taskParameters)
        
        load(fullfile(runner.pathway, 'Subfunctions', 'realtime', 'realtime_config.mat'), 'realt') % Load useful paths for real time
        
        cd(fullfile(realt.BCI2000_path, 'batch'))
        system(realt.batch_filename) % Load batch file to start BCI2000
        waitfor(msgbox({'Click to continue';'(When listening has been started)'}));
        cd(runner.pathway);
        
    end
    
    [param, eyetracker] = ConfiDecid_taskParameters(runner, participant, flags); % Get parameters
    
    if strcmp(participant.protocol,'cognition')
        
        if participant.session == 0 % Training
            ConfiDecid_training_cognition(participant, flags, param, eyetracker)
        else
            ConfiDecid_experiment_cognition(participant, flags, param, eyetracker)
        end
        
    elseif strcmp(participant.protocol, 'stim')
        
        if ~isfield(participant, 'elec') % Training
            ConfiDecid_training_stim(participant, flags, param, eyetracker)
        else
            ConfiDecid_experiment_stim(participant, flags, param, eyetracker)
        end
        
    elseif strcmp(participant.protocol, 'realtime')
        
        if participant.session == 0 % Training
            ConfiDecid_training_realtime(participant, flags, param, eyetracker, realt)
        else
            ConfiDecid_experiment_realtime(participant, flags, param, eyetracker, realt)
        end
        
    end
    
catch ME
    passation.tmpfile = fullfile(participant.dataFolder,...
        sprintf('%s_%s_%s_tmp.mat', participant.info.nameid, datestr(participant.date,'yyyymmdd_HHMM'), participant.sess_name));
    
    eyetracker = [];
    
    ConfiDecid_escape(passation, flags, eyetracker)
    disp(ME);
    rethrow(ME);
end
