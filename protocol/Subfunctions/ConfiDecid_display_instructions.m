% Display instruction slides

% INPUTS :
% param = structure with fields :
%   - param.screen.window
%   - param.deviceNumber
%   - param.key.left/right
%   - param.instr.slides
%   - param.instr.destRect

function instr = ConfiDecid_display_instructions(param)

firstFrame = true;
slideIdx = 0;

while slideIdx ~= length(param.instr.slides) % Jusqu'� la fin des diapos d'instruction
    
    % Draw things
    Screen('DrawTexture', param.screen.window, param.instr.slides(slideIdx+1), [], param.instr.destRect); % Instructions
    DrawFormattedText(param.screen.window, ' ', 'center', 'center'); % Pour charger le text renderer avant de commencer
    
    % Check the HID device (keyboard, gamepad...) to see if a button has been pressed
    [instr.keyIsDown, instr.secsPress, instr.keyCode] = KbCheck(param.deviceNumber);
    
    % Change slide when a button is pressed
    if instr.keyCode(param.key.left) && slideIdx ~= 0
        [instr.secsRelease] = KbReleaseWait(param.deviceNumber);
        slideIdx = slideIdx - 1;
    elseif instr.keyCode(param.key.right) || instr.keyCode(param.key.valid)
        [instr.secsRelease] = KbReleaseWait(param.deviceNumber);
        slideIdx = slideIdx + 1;
    end
    
    % Flip to the screen
    if firstFrame == true % Get a time stamp at the beginning of the experiment
        instr.onset = Screen('Flip', param.screen.window);
        firstFrame = false;
    else
        Screen('Flip', param.screen.window);
    end
    
end

end