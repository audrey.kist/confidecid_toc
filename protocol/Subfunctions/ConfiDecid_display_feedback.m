% Feedback au d�fi

function feedback = ConfiDecid_display_feedback(param, challenge)

% Pr�sentation du feedback
Screen('TextSize', param.screen.window , param.default_textSize);
DrawFormattedText(param.screen.window, challenge.feedback{1}, 'center', 'center', challenge.feedback{2}); % Real feedback
feedback.onset = Screen('Flip', param.screen.window, [], 1);
param.fun.trigger(param.feed.trig.disp, param.hport); % Trigger � l'onset du feedback
feedback.offset = Screen('Flip', param.screen.window, feedback.onset + param.fun.secTiming(param.challenge.feedDur, param.screen.monitorFlipInterval)); % Dur�e = 0.5 s

end