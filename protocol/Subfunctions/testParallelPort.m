
if ispc % Windows
    CloseParPort;
    OpenParPort;
    hport = [];
    trigger = @SendTrigger; % Usage : trigger(trig, hport)
end

trig.start = 1;
trigger(trig.start,hport);

return


function trig = SendTrigger(trig,hport)
if isempty(hport) % Parallel port
    if ispc % Windows
        WriteParPort(trig);
        WaitSecs(0.010);
        WriteParPort(0);
    end
end
end