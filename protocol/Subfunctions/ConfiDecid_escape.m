%% Escape function
% Close everything open and save eyetracker data (just in case)

function ConfiDecid_escape(passation, flags, eyetracker)

try
    Priority(0);
    sca;
    ShowCursor;
    if flags.eyetracker == 1 % Tobii
        eyetracker.tobii.stop_gaze_data(); % Stop recording
        eyetracker.data.gaze = convertTobiiData(eyetracker.data); % Convert eye-tracker data to save them faster
        eyetracker = eyetracker.data;
        save([passation.tmpfile(1:end-4), '_eye_tmp.mat'], 'eyetracker'); % Backup of eye-tracker data
    end
    ListenChar(0);
catch ER
    disp(ER);
end

end