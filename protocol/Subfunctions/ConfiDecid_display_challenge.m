% Affichage du jeu de pr�cision

function challenge = ConfiDecid_display_challenge(trial_id, param, choice, passation, flags, eyetracker)

% D�terminer le r�sultat du jeu dans le cas de la loterie
if param.choice.agentivity(trial_id) == 0 % D�fi = loterie
    
    challenge.secsPress = NaN;
    challenge.resp_key = NaN;
    
    proba = param.choice.proba(choice.stakes.diff-9)/100; % Probability of winning
    lotteryOut = randsample([0,1],1,true,[1-proba,proba]); % Select the lottery result according to the probability of winning
    
    if lotteryOut == 1 % Win
        stopPos = randsample(choice.diff_rect(1):choice.diff_rect(3),1);
    else % Lose
        stopPos = randsample([param.challenge.dotStartPos + (param.challenge.pixelToCross/2):choice.diff_rect(1),...
            choice.diff_rect(3):choice.diff_rect(3) + (param.challenge.pixelToCross/2)],1);
    end
    
end

% D�terminer les coordonn�es du tunnel cachant la balle
challenge.tunnel_rect = [[param.challenge.dotStartPos+(param.challenge.pixelToCross/2); choice.diff_rect(2)-1; choice.diff_rect(1); choice.diff_rect(4)+1],...
    [choice.diff_rect(3); choice.diff_rect(2)-1; param.screen.Xpixels; choice.diff_rect(4)+1]]; % Rows = left, top, right, bottom border ; Columns = number of rectangles
challenge.entrance_size = 15;
challenge.tunnel_entrance = [param.challenge.dotStartPos+(param.challenge.pixelToCross/2) - 2*challenge.entrance_size, choice.diff_rect(2)-1, param.challenge.dotStartPos+(param.challenge.pixelToCross/2), choice.diff_rect(4)+1];
challenge.tunnel_entrance_rect = [param.challenge.dotStartPos+(param.challenge.pixelToCross/2) - challenge.entrance_size, choice.diff_rect(2)-1, param.challenge.dotStartPos+(param.challenge.pixelToCross/2) + challenge.entrance_size, choice.diff_rect(4)+1];

time = 0;
challenge.start = 0;
estimMade = false;

while estimMade == false
    
    % Position of the dot on this frame
    dotXpos = param.challenge.dotStartPos + param.challenge.velocity * time;
    
    % Draw things
    Screen('FillRect', param.screen.window, param.color.grey, challenge.tunnel_entrance_rect); % Rectangle indicating the entrance of the tunnel
    Screen('FillOval', param.screen.window, param.color.darkgrey, challenge.tunnel_entrance); % Oval indicating the entrance of the tunnel
    Screen('DrawDots', param.screen.window, [dotXpos param.screen.yCenter], param.challenge.dotSize, param.color.white, [], 2); % Balle
    Screen('FillRect', param.screen.window, param.color.grey, challenge.tunnel_rect); % Fill the tunnel
    Screen('FillRect', param.screen.window, choice.diff_color, choice.diff_rect); % Fill the difficulty box
    Screen('DrawLines', param.screen.window, choice.diff_rect_out, 2, param.color.white); % Outlines of the difficulty box
    
    if param.choice.agentivity(trial_id) == 1 % D�fi = jeu de pr�cision
        
        % Check the HID device (keyboard, gamepad...) to see if a button has been pressed
        [~, challenge.secsPress, challenge.keyCode] = KbCheck(param.deviceNumber);
        
        if challenge.keyCode(param.key.valid)
            % param.fun.trigger(param.challenge.trig.resp,param.hport); % Trigger � la r�ponse au d�fi
            estimMade = true;
            [challenge.secsRelease] = KbReleaseWait(param.deviceNumber);
            challenge.resp_key = param.fun.whichKey(challenge.keyCode); % Num�ro du bouton pr�ss�
        elseif challenge.keyCode(param.key.escape)
            challenge.escape = 1;
            ConfiDecid_escape(passation, flags, eyetracker);
            return
        end
        
    else % Lottery game
        
        if dotXpos >= stopPos
            estimMade = true;
            challenge.secsRelease = vbl;
        end
        
    end
    
    % Flip to the screen
    vbl = Screen('Flip', param.screen.window);
    
    if time == 0 % First screen flip
        challenge.start = vbl; % Get a time stamp at the beginning of the estimation
    end
    
    % Increment the time
    time = time + param.screen.monitorFlipInterval;
    
end

% D�terminer la r�ussite � l'estimation
challenge.estimDur = challenge.secsPress - challenge.start; % dur�e avant l'appuie
challenge.perf = param.challenge.timeToCross - challenge.estimDur; % performance (par rapport au temps pour arriver au centre de l'�cran)

if (~isnan(challenge.perf) && abs(challenge.perf) <= choice.tolerance) || (exist('lotteryOut','var') && lotteryOut == 1) % R�ussi
    challenge.gain_trial = choice.stakes.gain;
    challenge.feedback = {double(['+' num2str(choice.stakes.gain / 10) ' �']); param.color.green};
    challenge.feedData = 1;
else % Rat�
    challenge.gain_trial = - choice.stakes.loss;
    challenge.feedback = {double(['-' num2str(choice.stakes.loss / 10) ' �']); param.color.red};
    challenge.feedData = 0;
end

end