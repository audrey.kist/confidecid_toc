%% CONFIDENCE DECID EXPERIMENT FOR STIMULATION PROTOCOLE

% Param�tres de stimulation :
%   - Fr�quence : 50 Hz
%   - Intensit� : Intensit� au-dessous de laquelle il commence a y avoir des "after-discharges" et/ou des effets cliniques (max 3 mA)
%   - Dur�e : 5 sec
%   - Intervalle inter-stimulation = min 1 min

function ConfiDecid_experiment_stim(participant, flags, param, eyetracker)

%% Data collection

passation = [];

% D�finir le nom du fichier temporaire o� sauvegarder les infos
passation.tmpfile = fullfile(participant.dataFolder,...
    sprintf('%s_%s_%s_tmp', participant.info.nameid, datestr(participant.date,'yyyymmdd_HHMM'), participant.sess_name));

% D�finir le nom du fichier o� sauvegarder les infos finales
passation.filename = fullfile(participant.dataFolder,...
    sprintf('%s_%s', participant.identifier, participant.sess_name));

% Enregistrer les donn�es de la passation
passation.taskParameters = fileread(which('ConfiDecid_taskParameters')); % Garder une trace des param�tres utilis�s pour la session
passation.running = fileread(sprintf('%s.m',mfilename('fullpath'))); % Garder une trace du script qui a �t� utilis� pour la session
passation.screen = param.screen; % R�solution de l'�cran
passation.participant = participant;
passation.flags = flags;
passation.logfile.moy_estim = param.choice.moy_estim;
passation.logfile.std_estim = param.choice.std_estim;

% Initialisation de la matrice data
data = NaN(param.nb_trial,13);

% Trigger au d�but de la session
start = GetSecs;
param.fun.trigger(param.start.trig, param.hport);

passation.logfile.start = start; % Time at the beginning of the session

%% Eye-tracker start recording

if passation.flags.eyetracker == 1 % Tobii
    
    tobiiTrackStatus(param.screen.window, eyetracker.tobii, param.screen, param.color); % Get user eyes position
    tobiiCalibration(param.screen.window, eyetracker.tobii, param.screen, param.color); % Calibration
    eyetracker.tobii.get_gaze_data(); % Start collecting data
    eyetracker.data.event = []; % Matrix to store events
    eyetracker.data.gaze = []; % Matrix to store gaze data
    
end

%% Instructions

instr = ConfiDecid_display_instructions(param);

% Save data
passation.instructions.onset = instr.onset; % Onset des instructions
passation.instructions.offset = instr.secsRelease; % Offset des instructions
passation.instructions.resp_key = param.fun.whichKey(instr.keyCode); % Num�ro du dernier bouton pr�ss�
passation.instructions.resp_press = instr.secsPress; % Button press time

%% Initialisation variables

diff = 30; % Difficult� de d�part

%% DEBUT DE LA BOUCLE A TRAVERS LES ESSAIS

for trial_id = 1:param.nb_trial
    
    %% D�clenchement manuel du choix
    % Attendre le clique de la souris pour d�clencher le choix
    
    stim.trig = 2+(param.choice.stim(trial_id)*198); % Trigger pour savoir si l'essai est stim (200) ou non stim (2)
    
    Screen('FillRect', param.screen.window, param.color.black); % Colorer l'�cran en noir
    param.fun.trigger(stim.trig, param.hport); % Envoi du trigger
    Screen('Flip', param.screen.window);
    GetClicks(); % Attendre le clique de la souris
    
    % Save data
    passation.logfile.stim(trial_id,:) = stim.trig; % Stim = 200 / Non stim = 2
    
    %% Choice task
    param.choice.stakes(trial_id,1) = diff; % Trial difficulty
    
    [choice, eyetracker] = ConfiDecid_display_choice(trial_id, param, passation, flags, eyetracker);
    
    if isfield(choice, 'escape') && choice.escape == 1
        return
    end
    
    % Save data
    passation.logfile.choice.agentivity(trial_id,:) = param.choice.agentivity(trial_id); % Agentivit� (1 = agent ; 0 = non agent)
    passation.logfile.choice.gain(trial_id,:) = choice.prospect.gain; % Gain prospect
    passation.logfile.choice.loss(trial_id,:) = choice.prospect.loss; % Loss prospect
    passation.logfile.choice.difficulty(trial_id,:) = choice.prospect.diff; % Difficult� de l'essai
    passation.logfile.choice.config(trial_id,:) = choice.trig.config; % Configuration spatiale du choix
    passation.logfile.choice.onset(trial_id,:) = choice.onset; % Onset du choix
    passation.logfile.choice.offset(trial_id,:) = choice.secsRelease; % Offset du choix
    passation.logfile.choice.resp_key(trial_id,:) = param.fun.whichKey(choice.keyCode); % Num�ro du bouton pr�ss�
    passation.logfile.choice.resp_press(trial_id,:) = choice.secsPress; % Button press time
    passation.logfile.choice.choiceMade{trial_id,:} = choice.made; % OUI ou NON
    passation.logfile.choice.noChoice_onset(trial_id,:) = choice.onset_no; % Onset de l'�cran lors du choix NON
    passation.logfile.choice.noChoice_offset(trial_id,:) = choice.offset_no; % Offset de l'�cran choix NON
    choice.rt = choice.secsPress - choice.onset; % Temps de r�action pour choisir
    
    %% Confidence rating
    
    confidence = ConfiDecid_display_rating(param, 'confidence', passation, flags, eyetracker); % Display confidence rating
    
    if isfield(confidence, 'escape') && confidence.escape == 1
        return
    end
    
    % Save data
    passation.logfile.confidence.onset(trial_id,:) = confidence.onset; % Onset du confidence rating
    passation.logfile.confidence.offset(trial_id,:) = confidence.secsRelease; % Offset du confidence rating
    passation.logfile.confidence.resp_press(trial_id,:) = confidence.secsPress; % Button press time
    passation.logfile.confidence.cursorPosInit(trial_id,:) = ((confidence.cursor_Xpos(1) - param.rating.linePos(1)) / param.rating.pixelsPerPress) + 1; % Position initiale du curseur
    passation.logfile.confidence.value(trial_id,:) = confidence.rating; % Confidence rating de 1 � 100
    confidence.rt = passation.logfile.confidence.resp_press(trial_id)-passation.logfile.confidence.onset(trial_id);
    
    %% D�fi (precision task or lottery game)
    % Precision task = Il faut appuyer lorsqu'on pense que le point se situe dans le rectangle de difficult�
    
    challenge = ConfiDecid_display_challenge(trial_id, param, choice, passation, flags, eyetracker);
    
    if isfield(challenge, 'escape') && challenge.escape == 1
        return
    end
    
    % Save data
    passation.logfile.challenge.onset(trial_id,:) = challenge.start; % D�but du d�fi
    passation.logfile.challenge.offset(trial_id,:) = challenge.secsRelease; % Fin du d�fi
    passation.logfile.challenge.resp_key(trial_id,:) = challenge.resp_key; % Num�ro du bouton pr�ss�
    passation.logfile.challenge.resp_press(trial_id,:) = challenge.secsPress; % Button press time
    passation.logfile.challenge.timePerf(trial_id,:) = challenge.perf; % Performance par rapport au temps pour arriver au centre de l'�cran
    passation.logfile.challenge.success(trial_id,:) = challenge.feedData; % 1 = r�ussi, 0 = rat�
    
    % Gains totaux
    param.challenge.gain_tot = param.challenge.gain_tot + challenge.gain_trial;
    
    % Modifier la taille de la cible en fonction de la r�ussite
    if challenge.feedData == 1 && diff < 49 % R�ussi
        diff = diff + 1;
    elseif challenge.feedData == 0 && diff > 10 % Perdu
        diff = diff - 1;
    end
    
    %% Feedback
    
    feedback = ConfiDecid_display_feedback(param, challenge);
    
    if isfield(feedback, 'escape') && feedback.escape == 1
        return
    end
    
    % Save data
    passation.logfile.challenge.feed_onset(trial_id,:) = feedback.onset;
    passation.logfile.challenge.feed_offset(trial_id,:) = feedback.offset;
    
    %% Inter-trial intervalle
    
    % Intervalle entre la fin de l'estimation et le d�but de l'essai suivant
    vbl = Screen('Flip', param.screen.window);
    Screen('Flip', param.screen.window, vbl + param.fun.secTiming(param.end.interTrial, param.screen.monitorFlipInterval)); % Dur�e = 500 ms
    
    %% Temporary backup
    save(passation.tmpfile, 'passation');
    
    % Save data matrix
    % Colonne 1 = vide car indice du participant lors des analyses
    data(trial_id,2) = stim.trig; % Stim = 200 / Non stim = 2
    data(trial_id,3) = choice.trig.config; % Configuration du choix
    data(trial_id,4) = choice.prospect.gain; % Quantit� de gains
    data(trial_id,5) = choice.prospect.loss; % Quantit� de pertes
    data(trial_id,6) = choice.prospect.diff; % Difficult� du d�fi
    
    if strcmp(choice.made, param.choice.decline_caption) == 1 % r�ponse = NON
        data(trial_id,7) = 0;
    else % R�ponse = OUI
        data(trial_id,7) = 1;
    end
    
    data(trial_id,8) = choice.rt; % Temps de r�action pour faire le choix
    data(trial_id,9) = confidence.rating; % Note de confiance
    data(trial_id,10) = confidence.rt; % Temps de r�action pour noter la confiance
    data(trial_id,11) = param.choice.agentivity(trial_id); % Agentivit� du d�fi (1 = agent ; 0 = non agent)
    data(trial_id,12) = challenge.feedData; % R�ussite au d�fi
    data(trial_id,13) = challenge.estimDur; % Temps de r�action au d�fi
    
    save([passation.tmpfile, '_data.mat'], 'data'); % Temporary backup of data
    
    %% Eye-tracker : create data matrix
    
    if passation.flags.eyetracker == 1 % Tobii
        eyetracker.data.gaze = [eyetracker.data.gaze , eyetracker.tobii.get_gaze_data()];
    end
    
end % End of the loop across trials

%% Fin de la session

% Affichage du gain total
DrawFormattedText(param.screen.window, 'Vos GAINS totaux sont de :', 'center', 'center', param.color.white, [], [], [], [], [], [0 param.screen.Ypixels*0.45 param.screen.Xpixels param.screen.Ypixels*0.45]);
DrawFormattedText(param.screen.window, double([' ' num2str(param.challenge.gain_tot / 10) ' �']), 'center', 'center', param.color.white, [], [], [], [], [], [0 param.screen.Ypixels*0.55 param.screen.Xpixels param.screen.Ypixels*0.55]);

vbl = Screen('Flip', param.screen.window, [], 1);
Screen('Flip', param.screen.window, vbl + param.fun.secTiming(param.end.dur, param.screen.monitorFlipInterval));

%% Save data
passation.logfile.gain_tot = param.challenge.gain_tot / 10; % Gain total en �

save(passation.filename, 'passation'); % Enregistrer le fichier LOGFILE final
delete([passation.tmpfile, '.mat']); % Supprimer le fichier temporaire

save([passation.filename, '_data.mat'], 'data'); % Enregistrer le fichier DATA final
delete([passation.tmpfile, '_data.mat']); % Supprimer le fichier temporaire

%% End eye-tracker and save data
EyeTrackerFilename = [passation.filename, '_eye'];

if passation.flags.eyetracker == 1 % Tobii
    
    eyetracker.tobii.stop_gaze_data(); % Stop recording
    tic
    eyetracker.data.gaze = convertTobiiData(eyetracker.data); % Convert eye-tracker data to save them faster
    eyetracker = eyetracker.data;
    save(EyeTrackerFilename, 'eyetracker');
    toc
    
end

%% END
sca;

end