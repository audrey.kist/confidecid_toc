% Affichage du choix
% + affichage des sommes en jeu lorsque la r�ponse est NON

function [choice, eyetracker] = ConfiDecid_display_choice(trial_id, param, passation, flags, eyetracker)

% Param�tres

% D�finir l'offre de difficult�, gain et perte � afficher
choice.stakes.diff = param.choice.stakes(trial_id,1); % Trial difficulty
choice.stakes.gain = param.choice.stakes(trial_id,2); % Gain offer
choice.stakes.loss = param.choice.stakes(trial_id,3); % Loss offer

% Save data
choice.prospect.diff = choice.stakes.diff;
choice.prospect.gain = choice.stakes.gain;
choice.prospect.loss = choice.stakes.loss;

% D�finir la position des pi�ces
if isfield(param.choice,'defined_top_locs') && isfield(param.choice,'defined_bottom_locs')
    top_locs = param.choice.defined_top_locs(:,:,trial_id);
    bottom_locs = param.choice.defined_bottom_locs(:,:,trial_id);
else % Si la variable n'existe pas, on rend al�atoire la position des pi�ces � chaque essai
    top_locs = Shuffle(param.choice.top_locs,2);
    bottom_locs = Shuffle(param.choice.bottom_locs,2);
end

% Pr�-remplir les matrices contenant la position des gains/pertes
destRectGain = zeros(choice.stakes.gain,4);
destRectLoss = zeros(choice.stakes.loss,4);

% Boucles permettant de dessiner les pieces de gain et perte � chaque essai
% en affichant al�atoirement le gain et la perte en haut ou en bas

if param.choice.intervert_gain_loss(trial_id) == 0 % gains en haut et pertes en bas
    
    for gain_nb = 1:choice.stakes.gain % d�finir les coordonn�es des gains
        destRectGain(gain_nb,:) = CenterRectOnPointd([0 0 param.choice.coin_prop param.choice.coin_prop], top_locs(gain_nb,1)*param.screen.Xpixels, top_locs(gain_nb,2)*param.screen.Ypixels); % Positions of the gain coins
    end
    
    for loss_nb = 1:choice.stakes.loss % d�finir les coordonn�es des pertes
        destRectLoss(loss_nb,:) = CenterRectOnPointd([0 0 param.choice.coin_prop param.choice.coin_prop], bottom_locs(loss_nb,1)*param.screen.Xpixels, bottom_locs(loss_nb,2)*param.screen.Ypixels); % Positions of the loss coins
    end
    
elseif param.choice.intervert_gain_loss(trial_id) == 1 % gains en bas et pertes en haut
    
    for gain_nb = 1:choice.stakes.gain % d�finir les coordonn�es des gains
        destRectGain(gain_nb,:) = CenterRectOnPointd([0 0 param.choice.coin_prop param.choice.coin_prop], bottom_locs(gain_nb,1)*param.screen.Xpixels, bottom_locs(gain_nb,2)*param.screen.Ypixels); % Positions of the gain coins
    end
    
    for loss_nb = 1:choice.stakes.loss % d�finir les coordonn�es des pertes
        destRectLoss(loss_nb,:) = CenterRectOnPointd([0 0 param.choice.coin_prop param.choice.coin_prop], top_locs(loss_nb,1)*param.screen.Xpixels, top_locs(loss_nb,2)*param.screen.Ypixels); % Positions of the loss coins
    end
    
end

% D�terminer les coordonn�es du rectangle repr�sentant la diff
choice.tolerance = param.choice.moy_estim + param.choice.z_score(choice.stakes.diff-9) * param.choice.std_estim; % 1 seconde +/- la tol�rance pour r�ussir

choicePos = round(choice.tolerance * param.challenge.velocity);
choice.diff_rect = [param.screen.xCenter - choicePos, param.screen.yCenter - 20, param.screen.xCenter + choicePos, param.screen.yCenter + 20]; % Difficulty box
choice.diff_rect_out = [param.screen.xCenter - choicePos, param.screen.xCenter + choicePos, param.screen.xCenter - choicePos, param.screen.xCenter + choicePos, param.screen.xCenter - choicePos, param.screen.xCenter - choicePos, param.screen.xCenter + choicePos, param.screen.xCenter + choicePos ;...
    param.screen.yCenter + 20, param.screen.yCenter + 20, param.screen.yCenter - 20, param.screen.yCenter - 20, param.screen.yCenter + 20, param.screen.yCenter - 20, param.screen.yCenter + 20, param.screen.yCenter - 20]; % Outline of the difficulty box

% D�finir la couleur du rectangle de difficult� en fonction de l'agentivit�
if param.choice.agentivity(trial_id) == 1 % D�fi = jeu de pr�cision
    choice.diff_color = param.color.blue; % Rectangle repr�sentant la difficult� = bleu
else % D�fi = jeu de loterie
    choice.diff_color = param.color.purple; % Rectangle repr�sentant la difficult� = violet
end

% Afficher al�atoirement la r�ponse OUI ou NON � gauche ou � droite
if param.choice.intervert_y_n(trial_id) == 0 % OUI � gauche, NON � droite
    left_choice = param.choice.accept_caption;
    right_choice = param.choice.decline_caption;
elseif param.choice.intervert_y_n(trial_id) == 1 % OUI � droite, NON � gauche
    right_choice = param.choice.accept_caption;
    left_choice = param.choice.decline_caption;
end

% D�finir le code permettant de retrouver la position des gains/pertes (haut/bas) et du OUI/NON (gauche/droite)
if param.choice.intervert_gain_loss(trial_id) == 0 % gains en haut et pertes en bas
    if param.choice.intervert_y_n(trial_id) == 0 % OUI � gauche, NON � droite
        choice.trig.config = param.choice.trig.config - (-1 + param.choice.agentivity(trial_id))*10;
    elseif param.choice.intervert_y_n(trial_id) == 1 % OUI � droite, NON � gauche
        choice.trig.config = param.choice.trig.config + 1 - (-1 + param.choice.agentivity(trial_id))*10;
    end
elseif param.choice.intervert_gain_loss(trial_id) == 1 % gains en bas et pertes en haut
    if param.choice.intervert_y_n(trial_id) == 0 % OUI � gauche, NON � droite
        choice.trig.config = param.choice.trig.config + 2 - (-1 + param.choice.agentivity(trial_id))*10;
    elseif param.choice.intervert_y_n(trial_id) == 1 % OUI � droite, NON � gauche
        choice.trig.config = param.choice.trig.config + 3 - (-1 + param.choice.agentivity(trial_id))*10;
    end
end

% Code de l'offset du choix
choice.trig.confirm = param.choice.trig.confirm - (-1 + param.choice.agentivity(trial_id))*10;

%% Affichage du choix

firstFrame = true;
choice.made = false;

while choice.made == false
    
    % Draw things
    Screen('TextSize', param.screen.window , param.default_textSize);
    Screen('FillRect', param.screen.window, choice.diff_color, choice.diff_rect); % Fill the difficulty box
    Screen('DrawLines', param.screen.window, choice.diff_rect_out, 2); % Outlines of the difficulty box
    Screen('DrawTextures', param.screen.window, param.choice.gain_img, [], destRectGain'); % Gains coins
    Screen('DrawTextures', param.screen.window, param.choice.loss_img, [], destRectLoss'); % Loss coins
    DrawFormattedText(param.screen.window, left_choice, 'center', 'center', param.color.white, [], [], [], [], [], [param.screen.Xpixels*0.15 param.screen.yCenter param.screen.Xpixels*0.35 param.screen.yCenter]); % Left text
    DrawFormattedText(param.screen.window, right_choice, 'center', 'center', param.color.white, [], [], [], [], [], [param.screen.Xpixels*0.65 param.screen.yCenter param.screen.Xpixels*0.85 param.screen.yCenter]); % Right text
    
    if flags.photodiode == 1 % If photodiode
        Screen('FillOval', param.screen.window, [], param.photodpos);
    end
    
    % Check the HID device (keyboard, gamepad...) to see if a button has been pressed
    [~, choice.secsPress, choice.keyCode] = KbCheck(param.deviceNumber);
    
    if choice.keyCode(param.key.left)
        param.fun.trigger(choice.trig.confirm, param.hport); % Trigger � la r�ponse au choix
        choice.made = left_choice;
    elseif choice.keyCode(param.key.right)
        param.fun.trigger(choice.trig.confirm, param.hport); % Trigger � la r�ponse au choix
        choice.made = right_choice;
    elseif choice.keyCode(param.key.escape)
        choice.escape = 1;
        ConfiDecid_escape(passation, flags, eyetracker);
        return
    end
    
    % Flip to the screen
    if firstFrame == true
        choice.onset = Screen('Flip', param.screen.window); % Get a time stamp at the beginning of the trial
        param.fun.trigger(choice.trig.config, param.hport); % Trigger � l'onset du choix
        
        % Eye-tracker : Ev�nement � l'onset du choix
        if passation.flags.eyetracker == 1 % Tobii
            eyetracker.data.event(trial_id,1:2) = [choice.trig.config, eyetracker.op.get_system_time_stamp];
        end
        
        firstFrame = false;
    else
        Screen('Flip', param.screen.window);
    end
    
    if choice.made ~= false
        
        % Eye-tracker : Ev�nement � la r�ponse au choix
        if passation.flags.eyetracker == 1 % Tobii
            eyetracker.data.event(trial_id,3:4) = [choice.trig.confirm, eyetracker.op.get_system_time_stamp];
        end
        
        [choice.secsRelease] = KbReleaseWait(param.deviceNumber);
    end
    
end

%% D�terminer la suite de l'essai en fonction de la r�ponse
% Si OUI : offre accept�e = le sujet joue
% Si NON : offre rejet�e = le sujet joue �galement mais avec un gain et une perte minimum (10 centimes)

if strcmp(choice.made, param.choice.decline_caption) == 1 % r�ponse = NON
    
    choice.stakes.gain = 1;
    choice.stakes.loss = 1;
    destRectGain = CenterRectOnPointd([0 0 param.choice.coin_prop param.choice.coin_prop], param.screen.xCenter, param.screen.Ypixels * 0.3);
    destRectLoss = CenterRectOnPointd([0 0 param.choice.coin_prop param.choice.coin_prop], param.screen.xCenter, param.screen.Ypixels * 0.7);
    
    % Draw things
    Screen('FillRect', param.screen.window, choice.diff_color, choice.diff_rect); % Fill the difficulty box
    Screen('DrawLines', param.screen.window, choice.diff_rect_out, 2); % Outlines of the difficulty box
    Screen('DrawTextures', param.screen.window, param.choice.gain_img, [], destRectGain'); % Gains coins
    Screen('DrawTextures', param.screen.window, param.choice.loss_img, [], destRectLoss'); % Loss coins
    
    % Flip to the screen
    choice.onset_no = Screen('Flip', param.screen.window, [], 1);
    choice.offset_no = Screen('Flip', param.screen.window, choice.onset_no + param.fun.secTiming(param.choice.no_choice_dur, param.screen.monitorFlipInterval)); % Dur�e = 500 ms
    
else % r�ponse = OUI
    WaitSecs(param.choice.no_choice_dur);
    choice.onset_no = NaN;
    choice.offset_no = NaN;
end

end