%% TRAINING CONFIDANCE DECID
% Entra�nement � la t�che de choix ConfiDECID + permet de d�terminer la moyenne et l'�cart type de l'estimation propre � chaque sujet

function ConfiDecid_training_cognition(participant, flags, param, eyetracker)

%% Data collection

passation = [];

% Cr�er un dossier par participant (s'il n'existe pas d�j�)
if ~exist(participant.dataFolder, 'dir')
    mkdir(participant.dataFolder);
end

% D�finir le nom du fichier temporaire o� sauvegarder les infos
passation.tmpfile = fullfile(participant.dataFolder,...
    sprintf('%s_%s_%s_tmp', participant.info.nameid, datestr(participant.date,'yyyymmdd_HHMM'), participant.sess_name));

% D�finir le nom du fichier o� sauvegarder les infos finales
passation.filename = fullfile(participant.dataFolder,...
    sprintf('%s_%s', participant.identifier, participant.sess_name));

% Enregistrer les donn�es de la passation
passation.taskParameters = fileread(which('ConfiDecid_taskParameters')); % Garder une trace des param�tres utilis�s pour la session
passation.running = fileread(sprintf('%s.m',mfilename('fullpath'))); % Garder une trace du script qui a �t� utilis� pour la session
passation.screen = param.screen; % R�solution de l'�cran
passation.participant = participant;
passation.flags = flags;

% Trigger au d�but de l'entra�nement
start = GetSecs;
param.fun.trigger(param.training.start.trig, param.hport);

passation.logfile.start = start; % Time at the beginning of the session

%% Eye-tracker start recording

if passation.flags.eyetracker == 1 % Tobii
    
    tobiiTrackStatus(param.screen.window, eyetracker.tobii, param.screen, param.color); % Get user eyes position
    tobiiCalibration(param.screen.window, eyetracker.tobii, param.screen, param.color); % Calibration
    eyetracker.tobii.get_gaze_data(); % Start collecting data
    eyetracker.data.event = []; % Matrix to store events
    eyetracker.data.gaze = []; % Matrix to store gaze data
    
end

%% PARTIE 1 :
% D�fi d'estimation sans gains ni pertes + feedback sur la r�ussite au d�fi

% Instructions partie 1
param.instr.slides = param.training.part1.instr.slides;

instr = ConfiDecid_display_instructions(param);

% Save data
passation.logfile.part1.instructions.onset = instr.onset; % Onset des instructions
passation.logfile.part1.instructions.offset = instr.secsRelease; % Offset des instructions
passation.logfile.part1.instructions.resp_key = param.fun.whichKey(instr.keyCode); % Num�ro du dernier bouton pr�ss�
passation.logfile.part1.instructions.resp_press = instr.secsPress; % Button press time

%-------------------------------------------------------------------------%

% D�fi partie 1
% On d�termine la taille de cible pour laquelle le participant r�ussi dans 50% des cas
% On diminue la taille de la cible lorsqu'il r�ussit, on augmente la taille de la cible lorsqu'il perd

difficulty.stakes.gain = NaN;
difficulty.stakes.loss = NaN;

% Boucle � travers les essais
% On pr�sente le jeu jusqu'� stabilisation de la performance
% S'il n'y a pas de stabilisation, on fixe un nombre d'essai maximum

trial_id = 1;
estim_level(trial_id) = param.training.part1.moy_estim;
regularity(trial_id) = 0;

while trial_id <= param.training.part1.nb_trial_min || range(regularity(end-param.training.part1.plateau+1:end)) >= 2
    
    if trial_id == param.training.part1.nb_trial_max
        break
    end
    
    param.choice.agentivity(trial_id) = 1;
    
    % D�terminer les coordonn�es du rectangle repr�sentant la diff
    difficulty.tolerance = estim_level(trial_id); % 50% de chance de r�ussite
    
    choicePos = round(difficulty.tolerance * param.challenge.velocity);
    difficulty.diff_rect = [param.screen.xCenter - choicePos, param.screen.yCenter - 20, param.screen.xCenter + choicePos, param.screen.yCenter + 20]; % Difficulty box
    difficulty.diff_rect_out = [param.screen.xCenter - choicePos, param.screen.xCenter + choicePos, param.screen.xCenter - choicePos, param.screen.xCenter + choicePos, param.screen.xCenter - choicePos, param.screen.xCenter - choicePos, param.screen.xCenter + choicePos, param.screen.xCenter + choicePos ;...
        param.screen.yCenter + 20, param.screen.yCenter + 20, param.screen.yCenter - 20, param.screen.yCenter - 20, param.screen.yCenter + 20, param.screen.yCenter - 20, param.screen.yCenter + 20, param.screen.yCenter - 20]; % Outline of the difficulty box
    
    difficulty.diff_color = param.color.blue; % Rectangle repr�sentant la difficult� = bleu
    
    %-D�fi----------------------------------------------------------------%
    
    challenge = ConfiDecid_display_challenge(trial_id, param, difficulty, passation, flags, eyetracker);
    
    if isfield(challenge, 'escape') && challenge.escape == 1
        return
    end
    
    %-Feedback------------------------------------------------------------%
    
    if challenge.feedData == 1 % R�ussi
        feedText = '';
    else % Perdu
        if challenge.perf > 0 % Trop rapide
            feedText = 'trop rapide';
        else % Trop lent
            feedText = 'trop lent';
        end
    end
    
    Screen('TextSize', param.screen.window , param.default_textSize);
    DrawFormattedText(param.screen.window, feedText, 'center', param.screen.Ypixels * 0.35, param.color.white); % Info ('trop rapide' ou 'trop lent')
    Screen('FillRect', param.screen.window, param.color.grey, challenge.tunnel_entrance_rect); % Rectangle indicating the entrance of the tunnel
    Screen('FillOval', param.screen.window, param.color.darkgrey, challenge.tunnel_entrance); % Oval indicating the entrance of the tunnel
    Screen('FillRect', param.screen.window, param.color.grey, challenge.tunnel_rect); % Fill the tunnel
    Screen('FillRect', param.screen.window, challenge.feedback{2}, difficulty.diff_rect); % Fill the difficulty box
    Screen('DrawLines', param.screen.window, difficulty.diff_rect_out, 2, param.color.white); % Outlines of the difficulty box
    
    vbl = Screen('Flip', param.screen.window, [], 1);
    Screen('Flip', param.screen.window, vbl + param.fun.secTiming(param.training.part1.feedDur, param.screen.monitorFlipInterval));
    
    % Save data
    passation.logfile.part1.challenge.agentivity(trial_id,:) = param.choice.agentivity(trial_id); % Agentivit� = 1 (agent) dans cette partie
    passation.logfile.part1.challenge.onset(trial_id,:) = challenge.start; % D�but du d�fi
    passation.logfile.part1.challenge.offset(trial_id,:) = challenge.secsRelease; % Fin du d�fi
    passation.logfile.part1.challenge.resp_key(trial_id,:) = challenge.resp_key; % Num�ro du bouton pr�ss�
    passation.logfile.part1.challenge.resp_press(trial_id,:) = challenge.secsPress; % Button press time
    passation.logfile.part1.challenge.timePerf(trial_id,:) = challenge.perf; % Performance par rapport au temps pour arriver au centre de l'�cran
    passation.logfile.part1.challenge.success(trial_id,:) = challenge.feedData; % 1 = r�ussi, 0 = rat�
    
    %---------------------------------------------------------------------%
    
    % Update variables
    if trial_id > 1
        regularity(trial_id) = regularity(trial_id-1) - (challenge.feedData*2-1); % +1 si gagn� ; -1 si perdu
    end
    
    trial_id = trial_id + 1;
    
    % Modifier la taille de la cible en fonction de la r�ussite (apr�s l'incr�mentation de l'index de l'essai)
    if challenge.feedData == 1 % R�ussi
        estim_level(trial_id) = estim_level(trial_id-1) - param.training.part1.meanStep;
    else % Perdu
        estim_level(trial_id) = estim_level(trial_id-1) + param.training.part1.meanStep;
    end
    
end % Fin du while

% Calcul de la moyenne et de l'�cart type
moy_estim = mean(estim_level(end-param.training.part1.plateau+1:end)); % Moyenne de l'estimation (50% de r�ussite)
std_estim = std(abs(rmoutliers(passation.logfile.part1.challenge.timePerf))); % Ecart type

%% PARTIE 2 :
% Choix agent/non agent/les deux + D�fi + Feedback sur la r�ussite au d�fi

param.choice.moy_estim = moy_estim; % Moyenne
param.choice.std_estim = std_estim; % Ecart type
timePerf = moy_estim;

data = [];

for i = 1:length(param.training.part2.agentivity)
    
    % Instructions partie 2
    param.instr.slides = param.training.part2.(param.training.part2.agentivity{i}).instr.slides;
    
    instr = ConfiDecid_display_instructions(param);
    
    % Save data
    passation.logfile.part2.(param.training.part2.agentivity{i}).instructions.onset = instr.onset; % Onset des instructions
    passation.logfile.part2.(param.training.part2.agentivity{i}).instructions.offset = instr.secsRelease; % Offset des instructions
    passation.logfile.part2.(param.training.part2.agentivity{i}).instructions.resp_key = param.fun.whichKey(instr.keyCode); % Num�ro du dernier bouton pr�ss�
    passation.logfile.part2.(param.training.part2.agentivity{i}).instructions.resp_press = instr.secsPress; % Button press time
    
    %---------------------------------------------------------------------%
    
    % Choix + D�fi
    data(end+1:end+param.training.part2.(param.training.part2.agentivity{i}).nb_trial,1:15) = NaN(param.training.part2.(param.training.part2.agentivity{i}).nb_trial,15); % Matrice data
    timePerf(end+1:end+param.training.part2.(param.training.part2.agentivity{i}).nb_trial,1) = NaN(param.training.part2.(param.training.part2.agentivity{i}).nb_trial,1);
    
    param.choice.stakes = param.training.part2.(param.training.part2.agentivity{i}).choice.stakes;
    param.choice.agentivity = param.training.part2.(param.training.part2.agentivity{i}).choice.agentivity;
    param.choice.intervert_gain_loss = param.training.part2.(param.training.part2.agentivity{i}).choice.intervert_gain_loss;
    param.choice.intervert_y_n = param.training.part2.(param.training.part2.agentivity{i}).choice.intervert_y_n;
    
    for trial_id = 1:param.training.part2.(param.training.part2.agentivity{i}).nb_trial % Boucle � travers les essais
        
        %-Choix-----------------------------------------------------------%
        [choice, eyetracker] = ConfiDecid_display_choice(trial_id, param, passation, flags, eyetracker);
        
        if isfield(choice, 'escape') && choice.escape == 1
            return
        end
        
        % Save data
        passation.logfile.part2.(param.training.part2.agentivity{i}).choice.agentivity(trial_id,:) = param.choice.agentivity(trial_id); % Agentivit� (1 = agent ; 0 = non agent)
        passation.logfile.part2.(param.training.part2.agentivity{i}).choice.gain(trial_id,:) = choice.prospect.gain; % Gain prospect
        passation.logfile.part2.(param.training.part2.agentivity{i}).choice.loss(trial_id,:) = choice.prospect.loss; % Loss prospect
        passation.logfile.part2.(param.training.part2.agentivity{i}).choice.difficulty(trial_id,:) = choice.prospect.diff; % Difficult� de l'essai
        passation.logfile.part2.(param.training.part2.agentivity{i}).choice.config(trial_id,:) = choice.trig.config; % Configuration spatiale du choix
        passation.logfile.part2.(param.training.part2.agentivity{i}).choice.onset(trial_id,:) = choice.onset; % Onset du choix
        passation.logfile.part2.(param.training.part2.agentivity{i}).choice.offset(trial_id,:) = choice.secsRelease; % Offset du choix
        passation.logfile.part2.(param.training.part2.agentivity{i}).choice.resp_key(trial_id,:) = param.fun.whichKey(choice.keyCode); % Num�ro du bouton pr�ss�
        passation.logfile.part2.(param.training.part2.agentivity{i}).choice.resp_press(trial_id,:) = choice.secsPress; % Button press time
        passation.logfile.part2.(param.training.part2.agentivity{i}).choice.choiceMade{trial_id,:} = choice.made; % OUI ou NON
        passation.logfile.part2.(param.training.part2.agentivity{i}).choice.noChoice_onset(trial_id,:) = choice.onset_no; % Onset de l'�cran lors du choix NON
        passation.logfile.part2.(param.training.part2.agentivity{i}).choice.noChoice_offset(trial_id,:) = choice.offset_no; % Offset de l'�cran choix NON
        choice.rt = choice.secsPress - choice.onset; % Temps de r�action pour choisir
        
        %-D�fi------------------------------------------------------------%
        challenge = ConfiDecid_display_challenge(trial_id, param, choice, passation, flags, eyetracker);
        
        if isfield(challenge, 'escape') && challenge.escape == 1
            return
        end
        
        % Save data
        passation.logfile.part2.(param.training.part2.agentivity{i}).challenge.onset(trial_id,:) = challenge.start; % D�but du d�fi
        passation.logfile.part2.(param.training.part2.agentivity{i}).challenge.offset(trial_id,:) = challenge.secsRelease; % Fin du d�fi
        passation.logfile.part2.(param.training.part2.agentivity{i}).challenge.resp_key(trial_id,:) = challenge.resp_key; % Num�ro du bouton pr�ss�
        passation.logfile.part2.(param.training.part2.agentivity{i}).challenge.resp_press(trial_id,:) = challenge.secsPress; % Button press time
        passation.logfile.part2.(param.training.part2.agentivity{i}).challenge.timePerf(trial_id,:) = challenge.perf; % Performance par rapport au temps pour arriver au centre de l'�cran
        passation.logfile.part2.(param.training.part2.agentivity{i}).challenge.success(trial_id,:) = challenge.feedData; % 1 = r�ussi, 0 = rat�
        
        % Gains totaux
        param.challenge.gain_tot = param.challenge.gain_tot + challenge.gain_trial;
        
        %-Feedback--------------------------------------------------------%
        feedback = ConfiDecid_display_feedback(param, challenge);
        
        if isfield(feedback, 'escape') && feedback.escape == 1
            return
        end
        
        % Save data
        passation.logfile.part2.(param.training.part2.agentivity{i}).challenge.feed_onset(trial_id,:) = feedback.onset;
        passation.logfile.part2.(param.training.part2.agentivity{i}).challenge.feed_offset(trial_id,:) = feedback.offset;
        
        %-Temporary backup------------------------------------------------%
        save(passation.tmpfile, 'passation');
        
        % Save data matrix
        % Colonne 1 = vide car indice du participant lors des analyses
        idx = find(isnan(data(:,2)),1);
        data(idx,2) = participant.session; % Num�ro de la session
        data(idx,3) = choice.trig.config; % Configuration du choix
        data(idx,4) = choice.prospect.gain; % Quantit� de gains
        data(idx,5) = choice.prospect.loss; % Quantit� de pertes
        data(idx,6) = choice.prospect.diff; % Difficult� du d�fi
        
        if strcmp(choice.made, param.choice.decline_caption) == 1 % r�ponse = NON
            data(idx,7) = 0;
        else % R�ponse = OUI
            data(idx,7) = 1;
        end
        
        data(idx,8) = choice.rt; % Temps de r�action pour faire le choix
        data(idx,11) = param.choice.agentivity(trial_id); % Agentivit� du d�fi (1 = agent ; 0 = non agent)
        data(idx,12) = challenge.feedData; % R�ussite au d�fi
        data(idx,13) = challenge.estimDur; % Temps de r�action au d�fi
        
        save([passation.tmpfile, '_data.mat'], 'data'); % Temporary backup of data
        
        %-M�j moyenne et �cart type---------------------------------------%
        if param.choice.agentivity(trial_id) == 1 && abs(challenge.perf) < param.choice.moy_estim + (1.2 * param.choice.std_estim) % Seulement si agent et valeur non extr�me
            timePerf(idx+1) = challenge.perf;
        end
        
        if ~isnan(nanmedian(abs(rmoutliers(timePerf)))) && ~isnan(nanstd(abs(rmoutliers(timePerf))))
            param.choice.moy_estim = nanmedian(abs(rmoutliers(timePerf))); % nanmean(abs(timePerf)); % Moyenne
            param.choice.std_estim = nanstd(abs(rmoutliers(timePerf))); % Ecart type
        end
        
        %-Eye-tracker : create data matrix--------------------------------%
        
        if passation.flags.eyetracker == 1 % Tobii
            eyetracker.data.gaze = [eyetracker.data.gaze , eyetracker.tobii.get_gaze_data()];
        end
        
    end % Fin de la boucle � travers les essais
    
end % Fin de la boucle � travers l'agentivit�

% Fin de la partie 2 : affichage du gain total
DrawFormattedText(param.screen.window, 'Vos GAINS totaux sont de :', 'center', 'center', param.color.white, [], [], [], [], [], [0 param.screen.Ypixels*0.45 param.screen.Xpixels param.screen.Ypixels*0.45]);
DrawFormattedText(param.screen.window, double([' ' num2str(param.challenge.gain_tot / 10) ' �']), 'center', 'center', param.color.white, [], [], [], [], [], [0 param.screen.Ypixels*0.55 param.screen.Xpixels param.screen.Ypixels*0.55]);

vbl = Screen('Flip', param.screen.window, [], 1);
Screen('Flip', param.screen.window, vbl + param.fun.secTiming(param.end.dur, param.screen.monitorFlipInterval));

% Save data
passation.logfile.part2.moy_estim = param.choice.moy_estim; % Moyenne finale du d�fi
passation.logfile.part2.std_estim = param.choice.std_estim; % Ecart type final du d�fi
passation.logfile.part2.gain_tot = param.challenge.gain_tot / 10; % Gain total � la fin de la partie 2

% Enregistrer les variables d'int�r�ts (moyenne et �cart type)
filename = fullfile(participant.dataFolder, sprintf('%s_estim_param.mat', participant.identifier));
moy_estim = param.choice.moy_estim;
std_estim = param.choice.std_estim;
save(filename, 'moy_estim', 'std_estim');

%% ANALYSE : v�rifier les choix du sujet
% Probabilit� d'accepter le d�fi en fonction de chaque dimension de la t�che

task_dim_column = [4 5 6]; % Gains = colonne 4 ; Pertes = colonne 5 ; Diff = colonne 6 dans la matrice 'data'
difficulty = data(:,7); % Offre accept�e (1) ou rejet�e (0)
choice_p = NaN(4,3);
fig = figure;
hold on;

for task_dim = 1:3 % task_dim : 1 = gain, 2 = loss, 3 = diff
    
    prospect = data(:,task_dim_column(task_dim));
    id_q1 = find(prospect < 20);
    id_q2 = find(prospect < 30 & prospect >= 20);
    id_q3 = find(prospect < 40 & prospect >= 30);
    id_q4 = find(prospect < 50 & prospect >= 40);
    
    choice_p(:,task_dim) = [sum(difficulty(id_q1))/length(id_q1) sum(difficulty(id_q2))/length(id_q2)...
        sum(difficulty(id_q3))/length(id_q3) sum(difficulty(id_q4))/length(id_q4)]; % choice_p dim : task_levels (4 levels) by task dim (gain, loss, challenge_diff)
    
    % FIGURE : proba d'accepter pour chaque dimension (gain, loss, diff) et chaque sujet
    % On supprime les l�gendes car la figure va s'afficher devant le participant
    
    subplot(3,1,task_dim);
    plot(choice_p(:,task_dim),'ko-')
    
    ax = gca;
    ax.XTick = [1 2 3 4];
    ax.YTick = (0:.2:1);
    axis([0 5 0 1])
    
end

fig_name = fullfile(participant.dataFolder, sprintf('%s_task_dim_effect_on_choice.png', participant.identifier));
fig.PaperPosition = [0 0 10 10]; % Fig size [left bottom width height]
print(fig, fig_name, '-dpng');

close(fig);

%% AFFICHER LA FIGURE ET CHOISIR LA SUITE DU LOCA

choicePlot = imread(fig_name); % Load 'task_dim_effect_on_choice' figure
imageTexture = Screen('MakeTexture', param.screen.window, choicePlot); % Make the image into a texture

[s1, s2, ~] = size(choicePlot); % Get the size of the image
aspectRatio = s2 / s1;
imageHeights = param.screen.Ypixels .* 0.5; % Affichage de l'image � 50% de sa taille
imageWidths = imageHeights .* aspectRatio;
theRect = [0 0 imageWidths imageHeights];
dstRects = CenterRectOnPointd(theRect, param.screen.xCenter , param.screen.Ypixels*0.3);
leftTextColor = param.color.white;
rightTextColor = param.color.white;

choiceEndLoca = false;

while choiceEndLoca == false
    
    % Check the HID device (keyboard, gamepad...) to see if a button has been pressed
    [~, ~, keyCode] = KbCheck(param.deviceNumber);
    
    if keyCode(param.key.left) % VERSION A : refaire des essais avec des choix faciles
        leftTextColor = param.color.red;
        rightTextColor = param.color.white;
        part2bis = true;
    elseif keyCode(param.key.right) % VERSION B : directement passer � la partie 3
        rightTextColor = param.color.red;
        leftTextColor = param.color.white;
        part2bis = false;
    elseif exist('part2bis', 'var') && keyCode(param.key.valid) % Validation
        choiceEndLoca = true;
        KbReleaseWait(param.deviceNumber);
    elseif keyCode(param.key.escape)
        ConfiDecid_escape(passation, flags, eyetracker);
        return
    end
    
    % Draw things
    Screen('DrawTexture', param.screen.window, imageTexture, [], dstRects); % Draw the image to the screen
    Screen('TextSize', param.screen.window , param.default_textSize);
    DrawFormattedText(param.screen.window, 'Version A', 'center', 'center', leftTextColor, [], [], [], [], [], [param.screen.Xpixels*0.15 param.screen.Ypixels*0.7 param.screen.Xpixels*0.35 param.screen.Ypixels*0.7]); % Left text
    DrawFormattedText(param.screen.window, 'Version B', 'center', 'center', rightTextColor, [], [], [], [], [], [param.screen.Xpixels*0.65 param.screen.Ypixels*0.7 param.screen.Xpixels*0.85 param.screen.Ypixels*0.7]); % Right text
    DrawFormattedText(param.screen.window, ['Attendre l' char(39) 'exp�rimentateur'], 'center', param.screen.Ypixels*0.9, param.color.grey);
    
    % Flip to the screen
    Screen('Flip', param.screen.window);
    
end

%% PARTIE 2 BIS
% Si les gains, pertes et difficult� ne sont pas bien int�gr�s, on refait des essais identiques � la partie 2 avec :
% - Difficult� moyenne + (grands gains et petites pertes) OU (petits gains et grandes pertes)
% - Gains et pertes moyens + (petite difficult�) OU (grande difficult�)

if part2bis == true
    
    repet = -1;
    endPart2bis = false;
    good_choice = 0;
    
    while endPart2bis == false % On recommence la partie 2 bis tant qu'il n'y a pas au moins 3 choix corrects sur 4
        
        repet = repet + 1;
        
        % Choix + D�fi
        data(end+1:end+param.training.part2.bis.nb_trial,1:15) = NaN(param.training.part2.bis.nb_trial,15); % Matrice data
        timePerf(end+1:end+param.training.part2.bis.nb_trial,1) = NaN(param.training.part2.bis.nb_trial,1);
        
        param.choice.stakes = Shuffle(param.training.part2.bis.choice.stakes,2);
        param.choice.agentivity = Shuffle(param.training.part2.bis.choice.agentivity);
        param.choice.intervert_gain_loss = Shuffle(param.training.part2.bis.choice.intervert_gain_loss);
        param.choice.intervert_y_n = Shuffle(param.training.part2.bis.choice.intervert_y_n);
        
        for trial_id = 1:param.training.part2.bis.nb_trial % Boucle � travers les essais
            
            %-Choix-----------------------------------------------------------%
            [choice, eyetracker] = ConfiDecid_display_choice(trial_id, param, passation, flags, eyetracker);
            
            if isfield(choice, 'escape') && choice.escape == 1
                return
            end
            
            % Save data
            passation.logfile.part2.bis.choice.agentivity((param.training.part2.bis.nb_trial*repet)+trial_id,:) = param.choice.agentivity(trial_id); % Agentivit� (1 = agent ; 0 = non agent)
            passation.logfile.part2.bis.choice.gain((param.training.part2.bis.nb_trial*repet)+trial_id,:) = choice.prospect.gain; % Gain prospect
            passation.logfile.part2.bis.choice.loss((param.training.part2.bis.nb_trial*repet)+trial_id,:) = choice.prospect.loss; % Loss prospect
            passation.logfile.part2.bis.choice.difficulty((param.training.part2.bis.nb_trial*repet)+trial_id,:) = choice.prospect.diff; % Difficult� de l'essai
            passation.logfile.part2.bis.choice.config((param.training.part2.bis.nb_trial*repet)+trial_id,:) = choice.trig.config; % Configuration spatiale du choix
            passation.logfile.part2.bis.choice.onset((param.training.part2.bis.nb_trial*repet)+trial_id,:) = choice.onset; % Onset du choix
            passation.logfile.part2.bis.choice.offset((param.training.part2.bis.nb_trial*repet)+trial_id,:) = choice.secsRelease; % Offset du choix
            passation.logfile.part2.bis.choice.resp_key((param.training.part2.bis.nb_trial*repet)+trial_id,:) = param.fun.whichKey(choice.keyCode); % Num�ro du bouton pr�ss�
            passation.logfile.part2.bis.choice.resp_press((param.training.part2.bis.nb_trial*repet)+trial_id,:) = choice.secsPress; % Button press time
            passation.logfile.part2.bis.choice.choiceMade{(param.training.part2.bis.nb_trial*repet)+trial_id,:} = choice.made; % OUI ou NON
            passation.logfile.part2.bis.choice.noChoice_onset((param.training.part2.bis.nb_trial*repet)+trial_id,:) = choice.onset_no; % Onset de l'�cran lors du choix NON
            passation.logfile.part2.bis.choice.noChoice_offset((param.training.part2.bis.nb_trial*repet)+trial_id,:) = choice.offset_no; % Offset de l'�cran choix NON
            choice.rt = choice.secsPress - choice.onset; % Temps de r�action pour choisir
            
            %-D�fi------------------------------------------------------------%
            challenge = ConfiDecid_display_challenge(trial_id, param, choice, passation, flags, eyetracker);
            
            if isfield(challenge, 'escape') && challenge.escape == 1
                return
            end
            
            % Save data
            passation.logfile.part2.bis.challenge.onset((param.training.part2.bis.nb_trial*repet)+trial_id,:) = challenge.start; % D�but du d�fi
            passation.logfile.part2.bis.challenge.offset((param.training.part2.bis.nb_trial*repet)+trial_id,:) = challenge.secsRelease; % Fin du d�fi
            passation.logfile.part2.bis.challenge.resp_key((param.training.part2.bis.nb_trial*repet)+trial_id,:) = challenge.resp_key; % Num�ro du bouton pr�ss�
            passation.logfile.part2.bis.challenge.resp_press((param.training.part2.bis.nb_trial*repet)+trial_id,:) = challenge.secsPress; % Button press time
            passation.logfile.part2.bis.challenge.timePerf((param.training.part2.bis.nb_trial*repet)+trial_id,:) = challenge.perf; % Performance par rapport au temps pour arriver au centre de l'�cran
            passation.logfile.part2.bis.challenge.success((param.training.part2.bis.nb_trial*repet)+trial_id,:) = challenge.feedData; % 1 = r�ussi, 0 = rat�
            
            % Gains totaux
            param.challenge.gain_tot = param.challenge.gain_tot + challenge.gain_trial;
            
            %-Feedback--------------------------------------------------------%
            feedback = ConfiDecid_display_feedback(param, challenge);
            
            if isfield(feedback, 'escape') && feedback.escape == 1
                return
            end
            
            % Save data
            passation.logfile.part2.bis.challenge.feed_onset((param.training.part2.bis.nb_trial*repet)+trial_id,:) = feedback.onset;
            passation.logfile.part2.bis.challenge.feed_offset((param.training.part2.bis.nb_trial*repet)+trial_id,:) = feedback.offset;
            
            %-Temporary backup------------------------------------------------%
            save(passation.tmpfile, 'passation');
            
            % Save data matrix
            % Colonne 1 = vide car indice du participant lors des analyses
            idx = find(isnan(data(:,2)),1);
            data(idx,2) = participant.session; % Num�ro de la session
            data(idx,3) = choice.trig.config; % Configuration du choix
            data(idx,4) = choice.prospect.gain; % Quantit� de gains
            data(idx,5) = choice.prospect.loss; % Quantit� de pertes
            data(idx,6) = choice.prospect.diff; % Difficult� du d�fi
            
            if strcmp(choice.made, param.choice.decline_caption) == 1 % r�ponse = NON
                data(idx,7) = 0;
            else % R�ponse = OUI
                data(idx,7) = 1;
            end
            
            data(idx,8) = choice.rt; % Temps de r�action pour faire le choix
            data(idx,11) = param.choice.agentivity(trial_id); % Agentivit� du d�fi (1 = agent ; 0 = non agent)
            data(idx,12) = challenge.feedData; % R�ussite au d�fi
            data(idx,13) = challenge.estimDur; % Temps de r�action au d�fi
            
            save([passation.tmpfile, '_data.mat'], 'data'); % Temporary backup of data
            
            %-M�j moyenne et �cart type---------------------------------------%
            if param.choice.agentivity(trial_id) == 1 && abs(challenge.perf) < param.choice.moy_estim + (1.2 * param.choice.std_estim) % Seulement si agent et valeur non extr�me
                timePerf(idx+1) = challenge.perf;
            end
            param.choice.moy_estim = nanmedian(abs(rmoutliers(timePerf))); % nanmean(abs(timePerf)); % Moyenne
            param.choice.std_estim = nanstd(abs(rmoutliers(timePerf))); % Ecart type
            
            %-Eye-tracker : create data matrix--------------------------------%
            
            if passation.flags.eyetracker == 1 % Tobii
                eyetracker.data.gaze = [eyetracker.data.gaze , eyetracker.tobii.get_gaze_data()];
            end
            
            %-Good choice-----------------------------------------------------%
            % Evaluate whether the subject made the right choice according
            % to utility(accept) = p(success)*gain - (1-p(success))*loss
            
            utility = (param.choice.proba(choice.prospect.diff-9)*choice.prospect.gain) - ((100-param.choice.proba(choice.prospect.diff-9))*choice.prospect.loss);
            
            if data(idx,7)*2-1 == sign(utility) % Good choice
                good_choice = good_choice + 1;
            else % Bad choice
                good_choice = good_choice - 1;
            end
            
            % On ne sort pas de la boucle while tant qu'il n'y a pas au moins 3 choix corrects sur 4 on qu'on n'a pas atteint le nb max de r�p�tition
            if good_choice >= (param.training.part2.bis.nb_trial*repet) + param.training.part2.bis.nb_trial - 2*repet || repet == param.training.part2.bis.max_repet
                endPart2bis = true;
            else
                endPart2bis = false;
            end
            
        end % Fin de la boucle � travers les essais        
    end % Fin du while
    
    % Save data
    passation.logfile.part2.moy_estim = param.choice.moy_estim; % Moyenne finale du d�fi
    passation.logfile.part2.std_estim = param.choice.std_estim; % Ecart type final du d�fi
    passation.logfile.part2.gain_tot = param.challenge.gain_tot / 10; % Gain total � la fin de la partie 2 bis
    
    % Enregistrer les variables d'int�r�ts (moyenne et �cart type)
    filename = fullfile(participant.dataFolder, sprintf('%s_estim_param.mat', participant.identifier));
    moy_estim = param.choice.moy_estim;
    std_estim = param.choice.std_estim;
    save(filename, 'moy_estim', 'std_estim');
    
end

%% PARTIE 3
% T�che CONFIDENCE-DECID compl�te (8 essais d'entra�nement) :  choix + confiance + d�fi + feedback + mood rating

%-Instructions partie 3---------------------------------------------------%
param.instr.slides = param.training.part3.instr.slides;

instr = ConfiDecid_display_instructions(param);

% Save data
passation.logfile.part3.instructions.onset = instr.onset; % Onset des instructions
passation.logfile.part3.instructions.offset = instr.secsRelease; % Offset des instructions
passation.logfile.part3.instructions.resp_key = param.fun.whichKey(instr.keyCode); % Num�ro du dernier bouton pr�ss�
passation.logfile.part3.instructions.resp_press = instr.secsPress; % Button press time

%-------------------------------------------------------------------------%

data(end+1:end+param.training.part3.nb_trial,1:15) = NaN(param.training.part3.nb_trial,15); % Matrice data

param.choice.stakes = param.training.part3.choice.stakes;
param.choice.agentivity = param.training.part3.choice.agentivity;
param.choice.intervert_gain_loss = param.training.part3.choice.intervert_gain_loss;
param.choice.intervert_y_n = param.training.part3.choice.intervert_y_n;

for trial_id = 1:param.training.part3.nb_trial % Boucle � travers les essais
    
    %-Choice task---------------------------------------------------------%
    [choice, eyetracker] = ConfiDecid_display_choice(trial_id, param, passation, flags, eyetracker);
    
    if isfield(choice, 'escape') && choice.escape == 1
        return
    end
    
    % Save data
    passation.logfile.part3.choice.agentivity(trial_id,:) = param.choice.agentivity(trial_id); % Agentivit� (1 = agent ; 0 = non agent)
    passation.logfile.part3.choice.gain(trial_id,:) = choice.prospect.gain; % Gain prospect
    passation.logfile.part3.choice.loss(trial_id,:) = choice.prospect.loss; % Loss prospect
    passation.logfile.part3.choice.difficulty(trial_id,:) = choice.prospect.diff; % Difficult� de l'essai
    passation.logfile.part3.choice.config(trial_id,:) = choice.trig.config; % Configuration spatiale du choix
    passation.logfile.part3.choice.onset(trial_id,:) = choice.onset; % Onset du choix
    passation.logfile.part3.choice.offset(trial_id,:) = choice.secsRelease; % Offset du choix
    passation.logfile.part3.choice.resp_key(trial_id,:) = param.fun.whichKey(choice.keyCode); % Num�ro du bouton pr�ss�
    passation.logfile.part3.choice.resp_press(trial_id,:) = choice.secsPress; % Button press time
    passation.logfile.part3.choice.choiceMade{trial_id,:} = choice.made; % OUI ou NON
    passation.logfile.part3.choice.noChoice_onset(trial_id,:) = choice.onset_no; % Onset de l'�cran lors du choix NON
    passation.logfile.part3.choice.noChoice_offset(trial_id,:) = choice.offset_no; % Offset de l'�cran choix NON
    choice.rt = choice.secsPress - choice.onset; % Temps de r�action pour choisir
    
    %-Confidence rating---------------------------------------------------%
    
    confidence = ConfiDecid_display_rating(param, 'confidence', passation, flags, eyetracker); % Display mood rating
    
    if isfield(confidence, 'escape') && confidence.escape == 1
        return
    end
    
    % Save data
    passation.logfile.part3.confidence.onset(trial_id,:) = confidence.onset; % Onset du mood rating
    passation.logfile.part3.confidence.offset(trial_id,:) = confidence.secsRelease; % Offset du mood rating
    passation.logfile.part3.confidence.resp_press(trial_id,:) = confidence.secsPress; % Button press time
    passation.logfile.part3.confidence.cursorPosInit(trial_id,:) = ((confidence.cursor_Xpos(1) - param.rating.linePos(1)) / param.rating.pixelsPerPress) + 1; % Position initiale du curseur
    passation.logfile.part3.confidence.value(trial_id,:) = confidence.rating; % Mood rating de 1 � 100
    confidence.rt = passation.logfile.part3.confidence.resp_press(trial_id)-passation.logfile.part3.confidence.onset(trial_id);
    
    %-D�fi (precision task or lottery game)-------------------------------%
    % Precision task = Il faut appuyer lorsqu'on pense que le point se situe dans le rectangle de difficult�
    
    challenge = ConfiDecid_display_challenge(trial_id, param, choice, passation, flags, eyetracker);
    
    if isfield(challenge, 'escape') && challenge.escape == 1
        return
    end
    
    % Save data
    passation.logfile.part3.challenge.onset(trial_id,:) = challenge.start; % D�but du d�fi
    passation.logfile.part3.challenge.offset(trial_id,:) = challenge.secsRelease; % Fin du d�fi
    passation.logfile.part3.challenge.resp_key(trial_id,:) = challenge.resp_key; % Num�ro du bouton pr�ss�
    passation.logfile.part3.challenge.resp_press(trial_id,:) = challenge.secsPress; % Button press time
    passation.logfile.part3.challenge.timePerf(trial_id,:) = challenge.perf; % Performance par rapport au temps pour arriver au centre de l'�cran
    passation.logfile.part3.challenge.success(trial_id,:) = challenge.feedData; % 1 = r�ussi, 0 = rat�
    
    % Gains totaux
    param.challenge.gain_tot = param.challenge.gain_tot + challenge.gain_trial;
    
    %-Feedback------------------------------------------------------------%
    
    feedback = ConfiDecid_display_feedback(param, challenge);
    
    if isfield(feedback, 'escape') && feedback.escape == 1
        return
    end
    
    % Save data
    passation.logfile.part3.challenge.feed_onset(trial_id,:) = feedback.onset;
    passation.logfile.part3.challenge.feed_offset(trial_id,:) = feedback.offset;
    
    %-Mood rating---------------------------------------------------------%
    
    if ismember(trial_id,param.mood.rating_idx)
        
        mood = ConfiDecid_display_rating(param, 'mood', passation, flags, eyetracker); % Display mood rating
        
        if isfield(mood, 'escape') && mood.escape == 1
            return
        end
        
        % Save data
        passation.logfile.part3.mood.onset(trial_id,:) = mood.onset; % Onset du mood rating
        passation.logfile.part3.mood.offset(trial_id,:) = mood.secsRelease; % Offset du mood rating
        passation.logfile.part3.mood.resp_press(trial_id,:) = mood.secsPress; % Button press time
        passation.logfile.part3.mood.cursorPosInit(trial_id,:) = ((mood.cursor_Xpos(1) - param.rating.linePos(1)) / param.rating.pixelsPerPress) + 1; % Position initiale du curseur
        passation.logfile.part3.mood.value(trial_id,:) = mood.rating; % Mood rating de 1 � 100
        mood.rt = passation.logfile.part3.mood.resp_press(trial_id)-passation.logfile.part3.mood.onset(trial_id);
        
    else
        passation.logfile.part3.mood.onset(trial_id,:) = NaN;
        passation.logfile.part3.mood.offset(trial_id,:) = NaN;
        passation.logfile.part3.mood.resp_press(trial_id,:) = NaN;
        passation.logfile.part3.mood.cursorPosInit(trial_id,:) = NaN;
        passation.logfile.part3.mood.value(trial_id,:) = NaN;
        mood.rating = NaN;
        mood.rt = NaN;
    end
    
    %-Inter-trial---------------------------------------------------------%
    
    % Intervalle entre la fin de l'estimation et le d�but de l'essai suivant
    vbl = Screen('Flip', param.screen.window);
    Screen('Flip', param.screen.window, vbl + param.fun.secTiming(param.end.interTrial, param.screen.monitorFlipInterval)); % Dur�e = 500 ms
    
    %-Temporary backup----------------------------------------------------%
    save(passation.tmpfile, 'passation');
    
    % Save data matrix
    % Colonne 1 = vide car indice du participant lors des analyses
    idx = find(isnan(data(:,2)),1);
    data(idx,2) = participant.session;
    data(idx,3) = choice.trig.config; % Configuration du choix
    data(idx,4) = choice.prospect.gain; % Quantit� de gains
    data(idx,5) = choice.prospect.loss; % Quantit� de pertes
    data(idx,6) = choice.prospect.diff; % Difficult� du d�fi
    
    if strcmp(choice.made, param.choice.decline_caption) == 1 % r�ponse = NON
        data(idx,7) = 0;
    else % R�ponse = OUI
        data(idx,7) = 1;
    end
    
    data(idx,8) = choice.rt; % Temps de r�action pour faire le choix
    data(idx,9) = confidence.rating; % Note de confiance
    data(idx,10) = confidence.rt; % Temps de r�action pour noter la confiance
    data(idx,11) = param.choice.agentivity(trial_id); % Agentivit� du d�fi (1 = agent ; 0 = non agent)
    data(idx,12) = challenge.feedData; % R�ussite au d�fi
    data(idx,13) = challenge.estimDur; % Temps de r�action au d�fi
    data(idx,14) = mood.rating; % Note de l'humeur
    data(idx,15) = mood.rt; % Temps de r�action pour noter l'humeur
    
    save([passation.tmpfile, '_data.mat'], 'data'); % Temporary backup of data
    
    %-Eye-tracker : create data matrix------------------------------------%
    
    if passation.flags.eyetracker == 1 % Tobii
        eyetracker.data.gaze = [eyetracker.data.gaze , eyetracker.tobii.get_gaze_data()];
    end
    
end % End of the loop across trials

%-Fin de la partie 3------------------------------------------------------%

% Affichage du gain total
DrawFormattedText(param.screen.window, 'Vos GAINS totaux sont de :', 'center', 'center', param.color.white, [], [], [], [], [], [0 param.screen.Ypixels*0.45 param.screen.Xpixels param.screen.Ypixels*0.45]);
DrawFormattedText(param.screen.window, double([' ' num2str(param.challenge.gain_tot / 10) ' �']), 'center', 'center', param.color.white, [], [], [], [], [], [0 param.screen.Ypixels*0.55 param.screen.Xpixels param.screen.Ypixels*0.55]);

vbl = Screen('Flip', param.screen.window, [], 1);
Screen('Flip', param.screen.window, vbl + param.fun.secTiming(param.end.dur, param.screen.monitorFlipInterval));

passation.logfile.part3.gain_tot = param.challenge.gain_tot / 10; % Gain total en �

%% Save data
save(passation.filename, 'passation'); % Enregistrer le fichier LOGFILE final
delete([passation.tmpfile, '.mat']); % Supprimer le fichier temporaire

save([passation.filename, '_data.mat'], 'data'); % Enregistrer le fichier DATA final
delete([passation.tmpfile, '_data.mat']); % Supprimer le fichier temporaire

%% End eye-tracker and save data

EyeTrackerFilename = [passation.filename, '_eye'];

if passation.flags.eyetracker == 1 % Tobii
    
    eyetracker.tobii.stop_gaze_data(); % Stop recording
    tic
    eyetracker.data.gaze = convertTobiiData(eyetracker.data); % Convert eye-tracker data to save them faster
    eyetracker = eyetracker.data;
    save(EyeTrackerFilename, 'eyetracker');
    toc
    
end

%% END
sca;

end