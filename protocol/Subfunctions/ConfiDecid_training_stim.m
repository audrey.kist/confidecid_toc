%% TRAINING CONFIDANCE DECID FOR STIMULATION PROTOCOLE
% Entra�nement court � la t�che de choix ConfiDECID que le sujet a d�j� fait
% Permet de mettre � jour la moyenne et l'�cart type de l'estimation pour le sujet

function ConfiDecid_training_stim(participant, flags, param, eyetracker)

%% Data collection

passation = [];

% Cr�er un dossier par participant (s'il n'existe pas d�j�)
if ~exist(participant.dataFolder, 'dir')
    mkdir(participant.dataFolder);
end

% D�finir le nom du fichier temporaire o� sauvegarder les infos
passation.tmpfile = fullfile(participant.dataFolder,...
    sprintf('%s_%s_%s_tmp', participant.info.nameid, datestr(participant.date,'yyyymmdd_HHMM'), participant.sess_name));

% D�finir le nom du fichier o� sauvegarder les infos finales
passation.filename = fullfile(participant.dataFolder,...
    sprintf('%s_%s', participant.identifier, participant.sess_name));

% Enregistrer les donn�es de la passation
passation.taskParameters = fileread(which('ConfiDecid_taskParameters')); % Garder une trace des param�tres utilis�s pour la session
passation.running = fileread(sprintf('%s.m',mfilename('fullpath'))); % Garder une trace du script qui a �t� utilis� pour la session
passation.screen = param.screen; % R�solution de l'�cran
passation.participant = participant;
passation.flags = flags;

% Trigger au d�but de l'entra�nement
start = GetSecs;
param.fun.trigger(param.training.start.trig, param.hport);

passation.logfile.start = start; % Time at the beginning of the session

%% Eye-tracker start recording

if passation.flags.eyetracker == 1 % Tobii
    
    tobiiTrackStatus(param.screen.window, eyetracker.tobii, param.screen, param.color); % Get user eyes position
    tobiiCalibration(param.screen.window, eyetracker.tobii, param.screen, param.color); % Calibration
    eyetracker.tobii.get_gaze_data(); % Start collecting data
    eyetracker.data.event = []; % Matrix to store events
    eyetracker.data.gaze = []; % Matrix to store gaze data
    
end

%% PARTIE 1 :
% D�fi d'estimation sans gains ni pertes + feedback sur la r�ussite au d�fi

% Instructions partie 1
param.instr.slides = param.training.part1.instr.slides;

instr = ConfiDecid_display_instructions(param);

% Save data
passation.logfile.part1.instructions.onset = instr.onset; % Onset des instructions
passation.logfile.part1.instructions.offset = instr.secsRelease; % Offset des instructions
passation.logfile.part1.instructions.resp_key = param.fun.whichKey(instr.keyCode); % Num�ro du dernier bouton pr�ss�
passation.logfile.part1.instructions.resp_press = instr.secsPress; % Button press time

%-------------------------------------------------------------------------%

% D�fi partie 1
% On d�termine la taille de cible pour laquelle le participant r�ussi dans 50% des cas
% On diminue la taille de la cible lorsqu'il r�ussit, on augmente la taille de la cible lorsqu'il perd

difficulty.stakes.gain = NaN;
difficulty.stakes.loss = NaN;

% Boucle � travers les essais
% On pr�sente le jeu jusqu'� stabilisation de la performance
% S'il n'y a pas de stabilisation, on fixe un nombre d'essai maximum

trial_id = 1;
estim_level(trial_id) = param.training.part1.moy_estim;
regularity(trial_id) = 0;

while trial_id <= param.training.part1.nb_trial_min || range(regularity(end-param.training.part1.plateau+1:end)) >= 2
    
    if trial_id == param.training.part1.nb_trial_max
        break
    end
    
    param.choice.agentivity(trial_id) = 1;
    
    % D�terminer les coordonn�es du rectangle repr�sentant la diff
    difficulty.tolerance = estim_level(trial_id); % 50% de chance de r�ussite
    
    choicePos = round(difficulty.tolerance * param.challenge.velocity);
    difficulty.diff_rect = [param.screen.xCenter - choicePos, param.screen.yCenter - 20, param.screen.xCenter + choicePos, param.screen.yCenter + 20]; % Difficulty box
    difficulty.diff_rect_out = [param.screen.xCenter - choicePos, param.screen.xCenter + choicePos, param.screen.xCenter - choicePos, param.screen.xCenter + choicePos, param.screen.xCenter - choicePos, param.screen.xCenter - choicePos, param.screen.xCenter + choicePos, param.screen.xCenter + choicePos ;...
        param.screen.yCenter + 20, param.screen.yCenter + 20, param.screen.yCenter - 20, param.screen.yCenter - 20, param.screen.yCenter + 20, param.screen.yCenter - 20, param.screen.yCenter + 20, param.screen.yCenter - 20]; % Outline of the difficulty box
    
    difficulty.diff_color = param.color.blue; % Rectangle repr�sentant la difficult� = bleu
    
    %-D�fi----------------------------------------------------------------%
    
    challenge = ConfiDecid_display_challenge(trial_id, param, difficulty, passation, flags, eyetracker);
    
    if isfield(challenge, 'escape') && challenge.escape == 1
        return
    end
    
    %-Feedback------------------------------------------------------------%
    
    if challenge.feedData == 1 % R�ussi
        feedText = '';
    else % Perdu
        if challenge.perf > 0 % Trop rapide
            feedText = 'trop rapide';
        else % Trop lent
            feedText = 'trop lent';
        end
    end
    
    Screen('TextSize', param.screen.window , param.default_textSize);
    DrawFormattedText(param.screen.window, feedText, 'center', param.screen.Ypixels * 0.35, param.color.white); % Info ('trop rapide' ou 'trop lent')
    Screen('FillRect', param.screen.window, param.color.grey, challenge.tunnel_entrance_rect); % Rectangle indicating the entrance of the tunnel
    Screen('FillOval', param.screen.window, param.color.darkgrey, challenge.tunnel_entrance); % Oval indicating the entrance of the tunnel
    Screen('FillRect', param.screen.window, param.color.grey, challenge.tunnel_rect); % Fill the tunnel
    Screen('FillRect', param.screen.window, challenge.feedback{2}, difficulty.diff_rect); % Fill the difficulty box
    Screen('DrawLines', param.screen.window, difficulty.diff_rect_out, 2, param.color.white); % Outlines of the difficulty box
    
    vbl = Screen('Flip', param.screen.window, [], 1);
    Screen('Flip', param.screen.window, vbl + param.fun.secTiming(param.training.part1.feedDur, param.screen.monitorFlipInterval));
    
    % Save data
    passation.logfile.part1.challenge.agentivity(trial_id,:) = param.choice.agentivity(trial_id); % Agentivit� = 1 (agent) dans cette partie
    passation.logfile.part1.challenge.onset(trial_id,:) = challenge.start; % D�but du d�fi
    passation.logfile.part1.challenge.offset(trial_id,:) = challenge.secsRelease; % Fin du d�fi
    passation.logfile.part1.challenge.resp_key(trial_id,:) = challenge.resp_key; % Num�ro du bouton pr�ss�
    passation.logfile.part1.challenge.resp_press(trial_id,:) = challenge.secsPress; % Button press time
    passation.logfile.part1.challenge.timePerf(trial_id,:) = challenge.perf; % Performance par rapport au temps pour arriver au centre de l'�cran
    passation.logfile.part1.challenge.success(trial_id,:) = challenge.feedData; % 1 = r�ussi, 0 = rat�
    
    %---------------------------------------------------------------------%
    
    % Update variables
    if trial_id > 1
        regularity(trial_id) = regularity(trial_id-1) - (challenge.feedData*2-1); % +1 si gagn� ; -1 si perdu
    end
    
    trial_id = trial_id + 1;
    
    % Modifier la taille de la cible en fonction de la r�ussite (apr�s l'incr�mentation de l'index de l'essai)
    if challenge.feedData == 1 % R�ussi
        estim_level(trial_id) = estim_level(trial_id-1) - param.training.part1.meanStep;
    else % Perdu
        estim_level(trial_id) = estim_level(trial_id-1) + param.training.part1.meanStep;
    end
    
end % Fin du while

%% PARTIE 2
% T�che CONFIDENCE-DECID-STIM compl�te (8 essais d'entra�nement) :  choix + confiance + d�fi + feedback

data = [];

%-Instructions partie 2---------------------------------------------------%
param.instr.slides = param.training.part2.instr.slides;

instr = ConfiDecid_display_instructions(param);

% Save data
passation.logfile.part2.instructions.onset = instr.onset; % Onset des instructions
passation.logfile.part2.instructions.offset = instr.secsRelease; % Offset des instructions
passation.logfile.part2.instructions.resp_key = param.fun.whichKey(instr.keyCode); % Num�ro du dernier bouton pr�ss�
passation.logfile.part2.instructions.resp_press = instr.secsPress; % Button press time

%-------------------------------------------------------------------------%

data(end+1:end+param.training.part2.nb_trial,1:13) = NaN(param.training.part2.nb_trial,13); % Matrice data

param.choice.stakes = param.training.part2.choice.stakes;
param.choice.agentivity = param.training.part2.choice.agentivity;
param.choice.intervert_gain_loss = param.training.part2.choice.intervert_gain_loss;
param.choice.intervert_y_n = param.training.part2.choice.intervert_y_n;

for trial_id = 1:param.training.part2.nb_trial % Boucle � travers les essais
    
    %-Choice task---------------------------------------------------------%
    [choice, eyetracker] = ConfiDecid_display_choice(trial_id, param, passation, flags, eyetracker);
    
    if isfield(choice, 'escape') && choice.escape == 1
        return
    end
    
    % Save data
    passation.logfile.part2.choice.agentivity(trial_id,:) = param.choice.agentivity(trial_id); % Agentivit� (1 = agent ; 0 = non agent)
    passation.logfile.part2.choice.gain(trial_id,:) = choice.prospect.gain; % Gain prospect
    passation.logfile.part2.choice.loss(trial_id,:) = choice.prospect.loss; % Loss prospect
    passation.logfile.part2.choice.difficulty(trial_id,:) = choice.prospect.diff; % Difficult� de l'essai
    passation.logfile.part2.choice.config(trial_id,:) = choice.trig.config; % Configuration spatiale du choix
    passation.logfile.part2.choice.onset(trial_id,:) = choice.onset; % Onset du choix
    passation.logfile.part2.choice.offset(trial_id,:) = choice.secsRelease; % Offset du choix
    passation.logfile.part2.choice.resp_key(trial_id,:) = param.fun.whichKey(choice.keyCode); % Num�ro du bouton pr�ss�
    passation.logfile.part2.choice.resp_press(trial_id,:) = choice.secsPress; % Button press time
    passation.logfile.part2.choice.choiceMade{trial_id,:} = choice.made; % OUI ou NON
    passation.logfile.part2.choice.noChoice_onset(trial_id,:) = choice.onset_no; % Onset de l'�cran lors du choix NON
    passation.logfile.part2.choice.noChoice_offset(trial_id,:) = choice.offset_no; % Offset de l'�cran choix NON
    choice.rt = choice.secsPress - choice.onset; % Temps de r�action pour choisir
    
    %-Confidence rating---------------------------------------------------%
    
    confidence = ConfiDecid_display_rating(param, 'confidence', passation, flags, eyetracker); % Display mood rating
    
    if isfield(confidence, 'escape') && confidence.escape == 1
        return
    end
    
    % Save data
    passation.logfile.part2.confidence.onset(trial_id,:) = confidence.onset; % Onset du mood rating
    passation.logfile.part2.confidence.offset(trial_id,:) = confidence.secsRelease; % Offset du mood rating
    passation.logfile.part2.confidence.resp_press(trial_id,:) = confidence.secsPress; % Button press time
    passation.logfile.part2.confidence.cursorPosInit(trial_id,:) = ((confidence.cursor_Xpos(1) - param.rating.linePos(1)) / param.rating.pixelsPerPress) + 1; % Position initiale du curseur
    passation.logfile.part2.confidence.value(trial_id,:) = confidence.rating; % Mood rating de 1 � 100
    confidence.rt = passation.logfile.part2.confidence.resp_press(trial_id)-passation.logfile.part2.confidence.onset(trial_id);
    
    %-D�fi (precision task or lottery game)-------------------------------%
    % Precision task = Il faut appuyer lorsqu'on pense que le point se situe dans le rectangle de difficult�
    
    challenge = ConfiDecid_display_challenge(trial_id, param, choice, passation, flags, eyetracker);
    
    if isfield(challenge, 'escape') && challenge.escape == 1
        return
    end
    
    % Save data
    passation.logfile.part2.challenge.onset(trial_id,:) = challenge.start; % D�but du d�fi
    passation.logfile.part2.challenge.offset(trial_id,:) = challenge.secsRelease; % Fin du d�fi
    passation.logfile.part2.challenge.resp_key(trial_id,:) = challenge.resp_key; % Num�ro du bouton pr�ss�
    passation.logfile.part2.challenge.resp_press(trial_id,:) = challenge.secsPress; % Button press time
    passation.logfile.part2.challenge.timePerf(trial_id,:) = challenge.perf; % Performance par rapport au temps pour arriver au centre de l'�cran
    passation.logfile.part2.challenge.success(trial_id,:) = challenge.feedData; % 1 = r�ussi, 0 = rat�
    
    % Gains totaux
    param.challenge.gain_tot = param.challenge.gain_tot + challenge.gain_trial;
    
    %-Feedback------------------------------------------------------------%
    
    feedback = ConfiDecid_display_feedback(param, challenge);
    
    if isfield(feedback, 'escape') && feedback.escape == 1
        return
    end
    
    % Save data
    passation.logfile.part2.challenge.feed_onset(trial_id,:) = feedback.onset;
    passation.logfile.part2.challenge.feed_offset(trial_id,:) = feedback.offset;
    
    %-Inter-trial---------------------------------------------------------%
    
    % Intervalle entre la fin de l'estimation et le d�but de l'essai suivant
    vbl = Screen('Flip', param.screen.window);
    Screen('Flip', param.screen.window, vbl + param.fun.secTiming(param.end.interTrial, param.screen.monitorFlipInterval)); % Dur�e = 500 ms
    
    %-Temporary backup----------------------------------------------------%
    save(passation.tmpfile, 'passation');
    
    % Save data matrix
    % Colonne 1 = vide car indice du participant lors des analyses
    idx = find(isnan(data(:,2)),1);
    data(idx,2) = 0; % Session = 0 = training
    data(idx,3) = choice.trig.config; % Configuration du choix
    data(idx,4) = choice.prospect.gain; % Quantit� de gains
    data(idx,5) = choice.prospect.loss; % Quantit� de pertes
    data(idx,6) = choice.prospect.diff; % Difficult� du d�fi
    
    if strcmp(choice.made, param.choice.decline_caption) == 1 % r�ponse = NON
        data(idx,7) = 0;
    else % R�ponse = OUI
        data(idx,7) = 1;
    end
    
    data(idx,8) = choice.rt; % Temps de r�action pour faire le choix
    data(idx,9) = confidence.rating; % Note de confiance
    data(idx,10) = confidence.rt; % Temps de r�action pour noter la confiance
    data(idx,11) = param.choice.agentivity(trial_id); % Agentivit� du d�fi (1 = agent ; 0 = non agent)
    data(idx,12) = challenge.feedData; % R�ussite au d�fi
    data(idx,13) = challenge.estimDur; % Temps de r�action au d�fi
    
    save([passation.tmpfile, '_data.mat'], 'data'); % Temporary backup of data
    
    %-Eye-tracker : create data matrix------------------------------------%
    
    if passation.flags.eyetracker == 1 % Tobii
        eyetracker.data.gaze = [eyetracker.data.gaze , eyetracker.tobii.get_gaze_data()];
    end
    
end % End of the loop across trials

%-Fin de la partie 2------------------------------------------------------%

% Affichage du gain total
DrawFormattedText(param.screen.window, 'Vos GAINS totaux sont de :', 'center', 'center', param.color.white, [], [], [], [], [], [0 param.screen.Ypixels*0.45 param.screen.Xpixels param.screen.Ypixels*0.45]);
DrawFormattedText(param.screen.window, double([' ' num2str(param.challenge.gain_tot / 10) ' �']), 'center', 'center', param.color.white, [], [], [], [], [], [0 param.screen.Ypixels*0.55 param.screen.Xpixels param.screen.Ypixels*0.55]);

vbl = Screen('Flip', param.screen.window, [], 1);
Screen('Flip', param.screen.window, vbl + param.fun.secTiming(param.end.dur, param.screen.monitorFlipInterval));

passation.logfile.part2.gain_tot = param.challenge.gain_tot / 10; % Gain total en �

%% Save data
save(passation.filename, 'passation'); % Enregistrer le fichier LOGFILE final
delete([passation.tmpfile, '.mat']); % Supprimer le fichier temporaire

save([passation.filename, '_data.mat'], 'data'); % Enregistrer le fichier DATA final
delete([passation.tmpfile, '_data.mat']); % Supprimer le fichier temporaire

%% End eye-tracker and save data

EyeTrackerFilename = [passation.filename, '_eye'];

if passation.flags.eyetracker == 1 % Tobii
    
    eyetracker.tobii.stop_gaze_data(); % Stop recording
    tic
    eyetracker.data.gaze = convertTobiiData(eyetracker.data); % Convert eye-tracker data to save them faster
    eyetracker = eyetracker.data;
    save(EyeTrackerFilename, 'eyetracker');
    toc
    
end

%% END
sca;

end