%% CONFIDENCE DECID EXPERIMENT

function ConfiDecid_experiment_cognition(participant, flags, param, eyetracker)

%% Data collection

passation = [];

% D�finir le nom du fichier temporaire o� sauvegarder les infos
passation.tmpfile = fullfile(participant.dataFolder,...
    sprintf('%s_%s_%s_tmp', participant.info.nameid, datestr(participant.date,'yyyymmdd_HHMM'), participant.sess_name));

% D�finir le nom du fichier o� sauvegarder les infos finales
passation.filename = fullfile(participant.dataFolder,...
    sprintf('%s_%s', participant.identifier, participant.sess_name));

% Enregistrer les donn�es de la passation
passation.taskParameters = fileread(which('ConfiDecid_taskParameters')); % Garder une trace des param�tres utilis�s pour la session
passation.running = fileread(sprintf('%s.m',mfilename('fullpath'))); % Garder une trace du script qui a �t� utilis� pour la session
passation.screen = param.screen; % R�solution de l'�cran
passation.participant = participant;
passation.flags = flags;
passation.logfile.combiDim = param.choice.diff_gain_loss; % Matrice contenant les combinaisons des 3 param�tres utilis�s pour la session
passation.logfile.moy_estim = param.choice.moy_estim;
passation.logfile.std_estim = param.choice.std_estim;

% Initialisation de la matrice data
data = NaN(param.nb_trial,15);

% Trigger au d�but de la session
start = GetSecs;
param.fun.trigger(param.start.trig, param.hport);

passation.logfile.start = start; % Time at the beginning of the session

%% Eye-tracker start recording

if passation.flags.eyetracker == 1 % Tobii
    
    tobiiTrackStatus(param.screen.window, eyetracker.tobii, param.screen, param.color); % Get user eyes position
    tobiiCalibration(param.screen.window, eyetracker.tobii, param.screen, param.color); % Calibration
    eyetracker.tobii.get_gaze_data(); % Start collecting data
    eyetracker.data.event = []; % Matrix to store events
    eyetracker.data.gaze = []; % Matrix to store gaze data
    
end

%% Instructions

instr = ConfiDecid_display_instructions(param);

% Save data
passation.instructions.onset = instr.onset; % Onset des instructions
passation.instructions.offset = instr.secsRelease; % Offset des instructions
passation.instructions.resp_key = param.fun.whichKey(instr.keyCode); % Num�ro du dernier bouton pr�ss�
passation.instructions.resp_press = instr.secsPress; % Button press time


%% DEBUT DE LA BOUCLE A TRAVERS LES ESSAIS

for trial_id = 1:param.nb_trial
    
    %% Choice task
    
    [choice, eyetracker] = ConfiDecid_display_choice(trial_id, param, passation, flags, eyetracker);
    
    if isfield(choice, 'escape') && choice.escape == 1
        return
    end
    
    % Save data
    passation.logfile.choice.agentivity(trial_id,:) = param.choice.agentivity(trial_id); % Agentivit� (1 = agent ; 0 = non agent)
    passation.logfile.choice.gain(trial_id,:) = choice.prospect.gain; % Gain prospect
    passation.logfile.choice.loss(trial_id,:) = choice.prospect.loss; % Loss prospect
    passation.logfile.choice.difficulty(trial_id,:) = choice.prospect.diff; % Difficult� de l'essai
    passation.logfile.choice.config(trial_id,:) = choice.trig.config; % Configuration spatiale du choix
    passation.logfile.choice.onset(trial_id,:) = choice.onset; % Onset du choix
    passation.logfile.choice.offset(trial_id,:) = choice.secsRelease; % Offset du choix
    passation.logfile.choice.resp_key(trial_id,:) = param.fun.whichKey(choice.keyCode); % Num�ro du bouton pr�ss�
    passation.logfile.choice.resp_press(trial_id,:) = choice.secsPress; % Button press time
    passation.logfile.choice.choiceMade{trial_id,:} = choice.made; % OUI ou NON
    passation.logfile.choice.noChoice_onset(trial_id,:) = choice.onset_no; % Onset de l'�cran lors du choix NON
    passation.logfile.choice.noChoice_offset(trial_id,:) = choice.offset_no; % Offset de l'�cran choix NON
    choice.rt = choice.secsPress - choice.onset; % Temps de r�action pour choisir
    
    %% Confidence rating
    
    confidence = ConfiDecid_display_rating(param, 'confidence', passation, flags, eyetracker); % Display mood rating
    
    if isfield(confidence, 'escape') && confidence.escape == 1
        return
    end
    
    % Save data
    passation.logfile.confidence.onset(trial_id,:) = confidence.onset; % Onset du mood rating
    passation.logfile.confidence.offset(trial_id,:) = confidence.secsRelease; % Offset du mood rating
    passation.logfile.confidence.resp_press(trial_id,:) = confidence.secsPress; % Button press time
    passation.logfile.confidence.cursorPosInit(trial_id,:) = ((confidence.cursor_Xpos(1) - param.rating.linePos(1)) / param.rating.pixelsPerPress) + 1; % Position initiale du curseur
    passation.logfile.confidence.value(trial_id,:) = confidence.rating; % Mood rating de 1 � 100
    confidence.rt = passation.logfile.confidence.resp_press(trial_id)-passation.logfile.confidence.onset(trial_id);
    
    %% D�fi (precision task or lottery game)
    % Precision task = Il faut appuyer lorsqu'on pense que le point se situe dans le rectangle de difficult�
    
    challenge = ConfiDecid_display_challenge(trial_id, param, choice, passation, flags, eyetracker);
    
    if isfield(challenge, 'escape') && challenge.escape == 1
        return
    end
    
    % Save data
    passation.logfile.challenge.onset(trial_id,:) = challenge.start; % D�but du d�fi
    passation.logfile.challenge.offset(trial_id,:) = challenge.secsRelease; % Fin du d�fi
    passation.logfile.challenge.resp_key(trial_id,:) = challenge.resp_key; % Num�ro du bouton pr�ss�
    passation.logfile.challenge.resp_press(trial_id,:) = challenge.secsPress; % Button press time
    passation.logfile.challenge.timePerf(trial_id,:) = challenge.perf; % Performance par rapport au temps pour arriver au centre de l'�cran
    passation.logfile.challenge.success(trial_id,:) = challenge.feedData; % 1 = r�ussi, 0 = rat�
    
    % Gains totaux
    param.challenge.gain_tot = param.challenge.gain_tot + challenge.gain_trial;
    
    %% Feedback
    
    feedback = ConfiDecid_display_feedback(param, challenge);
    
    if isfield(feedback, 'escape') && feedback.escape == 1
        return
    end
    
    % Save data
    passation.logfile.challenge.feed_onset(trial_id,:) = feedback.onset;
    passation.logfile.challenge.feed_offset(trial_id,:) = feedback.offset;
    
    %% Mood rating
    
    if ismember(trial_id,param.mood.rating_idx)
        
        mood = ConfiDecid_display_rating(param, 'mood', passation, flags, eyetracker); % Display mood rating
        
        if isfield(mood, 'escape') && mood.escape == 1
            return
        end
        
        % Save data
        passation.logfile.mood.onset(trial_id,:) = mood.onset; % Onset du mood rating
        passation.logfile.mood.offset(trial_id,:) = mood.secsRelease; % Offset du mood rating
        passation.logfile.mood.resp_press(trial_id,:) = mood.secsPress; % Button press time
        passation.logfile.mood.cursorPosInit(trial_id,:) = ((mood.cursor_Xpos(1) - param.rating.linePos(1)) / param.rating.pixelsPerPress) + 1; % Position initiale du curseur
        passation.logfile.mood.value(trial_id,:) = mood.rating; % Mood rating de 1 � 100
        mood.rt = passation.logfile.mood.resp_press(trial_id)-passation.logfile.mood.onset(trial_id);
        
    else
        passation.logfile.mood.onset(trial_id,:) = NaN;
        passation.logfile.mood.offset(trial_id,:) = NaN;
        passation.logfile.mood.resp_press(trial_id,:) = NaN;
        passation.logfile.mood.cursorPosInit(trial_id,:) = NaN;
        passation.logfile.mood.value(trial_id,:) = NaN;
        mood.rating = NaN;
        mood.rt = NaN;
    end
    
    %% Inter-trial intervalle
    
    % Intervalle entre la fin de l'estimation et le d�but de l'essai suivant
    vbl = Screen('Flip', param.screen.window);
    Screen('Flip', param.screen.window, vbl + param.fun.secTiming(param.end.interTrial, param.screen.monitorFlipInterval)); % Dur�e = 500 ms
    
    %% Temporary backup
    save(passation.tmpfile, 'passation');
    
    % Save data matrix
    % Colonne 1 = vide car indice du participant lors des analyses
    data(trial_id,2) = participant.session;
    data(trial_id,3) = choice.trig.config; % Configuration du choix
    data(trial_id,4) = choice.prospect.gain; % Quantit� de gains
    data(trial_id,5) = choice.prospect.loss; % Quantit� de pertes
    data(trial_id,6) = choice.prospect.diff; % Difficult� du d�fi
    
    if strcmp(choice.made, param.choice.decline_caption) == 1 % r�ponse = NON
        data(trial_id,7) = 0;
    else % R�ponse = OUI
        data(trial_id,7) = 1;
    end
    
    data(trial_id,8) = choice.rt; % Temps de r�action pour faire le choix
    data(trial_id,9) = confidence.rating; % Note de confiance
    data(trial_id,10) = confidence.rt; % Temps de r�action pour noter la confiance
    data(trial_id,11) = param.choice.agentivity(trial_id); % Agentivit� du d�fi (1 = agent ; 0 = non agent)
    data(trial_id,12) = challenge.feedData; % R�ussite au d�fi
    data(trial_id,13) = challenge.estimDur; % Temps de r�action au d�fi
    data(trial_id,14) = mood.rating; % Note de l'humeur
    data(trial_id,15) = mood.rt; % Temps de r�action pour noter l'humeur
    
    save([passation.tmpfile, '_data.mat'], 'data'); % Temporary backup of data
    
    %% Eye-tracker : create data matrix
    
    if passation.flags.eyetracker == 1 % Tobii
        eyetracker.data.gaze = [eyetracker.data.gaze , eyetracker.tobii.get_gaze_data()];
    end
    
end % End of the loop across trials

%% Save ranking

ranking_file = fullfile(fileparts(participant.dataFolder), 'subj_ranking.mat');

if exist(ranking_file, 'file') % Si le fichier existe
    
    load(ranking_file, 'ranking');
    
    if ~ismember({participant.info.nameid}, ranking.subj_name) % Le sujet n'a pas son score enregistr�
        newRow = {height(ranking)+1, {participant.info.nameid}, param.challenge.gain_tot/10};
        ranking = [ranking ; newRow];
    elseif ismember({participant.info.nameid}, ranking.subj_name) && ranking.score(strcmp(ranking.subj_name, {participant.info.nameid})) < param.challenge.gain_tot/10 % Le sujet a d�j� fait une session mais avec un moins bon score
        ranking.score(strcmp(ranking.subj_name, {participant.info.nameid})) = param.challenge.gain_tot/10; % Mise � jour du score du sujet
    end
    
    ranking = sortrows(ranking,'score','descend'); % Sort the table by score
    [~,~,ic] = unique(ranking.score,'stable'); % Assign the same rank to repeated values 
    ranking.rank = ic; % Update ranking
    
else
    ranking = table(1, {participant.info.nameid}, param.challenge.gain_tot/10, 'VariableNames', {'rank', 'subj_name', 'score'});
end

save(ranking_file, 'ranking');

%% Fin de la session : Affichage du gain total et du rang

left_border = 0.25;
right_border = 0.75;
top_border = 0.6;
row_width = 0.05;
column_width = [0.2, 0.15, 0.15]; % sum(column_width) = right_border-left_border

% Trouver le rang correspondant au score de la session
if isempty(find(ranking.score <= param.challenge.gain_tot / 10, 1, 'first')) % Si aucun score du tableau "ranking" n'est plus petit
    rnk = height(ranking)+1; % Rang = dernier
else
    rnk = find(ranking.score <= param.challenge.gain_tot / 10, 1, 'first');
end

%-Affichage du gain total et du rang--------------------------------------%
Screen('TextSize', param.screen.window , param.default_textSize);
DrawFormattedText(param.screen.window, 'R�sultat de la session :', 'center', 'center', param.color.white, [], [], [], [], [], [0 param.screen.Ypixels*0.2 param.screen.Xpixels param.screen.Ypixels*0.2]);

DrawFormattedText(param.screen.window, participant.info.nameid, 'center', 'center', param.color.white, [], [], [], [], [], [param.screen.Xpixels*left_border, param.screen.Ypixels*0.4, param.screen.Xpixels*(left_border+column_width(1)), param.screen.Ypixels*0.4]); % Joueur
DrawFormattedText(param.screen.window, double([' ' num2str(param.challenge.gain_tot / 10) ' �']), 'center', 'center', param.color.white, [], [], [], [], [], [param.screen.Xpixels*(left_border+column_width(1)), param.screen.Ypixels*0.4, param.screen.Xpixels*(left_border+sum(column_width(1:2))), param.screen.Ypixels*0.4]); % Score
DrawFormattedText(param.screen.window, num2str(rnk), 'center', 'center', param.color.white, [], [], [], [], [], [param.screen.Xpixels*(left_border+sum(column_width(1:2))), param.screen.Ypixels*0.4, param.screen.Xpixels*right_border, param.screen.Ypixels*0.4]); % Rang

%-Affichage du classement-------------------------------------------------%

% Table titles
Screen('TextSize', param.screen.window , param.small_textSize);
DrawFormattedText(param.screen.window, 'Joueur', 'center', 'center', param.color.white, [], [], [], [], [], [param.screen.Xpixels*left_border, param.screen.Ypixels*(top_border-row_width), param.screen.Xpixels*(left_border+column_width(1)), param.screen.Ypixels*top_border]);
DrawFormattedText(param.screen.window, 'Score', 'center', 'center', param.color.white, [], [], [], [], [], [param.screen.Xpixels*(left_border+column_width(1)), param.screen.Ypixels*(top_border-row_width), param.screen.Xpixels*(left_border+sum(column_width(1:2))), param.screen.Ypixels*top_border]);
DrawFormattedText(param.screen.window, 'Rang', 'center', 'center', param.color.white, [], [], [], [], [], [param.screen.Xpixels*(left_border+sum(column_width(1:2))), param.screen.Ypixels*(top_border-row_width), param.screen.Xpixels*right_border, param.screen.Ypixels*top_border]);

% Fill table
background_color = [param.color.darkgrey ; param.color.grey];
for r = 1:5 % On affiche les 5 meilleurs scores
    
    % Fill background (alternate grey and dark grey background)
    Screen('FillRect', param.screen.window, background_color(mod(r,2)+2-1,:), [param.screen.Xpixels*left_border; param.screen.Ypixels*(top_border+row_width*(r-1)); param.screen.Xpixels*right_border; param.screen.Ypixels*(top_border+row_width*r)]);
    
    if r <= height(ranking)
        
        if ranking.score(r) == param.challenge.gain_tot/10 && strcmp(ranking.subj_name{r}, {participant.info.nameid}) % Le sujet est dans le classement des 5 meilleurs joueurs
            Screen('FillRect', param.screen.window, [param.color.green, 0.5], [param.screen.Xpixels*left_border; param.screen.Ypixels*(top_border+row_width*(r-1)); param.screen.Xpixels*right_border; param.screen.Ypixels*(top_border+row_width*r)]);
            Screen('FrameRect', param.screen.window, param.color.green, [param.screen.Xpixels*left_border; param.screen.Ypixels*(top_border+row_width*(r-1)); param.screen.Xpixels*right_border; param.screen.Ypixels*(top_border+row_width*r)], 5);
        end
        
        DrawFormattedText(param.screen.window, sprintf('%s', ranking.subj_name{r}), 'center', 'center', param.color.white, [], [], [], [], [], [param.screen.Xpixels*left_border, param.screen.Ypixels*(top_border+row_width*(r-1)), param.screen.Xpixels*(left_border+column_width(1)), param.screen.Ypixels*(top_border+row_width*r)]);
        DrawFormattedText(param.screen.window, sprintf('%.1f', ranking.score(r)), 'center', 'center', param.color.white, [], [], [], [], [], [param.screen.Xpixels*(left_border+column_width(1)), param.screen.Ypixels*(top_border+row_width*(r-1)), param.screen.Xpixels*(left_border+sum(column_width(1:2))), param.screen.Ypixels*(top_border+row_width*r)]);
        DrawFormattedText(param.screen.window, sprintf('%d', ranking.rank(r)), 'center', 'center', param.color.white, [], [], [], [], [], [param.screen.Xpixels*(left_border+sum(column_width(1:2))), param.screen.Ypixels*(top_border+row_width*(r-1)), param.screen.Xpixels*right_border, param.screen.Ypixels*(top_border+row_width*r)]);
    end
end

%-Pr�sentation � l'�cran--------------------------------------------------%
vbl = Screen('Flip', param.screen.window, [], 1);

%% Save data
passation.logfile.gain_tot = param.challenge.gain_tot / 10; % Gain total en �

save(passation.filename, 'passation'); % Enregistrer le fichier LOGFILE final
delete([passation.tmpfile, '.mat']); % Supprimer le fichier temporaire

save([passation.filename, '_data.mat'], 'data'); % Enregistrer le fichier DATA final
delete([passation.tmpfile, '_data.mat']); % Supprimer le fichier temporaire

%% End eye-tracker and save eye data

EyeTrackerFilename = [passation.filename, '_eye'];

if passation.flags.eyetracker == 1 % Tobii
    
    eyetracker.tobii.stop_gaze_data(); % Stop recording
    tic
    eyetracker.data.gaze = convertTobiiData(eyetracker.data); % Convert eye-tracker data to save them faster
    eyetracker = eyetracker.data;
    save(EyeTrackerFilename, 'eyetracker');
    toc
    
end

%% End final feedback
Screen('Flip', param.screen.window, vbl + param.fun.secTiming(param.end.dur, param.screen.monitorFlipInterval));

%% END
sca;

end