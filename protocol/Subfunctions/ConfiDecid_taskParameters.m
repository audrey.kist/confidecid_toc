%% DECID PARAMETERS

function [param, eyetracker] = ConfiDecid_taskParameters(runner, participant, flags)

%% Enable triggering
% Open trigger port & define SendTrigger

if flags.trigger_LPT % Pa
    
    if ispc % Windows
        CloseParPort;
        OpenParPort;
        param.hport = [];
        param.fun.trigger = @SendTrigger; % Usage : trigger(trig, hport)
    end
    
elseif flags.trigger_serial % Serial port
    
    IOPort('CloseAll')
    
    if IsWin % Windows
        %[param.hport] = IOPort('OpenSerialPort',FindSerialPort());
         [param.hport] = IOPort('OpenSerialPort','COM7'); % pour enregistrer LFP
         % avec triggers sur le convertisseur serial to usb � l'arri�re du
         % laptop
    end
    
    IOPort('ConfigureSerialPort',param.hport,'BaudRate=9600 Parity=None DataBits=8 StopBits=1');
    IOPort('Purge',param.hport);
    
    param.fun.trigger = @SendTrigger; % Usage : trigger(trig, hport)
    
else % No port
    % Do nothing on 'trigger' function calls
    param.hport = [];
    param.fun.trigger = @(trig, hport) []; % Usage : trigger(trig, hport)
end

%% Trigger codes

param.start.trig = 5; % D�but d'une session

param.choice.trig.config = 10; % Configuration du choix : 10 = gain haut, oui gauche ; 11 = gain haut, oui droite ; 12 = gain bas, oui gauche ; 13 = gain bas, oui droite cas agent / + 10 cas non agent
param.choice.trig.confirm = 14; % R�ponse au choix / + 10 cas non agent
param.confidence.trig.disp = 15; % Onset du confidence rating
param.confidence.trig.value = 150; % Confidence rating response
param.feed.trig.disp = 16; % Onset du feedback au d�fi
param.mood.trig.disp = 17; % Onset du mood rating
param.mood.trig.value = 170; % Mood rating response

param.training.start.trig = 4; % D�but de l'entra�nement

%% Screen & General settings

PsychDefaultSetup(2); % Here we call some default settings for setting up Psychtoolbox
screens = Screen('Screens'); % Get the screen numbers
screenNumber = max(screens); % Select the external screen if it is present (use 'min' to select the native screen)

% Define colors (luminance = 55%)
param.color.black = BlackIndex(screenNumber);
param.color.white = WhiteIndex(screenNumber);
param.color.grey = [(140/255) (140/255) (140/255)]; % Luminance = 55%
param.color.red = [1 (15/255) (47/255)]; % Luminance = 55%
param.color.green = [(7/255) (171/255) (100/255)]; % Luminance = 55%
param.color.blue = [(20/255) (145/255) (207/255)]; % Luminance = 55%
param.color.purple = [(175/255) (0/255) (234/255)]; % Luminance = 55%
param.color.orange = [1 (128/255) 0]; % Luminance = 65%
param.color.darkgrey = [(115/255) (115/255) (115/255)]; % Luminance = 45%

[param.screen.window, windowRect] = PsychImaging('OpenWindow', screenNumber, param.color.black); % Open an 'on screen' window and color it black
HideCursor;
[param.screen.Xpixels, param.screen.Ypixels] = Screen('WindowSize', param.screen.window); % Get the size of the 'on screen' window in pixels
[param.screen.xCenter, param.screen.yCenter] = RectCenter(windowRect); % Get the centre coordinate of the window in pixels
Screen('BlendFunction', param.screen.window, GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA); % Enable alpha blending for anti-aliasing
param.screen.monitorFlipInterval = Screen('GetFlipInterval', param.screen.window); % Query the frame duration
MaxPriority(param.screen.window); % Query the maximum priority level
rng('default'); % Reinitialize the random number generator to its startup configuration
rng('shuffle'); % Seed the random number generator.

% Screen-blink compatible timing
param.fun.secTiming = @(dt,ifi)(round(dt / ifi) - 0.5) * ifi; % dt = duration in secs

% Function to find which key was pressed
param.fun.whichKey = @(keyCode)(find(keyCode,1));

% Text options
Screen('TextFont', param.screen.window, 'Arial');
Screen('TextColor', param.screen.window, param.color.white);

% Generate values from the uniform distribution on the interval [a, b]:
% r = a + (b-a).*rand;

% Text display preferences
Screen('Preference', 'TextRenderer', 1);
param.default_textSize = round(param.screen.Xpixels*0.026); %50;
param.feed_textSize = round(param.screen.Xpixels*0.0365); %70; % Size of the feedback caption
param.small_textSize = round(param.screen.Xpixels*0.0156); %30;
format longG;

%% Photodiode

if flags.photodiode % If a photodiode is used
    
    param.photodpos = [50,-50,50,50]; % Position and width of the circle (posx, poxy, width, height)
    % Negative positions are reversed (e.g. P.photodpos = [50,-50,50,50] -> [50, screenheight - 50, 50,50];
    
    % Code from Nathan
    if param.photodpos(1) < 0
        param.photodpos(1) = param.screen.xCenter*2 + param.photodpos(1) - param.photodpos(3);
    end
    if param.photodpos(2) < 0
        param.photodpos(2) = param.screen.yCenter*2 + param.photodpos(2) - param.photodpos(4);
    end
    param.photodpos(3:4) = param.photodpos(1:2) + param.photodpos(3:4);
    
end

%% Responses settings

% devices = PsychHID('Devices'); % Don't work properly on windows so we use GetGamepadIndices
[gamepadIndices, productNames] = GetGamepadIndices;

if isempty(productNames) == 0 && strcmp(productNames(1), 'Logitech Dual Action') % Utilisation de la manette
    param.deviceNumber = gamepadIndices(1);
    
    % Key codes
    param.key.escape = 9;
    param.key.valid = 2;
    param.key.left = 5;
    param.key.right = 6;
    param.key.up = 5; %[5, 6];
    param.key.down = 6; %[7, 8];
    
else % Utilisation du clavier
    param.deviceNumber = GetKeyboardIndices;
    
    % Key codes
    param.key.escape = KbName('Escape');
    param.key.valid = KbName('Space');
    param.key.left = KbName('LeftArrow');
    param.key.right = KbName('RightArrow');
    param.key.up = KbName('UpArrow');
    param.key.down = KbName('DownArrow');
end

% Gamepad settings
% X = 1 ; A = 2 ; B = 3 ; Y = 4 ; LB = 5 ; RB = 6 ; LT = 7 ; RT = 8 ; Back = 9

%% Eye-tracker settings

if flags.eyetracker == 1 % Tobii
    
    eyetracker.op = EyeTrackingOperations();
    found_eyetrackers = eyetracker.op.find_all_eyetrackers();
    eyetracker.tobii = found_eyetrackers(1);
    
else % No eye-tracker
    
    eyetracker = [];
    
end

%% INSTRUCTIONS

param.instr.destRect = CenterRectOnPointd([0 0 param.screen.Xpixels*0.65 param.screen.Ypixels*0.86], param.screen.xCenter, param.screen.yCenter);

%% RATINGS

% Rating cursor
nb_sides = 3; % Number of sides for our polygon
angles_deg = linspace(-90, 270, nb_sides + 1); %linspace(0, 360, nb_sides + 1); % Angles at which our polygon vertices endpoints will be
param.rating.angles_rad_m = angles_deg * (pi / 180);
param.rating.radius_m = 30; % Size of the cursor

% RATING OPTIONS
param.rating.caption_Ypos = param.screen.Ypixels * 0.4; % Position en Y de la question
param.rating.scaleText_Ypos = param.screen.Ypixels * 0.6; % Position en Y du texte de la barre de notation
param.rating.pixelsPerPress = 8; % Amount of pixel we want our cursor to move on each button press
param.rating.YPosCursor = sin(param.rating.angles_rad_m) .* param.rating.radius_m + (param.screen.Ypixels * 0.68); % Position en Y du curseur
param.rating.minInitCursor = 25-1; % Note min � l'initialisation du curseur = 25
param.rating.maxInitCursor = 75-1; % Note max � l'initialisation du curseur = 75
param.rating.linePos = [param.screen.xCenter - (param.rating.pixelsPerPress * 99 / 2), param.screen.Ypixels * 0.65, param.screen.xCenter + (param.rating.pixelsPerPress * 99 / 2), param.screen.Ypixels * 0.65]; % Position of the rating bar : starting x and y positions, then ending x and y positions resectively
param.rating.litteLinePos = [param.rating.linePos(1) param.rating.linePos(1) param.rating.linePos(3) param.rating.linePos(3) ; (param.rating.linePos(2) + 10) (param.rating.linePos(2) - 10) (param.rating.linePos(4) + 10) (param.rating.linePos(4) - 10)]; % See Screen('DrawLines') for explanation -> line 1 = X coord ; line 2 = Y coord

% D�terminer la position min et max du curseur en X
param.rating.minXposVector = round(sin(linspace(0, 360, nb_sides + 1) * (pi / 180)) .* param.rating.radius_m + param.rating.linePos(1));
param.rating.maxXposVector = round(sin(linspace(0, 360, nb_sides + 1) * (pi / 180)) .* param.rating.radius_m + param.rating.linePos(3));

%% MOOD RATING

param.mood.caption = 'Comment vous sentez vous maintenant ?';
param.mood.left_scale_text = 'Mauvaise humeur';
param.mood.right_scale_text = 'Bonne humeur';

%% CONFIDENCE RATING

param.confidence.caption = 'Pensez-vous gagner ?';
param.confidence.left_scale_text = 'Pas s�r(e)';
param.confidence.right_scale_text = 'S�r(e)';

%% CHOICE TASK

% CHOICE TASK STIMULI

[aga, ~, ~] = imread(fullfile(runner.stimulidir, 'gain.png'));
param.choice.gain_img = Screen('MakeTexture', param.screen.window, aga);

[aga, ~, alpha] = imread(fullfile(runner.stimulidir, 'loss.png'));
aga(:, :, 4) = alpha;
param.choice.loss_img = Screen('MakeTexture', param.screen.window, aga);

param.choice.accept_caption = 'OUI';
param.choice.decline_caption = 'NON';

%-------------------------------------------------------------------------%

% CHOICE TASK OPTION

% Coordonn�es possibles des pi�ces � afficher
param.choice.top_locs = [ [ -320,-480 ]; [ -266,-473 ]; [ -187,-477 ]; [ -104,-478 ]; [ -52,-483 ]; [ 0,-465 ]; [ 47,-472 ]; [ 123,-476 ]; [ 190,-470 ]; [ 267,-485 ];...
    [ -324,-439 ]; [ -206,-425 ]; [ -120,-438 ]; [ -37,-443 ]; [ 57,-432 ]; [ 123,-437 ]; [ 181,-423 ]; [ 234,-440 ]; [ 275,-425 ]; [ 320,-416 ];...
    [ -304,-400 ]; [ -249,-410 ]; [ -165,-396 ]; [ -85,-400 ]; [ -12,-395 ]; [ 55,-390 ]; [ 117,-386 ]; [ 214,-390 ]; [ 273,-383 ]; [ 317,-375 ];...
    [ -312,-353 ]; [ -244,-367 ]; [ -168,-351 ]; [ -110,-359 ]; [ -48,-358 ]; [ 36,-350 ]; [ 100,-344 ]; [ 172,-358 ]; [ 276,-288 ]; [ 291,-330 ];...
    [ -319,-311 ]; [ -263,-329 ]; [ -105,-298 ]; [ -53,-313 ]; [ -8,-333 ]; [ 67,-303 ]; [ 136,-305 ]; [ 179,-313 ]; [ 230,-303 ]; [ 325,-292 ] ];
param.choice.top_locs(:,1) = (param.choice.top_locs(:,1)+960)/1920; % Pour pouvoir adapter � toutes les tailles d'�cran
param.choice.top_locs(:,2) = (param.choice.top_locs(:,2)+540)/1080;

param.choice.bottom_locs = [ [ -320,480 ]; [ -266,473 ]; [ -187,477 ]; [ -104,478 ]; [ -52,483 ]; [ 0,465 ]; [ 47,472 ]; [ 123,476 ]; [ 190,470 ]; [ 267,485 ];...
    [ -324,439 ]; [ -206,425 ]; [ -120,438 ]; [ -37,443 ]; [ 57,432 ]; [ 123,437 ]; [ 181,423 ]; [ 234,440 ]; [ 275,425 ]; [ 320,416 ];...
    [ -304,400 ]; [ -249,410 ]; [ -165,396 ]; [ -85,400 ]; [ -12,395 ]; [ 55,390 ]; [ 117,386 ]; [ 214,390 ]; [ 273,383 ]; [ 317,375 ];...
    [ -312,353 ]; [ -244,367 ]; [ -168,351 ]; [ -110,359 ]; [ -48,358 ]; [ 36,350 ]; [ 100,344 ]; [ 172,358 ]; [ 276,288 ]; [ 291,330 ];...
    [ -319,311 ]; [ -263,329 ]; [ -105,298 ]; [ -53,313 ]; [ -8,333 ]; [ 67,303 ]; [ 136,305 ]; [ 179,313 ]; [ 230,303 ]; [ 325,292 ] ];
param.choice.bottom_locs(:,1) = (param.choice.bottom_locs(:,1)+960)/1920; % Pour pouvoir adapter � toutes les tailles d'�cran
param.choice.bottom_locs(:,2) = (param.choice.bottom_locs(:,2)+540)/1080;

param.choice.coin_prop = param.screen.Xpixels*0.02; % Proportion de l'image r�elle � afficher

% Vecteur contenant tous les z-score par ordre de difficult� (de 1,0 � 5,0)
% Difficult� 1 = 75% ; 2 = 65% ; 3 = 55% ; 4 = 45% ; 5 = 35% de r�ussite (loi normale)
% Exemple : z_score[4] = 0.59 = z-score pour la difficult� 1,3
param.choice.z_score = [0.68, 0.65, 0.62, 0.59, 0.56, 0.53, 0.50, 0.47, 0.44, 0.42, 0.39, 0.36, 0.34, 0.31, 0.28, 0.26, 0.23, 0.21, 0.18, 0.16, 0.13, 0.11, 0.08, 0.06, 0.03, 0.00, -0.03, -0.06, -0.08, -0.11, -0.13, -0.16, -0.18, -0.21, -0.23, -0.26, -0.28, -0.31, -0.34, -0.36, -0.39];
param.choice.proba = 75:-1:35; % In percent

%-------------------------------------------------------------------------%

% Choice task timing
param.choice.no_choice_dur = 0.25; % Dur�e d'affichage des nouveaux enjeux lors d'un choix NON

%% CHALLENGE

param.challenge.gain_tot = 0; % Gain total (bas� sur la choice task) affich� � la fin d'un bloc

%-------------------------------------------------------------------------%

% Moving dot options
param.challenge.timeToCross = 1; % Temps mis par la balle pour arriver au centre de l'�cran
param.challenge.pixelToCross = 500; % Distance (en pixel) parcourue par la balle pour arriver au centre de l'�cran
param.challenge.velocity = param.challenge.pixelToCross / param.challenge.timeToCross;
param.challenge.dotStartPos = param.screen.xCenter - param.challenge.pixelToCross; % Position de d�part de la balle

param.challenge.dotSize = 20; % Taille de la balle

%-------------------------------------------------------------------------%

% Challenge timing
param.challenge.feedDur = 0.5; % Dur�e d'affichage du feedback apr�s le d�fi d'estimation

param.end.interTrial = 0.2; % Intervalle entre la fin de l'estimation et le d�but de l'essai suivant
param.end.dur = 10; % Dur�e du feedback � la fin d'une session

%% SPECIFIC OPTIONS
% Param�tres sp�cifiques � chaque sessions et/ou protocol

if ismember(participant.protocol, {'cognition','realtime'}) && participant.session == 0
    
    % Training
    
    %-PARTIE 1------------------------------------------------------------%
    
    % Load instructions image
    [aga, ~, ~] = imread(fullfile(runner.instructdir, 'Diapositive1.jpg'));
    param.training.part1.instr.slides(1) = Screen('MakeTexture', param.screen.window, aga);
    [aga, ~, ~] = imread(fullfile(runner.instructdir, 'Diapositive2.jpg'));
    param.training.part1.instr.slides(2) = Screen('MakeTexture', param.screen.window, aga);
    
    param.training.part1.moy_estim = 0.100; % Moyenne initiale de l'estimation
    param.training.part1.meanStep = 0.002; % Pas de difficult�
    
    param.training.part1.nb_trial_min = 30; % Nombre d'essais minimum
    param.training.part1.nb_trial_max = 80; % Nombre d'essais maximum
    param.training.part1.plateau = 5; % Nombre d'essais � consid�rer pour d�terminer si un plateau est atteint
    
    param.training.part1.feedDur = 0.3; % Dur�e d'affichage du feedback apr�s le d�fi d'estimation
    
    %-PARTIE 2------------------------------------------------------------%
    
    % Load instructions images AGENT
    [aga, ~, ~] = imread(fullfile(runner.instructdir, 'Diapositive3.jpg'));
    param.training.part2.agent.instr.slides(1) = Screen('MakeTexture', param.screen.window, aga);
    [aga, ~, ~] = imread(fullfile(runner.instructdir, 'Diapositive4.jpg'));
    param.training.part2.agent.instr.slides(2) = Screen('MakeTexture', param.screen.window, aga);
    [aga, ~, ~] = imread(fullfile(runner.instructdir, 'Diapositive5.jpg'));
    param.training.part2.agent.instr.slides(3) = Screen('MakeTexture', param.screen.window, aga);
    [aga, ~, ~] = imread(fullfile(runner.instructdir, 'Diapositive6.jpg'));
    param.training.part2.agent.instr.slides(4) = Screen('MakeTexture', param.screen.window, aga);
    [aga, ~, ~] = imread(fullfile(runner.instructdir, 'Diapositive7.jpg'));
    param.training.part2.agent.instr.slides(5) = Screen('MakeTexture', param.screen.window, aga);
    
    % Load instructions images NON AGENT
    [aga, ~, ~] = imread(fullfile(runner.instructdir, 'Diapositive8.jpg'));
    param.training.part2.no_agent.instr.slides(1) = Screen('MakeTexture', param.screen.window, aga);
    
    % Load instructions images MIX AGENT/NON AGENT
    [aga, ~, ~] = imread(fullfile(runner.instructdir, 'Diapositive9.jpg'));
    param.training.part2.mix.instr.slides(1) = Screen('MakeTexture', param.screen.window, aga);
    
    param.training.part2.agentivity = {'agent','no_agent','mix'};
    
    for i = 1:length(param.training.part2.agentivity)
        
        if ~ismember(param.training.part2.agentivity(i),'mix') % Agent ou Non agent
            nb_trial = 4; % Multiple de 2
            diff_gain_loss = [randsample(4,nb_trial,'true'), randsample(4,nb_trial,'true'), randsample(4,nb_trial,'true')];
        else % Les deux
            % Toutes les combinaisons possibles diff + gain + perte (= 64 essais)
            diff_gain_loss = [];
            diff_gain_loss(:,1) = [1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;2;2;2;2;2;2;2;2;2;2;2;2;2;2;2;2;3;3;3;3;3;3;3;3;3;3;3;3;3;3;3;3;4;4;4;4;4;4;4;4;4;4;4;4;4;4;4;4]; % Colonne 1 = Trial difficulty
            diff_gain_loss(:,2) = [1;1;1;1;2;2;2;2;3;3;3;3;4;4;4;4;1;1;1;1;2;2;2;2;3;3;3;3;4;4;4;4;1;1;1;1;2;2;2;2;3;3;3;3;4;4;4;4;1;1;1;1;2;2;2;2;3;3;3;3;4;4;4;4]; % Colonne 2 = Gain offer
            diff_gain_loss(:,3) = [1;2;3;4;1;2;3;4;1;2;3;4;1;2;3;4;1;2;3;4;1;2;3;4;1;2;3;4;1;2;3;4;1;2;3;4;1;2;3;4;1;2;3;4;1;2;3;4;1;2;3;4;1;2;3;4;1;2;3;4;1;2;3;4]; % Colonne 3 = Loss offer
        end
        
        param.training.part2.(param.training.part2.agentivity{i}).nb_trial = length(diff_gain_loss);
        
        stakesPart2 = selectStakes(diff_gain_loss, 'level');
        levelPart2 = round(length(stakesPart2)/4); % Pr�senter les essais par blocs de difficult� croissante (4 blocs)
        param.training.part2.(param.training.part2.agentivity{i}).choice.stakes = [Shuffle(stakesPart2(1:levelPart2,:),2); Shuffle(stakesPart2(levelPart2+1:levelPart2*2,:),2); Shuffle(stakesPart2(2*levelPart2+1:levelPart2*3,:),2); Shuffle(stakesPart2(3*levelPart2+1:levelPart2*4,:),2)]; % Valeurs et position des combinaisons des 3 param�tres (diff, gain, perte)
        param.training.part2.(param.training.part2.agentivity{i}).choice.intervert_gain_loss = Shuffle([zeros(1,param.training.part2.(param.training.part2.agentivity{i}).nb_trial/2),ones(1,param.training.part2.(param.training.part2.agentivity{i}).nb_trial/2)]); % Rend al�atoire la position des gains et des pertes
        param.training.part2.(param.training.part2.agentivity{i}).choice.intervert_y_n = Shuffle([zeros(1,param.training.part2.(param.training.part2.agentivity{i}).nb_trial/2),ones(1,param.training.part2.(param.training.part2.agentivity{i}).nb_trial/2)]); % Rend al�atoire la position du OUI/NON
        
        if ismember(param.training.part2.agentivity(i),'agent') % Agent
            param.training.part2.(param.training.part2.agentivity{i}).choice.agentivity = ones(param.training.part2.(param.training.part2.agentivity{i}).nb_trial,1); % 0 = Lottery game ; 1 = Precision task
        elseif ismember(param.training.part2.agentivity(i),'no_agent') % Non agent
            param.training.part2.(param.training.part2.agentivity{i}).choice.agentivity = zeros(param.training.part2.(param.training.part2.agentivity{i}).nb_trial,1); % 0 = Lottery game ; 1 = Precision task
        else % Les deux
            param.training.part2.(param.training.part2.agentivity{i}).choice.agentivity = Shuffle([zeros(param.training.part2.(param.training.part2.agentivity{i}).nb_trial/2,1); ones(param.training.part2.(param.training.part2.agentivity{i}).nb_trial/2,1)]);
        end
        
    end
    
    % REAL TIME PARAMETERS
    if strcmp(participant.protocol, 'realtime')
        param.cfg = [];
        param.cfg.channel = participant.elec; % Listened channel
        param.cfg.foilim = 50:10:150; % Analyzed frequencies
        param.cfg.blocksize = 30; % Size of the analyzed window in sec
        param.cfg.smoothing = 0;
    end
    
    %-PARTIE 2 BIS--------------------------------------------------------%
    
    diff_gain_loss = [];
    diff_gain_loss(:,1) = [2;2;1;4]; % Colonne 1 = Trial difficulty
    diff_gain_loss(:,2) = [4;1;2;2]; % Colonne 2 = Gain offer
    diff_gain_loss(:,3) = [1;4;2;2]; % Colonne 3 = Loss offer
    
    param.training.part2.bis.nb_trial = length(diff_gain_loss);
    param.training.part2.bis.choice.stakes = Shuffle(selectStakes(diff_gain_loss, 'level'),2); % Valeurs et position des combinaisons des 3 param�tres (diff, gain, perte)
    param.training.part2.bis.choice.intervert_gain_loss = Shuffle([zeros(1,param.training.part2.bis.nb_trial/2),ones(1,param.training.part2.bis.nb_trial/2)]); % Rend al�atoire la position des gains et des pertes
    param.training.part2.bis.choice.intervert_y_n = Shuffle([zeros(1,param.training.part2.bis.nb_trial/2),ones(1,param.training.part2.bis.nb_trial/2)]); % Rend al�atoire la position du OUI/NON
    param.training.part2.bis.choice.agentivity = Shuffle([zeros(param.training.part2.bis.nb_trial/2,1); ones(param.training.part2.bis.nb_trial/2,1)]); % 0 = Lottery game ; 1 = Precision task
    param.training.part2.bis.max_repet = 5; % Maximum number of repetitions of part 2 bis
    
    %-PARTIE 3------------------------------------------------------------%
    
    % Load instructions partie 3
    [aga, ~, ~] = imread(fullfile(runner.instructdir, 'Diapositive10.jpg'));
    param.training.part3.instr.slides(1) = Screen('MakeTexture', param.screen.window, aga);
    [aga, ~, ~] = imread(fullfile(runner.instructdir, 'Diapositive11.jpg'));
    param.training.part3.instr.slides(2) = Screen('MakeTexture', param.screen.window, aga);
    [aga, ~, ~] = imread(fullfile(runner.instructdir, 'Diapositive12.jpg'));
    param.training.part3.instr.slides(3) = Screen('MakeTexture', param.screen.window, aga);
    
    param.training.part3.nb_trial = 8; % Multiple de 4
    
    % CHOICE TASK
    diff_gain_loss = [randsample(4,param.training.part3.nb_trial,'true'), randsample(4,param.training.part3.nb_trial,'true'), randsample(4,param.training.part3.nb_trial,'true')];
    param.training.part3.choice.stakes = Shuffle(selectStakes(diff_gain_loss, 'level'),2); % Valeurs et position des combinaisons des 3 param�tres (diff, gain, perte)
    param.training.part3.choice.agentivity = Shuffle([zeros(param.training.part3.nb_trial/2,1); ones(param.training.part3.nb_trial/2,1)]); % 0 = Lottery game ; 1 = Precision task
    param.training.part3.choice.intervert_gain_loss = Shuffle([zeros(1,param.training.part3.nb_trial/2),ones(1,param.training.part3.nb_trial/2)]); % Rend al�atoire la position des gains et des pertes
    param.training.part3.choice.intervert_y_n = Shuffle([zeros(1,param.training.part3.nb_trial/2),ones(1,param.training.part3.nb_trial/2)]); % Rend al�atoire la position du OUI/NON
    
    % MOOD RATING
    param.mood.prop = 4; % 1 rating par bloc de 4 essais
    param.mood.min_interv = 2; % Intervalle minimum entre 2 ratings
    param.mood.rating_idx = ratingLoc(param.training.part3.nb_trial, param.mood.prop, param.mood.min_interv, param.training.part3.choice.agentivity); % D�finir l'emplacement des mood rating
    
elseif strcmp(participant.protocol, 'cognition')
    
    if participant.session ~= 0 % ConfiDecid experiment
        
        % Load instructions images
        [aga, ~, ~] = imread(fullfile(runner.instructdir, 'Diapositive13.jpg'));
        param.instr.slides(1) = Screen('MakeTexture', param.screen.window, aga);
        
        % CHOICE TASK
        
        % R�cup�rer la moyenne et l'�cart type du d�fi (on a v�rifi� que le fichier existait dans Decid_Runner)
        estimParam = fullfile(participant.dataFolder, [participant.identifier, '_estim_param.mat']); % Fichier .mat contenant la moyenne et l'�cart type du participant enregistr� � la fin de l'entrainement
        load(estimParam, 'moy_estim', 'std_estim'); % On charge les variables d'int�r�t
        param.choice.moy_estim = moy_estim;
        param.choice.std_estim = std_estim;
        
        % Vecteurs de toutes les combi possibles pour TD = 2 et 3 (= 32 essais)
        constant_combi(:,1) = [2;2;2;2;2;2;2;2;2;2;2;2;2;2;2;2;3;3;3;3;3;3;3;3;3;3;3;3;3;3;3;3]; % Colonne 1 = Trial difficulty
        constant_combi(:,2) = [1;1;1;1;2;2;2;2;3;3;3;3;4;4;4;4;1;1;1;1;2;2;2;2;3;3;3;3;4;4;4;4]; % Colonne 2 = Gain offer
        constant_combi(:,3) = [1;2;3;4;1;2;3;4;1;2;3;4;1;2;3;4;1;2;3;4;1;2;3;4;1;2;3;4;1;2;3;4]; % Colonne 3 = Loss offer
        
        % Fa�on de compl�ter les 32 essais restants :
        % Cas 1 : TD(1/2) = Diag1 ; TD(3/4) = Diag2
        cas(:,1,1) = [1;1;1;1;1;1;1;1;2;2;2;2;2;2;2;2;3;3;3;3;3;3;3;3;4;4;4;4;4;4;4;4]; % Colonne 1 = Trial difficulty
        cas(:,2,1) = [1;2;3;4;1;2;3;1;1;2;3;4;1;2;3;1;4;2;3;4;1;2;3;4;4;2;3;4;1;2;3;4]; % Colonne 2 = Gain offer
        cas(:,3,1) = [1;1;1;1;2;2;2;3;1;1;1;1;2;2;2;3;2;3;3;3;4;4;4;4;2;3;3;3;4;4;4;4]; % Colonne 3 = Loss offer
        
        % Cas 2 : TD(1/2) = Diag2 ; TD(3/4) = Diag1
        cas(:,1,2) = [1;1;1;1;1;1;1;1;2;2;2;2;2;2;2;2;3;3;3;3;3;3;3;3;4;4;4;4;4;4;4;4]; % Colonne 1 = Trial difficulty
        cas(:,2,2) = [4;2;3;4;1;2;3;4;4;2;3;4;1;2;3;4;1;2;3;4;1;2;3;1;1;2;3;4;1;2;3;1]; % Colonne 2 = Gain offer
        cas(:,3,2) = [2;3;3;3;4;4;4;4;2;3;3;3;4;4;4;4;1;1;1;1;2;2;2;3;1;1;1;1;2;2;2;3]; % Colonne 3 = Loss offer
        
        %---------------------------------------------------------------------%
        
        if participant.trials == 128 % LFP experiment
        
        lastCombiDim = fullfile(runner.resultdir, 'last_combi_dim.mat'); % Fichier contenant le dernier cas utilis�
        
        if exist(lastCombiDim, 'file') % Le fichier existe
            load(lastCombiDim, 'diff_gain_loss');
            
            if isequal(diff_gain_loss,[constant_combi;cas(:,:,1)]) % Dernier cas utilis� = cas 1
                diff_gain_loss = [constant_combi;cas(:,:,2)];
                save(lastCombiDim, 'diff_gain_loss');
            else % Dernier cas utilis� = cas 2
                diff_gain_loss = [constant_combi;cas(:,:,1)];
                save(lastCombiDim, 'diff_gain_loss');
            end
            
        else % le fichier n'existe pas
            
            casIdx = randsample([1,2],1);
            diff_gain_loss = [constant_combi;cas(:,:,casIdx)];
            save(lastCombiDim, 'diff_gain_loss');
            
        end
        
        elseif participant.trials == 64 % TOC EEG experiment
            
            CombiDim = fullfile(runner.resultdir, 'combi_dim.mat'); % Fichier contenant l'ordre des cas � utiliser
            
            if participant.session == 1 % Premi�re session
                
                % Toutes les combinaisons possibles se trouvent dans les 2
                % premi�res sessions. Les sessions 3 et 4 sont en "bonus"
                c = randperm(2);
                sessCombi(:,:,1) = cas(:,:,c(1));
                sessCombi(:,:,2) = cas(:,:,c(2));
                sessCombi(:,:,3:4) = repmat(constant_combi,1,1,2);
                save(CombiDim, 'sessCombi');
                
            else
                load(CombiDim, 'sessCombi');
            end
            
            whichSess = participant.session - floor((participant.session-1)/4)*4;
            diff_gain_loss = sessCombi(:,:,whichSess);
            
        end
        
        %---------------------------------------------------------------------%
        
        param.choice.diff_gain_loss = diff_gain_loss;
        
        param.nb_trial = length(diff_gain_loss)*2; % Number of trials (= m�me nb d'essais pour les essais agent et non agent)
        
        % Define trials order and add agentivity
        % Agent and non agent trials are exactly the same
        diff_gain_loss_agent = [repmat(selectStakes(diff_gain_loss, 'level'),2,1), [zeros(param.nb_trial/2,1); ones(param.nb_trial/2,1)]]; % Colonne 1:3 = diff_gain_loss ; Colonne 4 = Agentivit� (0 = Lottery game ; 1 = Precision task)
        diff_gain_loss_agent = Shuffle(diff_gain_loss_agent,2); % Rend al�atoire la position des combinaisons des 3 param�tres (diff, gain, perte) et de l'agentivit�
        
        param.choice.agentivity = diff_gain_loss_agent(:,4); % 0 = Lottery game ; 1 = Precision task
        param.choice.stakes = diff_gain_loss_agent(:,1:3); % Valeur et position des combinaisons des 3 param�tres (diff, gain, perte)
        
        param.choice.intervert_gain_loss = Shuffle([zeros(1,param.nb_trial/2),ones(1,param.nb_trial/2)]); % Rend al�atoire la position des gains et des pertes
        param.choice.intervert_y_n = Shuffle([zeros(1,param.nb_trial/2),ones(1,param.nb_trial/2)]); % Rend al�atoire la position du OUI/NON
        
        % MOOD RATING
        param.mood.prop = 4; % 1 rating par bloc de 4 essais
        param.mood.min_interv = 2; % Intervalle minimum entre 2 ratings
        param.mood.rating_idx = ratingLoc(param.nb_trial, param.mood.prop, param.mood.min_interv, param.choice.agentivity); % D�finir l'emplacement des mood rating
        
    end
    
elseif strcmp(participant.protocol, 'realtime')
    
    if participant.session ~= 0 % ConfiDecid realtime experiment
        
        % Load instructions images
        [aga, ~, ~] = imread(fullfile(runner.instructdir, 'Diapositive13.jpg'));
        param.instr.slides(1) = Screen('MakeTexture', param.screen.window, aga);
        
        % CHOICE TASK
        
        % R�cup�rer la moyenne et l'�cart type du d�fi (on a v�rifi� que le fichier existait dans Decid_Runner)
        estimParam = fullfile(participant.dataFolder, [participant.identifier, '_estim_param.mat']); % Fichier .mat contenant la moyenne et l'�cart type du participant enregistr� � la fin de l'entrainement
        load(estimParam, 'moy_estim', 'std_estim'); % On charge les variables d'int�r�t
        param.choice.moy_estim = moy_estim;
        param.choice.std_estim = std_estim;
        
        % Combinaisons possibles (32 essais) :
        % Cas 1 : Vecteurs de toutes les combi possibles pour TD = 2 et 3
        cas(:,1,1) = [2;2;2;2;2;2;2;2;2;2;2;2;2;2;2;2;3;3;3;3;3;3;3;3;3;3;3;3;3;3;3;3]; % Colonne 1 = Trial difficulty
        cas(:,2,1) = [1;1;1;1;2;2;2;2;3;3;3;3;4;4;4;4;1;1;1;1;2;2;2;2;3;3;3;3;4;4;4;4]; % Colonne 2 = Gain offer
        cas(:,3,1) = [1;2;3;4;1;2;3;4;1;2;3;4;1;2;3;4;1;2;3;4;1;2;3;4;1;2;3;4;1;2;3;4]; % Colonne 3 = Loss offer
        
        % Cas 1 : TD(1/2) = Diag1 ; TD(3/4) = Diag2
        cas(:,1,2) = [1;1;1;1;1;1;1;1;2;2;2;2;2;2;2;2;3;3;3;3;3;3;3;3;4;4;4;4;4;4;4;4]; % Colonne 1 = Trial difficulty
        cas(:,2,2) = [1;2;3;4;1;2;3;1;1;2;3;4;1;2;3;1;4;2;3;4;1;2;3;4;4;2;3;4;1;2;3;4]; % Colonne 2 = Gain offer
        cas(:,3,2) = [1;1;1;1;2;2;2;3;1;1;1;1;2;2;2;3;2;3;3;3;4;4;4;4;2;3;3;3;4;4;4;4]; % Colonne 3 = Loss offer
        
        % Cas 2 : TD(1/2) = Diag2 ; TD(3/4) = Diag1
        cas(:,1,3) = [1;1;1;1;1;1;1;1;2;2;2;2;2;2;2;2;3;3;3;3;3;3;3;3;4;4;4;4;4;4;4;4]; % Colonne 1 = Trial difficulty
        cas(:,2,3) = [4;2;3;4;1;2;3;4;4;2;3;4;1;2;3;4;1;2;3;4;1;2;3;1;1;2;3;4;1;2;3;1]; % Colonne 2 = Gain offer
        cas(:,3,3) = [2;3;3;3;4;4;4;4;2;3;3;3;4;4;4;4;1;1;1;1;2;2;2;3;1;1;1;1;2;2;2;3]; % Colonne 3 = Loss offer
        
        %---------------------------------------------------------------------%
        
        if participant.session == 1 % Premi�re session
            
            diff_gain_loss = cas(:,:,1); % On pr�sente le cas 1
            
        else % Autres sessions
            
            lastCombiDim = fullfile(runner.resultdir, 'last_combi_dim_realtime.mat'); % Fichier contenant le dernier cas utilis�
            
            if exist(lastCombiDim, 'file') % Le fichier existe
                load(lastCombiDim, 'diff_gain_loss');
                
                if isequal(diff_gain_loss, cas(:,:,2)) % Dernier cas utilis� = cas 2
                    diff_gain_loss = cas(:,:,3); % On pr�sente le cas 3
                    save(lastCombiDim, 'diff_gain_loss');
                elseif isequal(diff_gain_loss, cas(:,:,3)) % Dernier cas utilis� = cas 3
                    diff_gain_loss = cas(:,:,2); % On pr�sente le cas 2
                    save(lastCombiDim, 'diff_gain_loss');
                end
                
            else % le fichier n'existe pas
                
                casIdx = randsample([2,3],1);
                diff_gain_loss = cas(:,:,casIdx);
                save(lastCombiDim, 'diff_gain_loss');
                
            end
        end
        %---------------------------------------------------------------------%
        
        param.choice.diff_gain_loss = diff_gain_loss;
        
        % D�finir la configuration pour 1 bloc
        stakes = selectStakes(diff_gain_loss, 'level'); % Choisir les gains/pertes/difficult�s � pr�senter
        intervert_gain_loss = Shuffle([zeros(1,length(diff_gain_loss)/2),ones(1,length(diff_gain_loss)/2)])'; % Rend al�atoire la position des gains et des pertes
        intervert_y_n = Shuffle([zeros(1,length(diff_gain_loss)/2),ones(1,length(diff_gain_loss)/2)])'; % Rend al�atoire la position du OUI/NON
        
        for i = 1:length(diff_gain_loss)
            defined_top_locs(i,:) = {Shuffle(param.choice.top_locs,2)}; % Rend al�atoire la position des pi�ces du haut
            defined_bottom_locs(i,:) = {Shuffle(param.choice.bottom_locs,2)}; % Rend al�atoire la position des pi�ces du bas
        end
        
        % Ajouter l'agentivit� et up/down : On r�p�te le bloc de 32 essais 4 fois
        % pour avoir exactement les m�mes essais dans les 4 conditions :
        %   - agent up
        %   - agent down
        %   - non agent up
        %   - non agent down
        
        all_predefined_param = [num2cell(stakes), num2cell(intervert_gain_loss), num2cell(intervert_y_n), defined_top_locs, defined_bottom_locs];
        % Colonnes 1:3 = stakes ; 4 = intervert_gain_loss ; 5 = intervert_y_n ; 6 = defined_top_locs ; 7 = defined_bottom_locs
        
        % Colonne 8 = agentivit�
        all_predefined_param_agent = [repmat(all_predefined_param,2,1), [num2cell(zeros(length(all_predefined_param),1)); num2cell(ones(length(all_predefined_param),1))]];
        
        % Colonne 9 = up/down
        all_predefined_param_agent_updown = [repmat(all_predefined_param_agent,2,1), [num2cell(zeros(length(all_predefined_param_agent),1)); num2cell(ones(length(all_predefined_param_agent),1))]];
        all_predefined_param_agent_updown = Shuffle(all_predefined_param_agent_updown,2); % Rend al�atoire la position des combinaisons des param�tres
        
        % Put everything in the "param" structure
        param.choice.stakes = cell2mat(all_predefined_param_agent_updown(:,1:3)); % Valeur et position des combinaisons des 3 param�tres (diff, gain, perte)
        param.choice.intervert_gain_loss = cell2mat(all_predefined_param_agent_updown(:,4)); % Position des gains et des pertes
        param.choice.intervert_y_n = cell2mat(all_predefined_param_agent_updown(:,5)); % Position du OUI/NON
        param.choice.defined_top_locs = cell2mat(permute(all_predefined_param_agent_updown(:,6),[3 2 1]));
        param.choice.defined_bottom_locs = cell2mat(permute(all_predefined_param_agent_updown(:,7),[3 2 1]));
        param.choice.agentivity = cell2mat(all_predefined_param_agent_updown(:,8)); % 0 = Lottery game ; 1 = Precision task
        param.choice.updown = cell2mat(all_predefined_param_agent_updown(:,9)); % 0 = Down ; 1 = Up
        
        param.nb_trial = length(all_predefined_param_agent_updown); % Number of trials (= essais identiques pour agent/non agent et up/down)
        
        %---------------------------------------------------------------------%
        
        % MOOD RATING
        param.mood.prop = 4; % 1 rating par bloc de 4 essais
        param.mood.min_interv = 2; % Intervalle minimum entre 2 ratings
        param.mood.rating_idx = ratingLoc(param.nb_trial, param.mood.prop, param.mood.min_interv, param.choice.agentivity); % D�finir l'emplacement des mood rating
        
        %---------------------------------------------------------------------%
        
        % REAL TIME PARAMETERS
        param.cfg = [];
        param.cfg.channel = participant.elec; % Listened channel
        param.cfg.foilim = 50:10:150; % Analyzed frequencies
        param.cfg.blocksize = 10; % Size of the analyzed window in sec
        param.cfg.slidingw = 0.5; % Size of the sliding window in sec (Can't go under SampleBlockSize (?) = 32 samples transmitted at a time = 32/fsample sec)
        param.cfg.smoothing = 0;
        
        % R�cup�rer les seuils haut et bas pour l'�lectrode s�lectionn�e
        thresholdParam = fullfile(participant.dataFolder, sprintf('%s_f%df%d_threshold.mat', participant.identifier, param.cfg.foilim(1), param.cfg.foilim(end))); % Fichier .mat contenant les seuils du participant enregistr�s � la fin de l'entrainement
        load(thresholdParam, 'threshold_up', 'threshold_down'); % On charge les variables d'int�r�t
        param.cfg.threshold_up = threshold_up.(sprintf('ch%s',participant.elec));
        param.cfg.threshold_down = threshold_down.(sprintf('ch%s',participant.elec));
        
    end
    
elseif strcmp(participant.protocol, 'stim')
    
    % R�cup�rer la moyenne et l'�cart type du d�fi (cr�� lors du protocole "cognition")
    estimParam = fullfile(participant.dataFolder, [participant.identifier, '_estim_param.mat']); % Fichier .mat contenant la moyenne et l'�cart type du participant enregistr� � la fin de l'entrainement
    load(estimParam, 'moy_estim', 'std_estim'); % On charge les variables d'int�r�t
    param.choice.moy_estim = moy_estim;
    param.choice.std_estim = std_estim;
    
    %---------------------------------------------------------------------%
    
    if ~isfield(participant, 'elec') % Training
        
        %-PARTIE 1--------------------------------------------------------%
        
        % Load instructions image
        [aga, ~, ~] = imread(fullfile(runner.instructdir, 'Diapositive1.jpg'));
        param.training.part1.instr.slides(1) = Screen('MakeTexture', param.screen.window, aga);
        [aga, ~, ~] = imread(fullfile(runner.instructdir, 'Diapositive2.jpg'));
        param.training.part1.instr.slides(2) = Screen('MakeTexture', param.screen.window, aga);
        
        param.training.part1.moy_estim = moy_estim; % Moyenne initiale de l'estimation
        param.training.part1.meanStep = 0.002; % Pas de difficult�
        
        param.training.part1.nb_trial_min = 7; % Nombre d'essais minimum
        param.training.part1.nb_trial_max = 30; % Nombre d'essais maximum
        param.training.part1.plateau = 5; % Nombre d'essais � consid�rer pour d�terminer si un plateau est atteint
        
        param.training.part1.feedDur = 0.3; % Dur�e d'affichage du feedback apr�s le d�fi d'estimation
        
        %-PARTIE 2--------------------------------------------------------%
        
        % Load instructions partie 2
        [aga, ~, ~] = imread(fullfile(runner.instructdir, 'Diapositive10.jpg'));
        param.training.part2.instr.slides(1) = Screen('MakeTexture', param.screen.window, aga);
        [aga, ~, ~] = imread(fullfile(runner.instructdir, 'Diapositive11.jpg'));
        param.training.part2.instr.slides(2) = Screen('MakeTexture', param.screen.window, aga);
        [aga, ~, ~] = imread(fullfile(runner.instructdir, 'Diapositive12.jpg'));
        param.training.part2.instr.slides(3) = Screen('MakeTexture', param.screen.window, aga);
        
        param.training.part2.nb_trial = 8; % Multiple de 4
        
        % CHOICE TASK
        diff_gain_loss = [randsample(4,param.training.part2.nb_trial,'true'), randsample(4,param.training.part2.nb_trial,'true'), randsample(4,param.training.part2.nb_trial,'true')];
        param.training.part2.choice.stakes = Shuffle(selectStakes(diff_gain_loss, 'level'),2); % Valeurs et position des combinaisons des 3 param�tres (diff, gain, perte)
        param.training.part2.choice.agentivity = ones(param.training.part2.nb_trial,1); % 1 = Precision task
        param.training.part2.choice.intervert_gain_loss = Shuffle([zeros(1,param.training.part2.nb_trial/2),ones(1,param.training.part2.nb_trial/2)]); % Rend al�atoire la position des gains et des pertes
        param.training.part2.choice.intervert_y_n = Shuffle([zeros(1,param.training.part2.nb_trial/2),ones(1,param.training.part2.nb_trial/2)]); % Rend al�atoire la position du OUI/NON
        
    else % Stim task
        
        % Load instructions images
        [aga, ~, ~] = imread(fullfile(runner.instructdir, 'Diapositive13.jpg'));
        param.instr.slides(1) = Screen('MakeTexture', param.screen.window, aga);
        
        % CHOICE TASK
        
        % D�finir les enjeux, la position des essais et la position des pi�ces � afficher pour chaque essai
        dv = [[-40, -30]; [-30, -20]; [-20, -10]; repmat([[-10, -5]; [-5, 0]; [0, 5]; [5, 10]],2,1); [10, 20]; [20, 30]; [30, 40]]; % dv = gain - loss
        diff_gain_loss = selectStakes(dv, 'dv'); % Colonne 1:3 = diff_gain_loss
        
        % Stimulation tous les essais pairs ou impairs + ordre des choix al�atoire
        stimPosfile = fullfile(participant.dataFolder, sprintf('%s_stim_pos.mat',participant.identifier)); % Fichier .mat contenant la position des stim de la session pr�c�dente (impaire = 1, paire = 2)
        if exist(stimPosfile, 'file')
            load(stimPosfile,'stimPos');
        else
            stimPos = randi([1,2]);
        end
        
        stimPos = abs(floor(stimPos*0.5)-2); % Transforme les 1 en 2 et les 2 en 1
        % Ainsi, si session pr�c�dente : stim en position impaire (1) -> session actuelle : stim en position paire (2), et inversement
        save(stimPosfile,'stimPos');
        
        % On affiche exactement les m�mes essais en stim et non stim dans un ordre al�atoire
        param.choice.stakes = NaN(length(diff_gain_loss)*2,3);
        param.choice.stim = NaN(length(diff_gain_loss)*2,1);
        param.choice.defined_top_locs = NaN(length(param.choice.top_locs),2,length(diff_gain_loss)*2);
        param.choice.defined_bottom_locs = NaN(length(param.choice.bottom_locs),2,length(diff_gain_loss)*2);
        param.choice.intervert_gain_loss = NaN(1,length(diff_gain_loss)*2);
        param.choice.intervert_y_n = NaN(1,length(diff_gain_loss)*2);
        
        for i = 1:length(diff_gain_loss)
            locs.top(:,:,i) = Shuffle(param.choice.top_locs,2); % Rend al�atoire la position des pi�ces du haut
            locs.bottom(:,:,i) = Shuffle(param.choice.bottom_locs,2); % Rend al�atoire la position des pi�ces du bas
        end
        
        intervert.gain_loss = Shuffle([zeros(1,length(diff_gain_loss)/2),ones(1,length(diff_gain_loss)/2)]); % Rend al�atoire la position des gains et des pertes
        intervert.y_n = Shuffle([zeros(1,length(diff_gain_loss)/2),ones(1,length(diff_gain_loss)/2)]); % Rend al�atoire la position du OUI/NON
        
        for pos = 1:2 % Boucle � travers les deux conditions stim et non stim
            
            idx = Shuffle(1:length(diff_gain_loss)); % D�fini l'ordre des essais
            param.choice.stakes(pos:2:end,:) = diff_gain_loss(idx,:);
            if pos == stimPos
                param.choice.stim(pos:2:end,:) = 1;
            else
                param.choice.stim(pos:2:end,:) = 0;
            end
            param.choice.defined_top_locs(:,:,pos:2:end) = locs.top(:,:,idx); % D�fini la position des pi�ces en haut
            param.choice.defined_bottom_locs(:,:,pos:2:end) = locs.bottom(:,:,idx); % D�fini la position des pi�ces en bas
            param.choice.intervert_gain_loss(pos:2:end) = intervert.gain_loss(idx);
            param.choice.intervert_y_n(pos:2:end) = intervert.y_n(idx);
            
        end
        %-----------------------------------------------------------------%
        
        param.nb_trial = length(param.choice.stakes); % Same dv for stim and no stim trials
        param.choice.agentivity = ones(1,param.nb_trial);
        
    end
end

end % Fin de la fonction "task_parameters"

%% EMPLACEMENT DES MOOD RATINGS
% 1 mood rating par bloc de 4 essais avec un espacement minimum de 2 essais entre les ratings

function rating_idx = ratingLoc(nb_trial, prop, min_interv, agentivity)

% INPUT
%   - nb_trial = nombre d'essais
%   - prop = proportions de ratings
%   - min_interv = intervalle miminum entre les ratings

nb_rating = nb_trial/prop;
rating_idx = NaN(1,nb_rating);

if nb_rating ~= floor(nb_rating) % The number of rating is not an integer
end

rating_idx(1) = randsample(1:prop,1);

while ~isempty(find(isnan(rating_idx) == 1, 1)) || length(find(agentivity(rating_idx) == 1)) ~= length(find(agentivity(rating_idx) == 0)) % As long as there is not the same number of ratings under both conditions of agentivity
    
    for k = 2:nb_rating % Loop over rating index
        
        rating_idx(k) = rating_idx(k-1);
        
        while rating_idx(k) - rating_idx(k-1) <= min_interv
            rating_idx(k) = (k-1)*prop + randsample(1:prop,1);
        end
        
    end % End of the loop over rating index
end % End of the while

end

%% SELECTIONNER LES ENJEUX
% D�finir l'offre de difficult�, gain et perte � afficher � chaque essai

function stakes = selectStakes(data, option)

stakes = NaN(length(data),3);

if strcmp(option, 'level') % data = nb_trial*3 matrix (diff, gain, loss)
    
    % Echantillonnage continu de la difficult�, des gains et des pertes :
    % 1 = [10:19] ; 2 = [20:29] ; 3 = [30:39] ; 4 = [40:49]
    for idx = 1:length(data)
        stakes(idx,1) = randsample(data(idx,1)*10:data(idx,1)*10+9,1); % Colonne 1 = Trial difficulty
        stakes(idx,2) = randsample(data(idx,2)*10:data(idx,2)*10+9,1); % Colonne 2 = Gain offer
        stakes(idx,3) = randsample(data(idx,3)*10:data(idx,3)*10+9,1); % Colonne 3 = Loss offer
    end
    
elseif strcmp(option, 'dv') % data = nb_trial*2 matrix of dv(gain-loss) (lim_inf, lim_sup)
    % length(data) doit �tre >= 4
    
    % D�finir l'offre de gain et perte � afficher en fonction du delta value (dv)
    gain = NaN(1,length(data));
    loss = gain;
    uniformlevel = false;
    
    while uniformlevel == false
        
        for idx = 1:length(data)
            dv = randsample(data(idx,1):data(idx,2)-1,1); % lim_inf <= dv < lim_sup
            gain(idx) = randi([10,49],1); % Tirage au sort du gain entre 10 (minimum level 1) et 49 (maximum level 4)
            loss(idx) = gain(idx) - dv;
            
            while loss(idx) < 10 || loss(idx) >= 50 % Les gains/pertes ne peuvent pas �tre < � 10 ou >= � 50
                dv = randsample(data(idx,1):data(idx,2)-1,1);
                gain(idx) = randi([10,49],1);
                loss(idx) = gain(idx) - dv;
            end
        end
        
        % On veut � peu pr�s autant de gains et de pertes dans chaque intervalle ([10-19], [20-29], etc...)
        if length(find(10 <= gain & gain < 20)) >= floor(length(data)/4) && length(find(20 <= gain & gain < 30)) >= floor(length(data)/4) && length(find(30 <= gain & gain < 40)) >= floor(length(data)/4) && length(find(40 <= gain & gain < 50)) >= floor(length(data)/4) &&...
                length(find(10 <= loss & loss < 20)) >= floor(length(data)/4) && length(find(20 <= loss & loss < 30)) >= floor(length(data)/4) && length(find(30 <= loss & loss < 40)) >= floor(length(data)/4) && length(find(40 <= loss & loss < 50)) >= floor(length(data)/4)
            uniformlevel = true;
        end
    end
    
    stakes(:,2) = gain;
    stakes(:,3) = loss;
    
end

end

%% Envoi des triggers

function trig = SendTrigger(trig,hport)
if isempty(hport) % Parallel port
    if ispc % Windows
        WriteParPort(trig);
        WaitSecs(0.010);
        WriteParPort(0);
    end
else % Serial port
    IOPort('Write', hport, uint8(trig)); %char(trig)); % Avec char les codes 128, 130 -> 140, 142, 145 -> 156, 158 et 159 sont transform�s en trigger = 26 avec SystemPLUS
end
end
