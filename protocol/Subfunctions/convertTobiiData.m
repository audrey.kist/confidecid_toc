function data = convertTobiiData(trackerData)
% Convert objects create by Tobii Eye-tracker into structure with matrix
% To save time at recording

% Left Eye
a = [trackerData.gaze.LeftEye];
b = [a.GazePoint];
data.LeftEye.GazePoint.OnDisplayArea = cat(1,b.OnDisplayArea);
data.LeftEye.GazePoint.InUserCoordinateSystem = cat(1,b.InUserCoordinateSystem);
data.LeftEye.GazePoint.Validity = transpose(cellfun(@(c)c.isequal(1), {b.Validity}));

c = [a.Pupil];
data.LeftEye.Pupil.Diameter = cat(1,c.Diameter);
data.LeftEye.Pupil.Validity = transpose(cellfun(@(c)c.isequal(1), {c.Validity}));

d = [a.GazeOrigin];
data.LeftEye.GazeOrigin.InUserCoordinateSystem = cat(1,d.InUserCoordinateSystem);
data.LeftEye.GazeOrigin.InTrackBoxCoordinateSystem = cat(1,d.InTrackBoxCoordinateSystem);
data.LeftEye.GazeOrigin.Validity = transpose(cellfun(@(c)c.isequal(1), {d.Validity}));

% Right Eye
a = [trackerData.gaze.RightEye];
b = [a.GazePoint];
data.RightEye.GazePoint.OnDisplayArea = cat(1,b.OnDisplayArea);
data.RightEye.GazePoint.InUserCoordinateSystem = cat(1,b.InUserCoordinateSystem);
data.RightEye.GazePoint.Validity = transpose(cellfun(@(c)c.isequal(1), {b.Validity}));

c = [a.Pupil];
data.RightEye.Pupil.Diameter = cat(1,c.Diameter);
data.RightEye.Pupil.Validity = transpose(cellfun(@(c)c.isequal(1), {c.Validity}));

d = [a.GazeOrigin];
data.RightEye.GazeOrigin.InUserCoordinateSystem = cat(1,d.InUserCoordinateSystem);
data.RightEye.GazeOrigin.InTrackBoxCoordinateSystem = cat(1,d.InTrackBoxCoordinateSystem);
data.RightEye.GazeOrigin.Validity = transpose(cellfun(@(c)c.isequal(1), {d.Validity}));

% Device Time Stamp
data.DeviceTimeStamp = cat(1,trackerData.gaze.DeviceTimeStamp);

% System Time Stamp
data.SystemTimeStamp = cat(1,trackerData.gaze.SystemTimeStamp);

end