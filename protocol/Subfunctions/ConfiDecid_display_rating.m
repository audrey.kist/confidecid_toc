% Affichage d'un rating (mood or confidence) et d�placement du curseur

function rating = ConfiDecid_display_rating(param, rating_type, passation, flags, eyetracker)

% D�finir le type de notation (mood or confidence)
if strcmp(rating_type, 'mood')
    rating = param.mood;
elseif strcmp(rating_type, 'confidence')
    rating = param.confidence;
end

% D�finir la position en X du curseur de fa�on al�atoire � chaque rating entre les notes 25 et 75 (= 51 valeurs possibles)
rating.cursor_Xpos = Shuffle((param.rating.minInitCursor * param.rating.pixelsPerPress + param.rating.linePos(1)):param.rating.pixelsPerPress:(param.rating.maxInitCursor * param.rating.pixelsPerPress + param.rating.linePos(1)));
ratingXPosCursor = cos(param.rating.angles_rad_m) .* param.rating.radius_m + rating.cursor_Xpos(1); % X coordinates of the points defining the cursor

firstFrame = true;
confirmRating = false;
endRating = false;

while endRating == false % Until there is a rating
    
    % Check the HID device (keyboard, gamepad...) to see if a button has been pressed
    [~, rating.secsPress, keyCode] = KbCheck(param.deviceNumber);
    
    % Action depending on the button press
    if keyCode(param.key.left)
        ratingXPosCursor = ratingXPosCursor - param.rating.pixelsPerPress; % D�placement du curseur vers la gauche
    elseif keyCode(param.key.right)
        ratingXPosCursor = ratingXPosCursor + param.rating.pixelsPerPress; % D�placement du curseur vers la droite
        
    elseif keyCode(param.key.valid) % Il y a eu confirmation
        
        param.fun.trigger(rating.trig.value, param.hport); % Trigger lors de la confirmation de la note
        endRating = true; % Sortie de la boucle while
        rating.rating = ((ratingXPosCursor(1) - param.rating.linePos(1)) / param.rating.pixelsPerPress) + 1; % Mood rating de 1 � 100
        [rating.secsRelease] = KbReleaseWait(param.deviceNumber);
        
    elseif keyCode(param.key.escape)
        rating.escape = 1;
        ConfiDecid_escape(passation, flags, eyetracker);
        return
    end
    
    % We set bounds to make sure the cursor is always in front of a response
    if ratingXPosCursor < param.rating.minXposVector
        ratingXPosCursor = param.rating.minXposVector;
    elseif ratingXPosCursor > param.rating.maxXposVector
        ratingXPosCursor = param.rating.maxXposVector;
    end
    
    % Draw things
    Screen('TextSize', param.screen.window , param.default_textSize);
    DrawFormattedText(param.screen.window, rating.caption, 'center', param.rating.caption_Ypos, param.color.white); % Draw the rating question
    DrawFormattedText(param.screen.window, rating.left_scale_text, 'center', 'center', param.color.orange, [], [], [], [], [], [param.rating.linePos(1) param.rating.scaleText_Ypos param.rating.linePos(1) param.rating.scaleText_Ypos]); % Draw the left scale text
    DrawFormattedText(param.screen.window, rating.right_scale_text, 'center', 'center', param.color.orange, [], [], [], [], [], [param.rating.linePos(3) param.rating.scaleText_Ypos param.rating.linePos(3) param.rating.scaleText_Ypos]); % Draw the right scale text
    Screen('DrawLine', param.screen.window, param.color.white, param.rating.linePos(1), param.rating.linePos(2), param.rating.linePos(3), param.rating.linePos(4), 5); % Draw the rating bar
    Screen('DrawLines', param.screen.window, param.rating.litteLinePos, 5); % Draw the ends of the rating bar
    Screen('FillPoly', param.screen.window, param.color.orange, [ratingXPosCursor; param.rating.YPosCursor]', 1); % Draw the cursor
    if confirmRating == true
        Screen('FillPoly', param.screen.window, param.color.red, [ratingXPosCursor; param.rating.YPosCursor]', 1); % Draw the cursor in red
    end
    
    % Flip to the screen
    if firstFrame == true
        rating.onset = Screen('Flip', param.screen.window); % Get a time stamp at the beginning of rating
        param.fun.trigger(rating.trig.disp, param.hport); % Trigger � l'onset du rating
        firstFrame = false;
    else
        Screen('Flip', param.screen.window);
    end
    
end

end