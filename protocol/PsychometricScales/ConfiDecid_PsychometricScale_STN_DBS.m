                             % Psychometric scales :

% BAI
% YBOC
% BDI

clearvars;
close all;
clc;
sca;

%% Pathways

pathway = fileparts(mfilename('fullpath')); % File name of currently running code
initpath = pathway; % Chemin o� se trouve le fichier Matlab contenant les questions/r�ponses de tous les tests
resultdir = fullfile(pathway, 'Sujets'); % Dossier 'Sujets' � l'int�rieur duquel les donn�es seront enregistr�es

cd(pathway);
addpath(genpath(pathway)); % Add folder and its subfolders to search path

%% User interface
try
    output = PsychUI; % Display the graphical user interface (gui)
catch
    return
end

% Load questions & responses
load(fullfile(initpath, 'psychometricScales'), 'psychometricScales');

%% Set parameters
[window, color, screen, deviceNumber, key, default_textSize] = setParameters;

%% BAI settings

bai.instructions = ['INVENTAIRE DE BECK POUR L' char(39) 'ANXI�T� \n\n\n'...
    'Voici une liste de sympt�mes courants dus � l' char(39) 'anxi�t�. Veuillez lire chaque sympt�me attentivement. Indiquez, '...
    'avec le curseur sur le chiffre appropri�, � quel degr� vous avez �t� affect� par chacun de ces sympt�mes '...
    'depuis hier soir, aujourd' char(39) 'hui inclus. \n\n'...
    'Vous pouvez utiliser le clavier ou la manette pour d�placer le curseur et valider : \n'...
    'Avec le clavier - utilisez les fl�ches pour d�placer le curseur et "espace" pour valider. \n'...
    'Avec la manette - utilisez les boutons arri�res gauche/droite pour d�placer le curseur et "A" pour valider.'];
    
bai.linePos = [screen.Xpixels * 0.15, screen.Ypixels * 0.65, screen.Xpixels * 0.85, screen.Ypixels * 0.65]; % Position of the rating bar : starting x and y positions, then ending x and y positions respectively
bai.tickSpace = (bai.linePos(3) - bai.linePos(1)) / (length(psychometricScales.BAI.responses) - 1); %distance between each tick, define whith the number of BAIresponses, so automatically adapts to the nb of possible reponses
for i = 1:2:length(psychometricScales.BAI.responses)*2 % We want 7 ticks -> line 1 = X coord ; line 2 = Y coord  
    bai.litteLinePos(1,i:i+1) = repmat(bai.linePos(1) + (((i-1)/2)*bai.tickSpace),1,2); % X coord
    bai.litteLinePos(2,i:i+1) = [bai.linePos(2) + 10, bai.linePos(2) - 10]; %Y coord
end

bai.pixelsPerPress = bai.litteLinePos(1,3) - bai.litteLinePos(1,1); % Amount of pixel we want our cursor to move on each button press

%% YBOC settings

yboc.instructions = ['R�SISTANCE ET CONTROLE FACE AUX OBSESSIONS ET COMPULSIONS \n\n\n\n'...
    'Vous allez devoir r�pondre � 4 questions tir�es de l' char(39) '�chelle YBOC. \n'...
    'Les deux premi�res questions porteront sur vos obsessions et les deux derni�res sur vos compulsions. '...
    'Vous pouvez utiliser le clavier ou la manette pour d�placer le curseur et valider : \n'...
    'Avec le clavier - utilisez les fl�ches pour d�placer le curseur et "espace" pour valider. \n'...
    'Avec la manette - utilisez les boutons arri�res gauche/droite pour d�placer le curseur et "A" pour valider. \n\n'...
    'Rappel : \n'...
    '- Les obsessions sont des id�es, des images ou des impulsions qui s' char(39) 'insinuent dans votre esprit contre votre gr�s en d�pit de vos efforts pour leur r�sister. \n'...
    '- Les compulsions sont des actes, souvent r�p�titifs, que vous vous sentez pouss� � accomplir pour att�nuer votre angoisse ou votre malaise.'];

    
%Vous allez trouver un certain nombre de qualificatifs qui peuvent ou non s' char(39) 'appliquer � vous.\n\n'...
 %   'Par exemple : "acceptez-vous d' char(39) '�tre quelqu' char(39) 'un qui aime passer du temps avec les autres ?"\n\n'...
  %  'Pour chaque affirmation, r�pondez en d�pla�ant le curseur de gauche � droite, puis validez.'];

%% BDI settings

bdi.instructions = ['Nous allons �valuer comment vous vous sentez en ce moment. \n\n '...
    'Chaque �cran repr�sente un aspect de l' char(39) '�tat mental. Placez la fl�che � l' char(39) 'endroit qui correspond le mieux � votre �tat actuel.'...
    '\n\n Vous pouvez utiliser le clavier ou la manette pour d�placer le curseur et valider : \n'...
    'Avec le clavier - utilisez les fl�ches pour d�placer le curseur et "espace" pour valider. \n'...
    'Avec la manette - utilisez les boutons arri�res gauche/droite pour d�placer le curseur et "A" pour valider.'];

%% Global settings

% S�lectionner les donn�es en fonction des l'�chelle choisie
if strcmp(output.scale{1}, 'BAI')
    data = psychometricScales.BAI;
    options = bai;
    nb_trial = length(data.questions);
elseif strcmp(output.scale{1}, 'YBOC')
    data = psychometricScales.YBOC;
    options = yboc;
    nb_trial = length(data.questions);
elseif strcmp(output.scale{1}, 'BDI')
    data = psychometricScales.BDI;
    options = bdi;
    nb_trial = size(data.responses,1);
end

data.identifier = output.identifier;
data.scale = output.scale{1};

% Define folders to save the data
participantFolder = fullfile(resultdir, data.identifier); % Participant data folder
dataFolder = fullfile(participantFolder, 'PsychometricScales'); % Dossier contenant les donn�es des �chelles psychom�triques

%% INSTRUCTIONS

while ~KbCheck(deviceNumber)
    
    % Draw things
    Screen('TextSize', window , default_textSize);
    DrawFormattedText(window, options.instructions, 'center', 'center', color.white, 60); % Instructions
    
    % Check the HID device (keyboard, gamepad...) to see if a button has been pressed
    [keyIsDown, secsPress, keyCode] = KbCheck(deviceNumber);
    
    % Flip to the screen
    vbl = Screen('Flip', window);
    
end

KbReleaseWait(deviceNumber);

%% Questionnaire

if strcmp(data.scale, 'BAI')
    data = display_horizontal_rating(window, screen, deviceNumber, default_textSize, color, key, data, options, nb_trial, vbl);
else % YBOC and BDI
    data = display_vertical_rating(window, screen, deviceNumber, default_textSize, color, key, data, nb_trial, vbl);
end

%% Save data

if ~exist(participantFolder, 'dir') % Le dossier du participant n'existe pas
    mkdir(participantFolder); % On le cr��
end

if ~exist(dataFolder, 'dir') % Le dossier contenant les donn�es des �chelles psychom�triques n'existe pas
    mkdir(dataFolder); % On le cr��
end

% D�finir le nom du fichier o� sauvegarder les infos
filename = fullfile(...
    dataFolder,...
    sprintf('%s_%s_%s.mat',...
    data.identifier,...
    datestr(now,'yyyymmdd-HHMM'),...
    data.scale));

save(filename, 'data');

%% FIN
sca;
return

%% Dialog box

function [out] = PsychUI

% Create a figure
ui.fig = figure('Visible', 'off',...
    'Units', 'pixels',...
    'Position', [0 0 200 150],...
    'Menubar', 'none',...
    'Name', 'Options',...
    'NumberTitle', 'off',...
    'Resize', 'off');

movegui(ui.fig, 'center') % Put the figure in the center of the screen

% Create an editable area
uicontrol('Style', 'text',...
    'Units', 'pix',...
    'Position', [20 115 80 20],...
    'HorizontalAlignment', 'left',...
    'ForegroundColor', [(18/255) (157/255) (179/255)],...
    'FontWeight', 'bold',...
    'String', 'Participant ID : ');

ui.ed = uicontrol('Style', 'edit',...
    'units', 'pix',...
    'Position', [100 118 50 20],...
    'String', 'TESt');

% Create a radio button group for 'Eye-tracker'
ui.scale.bg = uibuttongroup(ui.fig,...
    'Title','Psychometric test',...
    'TitlePosition', 'lefttop',...
    'FontWeight', 'bold',...
    'Units', 'pix',...
    'Position', [20 55 163 52]);

ui.scale.r1 = uicontrol(ui.scale.bg,...
    'Style', 'radiobutton',...
    'Units', 'pix',...
    'Position',[10 10 135 20],...
    'String','BAI');

ui.scale.r2 = uicontrol(ui.scale.bg,...
    'Style', 'radiobutton',...
    'Units', 'pix',...
    'Position',[55 10 135 20],...
    'String','YBOC');

ui.scale.r3 = uicontrol(ui.scale.bg,...
    'Style', 'radiobutton',...
    'Units', 'pix',...
    'Position',[110 10 135 20],...
    'String','BDI');

ui.scale.bg.SelectedObject = ui.scale.r1; % D�terminer le bouton pr�-s�lectionn�

% Create a pushbutton 'ok'
ui.pb = uicontrol('Style', 'push',...
    'Units', 'pix',...
    'Position', [40 15 50 25],...
    'String', 'ok',...
    'Callback', {@callb, ui});

% Callback function for pushbutton 'ok'
    function [] = callb(varargin)
        output.identifier = ui.ed.String; % Get participant identifier
        output.scale{1,:} = ui.scale.bg.SelectedObject.String; % Get test option
        
        % Print to command line
        disp(['Participant name is :  ' output.identifier])
        disp(['Scale selected is : ' output.scale{1,:}]);
        
        expr = '[A-Z][A-Z][A-Z][a-z]'; % Expected format of participant identifier (NOMp)
        % Si le nom du participant n'est pas au format attendu : affichage d'un message d'information en rouge
        if length(output.identifier) < 4 || length(output.identifier) > 4 || isempty(regexp(output.identifier,expr,'match'))
            uicontrol('Style', 'text',...
                'Units', 'pix',...
                'Position', [20 90 160 20],...
                'HorizontalAlignment', 'center',...
                'ForegroundColor', 'red',...
                'String', 'Invalid format ! (NOMp expected)');
        else
            out = output; % Save data to workspace
            close(ui.fig); % Close figure
        end
    end

% Create a pushbutton 'cancel'
ui.pb = uicontrol('Style', 'push',...
    'Units', 'pix',...
    'Position', [115 15 50 25],...
    'String', 'cancel',...
    'Callback', {@callbcancel, ui});

% Callback function for pushbutton 'cancel'
    function [] = callbcancel(varargin)
        close(ui.fig); % Close figure
        disp('Experiment cancelled during setup !');
    end

% Make figure visible after adding all components
ui.fig.Visible = 'on';

% Block program execution until the figure is closed
uiwait(ui.fig);

end

function [window, color, screen, deviceNumber, key, default_textSize] = setParameters

%% Screen & General settings

PsychDefaultSetup(2); % Here we call some default settings for setting up Psychtoolbox
screens = Screen('Screens'); % Get the screen numbers
screenNumber = max(screens); % Select the external screen if it is present (use 'min' to select the native screen)

% Define colors (luminance � 55%, sauf pour le orange (65%))
color.black = BlackIndex(screenNumber);
color.white = WhiteIndex(screenNumber);
color.grey = [(140/255) (140/255) (140/255)];
color.red = [1 (15/255) (47/255)];
color.green = [(7/255) (171/255) (100/255)];
color.blue = [(18/255) (157/255) (179/255)];
color.orange = [1 (128/255) 0];

[window, windowRect] = PsychImaging('OpenWindow', screenNumber, color.black); % Open an 'on screen' window and color it black
HideCursor;
[screen.Xpixels, screen.Ypixels] = Screen('WindowSize', window); % Get the size of the 'on screen' window in pixels
[screen.xCenter, screen.yCenter] = RectCenter(windowRect); % Get the centre coordinate of the window in pixels
Screen('BlendFunction', window, GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA); % Enable alpha blending for anti-aliasing
monitorFlipInterval = Screen('GetFlipInterval', window); % Query the frame duration
MaxPriority(window); % Query the maximum priority level
rng('default'); % Reinitialize the random number generator to its startup configuration
rng('shuffle'); % Seed the random number generator.

% Screen-blink compatible timing
secTiming = @(dt,ifi)(round(dt / ifi) - 0.5) * ifi; % dt = duration in secs

% Function to find which key was pressed
whichKey = @(keyCode)(find(keyCode,1));

% Text options
Screen('TextFont', window, 'Arial');
Screen('TextColor', window, color.white);

% Generate values from the uniform distribution on the interval [a, b]:
% r = a + (b-a).*rand;

% Text display preferences
Screen('Preference', 'TextRenderer', 1);
default_textSize = 40;
format longG;

%% Responses settings

% devices = PsychHID('Devices'); % Don't work properly on windows so we use GetGamepadIndices
[gamepadIndices, productNames] = GetGamepadIndices;

if isempty(productNames) == 0 && strcmp(productNames(1), 'Logitech Dual Action') % Utilisation de la manette
    deviceNumber = gamepadIndices(1);
    
    % Key codes
    key.escape = 9;
    key.valid = 2;
    key.left = 5;
    key.right = 6;
    key.up = 5; %[5, 6];
    key.down = 6; %[7, 8];
    
else % Utilisation du clavier
    deviceNumber = GetKeyboardIndices;
    
    % Key codes
    key.escape = KbName('Escape');
    key.valid = KbName('Space');
    key.left = KbName('LeftArrow');
    key.right = KbName('RightArrow');
    key.up = KbName('UpArrow');
    key.down = KbName('DownArrow');
end

% Gamepad settings
% X = 1 ; A = 2 ; B = 3 ; Y = 4 ; LB = 5 ; RB = 6 ; LT = 7 ; RT = 8 ; Back = 9

end

function data = display_horizontal_rating(window, screen, deviceNumber, default_textSize, color, key, data, options, nb_trial, vbl)

% Cursor
nb_sides = 3; % Number of sides for our polygon
angles_deg = linspace(-90, 270, nb_sides + 1); % Angles at which our polygon vertices endpoints will be
angles_rad = angles_deg * (pi / 180);
radius = 30; % Size of the cursor

YPosCursor = sin(angles_rad) .* radius + (screen.Ypixels * 0.76); % Position en Y du curseur
scaleText_Ypos = screen.Ypixels * 0.7; % Position en Y du texte de la barre de notation
scaleInterText_Ypos = screen.Ypixels * 0.53; % Position en Y du texte de la barre de notation
caption_Ypos = screen.Ypixels * 0.3; % Position en Y de la question

% D�terminer la position min et max du curseur en X
minXposVector = round(cos(angles_rad) .* radius + options.linePos(1));
maxXposVector = round(cos(angles_rad) .* radius + options.linePos(3));

for trial_id = 1:nb_trial % Boucle � travers les essais
    
    % Affichage de la question et d�placement du curseur
    % D�finir la position initiale du curseur
    initCursor = Shuffle(1:length(data.responses));
    cursor_Xpos = options.linePos(3)-((initCursor(1)-1)*options.pixelsPerPress);
    XPosCursor = cos(angles_rad) .* radius + cursor_Xpos(1); % X coordinates of the points defining the cursor
    firstFrame = true;
    confirmRating = false;
    endRating = false;
    onset = vbl;
    nPress = 0;
    
    while endRating == false
        
        % Check the HID device (keyboard, gamepad...) to see if a button has been pressed
        [~, ~, keyCode] = KbCheck(deviceNumber);
        
        % Action depending on the button press and time
        if keyCode(key.left)
            XPosCursor = XPosCursor - options.pixelsPerPress;
            KbReleaseWait(deviceNumber);
            nPress = nPress + 1;
        elseif keyCode(key.right)
            XPosCursor = XPosCursor + options.pixelsPerPress;
            KbReleaseWait(deviceNumber);
            nPress = nPress + 1;
        elseif keyCode(key.valid) % Il y a eu confirmation
            confirmRating = true;
        elseif keyCode(key.escape)
            sca;
            return
        end
        
        % We set bounds to make sure the cursor is always in front of a response
        if XPosCursor < minXposVector
            XPosCursor = minXposVector;
        elseif XPosCursor > maxXposVector
            XPosCursor = maxXposVector;
        end
        
        % Draw things
        Screen('TextSize', window , default_textSize);
        DrawFormattedText(window, data.questions{trial_id}, 'center', caption_Ypos, color.white, 60); % Draw the question
        % Draw the scale text
        for n = 1:length(data.responses) 
            DrawFormattedText(window, data.responses{n}, 'center', 'center', color.orange, 15, [], [], [], [], [options.litteLinePos(1,n*2) scaleText_Ypos options.litteLinePos(1,n*2) scaleText_Ypos]);
        end
        for n = 1:size(data.resp_text,2) 
            DrawFormattedText(window, data.resp_text{trial_id,n}, 'center', 'center', color.white, 25, [], [], [], [], [options.litteLinePos(1,n*2) scaleInterText_Ypos options.litteLinePos(1,n*2) scaleInterText_Ypos+100]);
                                                                                                                       %[options.litteLinePos(1,n*4-2) scaleInterText_Ypos options.litteLinePos(1,n*4-2) scaleInterText_Ypos+100]); %Dessine le texte (resp_text) � une position de curseur sur deux
        end
        Screen('DrawLine', window, color.white, options.linePos(1), options.linePos(2), options.linePos(3), options.linePos(4),5); % Draw the rating bar
        Screen('DrawLines', window, options.litteLinePos, 5); % Draw the ticks on the rating bar
        Screen('FillPoly', window, color.orange, [XPosCursor; YPosCursor]', 1); % Draw the cursor
        if confirmRating == true
            Screen('FillPoly', window, color.red, [XPosCursor; YPosCursor]', 1); % Draw the cursor in red
        end
        
        % Flip to the screen
        if firstFrame == true
            onset = Screen('Flip', window); % Get a time stamp at the beginning of the rating
            firstFrame = false;
        else
            vbl = Screen('Flip', window);
        end
        
        % Confirmation
        if confirmRating == true
            [secsRelease] = KbReleaseWait(deviceNumber);
            endRating = true; % Sortie de la boucle while
            rating = ((XPosCursor(1) - options.linePos(1)) / options.pixelsPerPress); % Rating : BAS = de 0 � 6
        end
        
    end
    
    % Save data
    data.onset(trial_id,:) = onset; % Onset du rating
    data.offset(trial_id,:) = secsRelease; % Offset du rating
    data.cursorPosInit(trial_id,:) = ((cursor_Xpos(1) - options.linePos(1)) / options.pixelsPerPress); % Position initiale du curseur (BAS = de 0 � 6)
    data.nPress(trial_id,:) = nPress; % Nombre de fois o� un bouton a �t� appuy� (confirmation exclue)
    data.points(trial_id,:) = rating; % Rating (BAS = de 0 � 6)
    
end

end

function data = display_vertical_rating(window, screen, deviceNumber, default_textSize, color, key, data, nb_trial, vbl)

% Cursor
nb_sides = 3; % Number of sides for our polygon
angles_deg = linspace(0, 360, nb_sides + 1); % Angles at which our polygon vertices endpoints will be
angles_rad_q = angles_deg * (pi / 180);
radius_q = 30; %size of the cursor

quest_Ypos = screen.Ypixels * 0.25; % Position en Y de la question par rapport � l'�cran
rep_Ypos = linspace(screen.Ypixels * 0.5, screen.Ypixels * 0.875, size(data.responses,2));
pixelsPerPress = rep_Ypos(2) - rep_Ypos(1); % Amount of pixel we want our cursor to move on each button press
cursorYshift = 15; % D�calage entre la position du texte et la position du curseur en Y
cursorXshift = 100; % D�calage entre la position du texte et la position du curseur en X

% D�terminer la position min et max du curseur en Y
minYposVector = sin(angles_rad_q) .* radius_q + min(rep_Ypos - cursorYshift);
maxYposVector = sin(angles_rad_q) .* radius_q + max(rep_Ypos - cursorYshift);

quizz_wrapat = round(screen.Ypixels*0.056); %60;

for trial_id = 1:nb_trial % Boucle � travers les essais
    
    % Actualiser les r�ponses pr�sent�es
    Screen('TextSize', window , default_textSize);
    for r = 1:length(rep_Ypos)
        [~, ~, text.(sprintf('bounds%d',r)), ~] = DrawFormattedText(window, data.responses{trial_id,r}, 'center', rep_Ypos(r)); % textbounds = [left,top,right,bottom]
    end
    
    % D�finir la position du curseur en X et en Y
    minLeftPos = min([text.bounds1(1); text.bounds2(1); text.bounds3(1); text.bounds4(1)]); % Min of the left bounding box of each response = Permet d'adapter la position en X du curseur � chaque question
    cursor_Ypos = Shuffle(rep_Ypos - cursorYshift); % D�finir la position en Y du curseur de fa�on al�atoire � chaque question
    
    xPosVector = cos(angles_rad_q) .* radius_q + (minLeftPos - cursorXshift); % X and Y coordinates of the points defining our polygon
    yPosVector = sin(angles_rad_q) .* radius_q + cursor_Ypos(1);
    
    % D�placement du curseur et passage � l'essai suivant
    firstFrame = true;
    endTrial = false;
    onset = vbl;
    
    while endTrial == false
        
        % Draw things
        if strcmp(data.scale, 'YBOC')
            DrawFormattedText(window, data.questions{trial_id}, 'center', quest_Ypos, color.white, quizz_wrapat); % Draw the question
        else % BDI
            DrawFormattedText(window, data.questions{:}, 'center', quest_Ypos, color.white, quizz_wrapat);
        end
        for r = 1:length(rep_Ypos)
            DrawFormattedText(window, data.responses{trial_id,r}, 'center', rep_Ypos(r), color.white); % Draw the response #r
        end
        Screen('FillPoly', window, color.orange, [xPosVector; yPosVector]', 1); % Draw the cursor
        
        % Check the HID device (keyboard, gamepad...) to see if a button has been pressed
        [~, ~, keyCode] = KbCheck(deviceNumber);
        
        % Action depending on the button press
        if keyCode(key.valid) % Il y a eu confirmation
            endTrial = true;
            [secsRelease] = KbReleaseWait(deviceNumber);
        elseif any(keyCode(key.up))
            yPosVector = yPosVector - pixelsPerPress;
            KbReleaseWait(deviceNumber);
        elseif any(keyCode(key.down))
            yPosVector = yPosVector + pixelsPerPress;
            KbReleaseWait(deviceNumber);
        elseif keyCode(key.escape)
            sca;
            return
        end
        
        % We set bounds to make sure the cursor is always in front of a response
        if yPosVector < minYposVector
            yPosVector = maxYposVector;
        elseif yPosVector > maxYposVector
            yPosVector = minYposVector;
        end
        
        % Flip to the screen
        if firstFrame == true
            onset = Screen('Flip', window); % Get a time stamp at the quizz display
            firstFrame = false;
        else
            vbl = Screen('Flip', window);
        end
        
    end
    
    % D�finir la r�ponse s�lectionn�e par le sujet : Lorsqu'on sort de la boucle 'while' c'est qu'il y a eu confirmation
    final_pos = abs(yPosVector - rep_Ypos') < 100;
    select = ismember(final_pos, [1 1 1 1], 'rows');
    rep_select = data.responses{trial_id,select};
    
    % Save data
    data.onset(trial_id,:) = onset; % Onset du quizz
    data.offset(trial_id,:) = secsRelease; % Offset du quiz
    data.givenResp{trial_id,:} = rep_select; % R�ponse donn�e
    data.points{trial_id,:} = find(select)-1; % Point associ� � la r�ponse
    
end % Fin de la boucle � travers les essais

end
