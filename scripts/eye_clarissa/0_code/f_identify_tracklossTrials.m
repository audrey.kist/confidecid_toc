function trialswTrackloss = f_identify_tracklossTrials(trialCol, tracklossCol, maxTrackloss)

% f_identify_tracklossTrials   Identifies trials with an amount of trackloss
%   superior or equal to given threshold.
%
%   Input (first 2 inputs should have equal length):
%         - trialCol: column of trials (1 row per sample);
%         - tracklossCol: column of trackloss (1 row per sample);
%         - maxTrackloss: percentage of trackloss authorized;
%
%   Output: vector of trial numbers, which have an amount of trackloss >=
%   trackloss threshold
%
%   Clarissa Baratin, March 2021

nTrials = max(trialCol);
trialswTrackloss = [];

for iTrial = 1:nTrials
    
    trialRows = find(trialCol == iTrial);
    trialLength = length(trialRows);
    tracklossLength = length(find(tracklossCol(trialRows, 1) == 0));
    percentTrackloss = tracklossLength/trialLength; %in decimals
    
    if percentTrackloss >= maxTrackloss
        trialswTrackloss(size(trialswTrackloss, 1) + 1, 1) = iTrial;
    end
end
    