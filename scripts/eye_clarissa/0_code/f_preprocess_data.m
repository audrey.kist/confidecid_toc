function f_preprocess_data(anls_lvl, varargin)

% f_preprocess_data   Calls preprocessing pipeline for a single subject
%   or all subjects. Single subject will be concatenated automatically with
%   other subjects preprocessed previously (if any).
%
%   Input: 'all' to do preprocessing for all subjects or 'single' & 
%   'subject ID' to do preprocessing for a single subject. In this case,
%   varargin = 'subject ID'.
%
%   Output: none. All plots and data saved directly to output folders.
%
%  Clarissa Baratin, March 2021

close all
clear subjects

%% set paths

paths.DIR = 'C:\Users\Clarissa\Documents\decid_eye\analysis\manip1&2\0_code';
paths.LOAD_DATA_PATH = 'C:\Users\Clarissa\Documents\decid_eye\analysis\manip1&2\1_pipeline\0_f_import_data\out';
paths.OUT_PATH = 'C:\Users\Clarissa\Documents\decid_eye\analysis\manip1&2\1_pipeline\fixations\1_f_preprocess_data\out';
paths.PLOTS_PATH = 'C:\Users\Clarissa\Documents\decid_eye\analysis\manip1&2\1_pipeline\fixations\1_f_preprocess_data\plots';

addpath('C:\Users\Clarissa\Documents\MATLAB\Downloads\Packages\gramm')% plotting package

%% options struct

options.MaxGapLength = 90*0.075; %nb of timestamps in 75 ms (threshold from tobii) for 90 Hz sampling
options.NoiseReducWindow = 5; % 90/5 = window of approximately 0.056 s or 56 ms 
options.VelocityThresh = 50; %!!!! should be changed depending on the noise in the data, check this, try different noise reductions

%% call preprocessing pipeline

all_subjects_data = importdata(fullfile(paths.LOAD_DATA_PATH, 'subjects.mat'));

switch anls_lvl
    case 'single'
        
        % load subject-specific data
        subID = varargin{1};
        subStructRow = find(strcmp({all_subjects_data.name}, subID));
        subject_data = all_subjects_data(subStructRow);
        
        % create participant folder (if doesn't exist)
        cd(paths.PLOTS_PATH)
        
        if ~exist(sprintf('sub_%s', subID), 'dir')
            mkdir(sprintf('sub_%s', subID));
        end
        
        paths.SUB_FOLDER = fullfile(paths.PLOTS_PATH, sprintf('sub_%s', subID));
        
        cd(paths.DIR)
        
        %%%%%% PREPROCESS SUBJECT %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        preprocessed_subject_data = f_preprocess_lvl1(options, paths, subject_data);
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        
        % if it is first subject to be processed
        if ~isfile(fullfile(paths.OUT_PATH, 'subjects.mat'))  % output folder empty, no data preprocessed previously
            
            subjects = preprocessed_subject_data;
            save(fullfile(paths.OUT_PATH, 'subjects.mat'), 'subjects');
            
        else
            
            % load all previously processed subjects
            load(fullfile(paths.OUT_PATH, 'subjects.mat'));
            
            % concatenate prev processed & new subject
            subjects(size(subjects, 2) + 1).name = preprocessed_subject_data.name;
            subjects(size(subjects, 2)).trial_characteristics = preprocessed_subject_data.trial_characteristics;
            subjects(size(subjects, 2)).manip = preprocessed_subject_data.manip;
            subjects(size(subjects, 2)).data_fixations = preprocessed_subject_data.data_fixations;
            subjects(size(subjects, 2)).fixSum = preprocessed_subject_data.fixSum;
            subjects(size(subjects, 2)).Log = preprocessed_subject_data.Log;
            
            save(fullfile(paths.OUT_PATH, 'subjects.mat'), 'subjects');
        end
        
    case 'all'
        
        if exist(fullfile(paths.OUT_PATH, 'subjects.mat'))           
            delete(fullfile(paths.OUT_PATH, 'subjects.mat'))           
        end
        
        nSub = length(all_subjects_data);
        
        for iSub = 1:nSub
            
            subject_data = all_subjects_data(iSub);
            subID = subject_data.name;
            
            % create participant folder (if doesn't exist)
            cd(paths.PLOTS_PATH)
            
            if ~exist(sprintf('sub_%s', subID), 'dir')
                mkdir(sprintf('sub_%s', subID));
            end
            
            paths.SUB_FOLDER = fullfile(paths.PLOTS_PATH, sprintf('sub_%s', subID));
            
            cd(paths.DIR)
            
            %%%%%% PREPROCESS SUBJECT %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            preprocessed_subject_data = f_preprocess_lvl1(options, paths, subject_data);
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            
            % if it is first subject to be processed
            if ~isfile(fullfile(paths.OUT_PATH, 'subjects.mat'))  % output folder empty, no data preprocessed previously
                
                subjects = preprocessed_subject_data;
                save(fullfile(paths.OUT_PATH, 'subjects.mat'), 'subjects');
                
            else
                
                % load all previously processed subjects
                load(fullfile(paths.OUT_PATH, 'subjects.mat'));
                
                % concatenate prev processed & new subject
                subjects(size(subjects, 2) + 1).name = preprocessed_subject_data.name;
                subjects(size(subjects, 2)).trial_characteristics = preprocessed_subject_data.trial_characteristics;
                subjects(size(subjects, 2)).manip = preprocessed_subject_data.manip;
                subjects(size(subjects, 2)).data_fixations = preprocessed_subject_data.data_fixations;
                subjects(size(subjects, 2)).fixSum = preprocessed_subject_data.fixSum;
                subjects(size(subjects, 2)).Log = preprocessed_subject_data.Log;
                
                save(fullfile(paths.OUT_PATH, 'subjects.mat'), 'subjects');
            end
        end
end

%% plot fixations of all subjects together

load(fullfile(paths.OUT_PATH, 'subjects.mat'));

figure('Position', [100 100 1500 1000]);

subplots_y = [1 2 3 4 5 6 7 1 2 3 4 5 6 7 1 2 3 4 5 6 7 1 2 3 4 5 6 7 1 2 3 4 5 6 7 1 2 3 4 5 6 7];
subplots_x = [1 1 1 1 1 1 1 2 2 2 2 2 2 2 3 3 3 3 3 3 3 4 4 4 4 4 4 4 5 5 5 5 5 5 5 6 6 6 6 6 6 6];

for iSub = 1:size(subjects, 2)
    subplot_x = subplots_x(iSub);
    subplot_y = subplots_y(iSub);

    data = subjects(iSub).data_fixations(subjects(iSub).data_fixations(:, 7) == 1, [9,11:12]);
    g(subplot_x,subplot_y) = gramm('x', data(:,2), 'y', data(:,3));
    g(subplot_x,subplot_y).geom_point();
    g(subplot_x,subplot_y).set_point_options('base_size', 1);
    g(subplot_x,subplot_y).axe_property('XLim', [0 1], 'YLim', [0 1]);
    g(subplot_x,subplot_y).set_title(sprintf('Subject %d', iSub));
end

g.draw;
saveas(gcf, fullfile(paths.PLOTS_PATH, 'finalUnclassifiedFixations.png'))


close all
clear g
figure('Position', [100 100 1500 1000]);

subplots_y = [1 2 3 4 5 6 7 1 2 3 4 5 6 7 1 2 3 4 5 6 7 1 2 3 4 5 6 7 1 2 3 4 5 6 7 1 2 3 4 5 6 7];
subplots_x = [1 1 1 1 1 1 1 2 2 2 2 2 2 2 3 3 3 3 3 3 3 4 4 4 4 4 4 4 5 5 5 5 5 5 5 6 6 6 6 6 6 6];

for iSub = 1:size(subjects, 2)
    subplot_x = subplots_x(iSub);
    subplot_y = subplots_y(iSub);

    data = subjects(iSub).data_fixations(subjects(iSub).data_fixations(:, 7) == 1, [9,11:12]);
    g(subplot_x,subplot_y) = gramm('x', data(:,2), 'y', data(:,3), 'color', data(:,1));
    g(subplot_x,subplot_y).geom_point();
    g(subplot_x,subplot_y).set_point_options('base_size', 1);
    g(subplot_x,subplot_y).axe_property('XLim', [0 1], 'YLim', [0 1]);
    g(subplot_x,subplot_y).set_title(sprintf('Subject %d', iSub));
end

g.draw;
saveas(gcf, fullfile(paths.PLOTS_PATH, 'finalClassifiedFixations.png'))

%% save data

for iSub = 1:size(subjects, 2)
    subjects(iSub).Log.RemoveDataOpts = options;
    subjects(iSub).Log.RemoveDataPaths = paths;
end

save(fullfile(paths.OUT_PATH, 'subjects.mat'), 'subjects');

end



% %% check that other data has been preprocessed previously if 'single' is chosen

% if numel(dir(paths.OUT_PATH)) <= 2 && isequal(anls_lvl, 'single')  % output folder empty, no data preprocessed previously
%
%     error ('Running a single subject is only possible if other subjects were preprocessed previously.')
%
% end
