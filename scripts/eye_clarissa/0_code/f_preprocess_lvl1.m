function subject_data = f_preprocess_lvl1(options, paths, subject_data)

% corrected how the last fixation was included in fixation_coord

%% Preprocessing script for the following steps:
% 1. detection & interpolation of missing data
% 2. smoothing
% 3. eye velocity calculation, I-VT classifier: classification of data into fixation and saccade samples, grouping of samples into fixations, numbering of fixations
% 4. removal of time between trials
% 5. classification of fixations by ROI (region of interest). This is done using a knn classifier. Implementation available in ROI_knn_classifier_fixations notebook.
% 6. determine meaning of ROI (location of yes/no/gains/losses changes each trial)
% 7. Interpolate fixations using fixation_tuning.m
% 8. removal of fixations of 1 datapoint only


% algo implementation (from doc
% https://stemedhub.org/resources/2173/download/Tobii_WhitePaper_TobiiIVTFixationFilter.pdf)


%% detection & interpolation of missing data
% done for all coordinate columns

subject_data.data_interp = subject_data.data;

[subject_data.data_interp(:,[3,4, 7:12]), missing_data] = f_interpolate_data(subject_data.data(:,[3,4, 7:12]), subject_data.data(:,5), subject_data.data(:,6), options.MaxGapLength);

%% update trackloss column
% trackloss = 1 (no trackloss) for interpolated data

nFrags = length(missing_data);

if nFrags > 2
    for iFrag = 2:nFrags
        
        FirstRow = missing_data(iFrag, 1);
        LastRow = missing_data(iFrag, 2);
        
        subject_data.data_interp(FirstRow:LastRow, 6) = 1;
    end
end
    
%% plot check

if length(missing_data) >= 20
    
    iPlot = 1;
    nPlots = 20;
    
    figure
    
    while iPlot <= nPlots
        
        nFrag = randi([2 + 1 length(missing_data) - 1], 1, 1); %frag 1 has NaN, avoid taking first and last fragments in case there aren't enough samples before/after (do +10 and -10)
        
        subplot(5,4,iPlot);
        y1 = subject_data.data(missing_data(nFrag)-10:missing_data(nFrag)+10,4); % y-axis of pic
        y2 = subject_data.data(missing_data(nFrag)-10:missing_data(nFrag)+10,3); % x-axis of pic
        plot(y1)
        hold on
        plot(y2)
        
        subplot(5,4, iPlot+1);
        y1 = subject_data.data_interp(missing_data(nFrag)-10:missing_data(nFrag)+10,4); % y-axis of pic
        y2 = subject_data.data_interp(missing_data(nFrag)-10:missing_data(nFrag)+10,3); % x-axis of pic
        plot(y1)
        hold on
        plot(y2)
        
        iPlot = iPlot + 2;
    end
    
    saveas(gcf, fullfile(paths.SUB_FOLDER, 'data_interpolation.png'))
else
    print('Not enough missing data to generate 20 plots')
end

%% smoothing/noise reduction: moving median implemented using matlab function
% done for all coordinate columns

subject_data.data_smoothed = subject_data.data_interp;
subject_data.data_smoothed(:, [3,4, 7:12]) = movmedian(subject_data.data_interp(:,[3,4, 7:12]), options.NoiseReducWindow);

%% plot check

iPlot = 1;
nPlots = 20;

figure

while iPlot <= nPlots
    
    nSample = randi([101 length(subject_data.data_interp)-100], 1, 1);
    
    subplot(5,4,iPlot);
    y1 = subject_data.data_interp(nSample-100:nSample+100,4); % y-axis of pic
    y2 = subject_data.data_interp(nSample-100:nSample+100,3); % x-axis of pic
    plot(y1)
    hold on
    plot(y2)
    
    subplot(5,4, iPlot+1);
    y1 = subject_data.data_smoothed(nSample-100:nSample+100,4); % y-axis of pic
    y2 = subject_data.data_smoothed(nSample-100:nSample+100,3); % x-axis of pic
    plot(y1)
    hold on
    plot(y2)
    
    iPlot = iPlot + 2;
end

saveas(gcf, fullfile(paths.SUB_FOLDER, 'data_smoothing.png'))

%% calculate eye movement velocities

subject_data.data_fixations = [];
subject_data.fixation_coord = [];

subject_data.data_fixations = subject_data.data_smoothed(:, 1:6);
[subject_data.data_fixations(:,7:8), subject_data.fixation_coord] = f_IVT_classifier(subject_data.data_smoothed(:,3), subject_data.data_smoothed(:,4), subject_data.data_smoothed(:,7:9), subject_data.data_smoothed(:,10:12), options.VelocityThresh);

%% plot check (plot trials). Here, only fixations in one of 4 ROIS plotted.

iPlot = 1;
nPlots = 20;

figure

while iPlot <= nPlots
    
    nSample = randi([101 length(subject_data.data_interp)-100], 1, 1);
    
    subplot(5,4,iPlot);
    y1 = subject_data.data_fixations(nSample-100:nSample+100, 4); % y-axis of pic
    y2 = subject_data.data_fixations(nSample-100:nSample+100, 3); % x-axis of pic
    y3 = subject_data.data_fixations(nSample-100:nSample+100, 7); %
    yyaxis left
    plot(y1)
    hold on
    plot(y2)
    hold on
    yyaxis right
    axis([0 inf -0.2 1.2]);
    plot(y3)
    
    iPlot = iPlot + 1;
end

saveas(gcf, fullfile(paths.SUB_FOLDER, 'velocity_classification.png'))

% %% save data
% 
% subject_data = rmfield(subject_data, {'data', 'data_smoothed', 'data_interpolated'});
% save('C:\Users\Clarissa\Documents\decid_eye\analysis\1_pipeline\1_preprocess_data\out\subject_data_v2.mat', 'subject_data')

%% remove time between choice trials
  
remove_rows = find(subject_data.data_fixations(:,2) == 0);

% remove time between choice trials from main matrix
subject_data.data_fixations(remove_rows, :) = [];

% get nbs of fixations to be removed and remove from fixation_coord matrix
keep_fixations = unique(subject_data.data_fixations(:, 8));
keep_fixations = nonzeros(keep_fixations);

subject_data.fixation_coord = subject_data.fixation_coord(keep_fixations,:);

%% classify fixations
% tried to do this by session, didn't change much, see SessionbySess script

% nSess = max(subject_data.data_fixations(:, 1));

%for iSess = 1:nSess % note: tried per session
    
%    sessRows = subject_data.data_fixations(:, 1) == iSess;
    % apply model
    data = subject_data.fixation_coord(:, 2:3);
    subject_data.fixation_coord(:, 4) = f_ROI_knn_classifier(data);
    
%end

% add ROIs to subjects.data_fixations(:,9) to have all relevant data in
% single matrix
nFix = length(subject_data.fixation_coord);

for iFix = 1:nFix
    
    fixation_nb = subject_data.fixation_coord(iFix, 1);
    
    fixation_rows = subject_data.data_fixations(:, 8) == fixation_nb;
    fixation_ROI = subject_data.fixation_coord(iFix, 4);
    fixation_xcoord = subject_data.fixation_coord(iFix, 2);
    fixation_ycoord = subject_data.fixation_coord(iFix, 3);
    
    subject_data.data_fixations(fixation_rows, 9) = fixation_ROI;
    subject_data.data_fixations(fixation_rows, 11) = fixation_xcoord;
    subject_data.data_fixations(fixation_rows, 12) = fixation_ycoord;

end

%% plot fixations, before/after classification
% (after this, fixation.coord not used anymore)

close all
clear g
figure('Position',[100 100 400 300]);

plot_data = subject_data.fixation_coord(:,2:3);
g = gramm('x', plot_data(:,1), 'y', plot_data(:,2));
g.geom_point();
g.set_point_options('base_size', 1);
g.axe_property('XLim', [0 1], 'YLim', [0 1]);

g.draw;
f_saveToMultipleFormats(fullfile(paths.SUB_FOLDER, 'fixations_preclassification'))

% plot
clear g
figure('Position',[100 100 400 300]);

plot_data = subject_data.fixation_coord(:,2:3);
g = gramm('x', plot_data(:,1), 'y', plot_data(:,2), 'color', subject_data.fixation_coord(:,4));
g.geom_point();
g.set_point_options('base_size', 1);
g.axe_property('XLim', [0 1], 'YLim', [0 1]);

g.draw;
f_saveToMultipleFormats(fullfile(paths.SUB_FOLDER, 'fixations_postclassification'))

%% delete all irrelevant matrices from previous steps

subject_data = rmfield(subject_data, {'data', 'data_interp', 'data_smoothed', 'fixation_coord'});

%% interpolate fixations

% Re-classify fixations outside of ROIs located between 2 fixations to the same ROI as a fixation to that ROI
% here, fixation nbs do not need to change
[subject_data.data_fixations(:, 9), subject_data.Log.ReclassifiedFixs] = f_interpolate_fixations(subject_data.data_fixations(:, 2), subject_data.data_fixations(:, 8), subject_data.data_fixations(:, 9));

% Re-classify zeros between 2 fixations to the same ROI as a fixation to that ROI.
% fixation nbs are not updated within function
[subject_data.data_fixations(:, 7), subject_data.data_fixations(:, 6), subject_data.data_fixations(:, 9), subject_data.Log.ReclassifiedZeros] = f_interpolate_zeros(subject_data.data_fixations(:, 2), subject_data.data_fixations(:, 7), subject_data.data_fixations(:, 6), subject_data.data_fixations(:, 8), subject_data.data_fixations(:, 9));

% !!! At this point, fixation numbers are not accurate anymore !!!

%% re-number fixations (useful to easily loop through fixations)

subject_data.data_fixations(:, 8) = f_number_fixations(subject_data.data_fixations(:, 2), subject_data.data_fixations(:, 7));

%% clean up: remove fixations of less than 2 datapoints

% here, could remove short fixations (less than 60ms according to Tobii).
% Not done. 

% get nb of fixations
nFix = max(subject_data.data_fixations(:, 8));

for iFix = 1:nFix
    
    % get fixation info
    fixation_length = length(subject_data.data_fixations(subject_data.data_fixations(:, 8) == iFix, 8));
    fixation_rows = subject_data.data_fixations(:, 8) == iFix;
    
    if fixation_length < 2
        subject_data.data_fixations(fixation_rows, 7) = 0; % becomes not a fixation (fixations = 1)
        subject_data.data_fixations(fixation_rows, 9) = 6; % 6 = no class
    end
end

%% re-number fixations (using binary fixation column)

subject_data.data_fixations(:, 8) = f_number_fixations(subject_data.data_fixations(:, 2), subject_data.data_fixations(:, 7));

%% determine meaning of each fixation

fixation_ROI_meaning = f_fix_meaning(subject_data.data_fixations(:,2), subject_data.trial_characteristics(:,1), subject_data.data_fixations(:,9), subject_data.manip);
subject_data.data_fixations(:,10) = fixation_ROI_meaning;

%% plot a few trials over time

close all
clear g
figure('Position', [100 100 1200 900]);

subplots_y = [1 2 3 4 1 2 3 4 1 2 3 4 1 2 3 4];
subplots_x = [1 1 1 1 2 2 2 2 3 3 3 3 4 4 4 4];

trialsToPlot = [1 10 20 30 40 50 55 60 65 70 80 90 100 105 110 120];

for iTrial = 1:length(subplots_y)
    
    trialToPlot = trialsToPlot(iTrial);
    
    subplot_x = subplots_x(iTrial);
    subplot_y = subplots_y(iTrial);

    data = subject_data.data_fixations(subject_data.data_fixations(:, 2) == trialToPlot, 10);
    g(subplot_x,subplot_y) = gramm('x', [1:length(data)]'/90, 'y', data);
    g(subplot_x,subplot_y).geom_line();
    %g(subplot_x,subplot_y).set_point_options('base_size', 1);
%    g(subplot_x,subplot_y).axe_property('XTick', [1:length(data)]', 'YLim', [0 1]);
    g(subplot_x,subplot_y).set_title(sprintf('Choice %d', subject_data.trial_characteristics(trialToPlot, 5)));
    g(subplot_x,subplot_y).set_names('x','Time (s)','y','Fixation meaning');
end

g.draw;
f_saveToMultipleFormats(fullfile(paths.SUB_FOLDER, 'trialsOverTime'))

%% make fixation matrix per trial including:
% fixation nb, duration from choice onset, duration of fix, location, location meaning
    
nTrials = max(subject_data.data_fixations(:,2));

for iTrial = 1:nTrials
    
    trial_rows = find(subject_data.data_fixations(:,2) == iTrial);
    fixations_in_trial = nonzeros(unique(subject_data.data_fixations(trial_rows, 8)));
    nFix = length(fixations_in_trial); %
    subject_data.fixSum.trial(iTrial).fixData = [];
    
    if nFix > 0
        
        for iFix = 1:nFix
            
            fixation_nb = fixations_in_trial(iFix);
            fixation_rows = find(subject_data.data_fixations(:, 8) == fixation_nb);
            
            choice_onset_time = subject_data.data_fixations(trial_rows(1), 5);
            fixation_onset_time = subject_data.data_fixations(fixation_rows(1), 5);
            duration_from_choice_onset = fixation_onset_time - choice_onset_time;
            fixation_offset_time = subject_data.data_fixations(fixation_rows(end), 5);
            duration = fixation_offset_time - fixation_onset_time;
            location = subject_data.data_fixations(fixation_rows(1), 9);
            meaning = subject_data.data_fixations(fixation_rows(1), 10);
%            coords = subject_data.data_fixations(fixation_rows(1), 11:12);
            
            subject_data.fixSum.trial(iTrial).fixData(iFix, 1) = iFix;
            subject_data.fixSum.trial(iTrial).fixData(iFix, 2) = duration_from_choice_onset/1000000;
            subject_data.fixSum.trial(iTrial).fixData(iFix, 3) = duration/1000000;
            subject_data.fixSum.trial(iTrial).fixData(iFix, 4) = location;
            subject_data.fixSum.trial(iTrial).fixData(iFix, 5) = meaning;
%            subject_data.fixSum.trial(iTrial).fixData(i_fix, 6:7) = coords;
        end
    else
        subject_data.fixSum.trial(iTrial).fixData = nan;
    end
end

