function numberedFix = f_number_fixations(trials, binaryFix)
 
% f_number_fixations   Numbers fixations from 1 to nFix, based on fixation (0 = no/1 = yes) column. 
%
%   Input:  - vector of fixation nbs (1 row per sample)
%           - vector of fixations, binary (1 = fixation, 0 = not fixation) (1 row per sample)
%
%   Output: vector of fixation numbers (1 row per sample)
%
%   Clarissa Baratin, 2020, edited Feb 2021

nTrials = max(trials);
numberedFix = zeros(length(binaryFix),1);
    
    % for each trial
    for iTrial = 1:nTrials
        
        % get trial rows
        trial_rows = find(trials == iTrial);
        
        iSamp = 1; % sample within the trial
        
        while iSamp < length(trial_rows)
            
            current_sample = trial_rows(iSamp); % sample nb for all subject data
            
            % if current sample has value of 1
            if binaryFix(current_sample) == 1
                
                % number of fixation
                fixation_nb = max(numberedFix) + 1;
                
                % keep going until current sample is not 1
                while iSamp <= length(trial_rows) && ...
                        binaryFix(current_sample) == 1
                  
                    % number the fixation
                    numberedFix(current_sample) = fixation_nb;
                   
                    iSamp = iSamp + 1;
                    current_sample = current_sample + 1;
                end                
            else
                iSamp = iSamp + 1;
                current_sample = current_sample + 1;
            end
        end
    end
end