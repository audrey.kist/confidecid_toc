function f_saveToMultipleFormats_gramm(file_path, file_name, gramm)

addpath('C:\Users\Clarissa\Documents\MATLAB\Downloads\Packages\gramm')% plotting package

extensions = {'eps','fig','png'};

if gramm == 0
    
    for k = 1:length(extensions)
        saveas(gcf, fullfile(file_path, file_name), extensions{k})
    end
    
elseif gramm == 1
    
    for k = 1:length(extensions)
        g.export('export_path', file_path, 'file_name', file_name, 'file_type', extensions{k});
    end
end
