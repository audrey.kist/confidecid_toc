function f_fixation_behav_lvl2(options, paths, subjects, stats_Lvl1)

% f_fixation_behav_lvl2    Analyzes fixation info on a group level. To be
% run from f_fixation_behav.

%% initialize paths

paths.OUT_PATH = fullfile(paths.OUT_PATH, 'lvl2_agent\');
paths.PLOTS_PATH = fullfile(paths.PLOTS_PATH, 'lvl2_agent\');

%% get useful info

nSub = length(subjects);
stats = struct(); % initialize stats structure

% max nb of trials
for iSub = 1:nSub
    nbTrials(1, iSub) = length(subjects(iSub).fixSum.trial);
end

maxNTrials = max(nbTrials);

%% ANALYSES

% Nb of fixations to gains losses as a function of utility
mean_nFixByUtil = nanmean(stats_Lvl1.prctile.bin_var_nFix, 2);

%%%% T-TESTS AGAINST 0 OF BETAS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
[h_nFixByUtil,p_nFixByUtil,ci_nFixByUtil,stats_nFixByUtil] = ttest(stats_Lvl1.glm.betas_nFix(2,:))
[h_timeAdv,p_timeAdv,ci_timeAdv,stats_timeAdv] = ttest(stats_Lvl1.glm.betas_TimeAdv(2,:))
[h_timeAdv_GLD,p_timeAdv_GLD,ci_timeAdv_GLD,stats_timeAdv_GLD] = ttest(stats_Lvl1.glm.betas_TimeAdv_controllingGLD(2,:))

%%%% PAIRED T-TESTS OF PSEs %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
plausibleVals = stats_Lvl1.PSE.lastFixGains > -40 & stats_Lvl1.PSE.lastFixGains < 40 & stats_Lvl1.PSE.lastFixLosses > -40 & stats_Lvl1.PSE.lastFixLosses < 40;
[h_PSElastFix,p_PSElastFix,ci_PSElastFix,stats_PSElastFix] = ttest(stats_Lvl1.PSE.lastFixGains(plausibleVals), stats_Lvl1.PSE.lastFixLosses(plausibleVals))
[h_PSEtimeAdv,p_PSEtimeAdv,ci_PSEtimeAdv,stats_PSEtimeAdv] = ttest(stats_Lvl1.PSE.TimeAdvG, stats_Lvl1.PSE.TimeAdvL)

[h_PSEaltlastFix,p_PSEaltlastFix,ci_PSEaltlastFix,stats_PSEaltlastFix] = ttest(stats_Lvl1.PSE_alt.ywhenxis0_lastFixGains, stats_Lvl1.PSE_alt.ywhenxis0_lastFixLosses)

%%% MIXED LINEAR MODELS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

lme_nFix = fitglme(stats_Lvl1.tbl, 'nFix ~ utilAbs + (1 | subject)')
lme2_nFix = fitglme(stats_Lvl1.tbl, 'nFix ~ utilAbs + (1 + utilAbs | subject)')

lme_choice = fitglme(stats_Lvl1.tbl, 'choice ~ gainsTimeAdv + (1 | subject)', 'Distribution', 'Binomial')
lme2_choice = fitglme(stats_Lvl1.tbl, 'choice ~ gainsTimeAdv + (1 + gainsTimeAdv | subject)', 'Distribution', 'Binomial')

lme2_choice_controlsGLD = fitglme(stats_Lvl1.tbl, 'choice ~ gainsTimeAdv + gains + losses + difficulty + (1 + gainsTimeAdv + gains + losses + difficulty | subject)', 'Distribution', 'Binomial')

lme_nFix_GLD = fitglme(stats_Lvl1.tbl, 'nFix ~ gains + losses + difficulty + (1 | subject)')

lme_nFix_G = fitglme(stats_Lvl1.tbl, 'nFixG ~ gains + losses + difficulty + (1 + gains + losses + difficulty | subject)')
lme_nFix_L = fitglme(stats_Lvl1.tbl, 'nFixL ~ gains + losses + difficulty + (1 + gains + losses + difficulty | subject)')

lme_timeG = fitglme(stats_Lvl1.tbl, 'gainsTime ~ gains + losses + difficulty + (1 + gains + losses + difficulty | subject)')
lme_timeL = fitglme(stats_Lvl1.tbl, 'lossesTime ~ gains + losses + difficulty + (1 + gains + losses + difficulty | subject)')

stats.lme.lme2_nFix = lme2_nFix;
stats.lme.lme2_choice_controlsGLD = lme2_choice_controlsGLD;

%
% tbl_nbfix = table(fixations_per_trial_binaverages(:,1),fixations_per_trial_binaverages(:,4),fixations_per_trial_binaverages(:,2),'VariableNames',{'utility','fixation_nb','subject'});
% lme_nbfix = fitlme(tbl_nbfix,'fixation_nb~utility+(1|subject)' )
%

%% PLOTS

%%%%% GENERATE PLOTS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

switch options.bin_method
    case 'nBin'     % only one implemented at the moment
        nbBins = options.nBin;
    case 'nElt'
        
    case 'prctile'
        nbBins = length(options.prctile4);
end

g1 = gramm('x', [1:nbBins], 'y', stats_Lvl1.(options.bin_method).bin_var_nFix'); % y equivalent to nanmean_RespTbyZscRating_y', but allows CI plotting
g1.geom_point();
g1.stat_summary('geom',{'point','black_errorbar'});
g1.set_names('x','|Utility|, percent rank', 'y', 'NB of fixations to gains/losses');
g1.axe_property('XTick', [1:1:nbBins], 'xticklabels', [20, 40, 60, 80, 100], 'YTick', [0:1:5]);

g8 = gramm('x', [1:nbBins], 'y', stats_Lvl1.(options.bin_method).bin_var_nFixG'); % y equivalent to nanmean_RespTbyZscRating_y', but allows CI plotting
g8.geom_point();
g8.stat_summary('geom',{'point','black_errorbar'});
g8.set_names('x','gains, percent rank', 'y', 'NB of fixations to gains');
%g8.axe_property('XTick', [1:1:nbBins], 'xticklabels', [20, 40, 60, 80, 100], 'YTick', [0:1:5]);

g9 = gramm('x', [1:nbBins], 'y', stats_Lvl1.(options.bin_method).bin_var_nFixL'); % y equivalent to nanmean_RespTbyZscRating_y', but allows CI plotting
g9.geom_point();
g9.stat_summary('geom',{'point','black_errorbar'});
g9.set_names('x','losses, percent rank', 'y', 'NB of fixations to losses');

% choice as a function of G-L, differentiating between trials where last
% fixation is to the right from those where last fix is to the left
g2 = gramm('x', [1:nbBins], 'y', vertcat(stats_Lvl1.(options.bin_method).bin_var_choice_lastfixG', stats_Lvl1.(options.bin_method).bin_var_choice_lastfixL'), 'color', [repelem(2, nSub) repelem(1, nSub)]');
g2.stat_summary('type', 'sem', 'geom', 'errorbar');
g2.set_names('x','G-L','y','P(accept)', 'color', 'Last fix to G(2) or L(1)');
g2.axe_property('XTick', [1 nbBins],'xticklabels',{'Max L', 'Max G'});

% PSE plot
% compare PSE (point of subjctive equality) for last fixation to gains/losses
% this allows us to determine wether there is a significant shift in
% logistic curves
g3 = gramm('x', [ones(1, nSub), 2*ones(1, nSub)], 'y', [stats_Lvl1.PSE.lastFixGains, stats_Lvl1.PSE.lastFixLosses], 'group', [1:nSub, 1:nSub]');
g3.geom_point();
g3.geom_line();
g3.set_names('y','PSE (= point of subjective equality, where P(accept) = 0.5)', 'x', '');
%g.set_title('Choice = no');
g3.axe_property('XLim', [0 3], 'XTick', [1 2], 'xticklabels', {'Last fix gains', 'Last fix losses'});

g7 = gramm('x', [ones(1, nSub), 2*ones(1, nSub)], 'y', [stats_Lvl1.PSE_alt.ywhenxis0_lastFixGains, stats_Lvl1.PSE_alt.ywhenxis0_lastFixLosses], 'group', [1:nSub, 1:nSub]');
g7.geom_point();
g7.geom_line();
g7.set_names('y','P(accept) when G-L = 0)', 'x', '');
%g.set_title('Choice = no');
g7.axe_property('XLim', [0 3], 'XTick', [1 2], 'xticklabels', {'Last fix gains', 'Last fix losses'});

% choice as a function of time advantage
g4 = gramm('x', [1:nbBins], 'y', stats_Lvl1.(options.bin_method).bin_var_choiceTimeAdv'); % y equivalent to nanmean_RespTbyZscRating_y', but allows CI plotting
g4.geom_point();
g4.stat_summary('geom',{'point','black_errorbar'});
g4.set_names('x','Time advantage gains', 'y', 'p(accept)');
g4.axe_property('XTick', [1:1:nbBins],'xticklabels', [1:nbBins], 'YTick', [0:1:5]);

% choice as a function of G-L, differentiating between trials where Tg > Tl
% and those where Tl > Tg
g5 = gramm('x', [1:nbBins], 'y', vertcat(stats_Lvl1.(options.bin_method).bin_var_choiceTimeAdvG', stats_Lvl1.(options.bin_method).bin_var_choiceTimeAdvL'), 'color', [repelem(2, nSub) repelem(1, nSub)]');
%g2.geom_point();
g5.stat_summary();
g5.set_names('x','G-L','y','P(accept)', 'color', 'More time looking at G(2) or L(1)');
g5.axe_property('XTick', [1 nbBins],'xticklabels',{'Max L', 'Max G'});

% PSE plot
g6 = gramm('x', [ones(1, nSub), 2*ones(1, nSub)], 'y', [stats_Lvl1.PSE.TimeAdvG, stats_Lvl1.PSE.TimeAdvL], 'group', [1:nSub, 1:nSub]');
g6.geom_point();
g6.geom_line();
g6.set_names('y','PSE (= point of subjective equality, where P(accept) = 0.5)', 'x', '');
%g.set_title('Choice = no');
g6.axe_property('XLim', [0 3], 'XTick', [1 2], 'xticklabels', {'Time adv gains', 'Time adv losses'});

%%%%% PLOT AND SAVE %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

figure('Position',[100 100 500 500]);
g1.draw();
f_saveToMultipleFormats(fullfile(paths.PLOTS_PATH, 'nFixByUtil'))

figure('Position',[100 100 800 500]);
g2.draw();
f_saveToMultipleFormats(fullfile(paths.PLOTS_PATH, 'choiceAsFunctionOfValueDiff_lastFix'))

figure('Position',[100 100 500 500]);
g3.draw();
f_saveToMultipleFormats(fullfile(paths.PLOTS_PATH, 'choiceAsFunctionOfValueDiff_lastFix_PSE'))

figure('Position',[100 100 500 500]);
g4.draw();
f_saveToMultipleFormats(fullfile(paths.PLOTS_PATH, 'choiceAsFunctionOfTimeAdv'))

figure('Position',[100 100 800 500]);
g5.draw();
f_saveToMultipleFormats(fullfile(paths.PLOTS_PATH, 'choiceAsFunctionOfValueDiff_TimeAdv'))

figure('Position',[100 100 500 500]);
g6.draw();
f_saveToMultipleFormats(fullfile(paths.PLOTS_PATH, 'choiceAsFunctionOfValueDiff_TimeAdv_PSE'))

figure('Position',[100 100 500 500]);
g7.draw();

figure('Position',[100 100 500 500]);
g8.draw();

figure('Position',[100 100 500 500]);
g9.draw();

%% save data

stats.options = options;
stats_Lvl2 = stats;

save(fullfile(paths.OUT_PATH, 'subjects.mat'), 'subjects')
save(fullfile(paths.OUT_PATH, 'stats_Lvl2.mat'), 'stats_Lvl2')

