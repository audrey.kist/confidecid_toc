
%% set paths

addpath('C:\Users\Clarissa\Documents\MATLAB\Downloads\Packages\gramm')% plotting package
paths.LOAD_DATA_PATH_LVL2 = 'C:\Users\Clarissa\Documents\decid_eye\analysis\manip1&2\1_pipeline\fixations\3_f_fixation_behav\out\lvl1_agent\';
paths.PLOTS_PATH = 'C:\Users\Clarissa\Documents\decid_eye\analysis\manip1&2\Figs';

%% load data

stats_Lvl1 = importdata(fullfile(paths.LOAD_DATA_PATH_LVL2, 'stats_Lvl1.mat'));

%% set parameters

options = stats_Lvl1.options;
nbBins = length(options.prctile4);

%% plot

g = gramm('x', [1:nbBins], 'y', stats_Lvl1.(options.bin_method).bin_var_choiceTimeAdv'); % y equivalent to nanmean_RespTbyZscRating_y', but allows CI plotting
g.set_color_options('map', 'matlab')
g.geom_jitter('alpha', 0.3)
g.stat_summary('type', 'sem', 'geom', {'point','errorbar'});
g.stat_glm('distribution', 'normal', 'geom', 'line', 'fullrange', 'true')
g.set_names('x','Time advantage towards gains, percentile', 'y', 'P(accept)');
g.axe_property('XTick', [1:1:nbBins],'xticklabels', [1:nbBins], 'YTick', [0:1:5]);

figure('Position',[100 100 400 400]);
g.draw();
f_saveToMultipleFormats(fullfile(paths.PLOTS_PATH, 'choiceAsFunctionOfTimeAdv'))

%% plot

g = gramm('x', stats_Lvl1.(options.bin_method).bin_val_choiceTimeAdv', 'y', stats_Lvl1.(options.bin_method).bin_var_choiceTimeAdv'); % y equivalent to nanmean_RespTbyZscRating_y', but allows CI plotting
g.set_color_options('map', 'matlab')
g.stat_summary('type', 'sem', 'geom', {'point','errorbar'});
g.stat_glm('distribution', 'normal', 'geom', 'line', 'fullrange', 'true')
g.set_names('x','Time advantage towards gains (%)', 'y', 'P(accept)');
%g.axe_property('XTick', [1:1:nbBins],'xticklabels', [1:nbBins], 'YTick', [0:1:5]);

figure('Position',[100 100 350 350]);
g.draw();
f_saveToMultipleFormats(fullfile(paths.PLOTS_PATH, 'choiceAsFunctionOfTimeAdv_AbsVals'))

%% plot with aDDM data

figure('Position',[100 100 400 400]);

g = gramm('x', [1:nbBins], 'y', stats_Lvl1.(options.bin_method).bin_var_choiceTimeAdv'); % y equivalent to nanmean_RespTbyZscRating_y', but allows CI plotting
g.set_color_options('map', [0.8 0.8 0.8; 0 0 0], 'n_color', 1, 'n_lightness', 1)
g.geom_jitter('alpha', 0.3)
g.stat_summary('type', 'sem', 'geom', {'point','errorbar'});
g.set_names('x','Time advantage towards gains, percentile', 'y', 'P(accept)');
g.axe_property('XTick', [1:1:nbBins],'xticklabels', [1:nbBins], 'YTick', [0:1:5]);
g.draw();

g.update('x', [1:nbBins], 'y', stats_Lvl1.(options.bin_method).bin_var_choiceTimeAdv');
g.set_color_options('map', [0.4 0.4 0.4; 0 0 0], 'n_color', 1, 'n_lightness', 1)
g.stat_summary('type', 'sem', 'geom', {'point','errorbar'});
g.draw();

g.update('x', [1:nbBins],'y',  stats_Lvl1.(options.bin_method).bin_var_choiceTimeAdv_addm')
g.set_color_options('map', [0.2 0.2 0.2; 1 1 1], 'n_color', 1, 'n_lightness', 1)
g.stat_summary('type', 'sem', 'geom', {'point','errorbar'});g.set_line_options('styles', {'--'});
g.draw();

f_saveToMultipleFormats(fullfile(paths.PLOTS_PATH, 'choiceAsFunctionOfTimeAdv_AbsVals_addm'))
