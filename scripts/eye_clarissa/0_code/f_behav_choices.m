function f_fixation_behav(anls_lvl)

% f_fixation_behav   Runs behavioural analyses for the fixations. 
%
%   Input:
%   	- analysis level: 1 for individual analyses, 2 for group analyses.
%   	  Lvl 1 must be run before lvl 2 (lvl 2 uses output saved during lvl 1). 
%
%   Output: 
%       - none. Runs function of corresponding analysis level, which saves
%         its own output struct and plots to corresponding 'out' folder.
%
% Clarissa Baratin, March 2021

%% set paths

paths.DIR = 'C:\Users\Clarissa\Documents\decid_eye\analysis\manip1&2\0_code';
paths.LOAD_DATA_PATH_LVL1 = 'C:\Users\Clarissa\Documents\decid_eye\analysis\manip1&2\1_pipeline\2_f_import_data\out#';
paths.OUT_PATH = 'C:\Users\Clarissa\Documents\decid_eye\analysis\manip1&2\1_pipeline\1_f_preprocess_data\out';
paths.PLOTS_PATH = 'C:\Users\Clarissa\Documents\decid_eye\analysis\manip1&2\1_pipeline\1_f_preprocess_data\plots';

addpath('C:\Users\Clarissa\Documents\MATLAB\Downloads\Packages\gramm')% plotting package

%% options struct

%% individual vs. multiple subjects scripts
switch anls_lvl
    case 1
        clear subjects
        subjects = load(paths.LOAD_DATA_PATH_LVL1); 
        subjects = subjects.subjects;
        f_behav_rating_lvl1(options, paths, subjects);
    case 2
        clear subjects
        close all
        subjects = load(paths.LOAD_DATA_PATH_LVL2); 
        subjects = subjects.subjects; 
        f_behav_rating_lvl2(options, paths, subjects);
end

end