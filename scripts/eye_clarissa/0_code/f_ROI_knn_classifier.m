function ROI_labels = f_ROI_knn_classifier(data)

% takes mean x/y coordinates of each fixation & returns fixation class
%
%    Input: 2 column matrix of mean x and y coordinates (1 row per fix)
%
%    Output: vector of fixation classes (1 row per fix)
%
%    for in-depth explanation of function, see ROI_classifier_fixations
%    notebook
%
%   Clarissa Baratin, 2020

%% Set ROIs based on screen ROI coordinates

rois = [0.32 0.68 0.03 0.25; 0.32 0.68 0.75 0.97; 0.20 0.30 0.44 0.55; 0.70 0.80 0.45 0.55; 0.45 0.55 0.45 0.55]; 

% make training set based on ROIs
train_set = [];
train_set_singleroi = [];
n_samp_per_roi = 100;
n_rois = length(rois);
 
X_COORD = 1;
Y_COORD = 2;
LABEL = 3;

for i_roi = 1:n_rois    
    train_set_singleroi(:, X_COORD) = rois(i_roi,1) + (rois(i_roi,2)-rois(i_roi,1))*rand(n_samp_per_roi,1); % you can generate N random numbers in the interval (a,b) with the formula r = a + (b-a).*rand(N,1).
    train_set_singleroi(:,Y_COORD) = rois(i_roi,3) + (rois(i_roi,4)-rois(i_roi,3))*rand(n_samp_per_roi,1);    
    train_set_singleroi(:,LABEL) = i_roi;
    train_set = vertcat(train_set_singleroi, train_set);
end

%% Add a reject class (geometry dependent on task)

% add a reject class to the training set for 4 lines: the 2 diagonals of
% the screen (x = y, x = 1-y) avoiding the center, a circle surrounding all 
% data and a rectangle around difficulty

border1 = 0.4;
border2 = 0.6;

line_1 = [];
line_2 = [];
line_3 = [];
line_4 = [];
n_samp_per_line = 20;

% reject class contains 4 diagonal lines
line_1(:, X_COORD) = 0 + (border1-0) * rand(n_samp_per_line,1);
line_1(:, Y_COORD) = line_1(:, X_COORD);
line_1(:, LABEL) = 6;
line_2(:, X_COORD) = border2 + (1-border2) * rand(n_samp_per_line,1);
line_2(:, Y_COORD) = line_2(:, X_COORD);
line_2(:, LABEL) = 6;
line_3(:, X_COORD) = 0 + (border1-0) * rand(n_samp_per_line,1);
line_3(:, Y_COORD) = 1-line_3(:, X_COORD);
line_3(:, LABEL) = 6;
line_4(:, X_COORD) = border2 + (1-border2) * rand(n_samp_per_line,1);
line_4(:, Y_COORD) = 1-line_4(:, X_COORD);
line_4(:, LABEL) = 6;

% a circle 
circle_1 = [];
circle_2 = [];
nb_samp_per_circle = 200;

for i_point = 1:nb_samp_per_circle
%    point = [(0.1 * cos(2 * pi * i_point / nb_samp_per_circle)) + 0.5, 0.1 * sin(2 * pi * i_point / nb_samp_per_circle) + 0.5, 6]; % Any point (x,y) on the path of the circle is x = rsin(?), y = rcos(?)
%    circle_1 = vertcat(circle_1, point);  
    point2 = [(0.5 * cos(2 * pi * i_point / nb_samp_per_circle)) + 0.5, 0.5 * sin(2 * pi * i_point / nb_samp_per_circle) + 0.5, 6];
    circle_2 = vertcat(circle_2, point2);  
end

% a rectangle 
rectangle_horizontallines = [];
rectangle_verticallines = [];
nb_samp_per_rectangle = 80;

rectangle_horizontallines(:, X_COORD) = border1 + (border2-border1) * rand(nb_samp_per_rectangle/2,1);
rectangle_horizontallines(:, Y_COORD) = repmat([border1 border2], 1, nb_samp_per_rectangle/4)';
rectangle_horizontallines(:, LABEL) = 6;

rectangle_verticallines(:, X_COORD) = repmat([border1 border2], 1, nb_samp_per_rectangle/4)';
rectangle_verticallines(:, Y_COORD) = border1 + (border2-border1) * rand(nb_samp_per_rectangle/2,1);
rectangle_verticallines(:, LABEL) = 6;

reject_class = vertcat(line_1, line_2, line_3, line_4, rectangle_horizontallines, rectangle_verticallines, circle_2);
train_set_w_reject = vertcat(train_set, reject_class);

%% Do classification

% construct classifier
Mdl2 = fitcknn(train_set_w_reject(:,X_COORD:Y_COORD),train_set_w_reject(:,LABEL),'NumNeighbors',1);

% use classifier on test set
ROI_labels = predict(Mdl2, data);

end