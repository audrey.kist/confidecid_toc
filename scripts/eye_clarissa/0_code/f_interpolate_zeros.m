function [binaryFix_zerointerp, trackloss_zerointerp, fixation_classes_zerointerp, LogInterpolatedZeros] = f_interpolate_zeros(trials, binaryFix, trackloss, fixation_nbs, fixation_classes)
 
% f_interpolate_zeros  Re-classifies zeros between 2 fixations to the same 
%   ROI as a fixation to that ROI. 
%
%   Input: - column of trial nbs (1 row per sample)
%          - column of fixations, binary (1 = fixation, 0 = not fixation) (1 row per sample)
%          - column of fixation nbs (1 row per sample)
%          - column of fixation classes (1 row per sample)
%
%   Output: - vector of fixations, binary (1 row per sample) with fixations
%           interpolated (some 0's become 1's)
%           - vector of fixation classes (1 row per sample) with fixations
%           interpolated
%           - vector of trackloss (1 row per sample) interpolated
%           
%
%   Clarissa Baratin, 2020, edited Feb 2021

fixation_classes_zerointerp = fixation_classes;
binaryFix_zerointerp = binaryFix;
trackloss_zerointerp = trackloss;

nb_trials = max(unique(trials));

nZerosInterpolated = 0;

% for each trial
for i_trial = 1:nb_trials
    
    % get subset of fixations in trial
    trial_rows = find(trials == i_trial);
    fixations_in_trial = unique(fixation_nbs(trial_rows, 1));
    fixations_in_trial = fixations_in_trial(fixations_in_trial ~= 0);
    
    % for each fixation, except last
    for i_fix = 1:length(fixations_in_trial)-1
        
        % get rows in between current and next fixations
        current_fixation_nb = fixations_in_trial(i_fix);
        next_fixation_nb = fixations_in_trial(i_fix + 1);
        
        current_fixation_last_row = find(fixation_nbs == current_fixation_nb, 1, 'last');
        next_fixation_first_row = find(fixation_nbs == next_fixation_nb, 1 );
        
        % if the current and next fixations are to the same location
        current_fixation_class = fixation_classes(current_fixation_last_row, 1);
        next_fixation_class = fixation_classes(next_fixation_first_row, 1);
        
        if current_fixation_class == next_fixation_class
            
            between_rows = (current_fixation_last_row + 1):(next_fixation_first_row - 1);
            
            binaryFix_zerointerp(between_rows, 1) = 1;
            trackloss_zerointerp(between_rows, 1) = 1;
            fixation_classes_zerointerp(between_rows, 1) = current_fixation_class;
            
            % keep log of interpolated zeros            
            nZerosInterpolated = nZerosInterpolated + 1;
            
            LogInterpolatedZeros(nZerosInterpolated).fixNb = current_fixation_nb; 
            LogInterpolatedZeros(nZerosInterpolated).nextFixNb = next_fixation_nb;
            LogInterpolatedZeros(nZerosInterpolated).rows = between_rows;
            LogInterpolatedZeros(nZerosInterpolated).fixClass = current_fixation_class;
            LogInterpolatedZeros(nZerosInterpolated).nextFixClass = next_fixation_class; 
            
        end
    end
end