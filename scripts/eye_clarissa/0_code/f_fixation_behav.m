function f_fixation_behav(anls_lvl)

% f_fixation_behav   Runs behavioural analyses for the fixations. 
%
%   Input:
%   	- analysis level: 1 for individual analyses, 2 for group analyses.
%   	  Lvl 1 must be run before lvl 2 (lvl 2 uses output saved during lvl 1). 
%
%   Output: 
%       - none. Runs function of corresponding analysis level, which saves
%         its own output struct and plots to corresponding 'out' folder.
%
% Clarissa Baratin, March 2021

%% set paths

paths.DIR = 'C:\Users\Clarissa\Documents\decid_eye\analysis\manip1&2\0_code';
paths.LOAD_DATA_PATH_LVL1 = 'C:\Users\Clarissa\Documents\decid_eye\analysis\manip1&2\1_pipeline\fixations\2_f_remove_data\out\subjects_agent.mat';
paths.LOAD_DATA_PATH_LVL2 = 'C:\Users\Clarissa\Documents\decid_eye\analysis\manip1&2\1_pipeline\fixations\3_f_fixation_behav\out\lvl1_agent\';
paths.OUT_PATH = 'C:\Users\Clarissa\Documents\decid_eye\analysis\manip1&2\1_pipeline\fixations\3_f_fixation_behav\out\';
paths.PLOTS_PATH = 'C:\Users\Clarissa\Documents\decid_eye\analysis\manip1&2\1_pipeline\fixations\3_f_fixation_behav\plots';
paths.MODEL_DATA = 'C:\Users\Clarissa\Documents\decid_eye\analysis\addm\GB\table';

addpath('C:\Users\Clarissa\Documents\MATLAB\Downloads\Packages\gramm')% plotting package
addpath('D:\project_2\analysis\teddy\code\utils_MS')

%% options struct

options.add_difficulty = 0; % 1 if we want to add diffuculty to gains/losses analyses, 0 if not
options.bin_method = 'prctile';
options.prctile = [10, 20, 30, 40, 50, 60, 70, 80, 90, 100];
options.prctile5 = [20, 40, 60, 80, 100];
options.prctile4 = [25, 50, 75, 100];
options.nBin = 4;
options.mean_med = 'mean';
options.addm = 0; %1 for addm modelling, 0 for no modelling

%% individual vs. multiple subjects scripts
switch anls_lvl
    case 1
        clear subjects
        subjects = load(paths.LOAD_DATA_PATH_LVL1); 
        subjects = subjects.subjects;
        
        if options.addm == 1
            ggmgl = table2array(readtable(fullfile(paths.MODEL_DATA, 'res_ggmgl_subj.csv'))); % ggmgl = gaze gains minus gaze losses
            ggmgl_paccept = ggmgl(:,[3:2:end]);
            ggmgl_timeAdv = ggmgl(:, 2);
            addm.ggmgl_paccept = ggmgl_paccept;
            addm.ggmgl_timeAdv = ggmgl_timeAdv;
        else
            addm = {};
        end
        
        f_fixation_behav_lvl1(options, paths, subjects, addm);
    case 2
        clear subjects
        close all
        subjects = load(fullfile(paths.LOAD_DATA_PATH_LVL2, 'subjects.mat')); 
        subjects = subjects.subjects; 
        stats_Lvl1 = load(fullfile(paths.LOAD_DATA_PATH_LVL2, 'stats_Lvl1.mat')); 
        stats_Lvl1 = stats_Lvl1.stats_Lvl1;
        f_fixation_behav_lvl2(options, paths, subjects, stats_Lvl1);
end

end