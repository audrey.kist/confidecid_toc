function f_fixation_behav_lvl1(options, paths, subjects, addm)

% f_fixation_behav_lvl1    Analyzes fixation info on an individual level.
%	Saves info to be used in f_behav_rating_lvl2. To be run from f_fixation_behav.

%% initialize paths

paths.OUT_PATH = fullfile(paths.OUT_PATH, 'lvl1_agent\');
paths.PLOTS_PATH = fullfile(paths.PLOTS_PATH, 'lvl1_agent\');

%% get useful info

nSub = length(subjects);

stats = struct(); % initialize stats structure

% max nb of trials
for iSub = 1:nSub
    nbTrials(1, iSub) = length(subjects(iSub).fixSum.trial);
end

maxNTrials = max(nbTrials);

stats.nSub = nSub;

%% initalize variables

tbl = table;

%% supblot formatting

% subplots_y = repmat([1 2 3 4 5], 1, round(nSub/5));
% a = repelem([1 2 3 4 5 6 7 8 9 10 11 12 13 14 15], 5);
% subplots_x = a(1:length(subplots_y));

subplots_y = [1 2 3 4 5 6 7 1 2 3 4 5 6 7 1 2 3 4 5 6 7 1 2 3 4 5 6 7 1 2 3 4 5 6 7 1 2 3 4 5 6 7];
subplots_x = [1 1 1 1 1 1 1 2 2 2 2 2 2 2 3 3 3 3 3 3 3 4 4 4 4 4 4 4 5 5 5 5 5 5 5 6 6 6 6 6 6 6];

%% Remove all irrelevant fixations (not to gains/losses/diff)

if options.add_difficulty == 0
    
    for iSub = 1:nSub
        
        nTrials = length(subjects(iSub).fixSum.trial);
        
        for iTrial = 1:nTrials
            
            if ~isnan(subjects(iSub).fixSum.trial(iTrial).fixData)
                
                deleteRows = subjects(iSub).fixSum.trial(iTrial).fixData(:, 4) ~= 1 & subjects(iSub).fixSum.trial(iTrial).fixData(:, 4)~= 2;
                subjects(iSub).fixSum.trial(iTrial).fixData(deleteRows, :) = [];
                
            end
            
            if isempty(subjects(iSub).fixSum.trial(iTrial).fixData) % this happens if all fixations were not to G/L
                
                subjects(iSub).fixSum.trial(iTrial).fixData = nan;
                
            end
        end
    end
    
elseif options.add_difficulty == 1
    
    for iSub = 1:nSub
        
        nTrials = length(subjects(iSub).fixSum.trial);
        
        for iTrial = 1:nTrials
            
            if ~isnan(subjects(iSub).fixSum.trial(iTrial).fixData)
                
                deleteRows = subjects(iSub).fixSum.trial(iTrial).fixData(:, 4) ~= 1 & subjects(iSub).fixSum.trial(iTrial).fixData(:, 4)~= 2 & subjects(iSub).fixSum.trial(iTrial).fixData(:, 4)~= 5;
                subjects(iSub).fixSum.trial(iTrial).fixData(deleteRows, :) = [];
                
            end
            
            if isempty(subjects(iSub).fixSum.trial(iTrial).fixData) % this happens if all fixations were not to G/L/D
                
                subjects(iSub).fixSum.trial(iTrial).fixData = nan;
                
            end
        end
    end
end

%% LAYER 1: GLMs per subject loop

for iSub = 1:nSub
    
    %%%% GET VARIABLES OF INTEREST  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    % get nb fixations for each trial in single vector
    nFix = [];
    nFixG = [];
    nFixL = [];
    lastFixClass = [];
    timeGains = [];
    timeLosses = [];
    timeDiff = [];
    gainsTimeAdv = [];
    lossTimeAdv = [];
    
    nTrials = length(subjects(iSub).fixSum.trial);
    
    for iTrial = 1:nTrials
        
        if ~isnan(subjects(iSub).fixSum.trial(iTrial).fixData) % gains/losses/diff fixations present in trial
            nFix(iTrial, 1) = size(subjects(iSub).fixSum.trial(iTrial).fixData, 1); % nb of fix to gains/losses/diff per trial
            nFixG(iTrial, 1) = size(subjects(iSub).fixSum.trial(iTrial).fixData(subjects(iSub).fixSum.trial(iTrial).fixData(:, 5) == 10), 1); % nb of fix to gains per trial
            nFixL(iTrial, 1) = size(subjects(iSub).fixSum.trial(iTrial).fixData(subjects(iSub).fixSum.trial(iTrial).fixData(:, 5) == 20), 1); % nb of fix to gains per trial
            lastFixClass(iTrial, 1) = subjects(iSub).fixSum.trial(iTrial).fixData(end, 5); % location of last fixation
            timeGains(iTrial, 1) = sum(subjects(iSub).fixSum.trial(iTrial).fixData(subjects(iSub).fixSum.trial(iTrial).fixData(:, 5) == 10, 3));
            timeLosses(iTrial, 1) = sum(subjects(iSub).fixSum.trial(iTrial).fixData(subjects(iSub).fixSum.trial(iTrial).fixData(:, 5) == 20, 3));
            timeDiff(iTrial, 1) = sum(subjects(iSub).fixSum.trial(iTrial).fixData(subjects(iSub).fixSum.trial(iTrial).fixData(:, 5) == 50, 3));
            
        else % no gains/losses/diff fixations present in trial
            if isnan(subjects(iSub).trial_characteristics(iTrial, 1)) % whole trial is nan -> nFix is NaN
                nFix(iTrial, 1) = nan; % nb of fix to gains/losses per trial
                nFixG(iTrial, 1) = nan;
                nFixL(iTrial, 1) = nan;
                lastFixClass(iTrial, 1) = nan;
                timeGains(iTrial, 1) = nan;
                timeLosses(iTrial, 1) = nan;
                timeDiff(iTrial, 1) = nan;
            else
                nFix(iTrial, 1) = 0; % trial is ok but no fix to gains/losses/diff per trial
                nFixG(iTrial, 1) = 0;
                nFixL(iTrial, 1) = 0;
                lastFixClass(iTrial, 1) = nan;
                timeGains(iTrial, 1) = nan;
                timeLosses(iTrial, 1) = nan;
                timeDiff(iTrial, 1) = nan;
            end
        end
        
        if options.add_difficulty == 0
            
            gainsTimeAdv(iTrial, 1) = ((timeGains(iTrial, 1) - timeLosses(iTrial, 1))/subjects(iSub).trial_characteristics(iTrial, 6)) * 100;
            
        elseif options.add_difficulty == 1
            
            gainsTimeAdv(iTrial, 1) = ((timeGains(iTrial, 1) - (timeLosses(iTrial, 1) + timeDiff(iTrial, 1)))/subjects(iSub).trial_characteristics(iTrial, 6)) * 100;
            lossTimeAdv(iTrial, 1) = ((timeLosses(iTrial, 1) - (timeGains(iTrial, 1) + timeDiff(iTrial, 1)))/subjects(iSub).trial_characteristics(iTrial, 6)) * 100;
            
        end
        
    end
    
    if options.add_difficulty == 0
        
        lastFixGainsRows = lastFixClass == 10;
        lastFixLossesRows = lastFixClass == 20;
        
        moreTimeLookingGains_Rows = gainsTimeAdv > 0;
        moreTimeLookingLosses_Rows = gainsTimeAdv < 0;
        
    elseif options.add_difficulty == 1
        
        lastFixGainsRows = lastFixClass == 10;
        lastFixLossesRows = lastFixClass == 20;
        lastFixLossesDiff = lastFixClass == 50;
        
        moreTimeLookingGains_Rows = gainsTimeAdv > 0;
        moreTimeLookingLosses_Rows = lossTimeAdv > 0;
    end
    
    gains = subjects(iSub).trial_characteristics(:, 2);
    losses = subjects(iSub).trial_characteristics(:, 3);
    difficulty = subjects(iSub).trial_characteristics(:, 4);
    utilAbs = abs(subjects(iSub).trial_characteristics(: ,7)); % absolute value of utility per trial
    gainsLossesDiff = gains - losses;
    choice = subjects(iSub).trial_characteristics(:, 5);
    
    %%%% MODELS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    % fit glm: nfix as a function of abs(utility)
    [betas_nFix, ~, stats_nFix] = glmfit(utilAbs, nFix, 'normal', 'identity');
    nFix_fit = glmval(betas_nFix, utilAbs, 'identity');
    
    % fit glm: nfix to G as a function of G-L value
    [betas_nFixG, ~, stats_nFixG] = glmfit(gains-losses, nFixG, 'normal', 'identity');
    nFixG_fit = glmval(betas_nFixG, gains-losses, 'identity');
    nFixG_fit_toplot = sortrows([gains-losses, nFixG_fit]); % only to make plotting fit as line easier
    
    % fit glm: nfix to L as a function of L value
    [betas_nFixL, ~, stats_nFixL] = glmfit(losses, nFixL, 'normal', 'identity');
    nFixL_fit = glmval(betas_nFixL, losses, 'identity');
    
    % logistic regression: choice as a function of G-L, for last fix to
    % gains
    [betas_gainsLossesDiff_G, ~, stats_gainsLossesDiff_G] = glmfit(gainsLossesDiff(lastFixGainsRows, 1), choice(lastFixGainsRows, 1), 'binomial', 'logit');
    gainsLossesDiff_fit_G = glmval(betas_gainsLossesDiff_G, gainsLossesDiff(lastFixGainsRows, 1), 'logit');
    gainsLossesDiff_fit_toplot_G = sortrows([gainsLossesDiff(lastFixGainsRows, 1), gainsLossesDiff_fit_G]); % only to make plotting fit as line easier
    
    % logistic regression: choice as a function of G-L, for last fix to
    % losses
    [betas_gainsLossesDiff_L, ~, stats_gainsLossesDiff_L] = glmfit(gainsLossesDiff(lastFixLossesRows, 1), choice(lastFixLossesRows, 1), 'binomial', 'logit');
    gainsLossesDiff_fit_L = glmval(betas_gainsLossesDiff_L, gainsLossesDiff(lastFixLossesRows, 1), 'logit');
    gainsLossesDiff_fit_toplot_L = sortrows([gainsLossesDiff(lastFixLossesRows, 1), gainsLossesDiff_fit_L]); % only to make plotting fit as line easier
    
    % fit glm: choice as a function of time advantage
    [betas_TimeAdv, ~, stats_TimeAdv] = glmfit(gainsTimeAdv, choice, 'normal', 'identity'); %
    TimeAdv_fit = glmval(betas_TimeAdv, gainsTimeAdv, 'identity');
    
    [betas_TimeAdv_controllingGLD, ~, stats_TimeAdv_controllingGLD] = glmfit([gainsTimeAdv, gains, losses, difficulty], choice, 'normal', 'identity'); %
    %TimeAdv_controlling_fit = glmval(betas_TimeAdv_controllingGL(1:2), gainsTimeAdv, 'identity');
    
    % logistic regression: choice as a function of G-L, for more time spent
    % looking at gains
    [betas_TimeAdvG, ~, stats_TimeAdvG] = glmfit(gainsLossesDiff(moreTimeLookingGains_Rows, 1), choice(moreTimeLookingGains_Rows, 1), 'binomial', 'logit');
    TimeAdvG_fit = glmval(betas_TimeAdvG, gainsLossesDiff(moreTimeLookingGains_Rows, 1), 'logit');
    TimeAdvG_fit_toplot = sortrows([gainsLossesDiff(moreTimeLookingGains_Rows, 1), TimeAdvG_fit]); % only to make plotting fit as line easier
    
    % logistic regression: choice as a function of G-L, for more time spent
    % looking at losses
    [betas_TimeAdvL, ~, stats_TimeAdvL] = glmfit(gainsLossesDiff(moreTimeLookingLosses_Rows, 1), choice(moreTimeLookingLosses_Rows, 1), 'binomial', 'logit');
    TimeAdvL_fit = glmval(betas_TimeAdvL, gainsLossesDiff(moreTimeLookingLosses_Rows, 1), 'logit');
    TimeAdvL_fit_toplot = sortrows([gainsLossesDiff(moreTimeLookingLosses_Rows, 1), TimeAdvL_fit]); % only to make plotting fit as line easier
    
    %%%% PSE calculation %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % calculate point of subjective equality
    
    PSE_lastFixGains = -(betas_gainsLossesDiff_G(1)/betas_gainsLossesDiff_G(2));  % if you let a + BX =0, then p = .50
    PSE_lastFixLosses = -(betas_gainsLossesDiff_L(1)/betas_gainsLossesDiff_L(2));  % if you let a + BX =0, then p = .50
    
    PSE_TimeAdvG = -(betas_TimeAdvG(1)/betas_TimeAdvG(2));  % if you let a + BX = 0, then p = .50
    PSE_TimeAdvL = -(betas_TimeAdvL(1)/betas_TimeAdvL(2));  % if you let a + BX = 0, then p = .50
    
    %%%% INSTEAD OF PSE, CALCULATE P(ACCEPT) WHEN G-L=0 %%%%%%%%%%%%%%%%%%%
    
    ywhenxis0_lastFixGains = glmval(betas_gainsLossesDiff_G, 0, 'logit');
    ywhenxis0_lastFixLosses = glmval(betas_gainsLossesDiff_L, 0, 'logit');
    
    %%%% PLOTS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    subplot_x = subplots_x(iSub);
    subplot_y = subplots_y(iSub);
    
    g1(subplot_x,subplot_y) = gramm('x', utilAbs, 'y', nFix_fit);
    g1(subplot_x,subplot_y).geom_line();
    g1(subplot_x,subplot_y).set_title(sprintf('Subject %d', iSub));
    g1(subplot_x,subplot_y).set_names('x', '|Utility|', 'y', 'nFix to G/L');
    % g1(subplot_x,subplot_y).axe_property('YLim', [0 10]);
    
    g8(subplot_x,subplot_y) = gramm('x', nFixG_fit_toplot(:, 1), 'y', nFixG_fit_toplot(:, 2));
    g8(subplot_x,subplot_y).geom_line();
    g8(subplot_x,subplot_y).set_title(sprintf('Subject %d', iSub));
    g8(subplot_x,subplot_y).set_names('x', 'gains-losses', 'y', 'nFix to G');
    
    g9(subplot_x,subplot_y) = gramm('x', losses, 'y', nFixL_fit);
    g9(subplot_x,subplot_y).geom_line();
    g9(subplot_x,subplot_y).set_title(sprintf('Subject %d', iSub));
    g9(subplot_x,subplot_y).set_names('x', 'losses', 'y', 'nFix to L');
    
    g2(subplot_x,subplot_y) = gramm('x', gainsLossesDiff_fit_toplot_G(:, 1), 'y', gainsLossesDiff_fit_toplot_G(:, 2));
    g2(subplot_x,subplot_y).geom_line();
    g2(subplot_x,subplot_y).set_title(sprintf('Subject %d, last fix G', iSub));
    g2(subplot_x,subplot_y).set_names('x', 'G - L', 'y', 'Choice');
    g2(subplot_x,subplot_y).axe_property('YLim', [0 1]);
    
    g3(subplot_x,subplot_y) = gramm('x', gainsLossesDiff_fit_toplot_L(:, 1), 'y', gainsLossesDiff_fit_toplot_L(:, 2));
    g3(subplot_x,subplot_y).geom_line();
    g3(subplot_x,subplot_y).set_title(sprintf('Subject %d, last fix L', iSub));
    g3(subplot_x,subplot_y).set_names('x', 'G - L', 'y', 'Choice');
    g3(subplot_x,subplot_y).axe_property('YLim', [0 1]);
    
    g4(subplot_x,subplot_y) = gramm('x', gainsTimeAdv, 'y', TimeAdv_fit);
    g4(subplot_x,subplot_y).geom_line();
    g4(subplot_x,subplot_y).set_title(sprintf('Subject %d', iSub));
    g4(subplot_x,subplot_y).set_names('x', 'Gains time advantage', 'y', 'Choice');
    g4(subplot_x,subplot_y).axe_property('YLim', [0 1]);
    
    %     g5(subplot_x,subplot_y) = gramm('x', gainsTimeAdv, 'y', TimeAdv_controlling_fit);
    %     g5(subplot_x,subplot_y).geom_line();
    %     g5(subplot_x,subplot_y).set_title(sprintf('Subject %d', iSub));
    %     g5(subplot_x,subplot_y).set_names('x', 'Gains time advantage', 'y', 'Choice, controlling for G, L, G-L');
    %     g5(subplot_x,subplot_y).axe_property('YLim', [0 1]);
    
    g6(subplot_x,subplot_y) = gramm('x', TimeAdvG_fit_toplot(:, 1), 'y', TimeAdvG_fit_toplot(:, 2));
    g6(subplot_x,subplot_y).geom_line();
    g6(subplot_x,subplot_y).set_title(sprintf('Subject %d, time adv G', iSub));
    g6(subplot_x,subplot_y).set_names('x', 'G - L', 'y', 'Choice');
    g6(subplot_x,subplot_y).axe_property('YLim', [0 1]);
    
    g7(subplot_x,subplot_y) = gramm('x', TimeAdvL_fit_toplot(:, 1), 'y', TimeAdvL_fit_toplot(:, 2));
    g7(subplot_x,subplot_y).geom_line();
    g7(subplot_x,subplot_y).set_title(sprintf('Subject %d, time adv L', iSub));
    g7(subplot_x,subplot_y).set_names('x', 'G - L', 'y', 'Choice');
    g7(subplot_x,subplot_y).axe_property('YLim', [0 1]);
    
    %%%% SAVE STATS CALCULATED PER SUBJECT %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    stats.glm.glmval_nFix = nan(maxNTrials, nSub);
    stats.glm.glmval_gainsLossesDiff_G = nan(maxNTrials, nSub);
    stats.glm.glmval_gainsLossesDiff_L = nan(maxNTrials, nSub);
    stats.glm.glmval_TimeAdv = nan(maxNTrials, nSub);
    stats.glm.glmval_TimeAdv_controllingGL = nan(maxNTrials, nSub);
    stats.glm.glmval_TimeAdvG = nan(maxNTrials, nSub);
    stats.glm.glmval_TimeAdvL = nan(maxNTrials, nSub);
    
    stats.glm.betas_nFix(:, iSub) = betas_nFix;
    stats.glm.stats_nFix(:, iSub) = stats_nFix;
    stats.glm.glmval_nFix(1:length(nFix_fit), iSub) = nFix_fit;
    
    stats.glm.betas_nFixG(:, iSub) = betas_nFixG;
    stats.glm.stats_nFixG(:, iSub) = stats_nFixG;
    stats.glm.glmval_nFixG(1:length(nFixG_fit), iSub) = nFixG_fit;
    
    stats.glm.betas_nFixL(:, iSub) = betas_nFixL;
    stats.glm.stats_nFixL(:, iSub) = stats_nFixL;
    stats.glm.glmval_nFixL(1:length(nFixL_fit), iSub) = nFixL_fit;
    
    stats.glm.betas_gainsLossesDiff_G(:, iSub) = betas_gainsLossesDiff_G;
    stats.glm.stats_gainsLossesDiff_G(:, iSub) = stats_gainsLossesDiff_G;
    stats.glm.glmval_gainsLossesDiff_G(1:length(gainsLossesDiff_fit_G), iSub) = gainsLossesDiff_fit_G;
    
    stats.glm.betas_gainsLossesDiff_L(:, iSub) = betas_gainsLossesDiff_L;
    stats.glm.stats_gainsLossesDiff_L(:, iSub) = stats_gainsLossesDiff_L;
    stats.glm.glmval_gainsLossesDiff_L(1:length(gainsLossesDiff_fit_L), iSub) = gainsLossesDiff_fit_L;
    
    stats.glm.betas_TimeAdv(:, iSub) = betas_TimeAdv;
    stats.glm.stats_TimeAdv(:, iSub) = stats_TimeAdv;
    stats.glm.glmval_TimeAdv(1:length(TimeAdv_fit), iSub) = TimeAdv_fit;
    
    stats.glm.betas_TimeAdv_controllingGLD(:, iSub) = betas_TimeAdv_controllingGLD;
    stats.glm.stats_TimeAdv_controllingGLD(:, iSub) = stats_TimeAdv_controllingGLD;
    %     stats.glm.glmval_TimeAdv_controllingGL(1:length(TimeAdv_controlling_fit), iSub) = TimeAdv_controlling_fit;
    
    stats.glm.betas_TimeAdvG(:, iSub) = betas_TimeAdvG;
    stats.glm.stats_TimeAdvG(:, iSub) = stats_TimeAdvG;
    stats.glm.glmval_TimeAdvG(1:length(TimeAdvG_fit), iSub) = TimeAdvG_fit;
    
    stats.glm.betas_TimeAdvL(:, iSub) = betas_TimeAdvL;
    stats.glm.stats_TimeAdvL(:, iSub) = stats_TimeAdvL;
    stats.glm.glmval_TimeAdvL(1:length(TimeAdvL_fit), iSub) = TimeAdvL_fit;
    
    stats.PSE.lastFixGains(:, iSub) = PSE_lastFixGains;
    stats.PSE.lastFixLosses(:, iSub) = PSE_lastFixLosses;
    
    stats.PSE.TimeAdvG(:, iSub) = PSE_TimeAdvG;
    stats.PSE.TimeAdvL(:, iSub) = PSE_TimeAdvL;
    
    stats.PSE_alt.ywhenxis0_lastFixGains(:, iSub) = ywhenxis0_lastFixGains;
    stats.PSE_alt.ywhenxis0_lastFixLosses(:, iSub) = ywhenxis0_lastFixLosses;
    
end

figure('Position', [100 100 1500 1000]);
g1.draw();

figure('Position', [100 100 1500 1000]);
g2.draw();

figure('Position', [100 100 1500 1000]);
g3.draw();

figure('Position', [100 100 1500 1000]);
g4.draw();

% figure('Position', [100 100 1500 1000]);
% g5.draw();

figure('Position', [100 100 1500 1000]);
g6.draw();

figure('Position', [100 100 1500 1000]);
g7.draw();

figure('Position', [100 100 1500 1000]);
g8.draw();

figure('Position', [100 100 1500 1000]);
g9.draw();


%% LAYER 2: Raw data per subject loop

for iSub = 1:nSub
    
    %%%% GET VARIABLES OF INTEREST  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    % get nb fixations for each trial in single vector
    nFix = [];
    nFixG = [];
    nFixL = [];
    lastFixClass = [];
    timeGains = [];
    timeLosses = [];
    timeDiff = [];
    gainsTimeAdv = [];
    lossTimeAdv = [];
    
    nTrials = length(subjects(iSub).fixSum.trial);
    
    for iTrial = 1:nTrials
        
        if ~isnan(subjects(iSub).fixSum.trial(iTrial).fixData) % gains/losses/diff fixations present in trial
            nFix(iTrial, 1) = size(subjects(iSub).fixSum.trial(iTrial).fixData, 1); % nb of fix to gains/losses/diff per trial
            nFixG(iTrial, 1) = size(subjects(iSub).fixSum.trial(iTrial).fixData(subjects(iSub).fixSum.trial(iTrial).fixData(:, 5) == 10), 1); % nb of fix to gains per trial
            nFixL(iTrial, 1) = size(subjects(iSub).fixSum.trial(iTrial).fixData(subjects(iSub).fixSum.trial(iTrial).fixData(:, 5) == 20), 1); % nb of fix to gains per trial
            lastFixClass(iTrial, 1) = subjects(iSub).fixSum.trial(iTrial).fixData(end, 5); % location of last fixation
            timeGains(iTrial, 1) = sum(subjects(iSub).fixSum.trial(iTrial).fixData(subjects(iSub).fixSum.trial(iTrial).fixData(:, 5) == 10, 3));
            timeLosses(iTrial, 1) = sum(subjects(iSub).fixSum.trial(iTrial).fixData(subjects(iSub).fixSum.trial(iTrial).fixData(:, 5) == 20, 3));
            timeDiff(iTrial, 1) = sum(subjects(iSub).fixSum.trial(iTrial).fixData(subjects(iSub).fixSum.trial(iTrial).fixData(:, 5) == 50, 3));
            
        else % no gains/losses/diff fixations present in trial
            if isnan(subjects(iSub).trial_characteristics(iTrial, 1)) % whole trial is nan -> nFix is NaN
                nFix(iTrial, 1) = nan; % nb of fix to gains/losses per trial
                nFixG(iTrial, 1) = nan;
                nFixL(iTrial, 1) = nan;
                lastFixClass(iTrial, 1) = nan;
                timeGains(iTrial, 1) = nan;
                timeLosses(iTrial, 1) = nan;
                timeDiff(iTrial, 1) = nan;
            else
                nFix(iTrial, 1) = 0; % trial is ok but no fix to gains/losses/diff per trial
                nFixG(iTrial, 1) = 0;
                nFixL(iTrial, 1) = 0;
                lastFixClass(iTrial, 1) = nan;
                timeGains(iTrial, 1) = nan;
                timeLosses(iTrial, 1) = nan;
                timeDiff(iTrial, 1) = nan;
            end
        end
        
        if options.add_difficulty == 0
            
            gainsTimeAdv(iTrial, 1) = ((timeGains(iTrial, 1) - timeLosses(iTrial, 1))/subjects(iSub).trial_characteristics(iTrial, 6)) * 100;
            
        elseif options.add_difficulty == 1
            
            gainsTimeAdv(iTrial, 1) = ((timeGains(iTrial, 1) - (timeLosses(iTrial, 1) + timeDiff(iTrial, 1)))/subjects(iSub).trial_characteristics(iTrial, 6)) * 100;
            lossTimeAdv(iTrial, 1) = ((timeLosses(iTrial, 1) - (timeGains(iTrial, 1) + timeDiff(iTrial, 1)))/subjects(iSub).trial_characteristics(iTrial, 6)) * 100;
            
        end
        
    end
    
    if options.add_difficulty == 0
        
        lastFixGainsRows = lastFixClass == 10;
        lastFixLossesRows = lastFixClass == 20;
        
        moreTimeLookingGains_Rows = gainsTimeAdv > 0;
        moreTimeLookingLosses_Rows = gainsTimeAdv < 0;
        
    elseif options.add_difficulty == 1
        
        lastFixGainsRows = lastFixClass == 10;
        lastFixLossesRows = lastFixClass == 20;
        lastFixLossesDiff = lastFixClass == 50;
        
        moreTimeLookingGains_Rows = gainsTimeAdv > 0;
        moreTimeLookingLosses_Rows = lossTimeAdv > 0;
    end
    
    gains = subjects(iSub).trial_characteristics(:, 2);
    losses = subjects(iSub).trial_characteristics(:, 3);
    difficulty = subjects(iSub).trial_characteristics(:, 4);
    utilAbs = abs(subjects(iSub).trial_characteristics(: ,7)); % absolute value of utility per trial
    gainsLossesDiff = gains - losses;
    choice = subjects(iSub).trial_characteristics(:, 5);
    
    %% get data for mixed linear models later on
    
    subject_col{1, iSub} = repelem(iSub, nTrials);
    nFix_col{1, iSub} = nFix';
    nFixG_col{1, iSub} = nFixG';
    nFixL_col{1, iSub} = nFixL';
    utilAbs_col{1, iSub} = utilAbs';
    choice_col{1, iSub} = choice';
    gainsTime_col{1, iSub} = timeGains';
    lossesTime_col{1, iSub} = timeLosses';
    gainsTimeAdv_col{1, iSub} = gainsTimeAdv';
    gains_col{1, iSub} = gains';
    losses_col{1, iSub} = losses';
    difficulty_col{1, iSub} = difficulty';
    
    %% PLOTS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    % plots
    subplot_x = subplots_x(iSub);
    subplot_y = subplots_y(iSub);
    
    g1(subplot_x,subplot_y).update('x', utilAbs, 'y', nFix);
    g1(subplot_x,subplot_y).geom_point();
    g1(subplot_x,subplot_y).set_point_options('base_size', 2);
    g1(subplot_x,subplot_y).set_color_options('chroma', 0, 'lightness', 0);
    
    g2(subplot_x,subplot_y).update('x', gainsLossesDiff(lastFixGainsRows, 1), 'y', choice(lastFixGainsRows, 1));
    g2(subplot_x,subplot_y).geom_point();
    g2(subplot_x,subplot_y).set_point_options('base_size', 2);
    g2(subplot_x,subplot_y).set_color_options('chroma', 0, 'lightness', 0);
    
    g3(subplot_x,subplot_y).update('x', gainsLossesDiff(lastFixLossesRows, 1), 'y', choice(lastFixLossesRows, 1));
    g3(subplot_x,subplot_y).geom_point();
    g3(subplot_x,subplot_y).set_point_options('base_size', 2);
    g3(subplot_x,subplot_y).set_color_options('chroma', 0, 'lightness', 0);
    
    g4(subplot_x,subplot_y).update('x', gainsTimeAdv, 'y', choice);
    g4(subplot_x,subplot_y).geom_point();
    g4(subplot_x,subplot_y).set_point_options('base_size', 2);
    g4(subplot_x,subplot_y).set_color_options('chroma', 0, 'lightness', 0);
    
    %     g5(subplot_x,subplot_y).update('x', gainsTimeAdv, 'y', choice); % how to plot raw data when controlling for other factors?
    %     g5(subplot_x,subplot_y).geom_point();
    %     g5(subplot_x,subplot_y).set_point_options('base_size', 2);
    %     g5(subplot_x,subplot_y).set_color_options('chroma', 0, 'lightness', 0);
    
    g6(subplot_x,subplot_y).update('x', gainsLossesDiff(moreTimeLookingGains_Rows, 1), 'y', choice(moreTimeLookingGains_Rows, 1));
    g6(subplot_x,subplot_y).geom_point();
    g6(subplot_x,subplot_y).set_point_options('base_size', 2);
    g6(subplot_x,subplot_y).set_color_options('chroma', 0, 'lightness', 0);
    
    g7(subplot_x,subplot_y).update('x', gainsLossesDiff(moreTimeLookingLosses_Rows, 1), 'y', choice(moreTimeLookingLosses_Rows, 1));
    g7(subplot_x,subplot_y).geom_point();
    g7(subplot_x,subplot_y).set_point_options('base_size', 2);
    g7(subplot_x,subplot_y).set_color_options('chroma', 0, 'lightness', 0);
    
    g8(subplot_x,subplot_y).update('x', gains-losses, 'y', nFixG);
    g8(subplot_x,subplot_y).geom_point();
    g8(subplot_x,subplot_y).set_point_options('base_size', 2);
    g8(subplot_x,subplot_y).set_color_options('chroma', 0, 'lightness', 0);
    
    g9(subplot_x,subplot_y).update('x', losses, 'y', nFixL);
    g9(subplot_x,subplot_y).geom_point();
    g9(subplot_x,subplot_y).set_point_options('base_size', 2);
    g9(subplot_x,subplot_y).set_color_options('chroma', 0, 'lightness', 0);
    
end

g1.draw();
f_saveToMultipleFormats(fullfile(paths.PLOTS_PATH, 'nFixByUtil'))

g2.draw();
f_saveToMultipleFormats(fullfile(paths.PLOTS_PATH, 'choiceByValueDiff_LastFixG'))

g3.draw();
f_saveToMultipleFormats(fullfile(paths.PLOTS_PATH, 'choiceByValueDiff_LastFixL'))

g4.draw();
f_saveToMultipleFormats(fullfile(paths.PLOTS_PATH, 'choiceByGainsTimeAdv'))

% g5.draw();
% f_saveToMultipleFormats(fullfile(paths.PLOTS_PATH, 'choiceByGainsTimeAdv_controllingForG_L_G-L'))

g6.draw();
f_saveToMultipleFormats(fullfile(paths.PLOTS_PATH, 'choiceByValueDiff_moreTimeLookingGains'))

g7.draw();
f_saveToMultipleFormats(fullfile(paths.PLOTS_PATH, 'choiceByValueDiff_moreTimeLookingLosses'))

g8.draw();
f_saveToMultipleFormats(fullfile(paths.PLOTS_PATH, 'nFixGByGains'))

g9.draw();
f_saveToMultipleFormats(fullfile(paths.PLOTS_PATH, 'nFixLByLosses'))

%% Bin data per subject; for presentation purposes later on

switch options.bin_method
    case 'nBin'
        f_bin = @(x,y,z,a) f_bin_nBinOpt(x,y,z,a);
        input = options.nBin;
    case 'nElt' % not implemented at the moment
        f_bin = @(x,y,z,a) f_bin_nElt(x,y,z,a);
        input = options.n_eltBin;
    case 'prctile'
        f_bin = @(x,y,z,a) f_bin_prctile(x,y,z,a);
        input = options.prctile;
        input5 = options.prctile5;
        input4 = options.prctile4;
end

for iSub = 1:nSub  % bin data per subject loop (doing this inside another subject loop allows to superpose(update) plots with those from previous loop if wanted)
    
    %%%% GET VARIABLES OF INTEREST  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    % get nb fixations for each trial in single vector
    nFix = [];
    nFixG = [];
    nFixL = [];
    lastFixClass = [];
    timeGains = [];
    timeLosses = [];
    timeDiff = [];
    gainsTimeAdv = [];
    lossTimeAdv = [];
    
    nTrials = length(subjects(iSub).fixSum.trial);
    
    for iTrial = 1:nTrials
        
        if ~isnan(subjects(iSub).fixSum.trial(iTrial).fixData) % gains/losses/diff fixations present in trial
            nFix(iTrial, 1) = size(subjects(iSub).fixSum.trial(iTrial).fixData, 1); % nb of fix to gains/losses/diff per trial
            nFixG(iTrial, 1) = size(subjects(iSub).fixSum.trial(iTrial).fixData(subjects(iSub).fixSum.trial(iTrial).fixData(:, 5) == 10), 1); % nb of fix to gains per trial
            nFixL(iTrial, 1) = size(subjects(iSub).fixSum.trial(iTrial).fixData(subjects(iSub).fixSum.trial(iTrial).fixData(:, 5) == 20), 1); % nb of fix to gains per trial
            lastFixClass(iTrial, 1) = subjects(iSub).fixSum.trial(iTrial).fixData(end, 5); % location of last fixation
            timeGains(iTrial, 1) = sum(subjects(iSub).fixSum.trial(iTrial).fixData(subjects(iSub).fixSum.trial(iTrial).fixData(:, 5) == 10, 3));
            timeLosses(iTrial, 1) = sum(subjects(iSub).fixSum.trial(iTrial).fixData(subjects(iSub).fixSum.trial(iTrial).fixData(:, 5) == 20, 3));
            timeDiff(iTrial, 1) = sum(subjects(iSub).fixSum.trial(iTrial).fixData(subjects(iSub).fixSum.trial(iTrial).fixData(:, 5) == 50, 3));
            
        else % no gains/losses/diff fixations present in trial
            if isnan(subjects(iSub).trial_characteristics(iTrial, 1)) % whole trial is nan -> nFix is NaN
                nFix(iTrial, 1) = nan; % nb of fix to gains/losses per trial
                nFixG(iTrial, 1) = nan;
                nFixL(iTrial, 1) = nan;
                lastFixClass(iTrial, 1) = nan;
                timeGains(iTrial, 1) = nan;
                timeLosses(iTrial, 1) = nan;
                timeDiff(iTrial, 1) = nan;
            else
                nFix(iTrial, 1) = 0; % trial is ok but no fix to gains/losses/diff per trial
                nFixG(iTrial, 1) = 0;
                nFixL(iTrial, 1) = 0;
                lastFixClass(iTrial, 1) = nan;
                timeGains(iTrial, 1) = nan;
                timeLosses(iTrial, 1) = nan;
                timeDiff(iTrial, 1) = nan;
            end
        end
        
        if options.add_difficulty == 0
            
            gainsTimeAdv(iTrial, 1) = ((timeGains(iTrial, 1) - timeLosses(iTrial, 1))/subjects(iSub).trial_characteristics(iTrial, 6)) * 100;
            
        elseif options.add_difficulty == 1
            
            gainsTimeAdv(iTrial, 1) = ((timeGains(iTrial, 1) - (timeLosses(iTrial, 1) + timeDiff(iTrial, 1)))/subjects(iSub).trial_characteristics(iTrial, 6)) * 100;
            lossTimeAdv(iTrial, 1) = ((timeLosses(iTrial, 1) - (timeGains(iTrial, 1) + timeDiff(iTrial, 1)))/subjects(iSub).trial_characteristics(iTrial, 6)) * 100;
            
        end
        
    end
    
    if options.add_difficulty == 0
        
        lastFixGainsRows = lastFixClass == 10;
        lastFixLossesRows = lastFixClass == 20;
        
        moreTimeLookingGains_Rows = gainsTimeAdv > 0;
        moreTimeLookingLosses_Rows = gainsTimeAdv < 0;
        
    elseif options.add_difficulty == 1
        
        lastFixGainsRows = lastFixClass == 10;
        lastFixLossesRows = lastFixClass == 20;
        lastFixLossesDiff = lastFixClass == 50;
        
        moreTimeLookingGains_Rows = gainsTimeAdv > 0;
        moreTimeLookingLosses_Rows = lossTimeAdv > 0;
    end
    
    gains = subjects(iSub).trial_characteristics(:, 2);
    losses = subjects(iSub).trial_characteristics(:, 3);
    difficulty = subjects(iSub).trial_characteristics(:, 4);
    utilAbs = abs(subjects(iSub).trial_characteristics(: ,7)); % absolute value of utility per trial
    gainsLossesDiff = gains - losses;
    choice = subjects(iSub).trial_characteristics(:, 5);
    
    %%%% BIN DATA %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    % NOTE: binning choices (0s and 1s) and getting their average is the
    % same as getting the probability of accepting
    
    % bin nbfix as a function of utility
    [bin_var_nFix, bin_val_nFix] = f_bin(options.mean_med, normalize(nFix), utilAbs, input4);
    
    % bin nbfixG as a function of gains
    [bin_var_nFixG, bin_val_nFixG] = f_bin(options.mean_med, normalize(nFixG), gains, input4);
    
    % bin nbfixL as a function of losses
    [bin_var_nFixL, bin_val_nFixL] = f_bin(options.mean_med, normalize(nFixL), losses, input4);
    
    % bin choice as a function of value diff, last fix gains
    [bin_var_choice_lastfixG, bin_val_choice_lastfixG] = f_bin(options.mean_med, choice(lastFixGainsRows, 1), gainsLossesDiff(lastFixGainsRows, 1), input);
    
    % bin choice as a function of value diff, last fix losses
    [bin_var_choice_lastfixL, bin_val_choice_lastfixL] = f_bin(options.mean_med, choice(lastFixLossesRows, 1), gainsLossesDiff(lastFixLossesRows, 1), input);
    
    % bin choice as a function of time advantage
    [bin_var_choiceTimeAdv, bin_val_choiceTimeAdv] = f_bin(options.mean_med, choice, gainsTimeAdv, input4);
    
    % bin choice as a function of value diff, more time spent looking at G
    [bin_var_choiceTimeAdvG, bin_val_choiceTimeAdvG] = f_bin(options.mean_med, choice(moreTimeLookingGains_Rows, 1), gainsLossesDiff(moreTimeLookingGains_Rows, 1), input);
    
    % bin choice as a function of value diff, more time spent looking at L
    [bin_var_choiceTimeAdvL, bin_val_choiceTimeAdvL] = f_bin(options.mean_med, choice(moreTimeLookingLosses_Rows, 1), gainsLossesDiff(moreTimeLookingLosses_Rows, 1), input);
    
    if options.addm == 1
        
        % bin p_accept as a function of time advantage
        [bin_var_choiceTimeAdv_addm, bin_val_choiceTimeAdv_addm] = f_bin(options.mean_med, addm.ggmgl_paccept(:, iSub), addm.ggmgl_timeAdv, input4);
        
    end
    
    %%%% SAVE STATS CALCULATED PER SUBJECT %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    stats.(options.bin_method).bin_var_nFix(:, iSub) = bin_var_nFix(:,1);
    stats.(options.bin_method).bin_val_nFix(:, iSub) = bin_val_nFix(:,1);
    
    stats.(options.bin_method).bin_var_nFixG(:, iSub) = bin_var_nFixG(:,1);
    stats.(options.bin_method).bin_val_nFixG(:, iSub) = bin_val_nFixG(:,1);
    
    stats.(options.bin_method).bin_var_nFixL(:, iSub) = bin_var_nFixL(:,1);
    stats.(options.bin_method).bin_val_nFixL(:, iSub) = bin_val_nFixL(:,1);
    
    stats.(options.bin_method).bin_var_choice_lastfixG(:, iSub) = bin_var_choice_lastfixG(:,1);
    stats.(options.bin_method).bin_val_choice_lastfixG(:, iSub) = bin_val_choice_lastfixG(:,1);
    
    stats.(options.bin_method).bin_var_choice_lastfixL(:, iSub) = bin_var_choice_lastfixL(:,1);
    stats.(options.bin_method).bin_val_choice_lastfixL(:, iSub) = bin_val_choice_lastfixL(:,1);
    
    stats.(options.bin_method).bin_var_choiceTimeAdv(:, iSub) = bin_var_choiceTimeAdv(:,1);
    stats.(options.bin_method).bin_val_choiceTimeAdv(:, iSub) = bin_val_choiceTimeAdv(:,1);
    
    stats.(options.bin_method).bin_var_choiceTimeAdvG(:, iSub) = bin_var_choiceTimeAdvG(:,1);
    stats.(options.bin_method).bin_val_choiceTimeAdvG(:, iSub) = bin_val_choiceTimeAdvG(:,1);
    
    stats.(options.bin_method).bin_var_choiceTimeAdvL(:, iSub) = bin_var_choiceTimeAdvL(:,1);
    stats.(options.bin_method).bin_val_choiceTimeAdvL(:, iSub) = bin_val_choiceTimeAdvL(:,1);
    
    if options.addm == 1
        stats.(options.bin_method).bin_var_choiceTimeAdv_addm(:, iSub) = bin_var_choiceTimeAdv_addm(:,1);
        stats.(options.bin_method).bin_val_choiceTimeAdv_addm(:, iSub) = bin_val_choiceTimeAdv_addm(:,1);
    end
    
end

%% make table for mixed models

stats.tbl = table([subject_col{:}]', [nFix_col{:}]', [nFixG_col{:}]', [nFixL_col{:}]', [utilAbs_col{:}]', [choice_col{:}]', [gainsTime_col{:}]', [lossesTime_col{:}]', [gainsTimeAdv_col{:}]', [gains_col{:}]', [losses_col{:}]', [difficulty_col{:}]', 'VariableNames',{'subject', 'nFix', 'nFixG', 'nFixL', 'utilAbs', 'choice', 'gainsTime', 'lossesTime', 'gainsTimeAdv', 'gains', 'losses', 'difficulty'});

%% save data

stats.options = options;
stats_Lvl1 = stats;

save(fullfile(paths.OUT_PATH, 'subjects.mat'), 'subjects')
save(fullfile(paths.OUT_PATH, 'stats_Lvl1.mat'), 'stats_Lvl1')
