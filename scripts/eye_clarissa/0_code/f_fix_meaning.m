function meaning = f_fix_meaning(trial_nb, trial_type, ROI_looked_at, manip)

% what_you_looking_at
%
%    Takes the trial type (display code of gains/losses/yes/no)
%    and gaze position (ROI looked at) and returns which of the
%    gains/losses/yes/no is being looked at.
%
% edited june 2020 to add difficulty

meaning = zeros(length(trial_nb),1);

switch manip
    
    case 1
        for x = 1:length(trial_nb)
            if trial_nb(x,1) == 0
                meaning(x,1) = 0;
            elseif trial_type(trial_nb(x,1),1) == 70 && ROI_looked_at(x,1) == 1 || trial_type(trial_nb(x,1),1) == 71 && ROI_looked_at(x,1) == 1 ||trial_type(trial_nb(x,1),1) == 72 && ROI_looked_at(x,1) == 2 || trial_type(trial_nb(x,1),1) == 73 && ROI_looked_at(x,1) == 2
                meaning(x,1) = 10;
            elseif trial_type(trial_nb(x,1),1) == 70 && ROI_looked_at(x,1) == 2 || trial_type(trial_nb(x,1),1) == 71 && ROI_looked_at(x,1) == 2 || trial_type(trial_nb(x,1),1) == 72 && ROI_looked_at(x,1) == 1 || trial_type(trial_nb(x,1),1) == 73 && ROI_looked_at(x,1) == 1
                meaning(x,1) = 20;
            elseif trial_type(trial_nb(x,1),1) == 70 && ROI_looked_at(x,1) == 3 || trial_type(trial_nb(x,1),1) == 71 && ROI_looked_at(x,1) == 4 || trial_type(trial_nb(x,1),1) == 72 && ROI_looked_at(x,1) == 3 || trial_type(trial_nb(x,1),1) == 73 && ROI_looked_at(x,1) == 4
                meaning(x,1) = 30;
            elseif trial_type(trial_nb(x,1),1) == 70 && ROI_looked_at(x,1) == 4 || trial_type(trial_nb(x,1),1) == 71 && ROI_looked_at(x,1) == 3 || trial_type(trial_nb(x,1),1) == 72 && ROI_looked_at(x,1) == 4 || trial_type(trial_nb(x,1),1) == 73 && ROI_looked_at(x,1) == 3
                meaning(x,1) = 40;
            elseif ROI_looked_at(x,1) == 5
                meaning(x,1) = 50;
            else
                meaning(x,1) = 0;
            end
        end
        
    case 2        
        for x = 1:length(trial_nb)
            if trial_nb(x,1) == 0
                meaning(x,1) = 0;
            elseif (trial_type(trial_nb(x,1),1) == 10 || trial_type(trial_nb(x,1),1) == 20) && ROI_looked_at(x,1) == 1 || (trial_type(trial_nb(x,1),1) == 11 || trial_type(trial_nb(x,1),1) == 21) && ROI_looked_at(x,1) == 1 || (trial_type(trial_nb(x,1),1) == 12 || trial_type(trial_nb(x,1),1) == 22) && ROI_looked_at(x,1) == 2 || (trial_type(trial_nb(x,1),1) == 13 || trial_type(trial_nb(x,1),1) == 23) && ROI_looked_at(x,1) == 2
                meaning(x,1) = 10;
            elseif (trial_type(trial_nb(x,1),1) == 10 || trial_type(trial_nb(x,1),1) == 20) && ROI_looked_at(x,1) == 2 || (trial_type(trial_nb(x,1),1) == 11 || trial_type(trial_nb(x,1),1) == 21) && ROI_looked_at(x,1) == 2 || (trial_type(trial_nb(x,1),1) == 12 || trial_type(trial_nb(x,1),1) == 22) && ROI_looked_at(x,1) == 1 || (trial_type(trial_nb(x,1),1) == 13 || trial_type(trial_nb(x,1),1) == 23) && ROI_looked_at(x,1) == 1
                meaning(x,1) = 20;
            elseif (trial_type(trial_nb(x,1),1) == 10 || trial_type(trial_nb(x,1),1) == 20) && ROI_looked_at(x,1) == 3 || (trial_type(trial_nb(x,1),1) == 11 || trial_type(trial_nb(x,1),1) == 21) && ROI_looked_at(x,1) == 4 || (trial_type(trial_nb(x,1),1) == 12 || trial_type(trial_nb(x,1),1) == 22) && ROI_looked_at(x,1) == 3 || (trial_type(trial_nb(x,1),1) == 13 || trial_type(trial_nb(x,1),1) == 23) && ROI_looked_at(x,1) == 4
                meaning(x,1) = 30;
            elseif (trial_type(trial_nb(x,1),1) == 10 || trial_type(trial_nb(x,1),1) == 20) && ROI_looked_at(x,1) == 4 || (trial_type(trial_nb(x,1),1) == 11 || trial_type(trial_nb(x,1),1) == 21) && ROI_looked_at(x,1) == 3 || (trial_type(trial_nb(x,1),1) == 12 || trial_type(trial_nb(x,1),1) == 22) && ROI_looked_at(x,1) == 4 || (trial_type(trial_nb(x,1),1) == 13 || trial_type(trial_nb(x,1),1) == 23) && ROI_looked_at(x,1) == 3
                meaning(x,1) = 40;
            elseif ROI_looked_at(x,1) == 5
                meaning(x,1) = 50;
            else
                meaning(x,1) = 0;
            end
        end
end   
        
end
        
        
        
        
        % for x = 1:length(data)
        %     if data(x,1) == 70 && data(x,2) == 1 || data(x,1) == 71 && data(x,2) == 1 || data(x,1) == 72 && data(x,2) == 2 || data(x,1) == 73 && data(x,2) == 2
        %         gaze(x,1) = 10;
        %     elseif data(x,1) == 70 && data(x,2) == 2 || data(x,1) == 71 && data(x,2) == 2 || data(x,1) == 72 && data(x,2) == 1 || data(x,1) == 73 && data(x,2) == 1
        %         gaze(x,1) = 20;
        %     elseif data(x,1) == 70 && data(x,2) == 3 || data(x,1) == 71 && data(x,2) == 4 || data(x,1) == 72 && data(x,2) == 3 || data(x,1) == 73 && data(x,2) == 4
        %         gaze(x,1) = 30;
        %     elseif data(x,1) == 70 && data(x,2) == 4 || data(x,1) == 71 && data(x,2) == 3 || data(x,1) == 72 && data(x,2) == 4 || data(x,1) == 73 && data(x,2) == 3
        %         gaze(x,1) = 40;
        %     else
        %         gaze(x,1) = 0;
        %     end
        % end