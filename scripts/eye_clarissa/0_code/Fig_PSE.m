
%% set paths

addpath('C:\Users\Clarissa\Documents\MATLAB\Downloads\Packages\gramm')% plotting package
paths.LOAD_DATA_PATH_LVL2 = 'C:\Users\Clarissa\Documents\decid_eye\analysis\manip1&2\1_pipeline\fixations\3_f_fixation_behav\out\lvl1_agent\';
paths.PLOTS_PATH = 'C:\Users\Clarissa\Documents\decid_eye\analysis\manip1&2\Figs';

%% load data

stats_Lvl1 = importdata(fullfile(paths.LOAD_DATA_PATH_LVL2, 'stats_Lvl1.mat'));

%% set parameters

options = stats_Lvl1.options;
nbBins = length(options.prctile);

plausibleVals = stats_Lvl1.PSE.lastFixGains > -40 & stats_Lvl1.PSE.lastFixGains < 40 & stats_Lvl1.PSE.lastFixLosses > -40 & stats_Lvl1.PSE.lastFixLosses < 40;
nSub = nnz(plausibleVals);

% PSE plot
% compare PSE (point of subjctive equality) for last fixation to gains/losses
% this allows us to determine wether there is a significant shift in
% logistic curves
g = gramm('x', [ones(1, nSub), 2*ones(1, nSub)], 'y', [stats_Lvl1.PSE.lastFixGains(plausibleVals), stats_Lvl1.PSE.lastFixLosses(plausibleVals)]);
g.stat_boxplot('width', 0.5)
%g.geom_point('alpha', 0.8);
g.geom_jitter('alpha', 0.8);
% g.update('group', [1:nSub, 1:nSub]')
% g.geom_line()
g.set_color_options('map', 'matlab')
g.set_names('y','PSE (V^G-V^L)', 'x', '');
%g.set_title('Choice = no');
g.axe_property('XLim', [0 3], 'XTick', [1 2], 'xticklabels', {'Last fix gains', 'Last fix losses'});


figure('Position',[100 100 350 350]);
g.draw();
f_saveToMultipleFormats(fullfile(paths.PLOTS_PATH, 'choiceAsFunctionOfValueDiff_lastFix_PSE'))
