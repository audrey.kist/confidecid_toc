function [] = f_import_data

% import_data    Creates a matrix with all the wanted participant data, from
% both experiments 1 and 2. No input. Output saved directly to OUT_PATH. 
%
% Clarissa Baratin, edited Feb 2021

% set participants and paths
MANIP1.SUBJECT_NAMES = {'REN_2018_TESn', 'REN_2018_MICg', 'REN_2018_LIMm', 'REN_2018_LEDa', 'REN_2018_LAGs', 'REN_2018_HERc', 'REN_2018_FABl', 'REN_2018_DALa', 'PRA_2019_BBBb', 'PRA_2018_AAAa', 'NAN_2018_REMp', 'MAR_2018_DANb', 'LYO_2018_PLAc', 'LYO_2018_HENb', 'LYO_2018_CADp', 'GRE_2019_MARa', 'GRE_2018_SELc', 'GRE_2018_MEBj', 'GRE_2018_CROa', 'GRE_2018_CAQe', 'GRE_2018_BOIl'};
MANIP2.SUBJECT_NAMES = {'GRE_2019_AUba', 'GRE_2019_BERj', 'GRE_2019_CHRc', 'GRE_2019_GEOa', 'GRE_2019_PLEc', 'GRE_2020_BERl', 'GRE_2020_FOUt', ...
    'GRE_2020_SOLm', 'LYO_2019_CAPp', 'LYO_2019_IBRa', 'REN_2019_BAIp', 'REN_2019_LAPp', 'REN_2019_LEBc', 'GRE_2020_JERr', 'GRE_2020_GENj', 'GRE_2020_KAYc', 'GRE_2021_GUIl'};
MANIP1.ROOT = 'C:\Users\Clarissa\Documents\decid_eye\database\manip1\';
MANIP2.ROOT = 'C:\Users\Clarissa\Documents\decid_eye\database\manip2\';
MANIP1.MODEL_ROOT = 'C:\Users\Clarissa\Documents\decid_eye\romane\Choice_model\';
MANIP2.MODEL_ROOT = 'C:\Users\Clarissa\Documents\decid_eye\analysis\manip2\Model';

OUT_PATH = 'C:\Users\Clarissa\Documents\decid_eye\analysis\manip1&2\1_pipeline\0_f_import_data\out';
addpath = 'C:\Users\Clarissa\Documents\decid_eye\analysis\0_code\';

% concatenate all participants and check there are no duplicates
allSubjects = [MANIP1.SUBJECT_NAMES, MANIP2.SUBJECT_NAMES];

if ~ isequal(length(unique(allSubjects)), length(allSubjects))
    error ('Participant duplicates present.')
end

% import model output (Vinckier)
model_data1 = importdata(fullfile(MANIP1.MODEL_ROOT, 'all_choice_data.mat'));
model_data2 = importdata(fullfile(MANIP2.MODEL_ROOT, 'all_choice_data.mat'));

% set up data structures
nSub = length(allSubjects);
subjects = struct([]);

% for each subject

for iSub = 1:nSub
    
    % create field for subject
    subjects(iSub).name = allSubjects{iSub};
    subjects(iSub).data = [];
    subjects(iSub).trial_characteristics = [];
    
    % get corresponding manip
    if  any(strcmp(subjects(iSub).name, MANIP1.SUBJECT_NAMES))
        subjects(iSub).manip = 1;
    elseif any(strcmp(subjects(iSub).name, MANIP2.SUBJECT_NAMES))
        subjects(iSub).manip = 2;
    end
    
    % get eye and experiment (behaviour) files
    if subjects(iSub).manip == 1
        eye_files = dir(strcat(MANIP1.ROOT, subjects(iSub).name, '\', subjects(iSub).name, '_DECID\RAW\Eye_tracker\*.mat')); %get all sessions info for eye-tracking data
        exp_files_path = strcat(MANIP1.ROOT, subjects(iSub).name, '\', subjects(iSub).name, '_DECID\RAW\Comportement\');
    elseif subjects(iSub).manip == 2
        eye_files = dir(strcat(MANIP2.ROOT, subjects(iSub).name, '\RAW\Eye_tracker\*.mat')); %get all sessions info for eye-tracking data
        exp_files_path = strcat(MANIP2.ROOT, subjects(iSub).name, '\', '\RAW\Comportement\');
    end
    
    nb_sessions = length(eye_files);
    
    % get data for each session
    for iSes = 1:nb_sessions
        
        eye_data = importdata(strcat(eye_files(iSes).folder, '\', eye_files(iSes).name));
        exp_data = importdata(strcat(exp_files_path, extractBefore(eye_files(iSes).name, '_eye'), '.mat'));
        
        length_per_session = length(eye_data.gaze.LeftEye.GazePoint.OnDisplayArea);
        session = zeros(length_per_session,12);
        
        % fill in session column
        session(:,1) = iSes;
        
        % fill in x and y coordinate columns (columns 1 and 2) and timestamp
        % column
        session(:,3) = eye_data.gaze.LeftEye.GazePoint.OnDisplayArea(:,1);
        session(:,4) = eye_data.gaze.LeftEye.GazePoint.OnDisplayArea(:,2);
        session(:,5) = eye_data.gaze.SystemTimeStamp;
        
        % fill in trackloss column
        session(:,6) = eye_data.gaze.LeftEye.GazePoint.Validity;
        
        % fill in coordinates from other reference system (see Tobii doc)
        session(:,7:9) = eye_data.gaze.LeftEye.GazeOrigin.InUserCoordinateSystem(:,1:3);
        session(:,10:12) = eye_data.gaze.LeftEye.GazePoint.InUserCoordinateSystem(:,1:3);
        
        % get nb trials
        nb_trials = length(eye_data.event);
        
        for j=1:nb_trials
            
            trial_nb_all_sess = size(subjects(iSub).trial_characteristics,1)+1;
            
            % find on/off times for trials
            %on_time = eye_data.event(j,2);
            on_time = exp_data.logfile.choice.onset(j,1)*1000000;
            
            %off_time = eye_data.event(j,4);
            off_time = exp_data.logfile.choice.resp_press(j,1)*1000000;
            
            % find gaze data corresponding to this trial
            trial_start = find(eye_data.gaze.SystemTimeStamp <= on_time, 1, 'last');
            trial_end = find(eye_data.gaze.SystemTimeStamp >= off_time, 1);
            
            % number trials (column )
            if iSes == 1
                session(trial_start:trial_end,2) = j;
            elseif    iSes > 1
                session(trial_start:trial_end,2) = j + max(subjects(iSub).data(:,2));
            end
            
            % add trial type to trial_characteristics
            
            % manip 1 --> (70 = gains en haut, OUI � gauche; 71 = gains en
            % haut, OUI � droite; 72 = gains en bas, OUI � gauche; 73 = gains
            % en bas, OUI � droite)
            % manip 2 --> (10/20 = gains en haut, OUI � gauche; 11/21 = gains en
            % haut, OUI � droite; 12/22 = gains en bas, OUI � gauche; 13/23 = gains
            % en bas, OUI � droite)
            
            trial_type = eye_data.event(j,1);
            subjects(iSub).trial_characteristics(trial_nb_all_sess,1) = trial_type;
            
            % add gains, losses, difficulty, choice columns for that trial
            trial_gain = exp_data.logfile.choice.gain(j,1);
            subjects(iSub).trial_characteristics(trial_nb_all_sess,2) = trial_gain;
            
            trial_loss = exp_data.logfile.choice.loss(j,1);
            subjects(iSub).trial_characteristics(trial_nb_all_sess,3) = trial_loss;
            
            trial_difficulty = exp_data.logfile.choice.difficulty(j,1);
            subjects(iSub).trial_characteristics(trial_nb_all_sess,4) = trial_difficulty;
            
            trial_choice = exp_data.logfile.choice.choiceMade(j,1);
            if isequal(trial_choice{1, 1} , 'OUI') || isequal(trial_choice{1, 1} , 'ANO') % 'ano' for subjects in prague
                subjects(iSub).trial_characteristics(trial_nb_all_sess,5) = 1;
            else
                subjects(iSub).trial_characteristics(trial_nb_all_sess,5) = 0;
            end
            
            % fill reaction time in s
            reaction_time = ((trial_end-trial_start)+1)/90; %length of trial (in samples)/ 90 because sample rate is 90 Hz
            subjects(iSub).trial_characteristics(trial_nb_all_sess,6) = reaction_time;
            
            if subjects(iSub).manip == 1
                
                % fill in Utility of trial (ouput of model from Vinckier)
                utility = model_data1.inversionResult.model.(subjects(iSub).name).posterior.muX(1,trial_nb_all_sess);
                
                % fill in the p(accept) of each trial (output of model from Vinckier)
                p_accept = model_data1.inversionResult.model.(subjects(iSub).name).posterior.muX(2,trial_nb_all_sess);
                
                % fill in the residuals of choice model (output of
                % model from Vinckier), meaning the aspect of p(accept) not explained by
                % utility model
                choice_residuals = model_data1.inversionResult.model.(subjects(iSub).name).out.suffStat.dy(1,trial_nb_all_sess);
                
            elseif subjects(iSub).manip == 2
                
                utility = model_data2.inversionResult.model.(subjects(iSub).name).posterior.muX(1,trial_nb_all_sess);
                p_accept = model_data2.inversionResult.model.(subjects(iSub).name).posterior.muX(2,trial_nb_all_sess);
                choice_residuals = model_data2.inversionResult.model.(subjects(iSub).name).out.suffStat.dy(1,trial_nb_all_sess);
            end
            
            subjects(iSub).trial_characteristics(trial_nb_all_sess,7) = utility;
            subjects(iSub).trial_characteristics(trial_nb_all_sess,8) = p_accept ;
            subjects(iSub).trial_characteristics(trial_nb_all_sess,9) = choice_residuals;
           
        end
        
        % concatenate with previous sessions (except for the first
        % session which will be concatenated to empty matrix)
        subjects(iSub).data = cat(1, subjects(iSub).data, session);
    end
end

fprintf('Saving data...\n');

save(fullfile(OUT_PATH, 'subjects.mat'), 'subjects')

end