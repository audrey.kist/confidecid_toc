function f_saveToMultipleFormats(file)

extensions = {'svg','fig','png'};

for k = 1:length(extensions)
    saveas(gcf, file, extensions{k})
end