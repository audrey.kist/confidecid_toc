
%% set paths

addpath('C:\Users\Clarissa\Documents\MATLAB\Downloads\Packages\gramm')% plotting package
paths.LOAD_DATA_PATH_LVL2 = 'C:\Users\Clarissa\Documents\decid_eye\analysis\manip1&2\1_pipeline\fixations\3_f_fixation_behav\out\lvl1_agent\';
paths.PLOTS_PATH = 'C:\Users\Clarissa\Documents\decid_eye\analysis\manip1&2\Figs';

%% load data

stats_Lvl1 = importdata(fullfile(paths.LOAD_DATA_PATH_LVL2, 'stats_Lvl1.mat'));

%% set parameters

options = stats_Lvl1.options;
nbBins = length(options.prctile4);

%% plot

g = gramm('x', [1:nbBins], 'y', stats_Lvl1.(options.bin_method).bin_var_nFix'); % y equivalent to nanmean_RespTbyZscRating_y', but allows CI plotting
g.set_color_options('map', 'matlab')
g.geom_jitter('alpha', 0.3)
g.stat_summary('type', 'sem', 'geom', {'point','errorbar'});
g.stat_glm('distribution', 'normal', 'geom', 'line', 'fullrange', 'true')
g.set_names('x','|Utility|, percentile', 'y', 'Normalized number of fixations');
g.axe_property('XTick', [1:1:nbBins], 'xticklabels', [25, 50, 75, 100]);

figure('Position',[100 100 350 350]);
g.draw();
f_saveToMultipleFormats(fullfile(paths.PLOTS_PATH, 'nFixByUtil'))

%% plot, using absolute x values

g = gramm('x', stats_Lvl1.(options.bin_method).bin_val_nFix', 'y', stats_Lvl1.(options.bin_method).bin_var_nFix'); % y equivalent to nanmean_RespTbyZscRating_y', but allows CI plotting
g.set_color_options('map', 'matlab')
%g.geom_jitter('alpha', 0.3)
g.stat_summary('type', 'sem', 'geom', {'point','errorbar'});
g.stat_glm('distribution', 'normal', 'geom', 'line', 'fullrange', 'true')
g.set_names('x','|Utility|, percentile', 'y', 'Normalized number of fixations');
%g.axe_property('XTick', [1:1:nbBins], 'xticklabels', [25, 50, 75, 100], 'YTick', [0:1:5]);

figure('Position',[100 100 350 350]);
g.draw();
f_saveToMultipleFormats(fullfile(paths.PLOTS_PATH, 'nFixByUtil_AbsVals'))
