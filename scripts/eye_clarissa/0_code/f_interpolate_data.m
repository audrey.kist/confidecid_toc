function [CoordColsInterpolated, missing] = f_interpolate_data(CoordColstoInterpolate, time_column, trackloss_column, max_gap_length)

% f_interpolate_data   Used to interpolate missing eye-tracking data of all coordinate systems used.
%
%   Input (first 4 inputs should have equal length):
%       - CoordColstoInterpolate: columns to interpolate, can be x,y,z coordinate columns in alll  coordinate systems;
%       - timepoints column;
%       - trackloss column;
%       - max gap length (if gap larger, don't interpolate);
%
%   Output:
%       Returns columns consisting of x, y, z coordinates
%       with gaps (identified by trackloss = 0) inferior to max_gap_length
%       interpolated. Additionally returns a 3d matrix of start (col 1) & end
%       (col 2) rows of missing data fragments. 3rd dimension corresponds
%       to column being interpolated. Note: in our case, 3rd dimension
%       should always give same result since the trackloss at each point is 
%       valid for all coordinate columns being interpolated. 
%
%   Clarissa Baratin, Feb 2020, edited Feb 2021


CoordColsInterpolated = CoordColstoInterpolate;
nCols = size(CoordColstoInterpolate, 2);

missing = nan(1,1,nCols);

for iCol = 1:nCols
    
    Col = CoordColstoInterpolate(:, iCol);
    Col_interpolated = Col;
    
    iRow = 2;
    
    while iRow <= length(Col)
        
        % Find where there is missing data & length of missing data segments
        nMissing = 0;
        
        if trackloss_column(iRow,1) == 0 % if data is missing. account for case where nan's happen at end of dataset
            
            next = iRow;
            
            while trackloss_column(next,1) == 0 && iRow + nMissing < length(Col) % count how much of following data is missing
                next = next+1;
                nMissing = nMissing + 1; % nb of values missing
            end
        end
        
        % If n <= threshold, then interpolate missing data
        if nMissing > 0 && nMissing <= max_gap_length
            
            % keep track of fragments interpolated
            missing(length(nonzeros(missing(:, 1, iCol))) + 1, 1, iCol) = iRow;
            missing(length(nonzeros(missing(:, 1, iCol))), 2, iCol) = iRow + nMissing - 1;
            
            for iSample = 0:nMissing - 1 % for each sample in missing data
                
                % step 1: create scaling factor
                sc_factor = (time_column(iRow + iSample, 1) - time_column(iRow - 1, 1))/(time_column(iRow + nMissing, 1) - time_column(iRow - 1, 1)); % equation indicated in white paper seems wrong
                
                % step 2: multiply w position of data of first valid sample after gap
                end_pos = sc_factor*(Col(iRow + nMissing, 1) - Col(iRow - 1, 1)); % not well indicated in tobii, classic linear interpolation
                
                % step 3: add result and position of data of last valid sample before gap
                pos = end_pos + Col(iRow - 1, 1);
                
                % replace NaNs with interpolated values
                Col_interpolated(iRow + iSample, 1) = pos;
            end
        end
        
        iRow = iRow + nMissing; % skip all samples already examined in while loop
        iRow = iRow + 1;
    end
    
    CoordColsInterpolated(:, iCol) = Col_interpolated;
end

