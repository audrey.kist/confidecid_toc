function f_remove_data

% f_remove_data  Script to choose/remove trials/participants from dataset.
% 
% Steps: 
%       1. Get trackloss trials from raw data;
%       2. Option: Replace them from trial_characteristics/fixSum;
%       3. Option: Remove agent or non-agent trials + replace utility;
%       4. Option: Get/replace w NaN trials with outlier RT.
%       5. Look into participant data quality: % yes responses, dims of
%       task taken into account to make choices. 
%       6. Make summary table of participant data.
%       7. Options: Remove participants.
%   
% Clarissa Baratin, 2021

clear all

%% set paths

paths.MAIN = 'C:\Users\Clarissa\Documents\decid_eye\analysis\manip1&2';
paths.DIR = fullfile(paths.MAIN, '0_code');
paths.LOAD_DATA_PATH = fullfile(paths.MAIN, '1_pipeline\fixations\1_f_preprocess_data\out');
paths.OUT_PATH = fullfile(paths.MAIN, '1_pipeline\fixations\2_f_remove_data\out');
paths.PLOTS_PATH = fullfile(paths.MAIN, '1_pipeline\fixations\2_f_remove_data\plots');

addpath('C:\Users\Clarissa\Documents\MATLAB\Downloads\Packages\gramm') % plotting package
addpath(paths.MAIN)
cd(paths.DIR)

%% options struct

% remove data? Yes or no options
options.RemoveTrialsBasedonTrackloss = 1;
options.RemoveTrialsBasedonRT = 1;
options.RemoveNonAgentTrials = 1;
options.RemoveAgentTrials = 0; %% !! Don't have utility for these trials only
options.RemoveParticipantsWhoAlwaysMakeSameChoice = 0;
options.RemoveParticipantsWhoDontUse2Dims = 0;

options.MaxTrackloss = 0.25; % percentage of trackloss authorized, 25% (Lim et al., 2011)
options.MinTrialDuration = 0.020; % 20 ms, look up ref
options.VelocityThresh = 50; %!!!! should be changed depending on the noise in the data, check this, try different noise reductions
options.MaxPercentAccept = 80; % max percentage of yes responses authorized
options.MinPercentAccept = 20; % min percentage of yes responses authorized

%% load data

subjects = importdata(fullfile(paths.LOAD_DATA_PATH, 'subjects.mat'));
nSub = length(subjects);

for iSub = 1:nSub
    maxTrials(iSub, 1) = length(subjects(iSub).trial_characteristics);
end

maxTrials = max(maxTrials);

%% get and replace with nan trials which have too much trackloss

figure

for iSub = 1:nSub
    
    % identify trials with trackloss
    subjects(iSub).Log.trialswTrackloss = f_identify_tracklossTrials(subjects(iSub).data_fixations(:, 2), subjects(iSub).data_fixations(:, 6), options.MaxTrackloss);
    
    % plot nb of trials with trackloss, per subject
    hold on
    scatter(iSub, length(subjects(iSub).Log.trialswTrackloss))
    xlabel('Subjects')
    ylabel(sprintf('Nb of trials with >= %d percent trackloss', options.MaxTrackloss*100))
    
end

saveas(gcf, fullfile(paths.PLOTS_PATH, 'trialswTrackloss.png'))

if options.RemoveTrialsBasedonTrackloss == 1
    
    for iSub = 1:nSub
        
        trialsToRemove = subjects(iSub).Log.trialswTrackloss;
        
        for iTrial = 1:length(trialsToRemove)
            
            trialToRemove = trialsToRemove(iTrial);
            
            subjects(iSub).trial_characteristics(trialToRemove, [2:5,7:9]) = nan;
            subjects(iSub).fixSum.trial(trialToRemove).fixData = nan;
        end
    end
end

%% Removing agent/non-agent trials

trialsToRemove = [];

if options.RemoveNonAgentTrials == 1
    
    % replace utility by the one calculated with the model using only agent
    % trials (by Romane)
    load('utility_agent_ConfiDecid.mat', 'utility_agent')
    
    for iSub = 1:nSub
        
        if subjects(iSub).manip == 2
            
            % find trials which are nonagent (= spatial configuration of 20/21/22/23)
            trialsToRemove = find(subjects(iSub).trial_characteristics(:, 1) >= 20);
            
            % remove them from trial_characteristics
            subjects(iSub).trial_characteristics(trialsToRemove, :) = [];
            
            % remove them from fixSum
            subjects(iSub).fixSum.trial(trialsToRemove) = [];
            
            % remove them from data_fixations
            for iTrial = 1:length(trialsToRemove)
                trial = trialsToRemove(iTrial);
                rowsToRemove = find(subjects(iSub).data_fixations(:,2) == trial); 
                subjects(iSub).data_fixations(rowsToRemove, :) = [];
            end       
            
            % replace utility by the one calculated with the model using only agent
            % trials (by Romane
            subID = subjects(iSub).name;
            util = utility_agent{strcmp({utility_agent{:, 1}}, subID), 2}';
            subjects(iSub).trial_characteristics(:, 7) = util;
        end
    end
    
%     close all
%     clear g
%     figure('Position', [100 100 1500 1000]);
%     
%     subplots_y = [1 2 3 4 5 6 7 1 2 3 4 5 6 7 1 2 3 4 5 6 7 1 2 3 4 5 6 7 1 2 3 4 5 6 7 1 2 3 4 5 6 7];
%     subplots_x = [1 1 1 1 1 1 1 2 2 2 2 2 2 2 3 3 3 3 3 3 3 4 4 4 4 4 4 4 5 5 5 5 5 5 5 6 6 6 6 6 6 6];
%     
%     for iSub = 1:size(subjects, 2)
%         subplot_x = subplots_x(iSub);
%         subplot_y = subplots_y(iSub);
%         
%         data = subjects(iSub).data_fixations(subjects(iSub).data_fixations(:, 7) == 1, [9,11:12]);
%         g(subplot_x,subplot_y) = gramm('x', data(:,2), 'y', data(:,3), 'color', data(:,1));
%         g(subplot_x,subplot_y).geom_point();
%         g(subplot_x,subplot_y).set_point_options('base_size', 1);
%         g(subplot_x,subplot_y).axe_property('XLim', [0 1], 'YLim', [0 1]);
%         g(subplot_x,subplot_y).set_title(sprintf('Subject %d', iSub));
%     end
%     
%     g.draw;
    %saveas(gcf, fullfil
end

% if options.RemoveAgentTrials == 1
%
%     for iSub = 1:nSub
%
%         if subjects(iSub).manip == 2
%
%             % find trials which are agent (= spatial configuration of 10/11/12/13)
%             trialsToRemove = find(subjects(iSub).trial_characteristics(:, 1) < 15);
%
%             % remove them from trial_characteristics
%             subjects(iSub).trial_characteristics(trialsToRemove, :) = [];
%
%             % remove them from fixSum
%             subjects(iSub).fixSum.trial(trialsToRemove) = [];
%         end
%     end
%
% end

%% Trial quality check

% RTs
RTs = nan(maxTrials, nSub);
nonOutlierVals = nan(maxTrials, nSub);

for iSub = 1:nSub
    
    indivRTs = subjects(iSub).trial_characteristics(:, 6);
    RTs(1:length(indivRTs), iSub) = indivRTs;
    
    % get outliers to remove later (1 = outlier)
    [indivNonoutlierVals, subjects(iSub).Log.RToutliers] = rmoutliers(indivRTs, 'mean'); % outlierVals for plotting purposes
    nonOutlierVals(1:length(indivNonoutlierVals), iSub) = indivNonoutlierVals;
end

% plot RTs of each subject
figure
g1 = gramm('x', [1:nSub], 'y', RTs);
g1.geom_point();
g1.set_color_options('map', 'brewer1')

g1.update('x', [1:nSub], 'y', nonOutlierVals);
g1.geom_point();
g1.set_color_options('map', 'brewer2')
g1.draw();

f_saveToMultipleFormats(fullfile(paths.PLOTS_PATH, 'RTs'))

% TRIAL LENGTH

% figure
% 
% for iSub = 1:nSub
%     
%     % identify trials which are too short
%     subjects(iSub).Log.shortTrials = find(subjects(iSub).trial_characteristics(:, 6) < options.MinTrialDuration);
%     
%     % plot nb of trials which are too short, per subject
%     hold on
%     scatter(iSub, length(subjects(iSub).Log.shortTrials))
%     xlabel('Subjects')
%     ylabel(sprintf('Nb of trials < %f s', options.MinTrialDuration))
%     
% end
% 
% f_saveToMultipleFormats(fullfile(paths.PLOTS_PATH, 'shortTrials'))

%% from here onwards, raw fixation data not needed anymore

subjects = rmfield(subjects, {'data_fixations'});

%% Removing trials

if options.RemoveTrialsBasedonRT == 1
    
    trialsToRemove = [];
    
    for iSub = 1:nSub
        
        trialsToRemove = find(subjects(iSub).Log.RToutliers);
        
        for iTrial = 1:length(trialsToRemove)
            
            trialToRemove = trialsToRemove(iTrial);
            
            subjects(iSub).trial_characteristics(trialToRemove, :) = nan;
            subjects(iSub).fixSum.trial(trialToRemove).fixData = nan;
        end
    end
end

% fill rows of trackloss trials, that were not completely nan, with nan's

for iSub = 1:nSub
        
    for iTrial = 1:length(subjects(iSub).trial_characteristics)
        
        if isnan(subjects(iSub).trial_characteristics(iTrial, [2:5]))
            
            subjects(iSub).trial_characteristics(iTrial, :) = nan;
        end

    end
end

%% Participant quality check

% MEDIAN PERCENTAGE YES RESPONSES
figure

T_percentAccept = [];

for iSub = 1:nSub
    
    % get percentage of trials where participant accepts
    subjects(iSub).Log.percentAccept = sum(subjects(iSub).trial_characteristics(:,5) == 1)/length(subjects(iSub).trial_characteristics(:,5))*100;
    
    % copy it in vector used for table later
    T_percentAccept(iSub) = subjects(iSub).Log.percentAccept;
    
    % plot nb of trials where participant accepts
    hold on
    scatter(iSub, subjects(iSub).Log.percentAccept)
    xlabel('Subjects')
    ylabel('% of yes responses')
    
end

f_saveToMultipleFormats(fullfile(paths.PLOTS_PATH, 'percentYes'))

% DIMENSIONS OF TASK TAKEN INTO ACCOUNT

for iSub = 1:nSub
    
    % Logistic regression to check the effect of gains/losses/difficulty on choices
    gains = subjects(iSub).trial_characteristics(:,2);
    losses = subjects(iSub).trial_characteristics(:,3);
    diff = subjects(iSub).trial_characteristics(:,4);
    choice = subjects(iSub).trial_characteristics(:,5);
    
    X = [gains losses diff];
    [b,dev,stats] = glmfit(X,choice,'binomial','link','logit');
    regression_results.betas(:, iSub) = b;
    regression_results.pvals(:, iSub) = stats.p;
    regression_results.stats(iSub).stats = stats;
    
end

% plot p-values of betas

close all
clear g

figure('Position',[100 100 1000 500]);

g = gramm('x', [1:nSub], 'y', regression_results.pvals(2:4,:), 'color', [1;2;3]);
g.geom_point();
g.set_names('x','Subjects','y','P-value of beta','color', 'gains/losses/diffculty');
g.axe_property('XTick', [0:1:nSub]);
g.draw;

f_saveToMultipleFormats(fullfile(paths.PLOTS_PATH, 'dimsPvals'))

%% make a summary table of all participant info

% Note: trackloss trials represented as % of all trials of subjects

nb = [1:length(subjects)]';

sig_pvals = regression_results.pvals < 0.05;
pvals_reg = regression_results.pvals;
%pvals_reg(sig_pvals) = -0.05;

G_pval = round(pvals_reg(2,:)', 3);
G_betas = round(regression_results.betas(2,:)', 3);
L_pval = round(pvals_reg(3,:)', 3);
L_betas = round(regression_results.betas(3,:)', 3);
D_pval = round(pvals_reg(4,:)', 3);
D_betas = round(regression_results.betas(4,:)', 3);

TTT = table(nb, T_percentAccept', G_betas, G_pval, L_betas, L_pval, D_betas, D_pval)

%% Removing subjects

subjectsToRemove = {};

if options.RemoveParticipantsWhoAlwaysMakeSameChoice == 1
    
    for iSub = 1:nSub
        
        if subjects(iSub).Log.percentAccept > options.MaxPercentAccept
            subjectsToRemove{length(subjectsToRemove)+1} = subjects(iSub).name;
            
        elseif subjects(iSub).Log.percentAccept < options.MinPercentAccept
            subjectsToRemove{length(subjectsToRemove)+1} = subjects(iSub).name;
        end
    end
end

if options.RemoveParticipantsWhoDontUse2Dims == 1
    
    for iSub = 1:nSub
        
        if sum([G_pval(iSub), L_pval(iSub), D_pval(iSub)] > 0.05) > 1 % if there are at least 2 dims for which the pval is > 0.05, delete subject
            
            subjectsToRemove{length(subjectsToRemove)+1} = subjects(iSub).name;
            
        end
    end
end

subjectsToRemove = unique(subjectsToRemove);


iSub = 1;

while iSub <= nSub
    
    if any(strcmp(subjectsToRemove, subjects(iSub).name))
        
        subjects(iSub) = [];
        nSub = length(subjects);
        iSub = iSub - 1;
        
    end
    
    iSub = iSub + 1;
end

%% save data

for iSub = 1:nSub
    subjects(iSub).Log.RemoveDataOpts = options;
    subjects(iSub).Log.RemoveDataPaths = paths;
end

save(fullfile(paths.OUT_PATH, 'subjects_agent.mat'), 'subjects');

end