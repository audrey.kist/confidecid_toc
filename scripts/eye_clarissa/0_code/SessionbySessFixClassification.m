%% Session by session exploration of fixations classification

% note: doing this post-processing, using the data_fixations columns

paths.MAIN = 'C:\Users\Clarissa\Documents\decid_eye\analysis\manip1&2';
paths.LOAD_DATA_PATH = fullfile(paths.MAIN, '1_pipeline\1_f_preprocess_data\out');
subjects = importdata(fullfile(paths.LOAD_DATA_PATH, 'subjects.mat'));
addpath('C:\Users\Clarissa\Documents\MATLAB\Downloads\Packages\gramm') % plotting package

nSub = 37; length(subjects);

%% Make matrix of fixation coordinates

for iSub = 1:nSub
    
    nFix = max(subjects(iSub).data_fixations(:, 8));
    
    for iFix = 1:nFix
        
        fixRows = find(subjects(iSub).data_fixations(:, 8) == iFix);
        
        subjects(iSub).fixCoord(iFix, 1) = subjects(iSub).data_fixations(fixRows(1), 1); % session nb
        subjects(iSub).fixCoord(iFix, 2) = iFix;
        subjects(iSub).fixCoord(iFix, 3) = subjects(iSub).data_fixations(fixRows(1), 11); % mean x
        subjects(iSub).fixCoord(iFix, 4) = subjects(iSub).data_fixations(fixRows(1), 12); % mean y
        
    end
end

%% Set ROIs based on screen ROI coordinates

rois = [0.32 0.68 0.03 0.25; 0.32 0.68 0.75 0.97; 0.20 0.30 0.44 0.55; 0.70 0.80 0.45 0.55; 0.45 0.55 0.45 0.55];

% make training set based on ROIs
train_set = [];
train_set_singleroi = [];
n_samp_per_roi = 100;
n_rois = length(rois);

X_COORD = 1;
Y_COORD = 2;
LABEL = 3;

for i_roi = 1:n_rois
    train_set_singleroi(:, X_COORD) = rois(i_roi,1) + (rois(i_roi,2)-rois(i_roi,1))*rand(n_samp_per_roi,1); % you can generate N random numbers in the interval (a,b) with the formula r = a + (b-a).*rand(N,1).
    train_set_singleroi(:,Y_COORD) = rois(i_roi,3) + (rois(i_roi,4)-rois(i_roi,3))*rand(n_samp_per_roi,1);
    train_set_singleroi(:,LABEL) = i_roi;
    train_set = vertcat(train_set_singleroi, train_set);
end

%% Add a reject class (geometry dependent on task)

% add a reject class to the training set for 4 lines: the 2 diagonals of
% the screen (x = y, x = 1-y) avoiding the center, a circle surrounding all
% data and a rectangle around difficulty

border1 = 0.4;
border2 = 0.6;

line_1 = [];
line_2 = [];
line_3 = [];
line_4 = [];
n_samp_per_line = 20;

% reject class contains 4 diagonal lines
line_1(:, X_COORD) = 0 + (border1-0) * rand(n_samp_per_line,1);
line_1(:, Y_COORD) = line_1(:, X_COORD);
line_1(:, LABEL) = 6;
line_2(:, X_COORD) = border2 + (1-border2) * rand(n_samp_per_line,1);
line_2(:, Y_COORD) = line_2(:, X_COORD);
line_2(:, LABEL) = 6;
line_3(:, X_COORD) = 0 + (border1-0) * rand(n_samp_per_line,1);
line_3(:, Y_COORD) = 1-line_3(:, X_COORD);
line_3(:, LABEL) = 6;
line_4(:, X_COORD) = border2 + (1-border2) * rand(n_samp_per_line,1);
line_4(:, Y_COORD) = 1-line_4(:, X_COORD);
line_4(:, LABEL) = 6;

% a circle
circle_1 = [];
circle_2 = [];
nb_samp_per_circle = 200;

for i_point = 1:nb_samp_per_circle
    %    point = [(0.1 * cos(2 * pi * i_point / nb_samp_per_circle)) + 0.5, 0.1 * sin(2 * pi * i_point / nb_samp_per_circle) + 0.5, 6]; % Any point (x,y) on the path of the circle is x = rsin(?), y = rcos(?)
    %    circle_1 = vertcat(circle_1, point);
    point2 = [(0.5 * cos(2 * pi * i_point / nb_samp_per_circle)) + 0.5, 0.5 * sin(2 * pi * i_point / nb_samp_per_circle) + 0.5, 6];
    circle_2 = vertcat(circle_2, point2);
end

% a rectangle
rectangle_horizontallines = [];
rectangle_verticallines = [];
nb_samp_per_rectangle = 80;

rectangle_horizontallines(:, X_COORD) = border1 + (border2-border1) * rand(nb_samp_per_rectangle/2,1);
rectangle_horizontallines(:, Y_COORD) = repmat([border1 border2], 1, nb_samp_per_rectangle/4)';
rectangle_horizontallines(:, LABEL) = 6;

rectangle_verticallines(:, X_COORD) = repmat([border1 border2], 1, nb_samp_per_rectangle/4)';
rectangle_verticallines(:, Y_COORD) = border1 + (border2-border1) * rand(nb_samp_per_rectangle/2,1);
rectangle_verticallines(:, LABEL) = 6;

reject_class = vertcat(line_1, line_2, line_3, line_4, rectangle_horizontallines, rectangle_verticallines, circle_2);
train_set_w_reject = vertcat(train_set, reject_class);

%% plot training set w. reject class

g = gramm('x', train_set_w_reject(:,1), 'y', train_set_w_reject(:,2), 'color', train_set_w_reject(:,3));
g.geom_point();
g.set_point_options('base_size', 3);
g.axe_property('XLim', [0 1], 'YLim', [0 1]);

%% construct classifier
Mdl2 = fitcknn(train_set_w_reject(:,X_COORD:Y_COORD),train_set_w_reject(:,LABEL),'NumNeighbors',1);
g.draw();

%% use classifier on test set

% apply model for each subject
for iSub = 1:nSub
    subjects(iSub).fixCoord(:, 5) = predict(Mdl2, subjects(iSub).fixCoord(:, 3:4));
end

%% plot fixations of all subjects (after all data cleaning is done)

close all
clear g
figure('Position', [100 100 1500 1000]);

subplots_y = [1 2 3 4 5 6 7 1 2 3 4 5 6 7 1 2 3 4 5 6 7 1 2 3 4 5 6 7 1 2 3 4 5 6 7 1 2 3 4 5 6 7];
subplots_x = [1 1 1 1 1 1 1 2 2 2 2 2 2 2 3 3 3 3 3 3 3 4 4 4 4 4 4 4 5 5 5 5 5 5 5 6 6 6 6 6 6 6];

for iSub = 1:nSub
    subplot_x = subplots_x(iSub);
    subplot_y = subplots_y(iSub);
    
    g(subplot_x,subplot_y) = gramm('x', subjects(iSub).fixCoord(:, 3), 'y', subjects(iSub).fixCoord(:, 4), 'color',  subjects(iSub).fixCoord(:, 5));
    g(subplot_x,subplot_y).geom_point();
    g(subplot_x,subplot_y).set_point_options('base_size', 1);
    g(subplot_x,subplot_y).axe_property('XLim', [0 1], 'YLim', [0 1]);
    g(subplot_x,subplot_y).set_title(sprintf('Subject %d', iSub));
end

g.draw;

%% redo session by session

for iSess = 1
    
    for iSub = 1:nSub
        
        subjects(iSub).data = [];
        
        sessRows = subjects(iSub).fixCoord(:, 1) == iSess;
        
        % apply model
        subjects(iSub).data = subjects(iSub).fixCoord(sessRows, 3:4);
        subjects(iSub).data(:, 4) = predict(Mdl2, subjects(iSub).data);
        
    end
    
    % plot fixations of all subjects (after all data cleaning is done)
    
    clear g
    figure('Position', [100 100 1500 1000]);
    
    subplots_y = [1 2 3 4 5 6 7 1 2 3 4 5 6 7 1 2 3 4 5 6 7 1 2 3 4 5 6 7 1 2 3 4 5 6 7 1 2 3 4 5 6 7];
    subplots_x = [1 1 1 1 1 1 1 2 2 2 2 2 2 2 3 3 3 3 3 3 3 4 4 4 4 4 4 4 5 5 5 5 5 5 5 6 6 6 6 6 6 6];
    
    for iSub = 1:nSub
        subplot_x = subplots_x(iSub);
        subplot_y = subplots_y(iSub);
        
        g(subplot_x,subplot_y) = gramm('x', subjects(iSub).data(:, 1), 'y', subjects(iSub).data(:, 2), 'color', subjects(iSub).data(:, 4));
        g(subplot_x,subplot_y).geom_point();
        g(subplot_x,subplot_y).set_point_options('base_size', 1);
        g(subplot_x,subplot_y).axe_property('XLim', [0 1], 'YLim', [0 1]);
        g(subplot_x,subplot_y).set_title(sprintf('Subject %d', iSub));
    end
    
    g.draw;
    
    for iSess = 2
        
        for iSub = 1:nSub
            
            subjects(iSub).data = [];
            
            sessRows = subjects(iSub).fixCoord(:, 1) == iSess;
            
            % apply model
            subjects(iSub).data = subjects(iSub).fixCoord(sessRows, 3:4);
            subjects(iSub).data(:, 4) = predict(Mdl2, subjects(iSub).data);
            
        end
        
        % plot fixations of all subjects (after all data cleaning is done)
        
        clear g
        figure('Position', [100 100 1500 1000]);
        
        subplots_y = [1 2 3 4 5 6 7 1 2 3 4 5 6 7 1 2 3 4 5 6 7 1 2 3 4 5 6 7 1 2 3 4 5 6 7 1 2 3 4 5 6 7];
        subplots_x = [1 1 1 1 1 1 1 2 2 2 2 2 2 2 3 3 3 3 3 3 3 4 4 4 4 4 4 4 5 5 5 5 5 5 5 6 6 6 6 6 6 6];
        
        for iSub = 1:nSub
            subplot_x = subplots_x(iSub);
            subplot_y = subplots_y(iSub);
            
            g(subplot_x,subplot_y) = gramm('x', subjects(iSub).data(:, 1), 'y', subjects(iSub).data(:, 2), 'color', subjects(iSub).data(:, 4));
            g(subplot_x,subplot_y).geom_point();
            g(subplot_x,subplot_y).set_point_options('base_size', 1);
            g(subplot_x,subplot_y).axe_property('XLim', [0 1], 'YLim', [0 1]);
            g(subplot_x,subplot_y).set_title(sprintf('Subject %d', iSub));
        end
        
        g.draw;
        
    end    
end

