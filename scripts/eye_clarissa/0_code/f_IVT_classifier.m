function [fixation_output, fixation_coord] = f_IVT_classifier(x_column, y_column, gaze_origin_data, gaze_point_data, velocity_threshold)

% fixation_classifier  Classifies each eye position sample as a fixation
%  and ouputs the fixation location on-screen.
%
%    Velocity calculator: Associates each eye position sample with a velocity in degrees/s.
%    First calculates the angle between two vectors determined by the position of
%    the eye at point y and the position of the gaze at samples y-1/y+1. Then
%    divides it by the time between both samples.
%
%    Input:
%      - x-coordinates column;
%      - y-coordinates column;
%      - 3-column matrix with gaze origin data in Tobii User Coordinate
%        System (x, y, z positions)
%      - 3-column matrix with gaze position data in Tobii User Coordinate
%        System (x, y, z positions)
%      - velocity threshold -> threshold above which samples are classified
%        as saccades and under which samples are classified as fixations
%
%    Output:
%       - 2-column matrix indicating if sample is a fixation (1 = yes/0 =
%       no) & the number of the fixation .
%       - 3-column matrix summary of fixations: fixation nb, mean x, mean y
%
%    Clarissa Baratin, Feb 2020, last edited Feb 2021
%
%    - edited May 2020 to remove 3rd column of matrix and add a 2-column matrix to
%    subjects struct which contains the mean coordinates of fixation.
%    Fixation location not calculated in here anymore. 
%    - also changed name from fixation_calssifier_ROIclassifier to
%    IVT_classifier


%% Velocity calculator

% add this if decide to change velocity window due to noise (see Tobii
% explanation)
% if velocity_window <3, velocity_window = 3; end % 3 points minimum to be able to do y, y+1, y-1

velocity_window = 3;
y = 2; % to be able to do y-1

while y < length(gaze_origin_data)
    eye_pos = gaze_origin_data(y,:);
    point1 = gaze_point_data(y-1,:);
    point2 = gaze_point_data(y+1,:);
    vec1 = point1 - eye_pos;
    vec2 = point2 - eye_pos;
    a = rad2deg(atan2(norm(cross(vec1,vec2)),dot(vec1,vec2))); % calculates the angle between both vectors, don't understand this very well
    time_diff = velocity_window/90;
    velocity = a/time_diff;
    fixation_output(y,1) = velocity;
    y = y+1;
end

fixation_output(length(fixation_output)+1, 1) = 0; % last row must be added to match subjects matrix

%% Classifier

% classify as fixations/saccades based on velocity (we are only interested
% in fixations)

for y = 1:length(fixation_output)
    % compare velocity to threshold
    if fixation_output(y,1) >= velocity_threshold
        fixation_output(y,1) = 0; % classify as saccade
    elseif fixation_output(y,1) < velocity_threshold
        fixation_output(y,1) = 1; % classify as fixation
    end
end

%% Fixation info

% collapse fixation samples into fixation groups (one group = one fixation)
fixation_output(:,2) = 0; % create column of zeros
fixation_coord = [];
y = 2;

while y < length(fixation_output)
    if fixation_output(y,1) == 1
        n = 0;
        next_sample = y;
        
        % number the fixation group
        fixation_nb = max(unique(fixation_output(:,2))) + 1; % number all samples in the fixation as the max number so far + 1
        while fixation_output(next_sample,1) == 1 && next_sample < length(fixation_output)
            fixation_output(next_sample,2) = fixation_nb;
            next_sample = next_sample + 1; % keep looping until classification is not 1 anymore
            n = n+1; % counter to keep track of nb of samples in fixation
        end
        
        % calculate mean of coordinates to determine where fixation group is centered 
        mean_x = nanmean(x_column(y:y+n-1,1));
        mean_y = nanmean(y_column(y:y+n-1,1));
        fixation_coord(fixation_nb,1) = fixation_nb;
        fixation_coord(fixation_nb,2:3) = [mean_x, mean_y];
        
        y = next_sample + 1; % get first while loop to skip samples examined in second while loop
        
    else y = y+1;
        
    end
end

