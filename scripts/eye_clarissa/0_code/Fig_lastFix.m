
%% set paths

addpath('C:\Users\Clarissa\Documents\MATLAB\Downloads\Packages\gramm')% plotting package
paths.LOAD_DATA_PATH_LVL2 = 'C:\Users\Clarissa\Documents\decid_eye\analysis\manip1&2\1_pipeline\fixations\3_f_fixation_behav\out\lvl1_agent\';
paths.PLOTS_PATH = 'C:\Users\Clarissa\Documents\decid_eye\analysis\manip1&2\Figs';

%% load data

stats_Lvl1 = importdata(fullfile(paths.LOAD_DATA_PATH_LVL2, 'stats_Lvl1.mat'));

%% set parameters

options = stats_Lvl1.options;
nbBins = length(options.prctile);

%% plot w. "plausible PSE participants" only

plausibleVals = stats_Lvl1.PSE.lastFixGains > -40 & stats_Lvl1.PSE.lastFixGains < 40 & stats_Lvl1.PSE.lastFixLosses > -40 & stats_Lvl1.PSE.lastFixLosses < 40;
nSub = nnz(plausibleVals);

% choice as a function of G-L, differentiating between trials where last
% fixation is to the right from those where last fix is to the left

g = gramm('x', [1:nbBins], 'y', vertcat(stats_Lvl1.(options.bin_method).bin_var_choice_lastfixG(:, plausibleVals)', stats_Lvl1.(options.bin_method).bin_var_choice_lastfixL(:, plausibleVals)'), 'color', [repelem(2, nSub) repelem(1, nSub)]');
%g.geom_jitter()
g.stat_summary('type', 'sem', 'geom', {'point','errorbar'});  %'line'
g.set_color_options('map', 'matlab')
g.stat_glm('distribution', 'binomial')
g.set_names('x','V^G - V^L , percentile','y','P(accept)', 'color', 'Last fix to G(2) or L(1)');
g.axe_property('XTick', [1:1:nbBins], 'xticklabels', [10, 20, 30, 40, 50, 60, 70, 80, 90, 100]);

figure('Position',[100 100 550 350]);
g.draw();
f_saveToMultipleFormats(fullfile(paths.PLOTS_PATH, 'choiceAsFunctionOfValueDiff_lastFix_plausibleData'))

%% same with absolute values of bins on x axis

g = gramm('x',  vertcat(stats_Lvl1.(options.bin_method).bin_val_choice_lastfixG(:, plausibleVals)', stats_Lvl1.(options.bin_method).bin_val_choice_lastfixL(:, plausibleVals)'), 'y', vertcat(stats_Lvl1.(options.bin_method).bin_var_choice_lastfixG(:, plausibleVals)', stats_Lvl1.(options.bin_method).bin_var_choice_lastfixL(:, plausibleVals)'), 'color', [repelem(2, nSub) repelem(1, nSub)]');
%g.geom_jitter()
g.stat_summary('type', 'sem', 'geom', {'point','errorbar'});  %'line'
g.set_color_options('map', 'matlab')
g.stat_glm('distribution', 'binomial')
g.set_names('x','V^G - V^L','y','P(accept)', 'color', 'Last fix to G(2) or L(1)');
%g.axe_property('XTick', [1:1:nbBins], 'xticklabels', [10, 20, 30, 40, 50, 60, 70, 80, 90, 100]);

figure('Position',[100 100 550 350]);
g.draw();
f_saveToMultipleFormats(fullfile(paths.PLOTS_PATH, 'choiceAsFunctionOfValueDiff_lastFix_plausibleData_AbsVals'))

%% plot w. all  participants

nSub = stats_Lvl1.nSub;

% choice as a function of G-L, differentiating between trials where last
% fixation is to the right from those where last fix is to the left

g = gramm('x', [1:nbBins], 'y', vertcat(stats_Lvl1.(options.bin_method).bin_var_choice_lastfixG', stats_Lvl1.(options.bin_method).bin_var_choice_lastfixL'), 'color', [repelem(2, nSub) repelem(1, nSub)]');
%g.geom_jitter()
g.stat_summary('type', 'sem', 'geom', {'point','errorbar'});  %'line'
g.set_color_options('map', 'matlab')
g.stat_glm('distribution', 'binomial')
g.set_names('x','G-L, percentile','y','P(accept)', 'color', 'Last fix to G(2) or L(1)');
g.axe_property('XTick', [1:1:nbBins], 'xticklabels', [10, 20, 30, 40, 50, 60, 70, 80, 90, 100]);

figure('Position',[100 100 800 500]);
g.draw();
f_saveToMultipleFormats(fullfile(paths.PLOTS_PATH, 'choiceAsFunctionOfValueDiff_lastFix_all'))

