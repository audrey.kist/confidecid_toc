function [fixation_classes_interp, LogReclassifiedFixs] = f_interpolate_fixations(trials, fixation_nbs, fixation_classes)
 
% f_interpolate_fixations   Re-classifies fixations outside of ROIs located 
%   between 2 fixations to the same ROI as a fixation to that ROI. 
%
%   Input: - column of trial nbs (1 row per sample)
%          - column of fixation nbs (1 row per sample)
%          - column of fixation classes (1 row per sample)
%
%   Output: vector of fixation classes (1 row per sample) with fixations
%           interpolated
%
%   Clarissa Baratin, 2020, edited Feb 2021

nb_trials = max(unique(trials));

fixation_classes_interp = fixation_classes;

nFixInterpolated = 0;

% for each trial
for i_trial = 1:nb_trials
    
    % get subset of fixations in trial
    trial_rows = find(trials == i_trial);
    fixations_in_trial = unique(fixation_nbs(trial_rows));
    fixations_in_trial = fixations_in_trial(fixations_in_trial ~= 0);
    
    % for each fixation, except extremes (can't look at n-1/n+1)
    for i_fix = 2:length(fixations_in_trial)-1
        
        fixation_nb = fixations_in_trial(i_fix);
        
        current_fixation_rows = find(fixation_nbs == fixation_nb);
        current_fixation_class = fixation_classes(current_fixation_rows(1));
        
        previous_fixation_rows = find(fixation_nbs == fixation_nb - 1);
        previous_fixation_class = fixation_classes(previous_fixation_rows(1));
        
        next_fixation_rows = find(fixation_nbs == fixation_nb + 1);
        next_fixation_class = fixation_classes(next_fixation_rows(1));
        
        % if fixation n is a reject and class(n-1) == class(n+1)
        if current_fixation_class == 6 && (previous_fixation_class == next_fixation_class)
            
            nFixInterpolated = nFixInterpolated + 1;
            
            % keep log of interpolated fixations
            LogReclassifiedFixs(nFixInterpolated).fixNb = fixation_nb; 
            LogReclassifiedFixs(nFixInterpolated).fixRows = current_fixation_rows;             
            LogReclassifiedFixs(nFixInterpolated).currentFixClass = current_fixation_class;
            LogReclassifiedFixs(nFixInterpolated).previousFixClass = previous_fixation_class;
            LogReclassifiedFixs(nFixInterpolated).nextFixClass = next_fixation_class;
            
            % class(n) = class(n-1) = class(n+1)
            new_class = previous_fixation_class;
            fixation_classes_interp(current_fixation_rows) = new_class;
        end
    end
end


% unit test (old version)

% example_data_fixation_coord = [2 1 1 2; 3 1 1 6; 4 1 1 2; 5 1 1 3; 7 1 1 2; 8 1 1 5; 9 1 1 6; 10 1 1 5];
%
% for i_fix = 2:length(example_data_fixation_coord)-1
%
%     fixation_nb = example_data_fixation_coord(i_fix,1);
%     current_fixation = example_data_fixation_coord(example_data_fixation_coord(:,1) == fixation_nb, :);
%     previous_fixation = example_data_fixation_coord(example_data_fixation_coord(:,1) == fixation_nb - 1, :);
%     next_fixation = example_data_fixation_coord(example_data_fixation_coord(:,1) == fixation_nb + 1, :);
%
%     % if fixation n is a reject and class(n-1) == class(n+1)
%     if current_fixation(4) == 6 && (previous_fixation(4) == next_fixation(4))
%
%         % class(n) = class(n-1) = class(n+1)
%         new_class = previous_fixation(4);
%         fixation_coord_rows_to_replace = find(example_data_fixation_coord(:,1) == fixation_nb);
%         example_data_fixation_coord(fixation_coord_rows_to_replace, 4) = new_class;
%     end
% end

end

