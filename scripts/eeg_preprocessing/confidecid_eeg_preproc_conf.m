%% CONFIDECID_EEG_PREPROC_CONF Script
% contains all preprocessing parameters that are used in 
% perithr_eeg_preprocessing and perithr_eeg_preprocing_all

addpath functions

%% checking user
if ismac
    user = 'michael';
else
    user = 'audrey';
end

fprintf(' [ conf ] setting user to %s\n',user);

% user-dependent paths
if strcmp(user,'audrey')
    conf.fieldtrip = 'D:/Documents/MATLAB/fieldtrip20201214'; % fieltrip toolbox path
    conf.bids = 'D:/OneDrive - PhD/OneDrive/Documents/01 data-bids';
elseif strcmp(user,'michael')
    conf.fieldtrip = '~/Dropbox/code/fieldtrip';
    conf.bids = '/Volumes/Data/data/BIDS/perithr_faces';
end

%% session-subject specific attributes
% which subject
conf.subj = 'sub-dals';
% which session
conf.ses = 'ses-off';
conf.stimfilter = 'off';
conf.stimfreq = 130; % cutoffs will be performed at freq and freq/2

%% required processing level
% which preproc stage to compute, e.g. {'stg1','stg2','stg3','stg4','stg5','stg6','stg7'}
% other stages will be loaded from stg files
conf.compute = {'stg2'};

% the level of debug plotting
conf.plotlevel = 2;


%% stg1 preproc parameters
conf.plotelec = [33 8 14 2 24 48 60 54 18]; % the electrodes to plot for debug, must be 9
conf.maxfreq = 45; % max freq for low pass filter in Hz
conf.minfreq = 0.5; % min freq for high pass filter in Hz
conf.hporder = 2; % high pass filter order
conf.bsfreq = [49,51]; % bandstop filter to remove 50hz
conf.resamp_freq = 500; % resampling freq in Hz

%% stg2 preproc parameters 
conf.event.lock = 'cue'; % type of time-locking 'cue' (cannot be 'stim' like in perithr)
conf.event.prestim = 1; % time before stimulus for epoching, in sec
conf.event.poststim = 1; % time after stimulus for epoching, in sec
conf.event.realign2photodiode = 0; % 1 = yes, realign events to photodiode for better timing
% the number of standard deviations that the photodiode needs to exceed for
% a peak to be considered as a visual frame
conf.event.mintrigdiff = 2000; 

%% stg4 preproc parameters
% number of independant components to compute (ICA)
conf.nic = -1; % set to -1 to use icapcavar
conf.icapcavar = 0.99;
conf.inspected_trials = [];
conf.cap_layout = 'acticap-64ch-standard2.mat';

%% create directories that don't exist
checkdirs(conf);