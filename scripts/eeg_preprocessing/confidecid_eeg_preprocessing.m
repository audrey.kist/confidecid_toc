%% CONFIDECID_EEG_PREPROCESSING Script
% pre-processing, part A : loops on a folder to process all eeg files
% based on the data contained in behavioral files.
%   step 1: freq filtering
%   step 2: epoching  
% all this data is saved in the DERIVATIVE folder.
%
% then proceeeding with pre-processing part B :
%   step 3 : visual inspection to remove defective electrodes
%   step 4 : ICA computing
%   step 5 : inspect independant components and reject EOG components ? 

%% SCRIPT

close all;
clear all;

% load configuration file. all parameters such as file directories, toolbox 
% directories etc. shall be specified here. 
confidecid_eeg_preproc_conf;


% initialize FieldTrip
addpath(conf.fieldtrip);                                                                                                                                                                                                                                                                                                

ft_defaults;

% getting directories - data should be organised as BIDS recommends
eegdir = [conf.bids '/' conf.subj '/' conf.ses '/eeg'];
behdir = [conf.bids '/' conf.subj '/' conf.ses '/beh'];

% derivdir collects processed data as .mat files
conf.derivdir = [conf.bids '/derivatives/eegprep/' conf.subj '/' conf.ses '/eeg'];

% script is based on the behavioral file name
file = [conf.subj '_' conf.ses '_task-confidecid'];

% setting filenames for later savings (stg1 and 2 are in preproc loop)
conf.stg2_filename2     = [conf.derivdir '/' file '_stg2'];
conf.stg3_filename      = [conf.derivdir '/' file '_stg3'];
conf.stg4_filename      = [conf.derivdir '/' file '_stg4_ica'];
conf.stg5_filename      = [conf.derivdir '/' file '_stg5'];
conf.stg6_filename      = [conf.derivdir '/' file '_stg6'];
conf.stg7_filename      = [conf.derivdir '/' file '_stg7'];

% setting d as a structure containing all the file names in the directory
d = dir([behdir '/' conf.subj '_' conf.ses '_task-confidecid*_beh.mat']);
fprintf(' [ init ] found %d files in %s\n',length(d),behdir);



%% stage 1 & stage 2 processing or data loading +
% selects eeg data and corresponding event data from beh file,
% for all runs, and puts them in data and evt tables.

if any(contains(conf.compute,'stg1')) || any(contains(conf.compute,'stg2'))

    % loops on all files of the behaviour directory:
    for r=1:length(d)
        conf.run = sprintf('run-0%d',r);
        fprintf('------------------\n   RUN-0%d   \n------------------\n',r);
        
        % getting existing files names
        conf.raw_filename   = d(r).name(1:end-8);
        conf.beh_filename   = [behdir '/' conf.raw_filename '_beh.mat'];
        conf.vhdr_filename  = [eegdir '/' conf.raw_filename '_eeg.vhdr'];
        % creating filenames for saving
        conf.stg1_filename  = [conf.derivdir '/' conf.raw_filename '_stg1'];
        conf.stg2_filename  = [conf.derivdir '/' conf.raw_filename '_stg2'];

        %% stage 1 preprocessing
        % only if required in the conf file
        % otherwise, gets file from the derivative folder
        if any(contains(conf.compute,'stg1'))
            fprintf(' [ stage1 ] start \n');
            % stage 1 preprocessing: filtering and downsampling 
            cfg = [];
            cfg.dataset = conf.vhdr_filename;
            rundata = ft_preprocessing(cfg);
            % saving photodiode index
            conf.idphotod = strcmp(rundata.label,'Photodiode');
            % filter and downsample
            rundata = stg1_processing(rundata, conf);
        else
            % load data from previously processed file
            fprintf(' [ stage1 ] skipping \n');
            x = load(conf.stg1_filename);
            rundata = x.data; 
            clear x;
        end
        
        %% stage 2 preprocessing: epoching
        if any(contains(conf.compute,'stg2'))
            fprintf(' [ stage2 ] start\n');
            % saving photodiode index
            conf.idphotod = strcmp(rundata.label,'Photodiode'); % TO DO HERE : TEST ON PHOTODIODE

            % stge 2 preproc: epoching
            rundata = stg2_processing_confidecid(rundata, conf);

            % next goal is to concatenate data
            % getting events table
            % runevt = readtable(conf.beh_filename,'FileType','text');
            behdata = load(conf.beh_filename);
            runevt = behdata.data;
           
            % building events table with associated data
            % if file is the first in the folder, then setting first event
            % otherwise appends event to list
            if r==1
                allruns_data = rundata;
                allruns_evt = runevt;
            else
                allruns_data = ft_appenddata([],allruns_data,rundata);
                allruns_evt = [allruns_evt ; runevt];
            end
            clear rundata runevt
        else
            fprintf(' [ stage2 ] skipping \n');
        end

    end
else
    fprintf(' [ stage1 ] skipping \n');
end


%% stage 2 preproc - part 2 : saving concatenated data
% after putting behavior data in the same structure as eeg
if any(contains(conf.compute,'stg2'))
    allruns_data.behav = allruns_evt;
    save(conf.stg2_filename2,'allruns_data');
else
    fprintf(' [ stage2 ] skipping \n');
    x = load(conf.stg2_filename2);
    allruns_data = x.allruns_data;
    clear x;
end

%% stage 3 preproc - visual inspection, reject trials, CAR
if any(contains(conf.compute,'stg3'))
    fprintf(' [ stage3 ] start\n');
    allruns_data = stg3_processing_confidecid(allruns_data, conf);
else
    fprintf(' [ stage3 ] skipping \n');
    x = load(conf.stg3_filename);
    allruns_data = x.data;
    clear x;
end

%% stage 4 preproc - Independant Components Analysis
if any(contains(conf.compute,'stg4'))
    fprintf(' [ stage4 ] start ICA\n');
    ica_data = stg4_processing(allruns_data, conf);
else
    fprintf(' [ stage4 ] skipping \n');
    x = load(conf.stg4_filename);
   % ica_data = x.data;
    ica_data= x.comp ;
    clear x;
end

%% stage 5 preproc - Removing EOG & DBS artefacts
if any(contains(conf.compute,'stg5'))
    fprintf(' [ stage5 ] start\n');
    % inspect data and reject artefact components
    allruns_data = stg5_processing_confidecid(allruns_data, ica_data, conf);
else
    fprintf(' [ stage5 ] skipping \n');
    x = load(conf.stg5_filename);
    allruns_data = x.data;
    clear x;
end

%% stage 6 preproc - Visual inspection
if any(contains(conf.compute,'stg6'))
    fprintf(' [ stage6 ] start\n');
    % last inspect data and reject artefact components
    allruns_data = stg6_processing(allruns_data, conf);
else
    fprintf(' [ stage6 ] skipping \n');
    x = load(conf.stg6_filename);
    allruns_data = x.data;
    clear x;
end

%% stage 7 preproc - cleaning behavioral data
if any(contains(conf.compute,'stg7'))
    fprintf(' [ stage7 ] start\n');
    allruns_data = stg7_processing_confidecid(allruns_data, conf);
    fprintf(' [ stage7 ] end\n');
else
    fprintf(' [ stage7 ] skipping \n');
end

%%
