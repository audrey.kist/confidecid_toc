function createdirs(conf,subj,ses)
%UNTITLED4 Summary of this function goes here
%   Detailed explanation goes here
d = [conf.bids '/derivatives/eegprep/' subj];
if ~exist(d,'dir')
    fprintf(' [ conf ] did not find %s dir, creating ... \n',d);
    mkdir(d);
end
d = [conf.bids '/derivatives/eegprep/' subj '/' ses];
if ~exist(d,'dir')
    fprintf(' [ conf ] did not find %s dir, creating ... \n',d);
    mkdir(d);
end
d = [conf.bids '/derivatives/eegprep/' subj '/' ses '/eeg/'];
if ~exist(d,'dir')
    fprintf(' [ conf ] did not find %s dir, creating ... \n',d);
    mkdir(d);
end

end

