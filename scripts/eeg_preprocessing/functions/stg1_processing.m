function data = stg1_processing(data, conf)
%STG1_PROCESSING stage 1 eeg data processing
% call as proc_data = stg1_processing(data,conf)
% loads data from .vhdr file
% low-pass filter 
% downsample 
% high-pass filter 
% cut-off to remove 50 Hz
% cut-off to remove DBS subharmonics if required
% display spectral analysis at each steps
% saving as .mat file

    %%  preparing Welch�s power spectral density estimate for raw data
    if conf.plotlevel > 1
        for el = 1:length(conf.plotelec)
            plotelec = conf.plotelec(el);
            [a, b] = pwelch(data.trial{1}(plotelec,:),data.fsample,0,data.fsample,data.fsample);
            spectra_struct(el).s0 = a;
            spectra_struct(el).w0 = b;
        end
        clear a ;clear b ;
    end
    
    %% low pass filtering
    cfg = [];
    cfg.lpfilter = 'yes';
    cfg.lpfreq = conf.maxfreq ; 
    fprintf(' [ stage1 ] lowpass filtering with cutoff at %.1f Hz\n',cfg.lpfreq);
    data = ft_preprocessing(cfg,data);
    
    % preparing Welch�s power spectral density estimate
    if conf.plotlevel > 1
        for el = 1:length(conf.plotelec)
            plotelec = conf.plotelec(el);
            [a, b] = pwelch(data.trial{1}(plotelec,:),data.fsample,0,data.fsample,data.fsample);
            spectra_struct(el).s1 = a;
            spectra_struct(el).w1 = b;
        end
        clear a ;clear b ;
    end

    %% downsampling
    cfg=[];
    cfg.resamplefs = conf.resamp_freq;
    fprintf(' [ stage1 ] resampling at %.0f Hz\n',cfg.resamplefs);
    data = ft_resampledata(cfg,data);
    
    % preparing Welch�s power spectral density estimate
    if conf.plotlevel > 1
        for el = 1:length(conf.plotelec)
            plotelec = conf.plotelec(el);
            [a, b] = pwelch(data.trial{1}(plotelec,:),data.fsample,0,data.fsample,data.fsample);
            spectra_struct(el).s2 = a;
            spectra_struct(el).w2 = b;
        end
        clear a ;clear b ;
    end
    
    %% high pass + notch filtering + saving
    cfg = [];
    cfg.hpfilter = 'yes';
    cfg.hpfiltord = conf.hporder;
    cfg.hpfreq = conf.minfreq ; 
    cfg.bsfilter = 'yes';
    cfg.bsfreq = conf.bsfreq;
    cfg.outputfile = conf.stg1_filename;  
    fprintf(' [ stage1 ] highpass filtering (order: %d) with cutoff at %.1f Hz\n',cfg.hpfiltord,cfg.hpfreq);    
    data = ft_preprocessing(cfg,data);
    
   % preparing Welch�s power spectral density estimate
    if conf.plotlevel > 1
        for el = 1:length(conf.plotelec)
            plotelec = conf.plotelec(el);
            [a, b] = pwelch(data.trial{1}(plotelec,:),data.fsample,0,data.fsample,data.fsample);
            spectra_struct(el).s3 = a;
            spectra_struct(el).w3 = b;
        end
        clear a ;clear b ;
    end
    
    %% DBS filtering if needed
    if contains(conf.stimfilter,'on') 
        bandstop = [(conf.stimfreq -1) (conf.stimfreq +1)]; 
        cfg = [];
        cfg.bsfilter = 'yes';
        cfg.bsfreq = bandstop;
        cfg.outputfile = conf.stg1_filename;  
        fprintf(' [ stage1 ] filtering %.0f Hz DBS freq with cutoff at %.0f-%.0f Hz\n',conf.stimfreq, bandstop');    
        data = ft_preprocessing(cfg,data);
        
        %stimfreq/2 notch filter
        bandstop = [(conf.stimfreq/2-1) (conf.stimfreq/2+1)]; 
        cfg = [];
        cfg.bsfilter = 'yes';
        cfg.bsfreq = bandstop;
        cfg.outputfile = conf.stg1_filename;  
        fprintf(' [ stage1 ] filtering %.0f Hz DBS subharmonics with cutoff at %.0f-%.0f Hz\n',conf.stimfreq/2, bandstop');    
        data = ft_preprocessing(cfg,data);     
            
        % preparing Welch�s power spectral density estimate
        if conf.plotlevel > 1  
            for el = 1:length(conf.plotelec)
                plotelec = conf.plotelec(el);
                [a, b] = pwelch(data.trial{1}(plotelec,:),data.fsample,0,data.fsample,data.fsample);
                spectra_struct(el).s4 = a;
                spectra_struct(el).w4 = b;
            end
            clear a ;clear b ;
        end
    end
    
    %% displaying Welch�s power spectral density estimate
    % on 9 electrodes specified in conf file
    if conf.plotlevel > 1
        h = figure(500); clf; 
        set(gcf,'Position', [10 10 1200 800])
        hold on;

        for el = 1:length(conf.plotelec)
            subplot(3,3,el);           
            plotelec = conf.plotelec(el);
            % h = figure(101); clf; 
            hold on;
            plot(spectra_struct(el).w0,10*log(spectra_struct(el).s0),'LineWidth',2);
            plot(spectra_struct(el).w1,10*log(spectra_struct(el).s1),'LineWidth',2);
            plot(spectra_struct(el).w2,10*log(spectra_struct(el).s2),'LineWidth',2);
            plot(spectra_struct(el).w3,10*log(spectra_struct(el).s3),'LineWidth',2);
            if contains(conf.stimfilter,'on') 
                plot(spectra_struct(el).w4,10*log(spectra_struct(el).s4),'LineWidth',2)
                if el==1
                    legend({'Original','Lowpassed','Downsampled','Highpassed','DBS filtered'},'Location','NorthEast');
                end              
             elseif el==1
                legend({'Original','Lowpassed','Downsampled','Highpassed'},'Location','NorthEast'); 
             end
            title(sprintf('Electrode %s',data.hdr.label{plotelec}));
            xlabel('Frequency [Hz]');
            xlim([0,150])
            ylim([-150,100])
            set(gca,'XTick',0:20:150);
            % customize grid
            ax1 = gca; % the first axes
            ax2 = axes('Position',ax1.Position,...
              'XAxisLocation','bottom',...
              'YAxisLocation','left',...
              'Color','none',... 
              'Ylim',ax1.YLim,...
              'XLim',ax1.XLim,...
              'TickLength',[0 0],...
              'YTick', [ax1.YLim(1):50:ax1.YLim(2)], ...
              'XTick', [ax1.XLim(1):10:ax1.XLim(2)],  ...
              'YTickLabel', [],  ...
              'XTickLabel', []  );
            linkaxes([ax1 ax2],'xy')
            grid on
            
        end
        % derivdir
        % saveas(h,['figs/' conf.subj '_' conf.ses '_' conf.run '_stage1_freq.png']);
        saveas(h,[conf.derivdir '/' conf.subj '_' conf.ses '_' conf.run '_stage1_freq.png']);
    end
        
end