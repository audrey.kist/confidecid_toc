function data = stg2_processing_confidecid(data, conf)
%STG2_PROCESSING stage 2 eeg data processing
% call as proc_data = stg2_processing(data, conf)
% segmenting data as trials, based on stage 1 mat file and vmrk file for
% events
% trials defined as sequence from gain/loss screen to yes/no choice
    
% for debug
% x = load(conf.stg1_filename);
% data = x.data; 
% clear x;
    %% define 
    % define trials based on custom-made getevents.m function
    cfg = [];
    cfg.dataset = conf.vhdr_filename ;
    cfg.trialfun = 'getevents';   
    cfg.myconf = conf; % copy config structure to help getevents
  
    % this gets the table of beginning and end of each trial
    cfg_stim = ft_definetrial(cfg);
  
    % this redefines the data for each trial, accordingly
    data = ft_redefinetrial(cfg_stim, data);

    % saving as .mat file
    cfg_stim.outputfile = conf.stg2_filename;
    data = ft_preprocessing(cfg_stim,data);
    
    %% plot photodiode signal for each segmented trial to check alignment
    if conf.plotlevel>1
        
        h = figure(201); clf; hold on;
        for i=1:length(data.trial)
            plot(data.time{i},data.trial{i}(conf.idphotod,:));
        end
        %set(gca,'XTick',-2:20);
        %xlim([-2 20]);
        xlabel('Time from cue [s]');
        ylabel('Photodiode');
        set(gcf,'Position',[200,200,400,400])
        saveas(h,[conf.derivdir '/' conf.subj '_' conf.ses '_' conf.run '_stage2_align.png']);
    end
end 
