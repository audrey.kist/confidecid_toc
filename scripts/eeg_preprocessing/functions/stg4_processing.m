function comp = stg4_processing(data, conf)
%STG4_PROCESSING stage 4 eeg data processing
% returns ICA results
%%
    % find all electrodes but aux (heog, veog, dbs, photodiode)
    keepchans = ~contains(data.label,{'HEOG','VEOG','DBSArtefact','Photodiode'});
    elecs = data.label(keepchans);
    
   

        % concat all trials
        conc = cell2mat(data.trial);
        conc = conc(keepchans,:);
        % apply pca
        [~,~,~,~,expl] = pca(conc.');
        %
        expl = expl*1e-2;
        cums = cumsum(expl);
        ncomp = find(cums > conf.icapcavar,1,'first');
       if conf.nic < 0  
        fprintf(' [ stage4 ] using %d components representing %.2f%% of the variance\n',ncomp,cums(ncomp)*1e2);

        if conf.plotlevel > 1
            h = figure(); hold on;
            plot(20*log10(expl),'LineWidth',2);
            plot(ncomp,20*log10(expl(ncomp)),'ro','LineWidth',2);
            ylim([-100,0])
            xlabel('# PC'); ylabel('Variance explained [dB]');
            set(gcf,'Position',[400,700,150,100]);
            %saveas(h,['figs/' conf.subj '_' conf.ses '_stage4_pcavar.png']);
            saveas(h,[conf.derivdir '/' conf.subj '_' conf.ses '_stage4_pcavar.png']);
        end
       else
        ncomp = conf.nic;
        fprintf(' [ stage4 ] using %d components (fixed) representing %.2f%% of the variance\n',ncomp,cums(ncomp)*1e2);
        
        
    end
    %% ICA
    cfg        = [];
    cfg.method = 'fastica';
    cfg.channel = elecs;
    cfg.fastica.numOfIC = ncomp; % number of ICA components
    cfg.randomseed = 1;  % fix random generator for reproducibility
    comp = ft_componentanalysis(cfg, data);

    % save independent components
    save(conf.stg4_filename,'comp');
end 
