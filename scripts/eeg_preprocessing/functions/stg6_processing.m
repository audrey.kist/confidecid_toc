function data = stg6_processing(data, conf)
%STG6_PROCESSING stage 3 eeg data processing
% visual inspection after all cleaning
% same as stage 3 but without CAR

    keepchans = ~contains(data.label,{'HEOG','VEOG','DBSArtefact','Photodiode'}); 
    elecs = data.label(keepchans);

    nb_electrodes = length(data.label);
    nb_trials = length(data.trial);  
    fprintf(' [ stage6 ] found %d electrodes for %d trials\n',nb_electrodes,nb_trials);
    
    go_on = true ;
    while go_on  
        fprintf([' [ stage6 ] what do you want to do?\n', ... 
            '- 1 inspect summary\n', ...
            '- 2 inspect electrodes\n', ...
            '- 3 inspect trials\n',...
            '- 4 custom cycle through trials \n',...
            '- 41 ft data browser \n',...
            '- 5 exit inspection\n']); 
        proc_choice = input('please enter choice:\n');  
        switch proc_choice
            case 1
                % visual inspection - summary 
                fprintf('starting summary inspection.\n')            
                cfg = [];
                cfg.method   = 'summary';
                cfg.keepchannel = 'yes';
                cfg.keeptrials = 'yes';
                cfg.channel = elecs ;    
                data = ft_rejectvisual(cfg,data);
            case 2
                % visual inspection of electrodes
                fprintf('starting electrodes visual inspection.\n')
                cfg = [];
                cfg.method   = 'channel';
                cfg.keepchannel = 'yes';
                cfg.keeptrials = 'yes';
                cfg.channel = elecs ;    
                data = ft_rejectvisual(cfg,data);
            case 3
                % visual inspection of trials 
                fprintf('starting trials visual inspection.\n')
                cfg = [];
                cfg.method   = 'trial';
                cfg.keepchannel = 'yes';
                cfg.keeptrials = 'yes';
                cfg.channel = elecs ;    
                data = ft_rejectvisual(cfg,data); 
            case 41
                % visual inspection trial by trial
                fprintf('starting trials visual inspection with ft databrowser.\n')
                cfg = [];
                cfg.channel = elecs ; % otherwise not visible wrt artifact channels
                cfg.viewmode ='vertical';
                cfg = ft_databrowser(cfg,data);    
            case 4
                % Plot electrodes activations for every trial
                fprintf('starting complete trials inspection.\n');
                figure(310);          
                % height of each trace on the plot
                % MP: changed to 100
                ticks = [(1:nb_electrodes)*100];% (nb_electrodes*300)];
                tr=1 ;
                while (0 < tr  && (tr < nb_trials +1)) 
                    fprintf('displaying trial %d \n',tr);
                    clf; hold on;
                    % plot each electrodes's activation, shifted by "ticks"
                    for el = 1:nb_electrodes -4 
                        plot(data.time{tr}, data.trial{1,tr}(el,:)+ticks(el));
                    end 
                    % plot stim onset
                    plot([0 0],ylim(),'k--')
                    xlabel('Time from stimulus [s]');
                    ylabel('Electrode');            
                    set(gca,'YTick',ticks(1:nb_electrodes -4 ),'YTickLabel',data.label(1:nb_electrodes -4));
                    axis ij tight
                    title(sprintf('Trial %d/%d',tr ,nb_trials));                    
                    next_step = input('please type 1 to go to next trial, -1 to previous etc.: ');
                    tr = tr + next_step;
                end
                fprintf('list of trials exceeded - exiting trial inspection.\n')
            case 5
                fprintf('exit visual inspection.\nproceeding to next step.\n')
                go_on = false;                 
            otherwise
                fprintf('could not understand answer.\nproceeding to next step.')
                go_on = false; 
        end
    end    

    %%  removing electrodes and trials 
    remove_order = input('do you want to remove electrodes or trials? y/n:\n','s');
    if remove_order == 'y'
        bad_electrodes = input('please type electrodes to be removed as {''xx'',''xx''}:\n');   
        bad_trials = input('please type trials to be removed as a vector:\n');   

       % deleting trials data 
        behav = data.behav;
        behav(bad_trials,:) = [];

        % listing trials to keep
        keeptrials = 1:length(data.trial); 
        keeptrials(bad_trials) = [];

        % listing electrodes to keep
        keepchans = ~contains(data.label,bad_electrodes); 
        elecs = data.label(keepchans);

        % deleting rejected trials and electrodes
        cfg = [];
        cfg.trials = keeptrials ;
        cfg.channel = elecs ;
        data = ft_preprocessing(cfg, data);    
    elseif remove_order =='n'
        fprintf('saving data as displayed.\n')
        behav = data.behav;
    else
        fprintf('did not understand request. saving data as displayed.\n');
        behav = data.behav;
    end 
    
    data.behav=behav; 
    save(conf.stg6_filename,'data');
    
end 
