function data = stg7_processing_confidecid(my_data, conf)
%STG7_PROCESSING stage 7 eeg data processing
% cleaning behavioral data and renaming 

    % renaming columns
    fprintf(' [ stage7 ] renaming behav data\n');
    
    newNames = ["subj","run","offer","gain","loss","diff","accept",...
        "rt_accept","confidence","rt_confidence","agent","win_stopball","rt_stopball","mood","rt_mood"];
    my_data.behav = array2table(my_data.behav,'VariableNames',newNames);
    
    % removing first column because it is useless
    my_data.behav(:,1) = [];
    
    % adding stimulation column 
    if strcmp(conf.ses,'ses-on')
        my_data.behav.stimulation = ones(height(my_data.behav),1);
    elseif strcmp(conf.ses,'ses-off')
        my_data.behav.stimulation = zeros(height(my_data.behav),1);
    else
        my_data.behav.stimulation = NaN(height(my_data.behav),1);
    end
    
    data = my_data;
    fprintf(' [ stage7 ] saving behav data\n');
    save(conf.stg7_filename,'data');
end

