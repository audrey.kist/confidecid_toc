function data = stg5_processing_confidecid(data, comp,conf)
%STG5_PROCESSING(data, comp,conf)
% stage 5 processing consists in removing HEOG, VEOG and DBS artefacts
% call as data = stg5_processing(data, comp,conf)
%       comp is an eeg data file containing main components from stg4
%       analysis
% works as follows:
%       1/ retrieves recorded EOG and DBS artefacts
%       2/ compares to independent components from stg4
%       3/ removes the component if correlated to EOG/artefact

    ncomp = length(comp.label);
    ntrial = length(comp.trial);
    fprintf(' [ stage5 ] found %d IC for %d trials\n',ncomp,ntrial);

    % get index of aux channels
    idheog = find(contains(data.label,'HEOG'));
    idveog = find(contains(data.label,'VEOG'));
    idart = find(contains(data.label,'DBSArtefact'));

    fprintf(' [ stage5 ] Computing correlation and spectra ... wait... ');
    
    %% computing IC vs Trials Correlation and spectral analysis
    cor = zeros(ncomp,3);
    for i = 1:ntrial % for every trial
        cor(:,:,i) = corr(comp.trial{i}.',data.trial{i}([idheog idveog idart],:).');
        for j=1:ncomp % for every component
            [spectr(:,j,i),w] = pwelch(comp.trial{i}(j,:).',comp.fsample,0,comp.fsample,comp.fsample);
        end
    end
    fprintf('done.  \n');
    
    %% Plot spectrum from each component
    h = figure(300); clf; hold on;
    j_ = 0; k = 0;
    for j=1:ncomp
        j_ = j_ + 1;
        subplot(4,4,j_);
        plot(w(1:80),10*log10(mean(spectr(1:80,j,:),3)));
        set(gca,'XTick',0:20:80);
        xlabel('Freq [hz]');
        title(sprintf('component %d',j));
        pause(0.1);
        title(sprintf('IC %d',j));
        if j_ >= 16
            % if there are more than 16 components we make a new figure
            k = k+1;
            j_ = 0;
            % saveas(h,['figs/' conf.subj '_' conf.ses '_stage5_icfreq_' num2str(k) '.png']);
            saveas(h,[conf.derivdir '/' conf.subj '_' conf.ses '_stage5_icfreq_' num2str(k) '.png']);
            h = figure(300+k); clf; hold on;
        end
    end
    k = k+1;
    saveas(h,[conf.derivdir '/' conf.subj '_' conf.ses '_stage5_icfreq_' num2str(k) '.png']);

    %% Plot trials from each component
    % in confidecid, each trial has a different length
    h = figure(310); clf; hold on;
    j_ = 0; k = 0;
    ntr = length(comp.trial);

    % all trials don't have the same number of samples so we trim to the
    % min
    nsamp = min(cellfun(@length,comp.trial));
    trim = @(x) x(:,1:nsamp);
    trimcells = cellfun(trim,comp.trial,'UniformOutput',0);

    % we reorganize trials into a 3D matrix
    reorg = reshape(cell2mat(trimcells),[ncomp,nsamp,ntr]);
   
    % not necessary in confidecid because stim is always at same position
   % [ons,ions] = sort(data.behav.stimpos*0.2);
   ons = ones(length(data.behav),1);
   ions = (1:1:length(data.behav))';
   

 %   if strcmp(conf.event.lock,'cue')
     t = linspace(-conf.event.prestim,4+conf.event.poststim,nsamp);
     
%     elseif strcmp(conf.event.lock,'stim')
%         t = linspace(-conf.event.prestim,conf.event.poststim,nsamp);
%     end
    % plotting
    for j=1:ncomp
        %%
        j_ = j_ + 1;
        subplot(4,4,j_);
        trials = squeeze(reorg(j,:,ions)).';
        
        imagesc(t,1:ntr,trials);
        hold on;
        plot([0 0],[1,ntr],'k');
        if strcmp(conf.event.lock,'cue')
            plot(ons,1:ntr,'k--');            
        elseif strcmp(conf.event.lock,'stim')
            plot(-ons,1:ntr,'k--');
        end
        % adjust scale
        sigma = max(std(trials.'));
        caxis(1.5*[-sigma,sigma])
        xlabel('Time [s]'); ylabel('Trials');
        title(sprintf('component %d',j));
        pause(0.1);
        title(sprintf('IC %d',j));
        if j_ >= 16
            % if there are more than 16 components we make a new figure
            k = k+1;
            j_ = 0;
            % saveas(h,['figs/' conf.subj '_' conf.ses '_stage5_ictrials_' num2str(k) '.png']);
            saveas(h,[conf.derivdir '/' conf.subj '_' conf.ses '_stage5_ictrials_' num2str(k) '.png']);
            h = figure(310+k); clf; hold on;
            title('IC to trials correlation');
        end
    end

    k = k+1;
    saveas(h,[conf.derivdir '/' conf.subj '_' conf.ses '_stage5_ictrials_' num2str(k) '.png']);

    %% Plot correlation with EOG and DBS artifact channel
    h = figure(321); clf; hold on;

    % average correlation across trials
    cor_heog =  mean(cor(:,1,:),3);
    cor_veog =  mean(cor(:,2,:),3);
    cor_dbs =  mean(cor(:,3,:),3);
    title('Average IC correlation accross trials');

    % plot average and standard deviation
    errorbar(cor_heog,std(cor(:,1,:),[],3));
    errorbar(cor_veog,std(cor(:,2,:),[],3));
    % plot dbs correlection only if dbs is on, otherwise useless +
    % confusing
    if strcmp(conf.ses,'ses-on')
        errorbar(cor_dbs,std(cor(:,3,:),[],3));
    end
    % errorbar(cor_dbs,std(cor(:,3,:),[],3));
    xlabel('IC');
    ylabel('Correlation (R)');
    set(gca,'XTick',1:ncomp);
    grid on
    % set(gcf,'Position',[100,100,700,200]);
    if strcmp(conf.ses,'ses-off')
        legend({'HEOG','VEOG'});
        title('Correlation with EOG channels');
    else
        legend({'HEOG','VEOG','DBS'});
        title('Correlation with EOG and DBS channels');
    end
    % title('Correlation with EOG and DBS channels');
    set(gcf, 'Position', get(0, 'Screensize')); %setting full screen for better saving resolution
    saveas(h,[conf.derivdir '/' conf.subj '_' conf.ses '_stage5_iccorr.png']);
    
    %% Plot topographic map of ICs
    h = figure(303);
    cfg = [];
    cfg.component = 1:ncomp;       % specify the component(s) that should be plotted
    cfg.layout    = conf.cap_layout ; % specify the layout file that should be used for plotting
    cfg.comment   = 'no';
    ft_topoplotIC(cfg, comp)
    set(gcf, 'Position', get(0, 'Screensize')); %setting full screen for better saving resolution
    saveas(h,[conf.derivdir '/' conf.subj '_' conf.ses '_stage5_ictopo.png']);

    %% Plot IC activations for every trial
    fprintf(' [ stage5 ] PRESS ANY KEY TO CYCLE THROUGH TRIALS\n');

    figure(410);
    % height of each trace on the plot
    ticks = [(1:ncomp)*300 ((ncomp+2)*300+(0:300:600))];
    for i=1:ncomp
        % each trace's little name
        tickslabel{i} = sprintf('IC %d',i);
    end
    % add aux channels at the bottom
    tickslabel{ncomp+1} = 'HEOG';
    tickslabel{ncomp+2} = 'VEOG';
    tickslabel{ncomp+3} = 'DBS';
    % for 1 out of 20 trials up to the end
    for tr = 1:20:ntrial
        clf; hold on;
        % plot each component's activation, shifted by "ticks"
        plot(comp.time{tr},bsxfun(@plus,comp.trial{tr}.',ticks(1:ncomp)));

        % plot aux channels
        plot(data.time{tr},data.trial{tr}(idheog,:)+ticks(ncomp+1));
        plot(data.time{tr},data.trial{tr}(idveog,:)+ticks(ncomp+2));
        plot(data.time{tr},data.trial{tr}(idart,:)+ticks(ncomp+3));

        % plot stim onset
        plot([0 0],ylim(),'k--')
        xlabel('Time from stimulus [s]');
        ylabel('IC');
        set(gca,'YTick',ticks,'YTickLabel',tickslabel);
        % invert plot and adjust axes
        axis ij tight
        title(sprintf('Trial %d/%d - press any key',tr ,ntrial));
        pause();

    end

    %% Artefacts removal
    % manually chosing components to remove:
    cfg = [];
    component = input('please type to be removed components as a row vector [,]:\n');
%     while ~isrow(component)
%         component = input('this is not a row vector. please type to be removed components as a row vector [,]:\n');
%     end     
    cfg.component = component;
    

    % save rejected components in a separate file:
    saverej = [conf.stg5_filename '_rejcomp.tsv'];
    tbl = table(cfg.component.',cor_heog(cfg.component),cor_veog(cfg.component),cor_dbs(cfg.component),'VariableNames',{'ic','corheog','corveog','cordbs'});
    writetable(tbl,saverej,'FileType','Text','Delimiter','\t');

    % reject components to get the proper stg5 data and saving:
    data = ft_rejectcomponent(cfg, comp, data);
    save(conf.stg5_filename,'data');

    close all;
end
