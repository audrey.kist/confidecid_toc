function data = stg3_processing_confidecid(data, conf)
%STG3_PROCESSING_CONFIDECID stage 3 eeg data processing
% visual inspection, removing faulty electrodes if needed
% and rereferencing to common average

    keepchans = ~contains(data.label,{'HEOG','VEOG','DBSArtefact','Photodiode'}); 
    elecs = data.label(keepchans);

    nb_electrodes = length(data.label);
    nb_trials = length(data.trial);  
    fprintf(' [ stage3 ] found %d electrodes for %d trials\n',nb_electrodes,nb_trials);
    
    go_on = true ;
    while go_on  
        fprintf([' [ stage3 ] what do you want to do?\n', ... 
            '- 1 inspect summary\n', ...
            '- 2 inspect electrodes\n', ...
            '- 3 inspect trials\n',...
            '- 4 custom cycle through trials \n',...
            '- 41 cycle through vEOG and hEOG \n',...
            '- 42 ft data browser \n',...
            '- 5 exit inspection\n']); 
        proc_choice = input('please enter choice:\n');  
        switch proc_choice
            case 1
                % visual inspection - summary 
                fprintf('starting summary inspection.\n')            
                cfg = [];
                cfg.method   = 'summary';
                cfg.keepchannel = 'yes';
                cfg.keeptrials = 'yes';
                cfg.channel = elecs ;    
                data = ft_rejectvisual(cfg,data);
            case 2
                % visual inspection of electrodes
                fprintf('starting electrodes visual inspection.\n')
                cfg = [];
                cfg.method   = 'channel';
                cfg.keepchannel = 'yes';
                cfg.keeptrials = 'yes';
                cfg.channel = elecs ;    
                data = ft_rejectvisual(cfg,data);
            case 3
                % visual inspection of trials 
                fprintf('starting trials visual inspection.\n')
                cfg = [];
                cfg.method   = 'trial';
                cfg.keepchannel = 'yes';
                cfg.keeptrials = 'yes';
                cfg.channel = elecs ;    
                data = ft_rejectvisual(cfg,data); 
            case 4
                % Plot electrodes activations for every trial
                fprintf('starting cycling through trials.\n');
                figure(310);          
                % height of each trace on the plot
                % MP: changed to 100
                ticks = [(1:nb_electrodes)*30];% (nb_electrodes*300)];
                tr=1 ;
                ellab = data.label;
                for e=1:length(ellab)
                    ellab{e} = [ellab{e} ' (' num2str(e) ')'];
                end
                while (0 < tr  && (tr < nb_trials +1)) 
                    fprintf('displaying trial %d \n',tr);
                    clf; hold on;
                    % plot each electrodes's activation, shifted by "ticks"
                    for el = 1:nb_electrodes -4 
                        % fprintf('electrode %d trial %d\n', el, tr);
                        plot(data.time{tr}, data.trial{1,tr}(el,:)+ticks(el));
                    end 
                    % plot stim onset
                    plot([0 0],ylim(),'k--')
                    xlabel('Time from stimulus [s]');
                    ylabel('Electrode');            
                    set(gca,'YTick',ticks(1:nb_electrodes -4 ),'YTickLabel',ellab(1:nb_electrodes -4));
                    axis ij tight
                    title(sprintf('Trial %d/%d',tr ,nb_trials));                    
                    next_step = input('please type 1 to go to next trial, -1 to previous etc.: ');
                    tr = tr + next_step;
                end
                fprintf('list of trials exceeded - exiting trial inspection.\n')
            case 41
                % visual inspection of EOG 
                fprintf('starting cycling through EOG trials.\n');
                figure(310);          
                % height of each trace on the plot
                % ticks = [(1:nb_electrodes)*30];% (nb_electrodes*300)];
                ticks = [(1:nb_electrodes)*30];% (nb_electrodes*300)];
                tr=1 ;
                ellab = data.label;
                for e=1:length(ellab)
                    ellab{e} = [ellab{e} ' (' num2str(e) ')'];
                end
                while (0 < tr  && (tr < nb_trials +1)) 
                    fprintf('displaying trial %d \n',tr);
                    clf; hold on;
                    % plot hEOG and vEOG, shifted by "ticks"
                    for el = nb_electrodes -3 : nb_electrodes -2
                        % fprintf('electrode %d trial %d\n', el, tr);
                        plot(data.time{tr}, data.trial{1,tr}(el,:)+ticks(el));
                    end 
                    % plot stim onset
                    plot([0 0],ylim(),'k--')
                    xlabel('Time from stimulus [s]');
                    ylabel('Electrode');            
                    set(gca,'YTick',ticks(nb_electrodes -3 : nb_electrodes -2),'YTickLabel',ellab(nb_electrodes -3 : nb_electrodes -2));
                    axis ij tight
                    title(sprintf('Trial %d/%d',tr ,nb_trials));                    
                    next_step = input('please type 1 to go to next trial, -1 to previous etc.: ');
                    tr = tr + next_step;
                end
                fprintf('list of trials exceeded - exiting trial inspection.\n')
            case 42
                % visual inspection trial by trial
                fprintf('starting trials visual inspection with ft databrowser.\n')
                cfg = [];
                cfg.channel = elecs ; % otherwise not visible wrt artifact channels
                cfg.viewmode ='vertical';
                cfg = ft_databrowser(cfg,data);    
            case 5
                fprintf('exit visual inspection.\nproceeding to next step.\n')
                go_on = false;                 
            otherwise
                fprintf('could not understand answer.\nproceeding to next step.')
                go_on = false; 
        end
    end    
    %%  removing electrodes and trials 
    bad_electrodes = input('please type electrodes to be removed as string array {''xx'',''xx''}:\n');  
    bad_trials = input('please type trials to be removed as a row vector [,]:\n');   
   
   % deleting trials data 
    behav = data.behav;
    behav(bad_trials,:) = [];
    
    % listing trials to keep   
    keeptrials = 1:length(data.trial); 
    keeptrials(bad_trials) = [];
    
    % listing electrodes to keep
    keepchans = ~contains(data.label,bad_electrodes); 
    elecs = data.label(keepchans);
    
    % deleting rejected trials and electrodes
    cfg = [];
    cfg.trials = keeptrials ;
    cfg.channel = elecs ;
    data = ft_preprocessing(cfg, data);    

    %% rereferencing to common average
    % fetch back FCz electrode

    keepchans = ~contains(data.label,{'HEOG','VEOG','DBSArtefact','Photodiode'}); 
    elecs = data.label(keepchans);
    cfg=[];
    cfg.reref = 'yes';
    cfg.refchannel = elecs; 
    cfg.implicitref = 'FCz';
    cfg.refmethod  = 'avg';
    data = ft_preprocessing(cfg, data); 
    
    %% saving stage 3 data 
    data.behav=behav; 
    save(conf.stg3_filename,'data');
    
end 
