function checkdirs(conf)
%CHECKDIRS Summary of this function goes here
%   checks if folders exist (to store prepared data)
%   + if not, creates
% if ~exist('figs','dir')
%     fprintf(' [ conf ] did not find figs dir, creating ... \n');
%     mkdir('figs');
% end
d = [conf.bids '/derivatives/'];
if ~exist(d,'dir')
    fprintf(' [ conf ] did not find %s dir, creating ... \n',d);
    mkdir(d);
end
d = [conf.bids '/derivatives/eegprep/'];
if ~exist(d,'dir')
    fprintf(' [ conf ] did not find %s dir, creating ... \n',d);
    mkdir(d);
end
d = [conf.bids '/derivatives/eegprep/' conf.subj];
if ~exist(d,'dir')
    fprintf(' [ conf ] did not find %s dir, creating ... \n',d);
    mkdir(d);
end
d = [conf.bids '/derivatives/eegprep/' conf.subj '/' conf.ses];
if ~exist(d,'dir')
    fprintf(' [ conf ] did not find %s dir, creating ... \n',d);
    mkdir(d);
end
d = [conf.bids '/derivatives/eegprep/' conf.subj '/' conf.ses '/eeg/'];
if ~exist(d,'dir')
    fprintf(' [ conf ] did not find %s dir, creating ... \n',d);
    mkdir(d);
end
% d = [conf.bids '/derivatives/behprep/' conf.subj '/beh/'];
% if ~exist(d,'dir')
%     fprintf(' [ conf ] did not find %s dir, creating ... \n',d);
%     mkdir(d);
% end
end

