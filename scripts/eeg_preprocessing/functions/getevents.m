function [trial,trig] = getevents(cfg)
%GETEVENTS custom function for ConfiDecid eeg preproc
%   get photodiode channel
%   read triggers in vmrk file
%   get events as in protocol
%             5; % D�but d'une session
%             10 = gain haut, oui gauche ; 
%             11 = gain haut, oui droite ; 
%             12 = gain bas, oui gauche ; 
%             13 = gain bas, oui droite cas agent / + 10 cas non agent
%             14; % R�ponse au choix / + 10 cas non agent
%             15; % Onset du confidence rating
%             150; % Confidence rating response
%             16; % Onset du feedback au d�fi
%             17; % Onset du mood rating
%             170; % Mood rating response
%             4; % D�but de l'entra�nement
%   read behavior file
%   read photodiode
    conf = cfg.myconf;

    % get photodiode channel.
    % All this processing is done at 5000 Hz (better accuracy)
    cfg = [];
    cfg.dataset = conf.vhdr_filename;
    cfg.channel = find(conf.idphotod);
    data = ft_preprocessing(cfg);

    %%

    % read triggers
    trig = ft_read_event(cfg.dataset);


    %% Get back events as in protocol (because of BrainProduct's weird encoding)
    % this part is because BrainProduct encodes events as:
    %       SXX c'est les 4 premiers bits 
    %       RXX les 4 derniers
    %           e.g. 15 is 0xF so 4 first bits so S15
    %           and 17 ist 0x11 so S1+R1
    % complicated for nothing (trust me and skip)
    val = zeros(1,length(trig)); % val with be the value of the trigger as specified in 
    % the task i. e. an int value and not S or R something : 10 11 12 13 for choice screen 
    idx = zeros(1,length(trig)); % le n� du sample de d�but
    bad = false(1,length(trig));

    for e=2:length(trig) % boucle sur la longueur du tableau trig
        val(e) = str2double(trig(e).value(3:end));
        idx(e) = trig(e).sample;
        if strcmp(trig(e).value(1),'R')
            if e+1<=length(trig)
                val2 = str2double(trig(e+1).value(3:end));
                if abs(trig(e).sample-trig(e+1).sample) < 50
                    next = val2;
                    bad(e+1) = 1;
                else
                    next = 0;
                end

            else
                next = 0;
            end
            val(e) = val(e)*16 + next;
        end
    end

    val(bad) = [];
    idx(bad) = [];

    %%
    % first trigger has 0 onset
    val(1) = [];
    idx(1) = [];

    % get trial start and end, encoded with frequency
    tstim = idx(ismember(val,[10;11;12;13;20;21;22;23]));
   % tbegtrial./data.fsample - conf.event.prestim;
    tbegtrial = tstim;
    tendtrial = idx(ismember(val,[14;24]));
    % tstim = idx(val==8);

    % check mean duration of trial
    % tdif = mean((tendtrial - tbegtrial)./data.fsample);
    tdif = mean((tendtrial - tbegtrial)./data.fsample);
    
    % fprintf(' [ events ] Found %d events (including %d cues, %d stims) - trial length: %.2f s\n',...
    %     length(val),length(tbegtrial),length(tstim),tdif);
    fprintf(' [ events ] Found %d events in eeg file, including %d cues corresponding to trials - mean trial length: %.2f s\n',... 
        length(val),length(tbegtrial),tdif);


    %% Read behav file to check if the number of trials is consistent
    fprintf(' [ events ] Reading behav from %s\n',conf.beh_filename);
    % load behavioral data
    behdata = load(conf.beh_filename);
    % get number of events
    num_evt = length(behdata.data);
    % get mean reaction times
    tdif_beh = mean(behdata.data(:,8));
    fprintf(' [ events ] Found %d trials in behav file - mean trial length: %.2f s\n', num_evt, tdif_beh);

    %% Read photodiode TO BE UPDATED FOR CONFIDECID
    % dos not work yet

    %%
    if conf.event.realign2photodiode
        %%
        % extract derivative of photodiode (so positive steps become peaks)
        photod = [diff(data.trial{1}(1,:)) 0]; % (add a zero to compensate diff)

        % zscore normalization with respect to a baseline computed one second before first
        % trig
        %base_mean = mean(photod(idx(1)+(-data.fsample:-1)));
        %base_std = std(photod(idx(1)+(-data.fsample:-1)));
        %base_mean = mean(photod(data.fsample*5+(1:data.fsample)));
        %base_std = std(photod(data.fsample*5+(1:data.fsample)));
        %photod = (photod - base_mean)./base_std;

        % find peaks corresponding to increase in luminance (i.e. frames)
        [pk,ipk] = findpeaks(photod,'MinPeakHeight',conf.event.mintrigdiff);

        % remove peaks occurring before the first trig
        pk(ipk < idx(1)) = [];
        ipk(ipk < idx(1)) = [];

        % since we know that peaks are every 200 ms, we can remove peaks until
        % we find the first two that are less than 1s apart

        id = 1;
        bad = false(size(ipk));
        while((ipk(id+1) - ipk(id)) > data.fsample)
            bad(id) = 1;
            id = id+1;
        end
        pk(bad) = [];
        ipk(bad) = [];

        % check time difference between each extracted frame
        dt = mode(diff(ipk)./data.fsample);
        fprintf(' [ events ] found %d frames (photodiode) - interval is %.2f [s]\n',length(ipk),dt);

        if conf.plotlevel > 1
            %% plot
            h = figure(100); clf; hold on;
            plot(data.time{1},photod);
            stem(data.time{1}(idx),val,'o');
            plot(data.time{1}(ipk),pk,'x');
            plot(xlim(),conf.event.mintrigdiff*[1 1],'r--');

            pause(0.1);
        end

        % for each event in the behav file, extract corresponding peaks
        % for i=1:nevt
        %     selpk = ipk > tbegtrial(i) & ipk < tendtrial(i);
        %     if i>1 && sum(selpk) ~= size(pktime,1)
        %         fprintf(' [ events ] !! Did not find the right number of peaks: %d, expected: %d, crash expected\n',...
        %             sum(selpk),size(pktime,1));
        %     end
        %     pktime(:,i) = ipk(selpk);
        %     % the peak corresponding to the face is given by evt.stimpos
        %     pktime_stim(i) = pktime(evt.stimpos(i),i);
        % end

        trans = find(diff(ipk) > 0.5*data.fsample);
        trans = [0 trans length(ipk)];
        num_evt = length(trans)-1;
        fprintf(' [ events ] found %d frames (photodiode) -> %d trials\n',length(ipk),num_evt);

        for i=1:length(evt.stimpos)
            selpk = trans(i)+(1:20);
            if i>1 && length(selpk) ~= size(pktime,1)
                fprintf(' [ events ] !! Did not find the right number of peaks: %d, expected: %d, crash expected\n',...
                    sum(selpk),size(pktime,1));
            end
            pktime(:,i) = ipk(selpk);
            % the peak corresponding to the face is given by evt.stimpos
            pktime_stim(i) = pktime(evt.stimpos(i),i);
        end

        % check for timing errrors
        % dt_stim = (pktime_stim(evt.intens>0) - tstim)./data.fsample;
        % fprintf(' [ events ] average trigger timing error: %.3f±%.3f ms (max: %2f)\n',mean(1e3*dt_stim),std(1e3*dt_stim),max(1e3*abs(dt_stim)));

        if conf.plotlevel > 1
            plot(data.time{1}(pktime_stim),pktime_stim*0+20,'ks','MarkerFaceColor','k');
            xlabel('Time [s]');
            xlim([0,80]);
            saveas(h,['figs/' conf.subj '_' conf.ses '_' conf.run '_events.png']);
        end

        % construct trial matrix (units: seconds)
        trial = nan(length(evt.stimpos),3);
        if strcmp(conf.event.lock,'cue')
            % align to the trial onset (fixation cross cue)
            trial(:,1) = pktime(1,:)./data.fsample - conf.event.prestim;
            trial(:,2) = (pktime(1,:)./data.fsample+4) + conf.event.poststim;
            % keep stim onset info as the offset
            trial(:,3) = trial(:,1)-(pktime_stim.')./data.fsample;
            [trial(:,1) (tbegtrial.')./data.fsample - conf.event.prestim]
        elseif strcmp(conf.event.lock,'stim')
            % align to the stim, in this case we don't care about trial start or
            % end
            trial(:,1) = pktime_stim./data.fsample - conf.event.prestim;
            trial(:,2) = pktime_stim./data.fsample + conf.event.poststim;
            trial(:,3) = -conf.event.prestim;%pktime_stim./data.fsample;
        else
            warning(['Unknown event locking (must be stim or cue: ' conf.event.lock]);
        end
    else
        %%
        % creating empty table of nan
        trial = nan(num_evt,3);
        % trial = nan(num_evt,2); 
        if strcmp(conf.event.lock,'cue')
            % align to the trial onset (fixation cross cue)
           % trial(:,1) = tbegtrial./data.fsample - conf.event.prestim;
            trial(:,1) = tbegtrial./data.fsample - 2*conf.event.prestim;
            % end of trial = time of reaction + poststim time
            trial(:,2) = tendtrial./data.fsample + conf.event.poststim;
            % stim offset is zero in confidecid and 
            % segmented trials starts just before the screen appears
            trial(:,3) = - 2*conf.event.prestim; % 
%             trial(:,1) = tbegtrial./data.fsample - conf.event.prestim;
%             % end of trial = time of reaction + poststim time
%             trial(:,2) = tendtrial./data.fsample + conf.event.poststim;
%             % stim offset is zero in confidecid and 
%             % segmented trials starts just before the screen appears
%             trial(:,3) = conf.event.prestim; % 
    %     elseif strcmp(conf.event.lock,'stim')
    %         % align to the stim, in this case we don't care about trial start or
    %         % end
    %         trial(:,1) = tstim./data.fsample - conf.event.prestim;
    %         trial(:,2) = tstim./data.fsample + conf.event.poststim;
    %         trial(:,3) = -conf.event.prestim; %pktime_stim./data.fsample;
        else
            warning(['Unknown event locking (must be cue only, no stim): ' conf.event.lock]);
        end

    end
    % finally, we multiply by the sampling frequency at which the data will be
    % resampled (remember the whole processing was done at 5000 hz
    trial = ceil((trial)*conf.resamp_freq);

    % trial = ceil(trial*conf.resamp_freq);

end

