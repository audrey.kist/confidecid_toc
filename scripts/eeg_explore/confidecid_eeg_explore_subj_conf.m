%% CONFIDECID_EEG_EXPLORE_SUBJ_CONF Script
% configuration parameters for confidecid_eeg_explore_subj
%

addpath functions

%% checking user
if ismac
    user = 'michael';
else
    user = 'audrey';
end

fprintf(' [ conf ] setting user to %s\n',user);

% user-dependent paths
if strcmp(user,'audrey')
    conf.fieldtrip = 'D:/Documents/MATLAB/fieldtrip20201214'; % fieltrip toolbox path
    conf.bids = 'D:/OneDrive - PhD/OneDrive/Documents/01 data-bids';
elseif strcmp(user,'michael')
    conf.fieldtrip = '~/Dropbox/code/fieldtrip';
    conf.bids = '/Volumes/Data/data/BIDS/perithr_faces';
end

%% session-subject specific attributes
% which subject
conf.subj = 'sub-dals';
% which session
% conf.ses = 'ses-on';
% conf.stimfreq = 130; % cutoffs will be performed at freq and freq/2

%% exploration stages to perform
% which exploration stage to compute, e.g. {'01_firstsec','02_lastsec','03_baselinesec','04_overallpower'}
% other stages will be loaded from previously saved files if necessary
conf.explore = {'04_overallpower'};

%% plot parameters
conf.plotlevel = 2; % level of debug plotting
conf.cap_layout = 'acticap-64ch-standard2.mat';


%% create directories that don't exist
checkdirs_eeg_explore(conf);

%% 01 params
% erp parameters
conf.interestwindow = [0.5 1];
conf.baselinewindow = [-0.5 -0];

%% 02 params
conf.trialwindow_end = [-0.5 0];
conf.baselinewindow_end = [-1 -0.5];
% align_end_of_data
conf.timebeforereaction = 1; % should be consistent with preproc length !
conf.timeafterreaction = 1;
conf.timebeforeend = 2; % time before end of data

%% 04 params



