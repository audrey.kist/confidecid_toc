%% plot_crf_electrode_subj_firstsecond

function plot_crf_electrode_subj_firstsecond_onoff(data_on, data_off, conf)

    cfg = [];
    cfg.baseline     = conf.cfg.baseline;
    cfg.baselinetype = conf.cfg.baselinetype;
   % cfg.maskstyle    = 'saturation';
   % cfg.zlim         = [0 30];
    cfg.xlim         = conf.cfg.xlim;
    cfg.ylim         = conf.cfg.ylim;
    cfg.channel      = conf.cfg.channel;
    cfg.layout       = conf.cap_layout;
    cfg.figure       = "gcf";
    
    my_title_overall = [conf.cfg.channel ' time-frequency representation'];
    my_title_on = [conf.subj ' ses-on'];
    my_title_off = [conf.subj ' ses-off'];  
    
    figure;
    subplot(2,1,1); ft_singleplotTFR(cfg, data_on); title(my_title_on); colorbar;
    subplot(2,1,2); ft_singleplotTFR(cfg, data_off); title(my_title_off); colorbar;
    sgtitle(my_title_overall)
    
    my_title = ['ses-onoff_singleplot_tfr_' conf.cfg.channel];
    save_subj_fig(gcf, my_title, conf)
    
end