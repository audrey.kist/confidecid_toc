%% EXP04_FIRSTSEC_SUBJ_CONFIDECID Script
% explore psd
% mandatory input :
%   data_on 
%   data_off
% output :
%   figures and models are recorded in the derivatives folder

function exp4_psd_subj_confidecid(my_data_on, my_data_off, conf)
%% for debut
my_data_on = data_on;
my_data_off = data_off;
    
    %% getting tfr
    fprintf(' [ explore_04_psd ] getting TFR\n');
    cfg              = [];
    cfg.output       = 'pow';
    cfg.channel      = 'all';
    cfg.method       = 'mtmconvol';
    cfg.taper        = 'hanning';
    cfg.foi          = 2:2:40;                          % analysis 2 to 30 Hz in steps of 2 Hz
    cfg.t_ftimwin    = ones(length(cfg.foi),1).*0.5;    % length of time window = 0.5 sec
    cfg.toi          = -1:0.05:1;                       % the time window "slides" from -0.5 to 1.5 in 0.05 sec steps
    TFRhann_on = ft_freqanalysis(cfg, my_data_on);       % dbs-on
    TFRhann_off = ft_freqanalysis(cfg, my_data_off);      % dbs-off
    
    %% plot TFR on all electrodes
    fprintf(' [ explore_04_psd ] plotting TFR for each electrode\n');
    
    cfg = [];
    cfg.baseline        = [-0.5 -0.3];
    cfg.baselinetype    = 'absolute';
    cfg.showlabels      = 'yes';
    cfg.layout          =  conf.cap_layout;
 %  cfg.channel         =
    cfg.ylim            = [0, 30];
    cfg.xlim            = [0 1];
    cfg.figure          = gcf;

    my_title_on = ['ses-on'];
    subplot(1,2,1); ft_multiplotTFR(cfg, TFRhann_on); title(my_title_on); colorbar;
    my_title_off = ['ses-off']; 
    subplot(1,2,2); ft_multiplotTFR(cfg, TFRhann_off); title(my_title_off); colorbar;
    my_title = [conf.subj ' time frequency representation multiplot'];
    sgtitle(my_title)  
    set(gcf,'Position',[10 50 900 400])
    save_subj_fig(gcf, 'ses-onoff_mutiplot_tfr', conf) 
   

    %% single plot TFR for several electrodes, on compared to off 
    electrodes_loop = {'FC3','CPz','AF4','AF7','C5','CP6','T7','PO7','O2'};

    conf.cfg = [];
    conf.cfg.baseline     = [-0.5 -0.3];
    conf.cfg.baselinetype = 'absolute';
   % cfg.maskstyle    = 'saturation';
    cfg.zlim         = [0 30];
    conf. cfg.xlim         = [0 1];
    conf.cfg.ylim         = [0 30];

    for el = 1:length(electrodes_loop)
        conf.cfg.channel      = electrodes_loop{el};
        plot_crf_electrode_subj_firstsecond_onoff(TFRhann_on, TFRhann_off, conf);
    end
    
     %% get trf separately for on-off accept-reject  

    cfg = []; 
    cfg.trials = (my_data_on.behav.accept ==1);
    cfg.toilim = [-0.5 2]; % here toilim is specified because trials are of various length and need cut-off
    data_on_accept = ft_redefinetrial(cfg, my_data_on);
        
    cfg = []; 
    cfg.trials = (my_data_on.behav.accept ==0);
    cfg.toilim = [-0.5 2];
    data_on_reject = ft_redefinetrial(cfg, my_data_on);
    
    cfg = []; 
    cfg.trials = (my_data_off.behav.accept ==1);
    cfg.toilim = [-0.5 2];
    data_off_accept = ft_redefinetrial(cfg, my_data_off);
    
    cfg = []; 
    cfg.trials = (my_data_off.behav.accept ==0);
    cfg.toilim = [-0.5 2];
    data_off_reject = ft_redefinetrial(cfg, my_data_off);   
    
    fprintf(' [ explore_04_psd ] getting TFR\n');
    cfg              = [];
    cfg.output       = 'pow';
    cfg.channel      = 'all';
    cfg.method       = 'mtmconvol';
    cfg.taper        = 'hanning';
    cfg.foi          = 2:2:40;                          % analysis 2 to 30 Hz in steps of 2 Hz
    cfg.t_ftimwin    = ones(length(cfg.foi),1).*0.5;    % length of time window = 0.5 sec
    cfg.toi          = -1:0.05:1;                       % the time window "slides" from -0.5 to 1.5 in 0.05 sec steps
    TFRhann_on_accept = ft_freqanalysis(cfg, data_on_accept);       % dbs-on
    TFRhann_off_accept = ft_freqanalysis(cfg, data_off_accept);      % dbs-off
    TFRhann_on_reject = ft_freqanalysis(cfg, data_on_reject);       % dbs-on
    TFRhann_off_reject = ft_freqanalysis(cfg, data_off_reject);      % dbs-off
    
    %% THIS PART DOES NOT WORK 
    cfg = [];
    cfg.baseline     = [-0.5 -0.3];
    cfg.baselinetype = 'absolute';
    cfg.xlim         = [0.6 0.8];
    cfg.zlim         = [0 10];
    cfg.ylim         = [20 30];
    cfg.marker       = 'on';
    cfg.layout       = conf.cap_layout;
    
    figure;
    
    subplot(221);ft_topoplotTFR(cfg, TFRhann_on_accept); title('on, accept');
    subplot(222);ft_topoplotTFR(cfg, TFRhann_on_reject); title('on, reject');
    subplot(223);ft_topoplotTFR(cfg, TFRhann_off_accept); title('off, accept');
    subplot(224);ft_topoplotTFR(cfg, TFRhann_off_reject); title('off, reject');
    save_subj_fig(gcf, 'ses-onoff_acceptreject_topoplot_tfr', conf) 
    
    
end