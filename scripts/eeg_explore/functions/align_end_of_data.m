%% align_end_of_data
% 
% returns the trial definition of the last period for a run
% time parameters are in the conf file.  

    %
function my_data = align_end_of_data(my_data,conf)   
    %% get samples info 
    sample_ends_matrix = get_last_sample(my_data,conf);
    my_data.sampleinfo_new = sample_ends_matrix(:,1:2);
    % btw where is the onset? 
    
    %% get times
    % this is a vector of timepoints. it will be the same for all trials. 
    timevector = [- conf.timebeforereaction : 1/my_data.fsample : conf.timeafterreaction];
    numberoftrials = length(my_data.trial);
    
    % create empty cell array
    my_data.time_new = cell(1,numberoftrials) ; 
    
    % copy same vector of time in all cells
    for i=1:numberoftrials
        my_data.time_new{i} = timevector;
    end
    
    %% get trial values
    % create empty cell array
    my_data.trial_new = cell(1,numberoftrials) ;

    % fill the array 
    for i = 1 : numberoftrials
        uniquetrial = my_data.trial{i};
        my_data.trial_new{i} = uniquetrial(:, end - (conf.timebeforeend * my_data.fsample) : end);
    end
    
    %% rename old and new vars
    oldnames = {'sampleinfo','trial','time'};
    newnames = {'sampleinfo_old','trial_old','time_old'};
	my_data = renamefields(my_data, oldnames, newnames);
    
    oldnames = {'sampleinfo_new','trial_new','time_new'};
    newnames = {'sampleinfo','trial','time'};
	my_data = renamefields(my_data, oldnames, newnames);
    
    % function returns my_data
end
