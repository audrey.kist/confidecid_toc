%% plot_topo_compare2 Script
% explore first second of eeg data
%
% mandatory input :
%   data_on 
%   data_off
%   conf
%   title, subtitle1, subtitle2: strings
%
% output :
%   figures and models are recorded in the derivatives folder
function plot_topo_compare2(my_title, ...
    data1, subtitle1, ...
    data2, subtitle2, ...
    my_timewindow, my_baseline, conf)
%function topo_compare_figure = plot_topo_compare(data1, data2, conf)

    figure(101); clf;
    set(gcf,'Position', [50 50 800 280])
    
    % topoplot conf
    cfg                 = [];
    cfg.xlim            = my_timewindow ;
    cfg.zlim            = [-7 7];
    cfg.baseline        = my_baseline;
    cfg.layout          = conf.cap_layout;
    cfg.marker          = 'off';
    cfg.comment         = 'no';

   
    % plot data 1
    subplot(1,2,1)
    ft_topoplotER(cfg, data1); 
    colorbar;
    title(subtitle1);

    % plot data 2
    subplot(1,2,2)
    ft_topoplotER(cfg, data2); 
    colorbar;  
    title(subtitle2);
    
    % overall title
     sgtitle(my_title) 

end