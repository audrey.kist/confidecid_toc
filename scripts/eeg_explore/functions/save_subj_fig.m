%% save_subj_fig Script
% 
% save current figure as png and matlab fig format in the deriv folder
% based on subjet name

function save_subj_fig(my_figure, my_nameextension, conf)   
    my_folder = conf.eegx_figures_dir;
    my_title = [conf.subj '_' my_nameextension ];
    saveas(gcf,[my_folder  '/' my_title '.png']);  
    savefig([my_folder  '/' my_title '.fig'])
    fprintf('========= saved as %s in png and fig format \n',my_title);
end