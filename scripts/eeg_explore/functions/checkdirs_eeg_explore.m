function checkdirs_eeg_explore(conf)
%CHECKDIRS Summary of this function goes here
%   checks if folders exist (to store prepared data)
%   + if not, creates
% if ~exist('figs','dir')
%     fprintf(' [ conf ] did not find figs dir, creating ... \n');
%     mkdir('figs');
% end
    d = [conf.bids '/derivatives/'];
    if ~exist(d,'dir')
        fprintf(' [ conf ] did not find %s dir, creating ... \n',d);
        mkdir(d);
    end
    d = [conf.bids '/derivatives/figures/'];
    if ~exist(d,'dir')
        fprintf(' [ conf ] did not find %s dir, creating ... \n',d);
        mkdir(d);
    end
    d = [conf.bids '/derivatives/eeg_explore/figures/'];
    if ~exist(d,'dir')
        fprintf(' [ conf ] did not find %s dir, creating ... \n',d);
        mkdir(d);
    end
    d = [conf.bids '/derivatives/eeg_explore/figures/' conf.subj];
    if ~exist(d,'dir')
        fprintf(' [ conf ] did not find %s dir, creating ... \n',d);
        mkdir(d);
    end   
    d = [conf.bids '/derivatives/eeg_explore/stats/'];
    if ~exist(d,'dir')
        fprintf(' [ conf ] did not find %s dir, creating ... \n',d);
        mkdir(d);
    end
    d = [conf.bids '/derivatives/eeg_explore/stats/' conf.subj];
    if ~exist(d,'dir')
        fprintf(' [ conf ] did not find %s dir, creating ... \n',d);
        mkdir(d);
    end

end

