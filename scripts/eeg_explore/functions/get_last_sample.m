%% get_last_sample
% 
% returns the trial definition of the last second for a run
%
% According to Fieldtrip:
% -----------------------
%  The trial definition "trl" is an Nx3 matrix, N is the number of trials.
%  The first column contains the sample-indices of the begin of each trial
%  relative to the begin of the raw data, the second column contains the
%  sample-indices of the end of each trial, and the third column contains
%  the offset of the trigger with respect to the trial. An offset of 0
%  means that the first sample of the trial corresponds to the trigger. A
%  positive offset indicates that the first sample is later than the trigger,
%  a negative offset indicates that the trial begins before the trigger.
 
function my_trialdef = get_last_sample(my_data, conf)

    % create an empty vector. according to ft_definetrial, 
    trial = zeros(length(my_data.time),3);
    
    % get the last time value of each sample,
    % given that each sample stops 1 sec after answer ???  TBC
    for i = 1:length(my_data.time)
        trial(i,2) = my_data.time{i}(end) + conf.timeafterreaction; % end 
        trial(i,1) = my_data.time{i}(end) -conf.timebeforereaction; % beg 
        trial(i,3) = -conf.timebeforereaction; % stim offest
    end

    % multiply by samplefreq to get sample number
     my_trialdef = trial * my_data.fsample; 

end