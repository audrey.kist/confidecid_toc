%% EXP01_FIRSTSEC_SUBJ_CONFIDECID Script
% explore first second of eeg data
% mandatory input :
%   data_on 
%   data_off
% output :
%   figures and models are recorded in the derivatives folder

function exp1_firstsec_subj_confidecid(my_data_on, my_data_off, conf)
    %% getting erp windows 
    trialwindow = conf.interestwindow ;
    baselinewindow = conf.baselinewindow ; 
    
    %% plot first second, on vs. off
    fprintf(' [ explore_01_firstsec ] create on-off topo figure for %s \n',conf.subj);
    figure(101); clf;
    set(gcf,'Position', [50 50 800 280])
 
    %create figure
    my_title = [conf.subj ' topoplot first second after presentation of choice'];
    plot_topo_compare2(my_title, ...
        my_data_on,"ses-on", ...
        my_data_off, "ses-off", ...  
        trialwindow, baselinewindow, conf)
  
    % save figure
    saveas(gcf,[conf.eegx_figures_dir  '/' conf.subj '_ses-onoff_topoplot_firstsec_alltrials.png']);  
    savefig([conf.eegx_figures_dir  '/' conf.subj '_ses-onoff_topoplot_firstsec_alltrials.fig'])
    fprintf(' [ explore_01_firstsec ] saved as %s_ses-onoff_topoplot_firstsec_alltrials.png \n',conf.subj);
    fprintf(' [ explore_01_firstsec ] saved as matlab fig file \n');
    
    %% same figure for accept / reject 
%     my_data_on = data_on;
%     my_data_off = data_off;
    
    fprintf(' [ explore_01_firstsec ] preparing accept-reject plots');
    
    cfg = []; 
    cfg.trials = (my_data_on.behav.accept ==1);
    cfg.toilim = [-0.5 2]; % here toilim is specified because trials are of various length and need cut-off
    data_on_accept = ft_redefinetrial(cfg, my_data_on);
    
    cfg = []; 
    cfg.trials = (my_data_on.behav.accept ==0);
    cfg.toilim = [-0.5 2];
    data_on_reject = ft_redefinetrial(cfg, my_data_on);
    
    cfg = []; 
    cfg.trials = (my_data_off.behav.accept ==1);
    cfg.toilim = [-0.5 2];
    data_off_accept = ft_redefinetrial(cfg, my_data_off);
    
    cfg = []; 
    cfg.trials = (my_data_off.behav.accept ==0);
    cfg.toilim = [-0.5 2];
    data_off_reject = ft_redefinetrial(cfg, my_data_off);   
    
    %create figure
    my_title = [conf.subj ' topoplot first second after presentation of choice'];
    plot_topo_compare4(my_title,...
        data_on_accept, "ses-on, accept",...
        data_on_reject, "ses-on, reject",...
        data_off_accept, "ses-off, accept",...
        data_off_reject, "ses-off, reject",...
        trialwindow, baselinewindow, conf)
    
    % save figure
    saveas(gcf,[conf.eegx_figures_dir  '/' conf.subj '_ses-onoff_acceptreject_topoplot_firstsec.png']);  
    savefig([conf.eegx_figures_dir  '/' conf.subj '_ses-onoff_acceptreject_topoplot_firstsec.fig'])
    fprintf(' [ explore_01_firstsec ] saved as %s_ses-onoff_acceptreject_topoplot_firstsec.png \n',conf.subj);
    fprintf(' [ explore_01_firstsec ] saved as matlab fig file \n');
    
end