%% CONFIDECID_EEG_EXPLORE_SUBJECT Script
%
%
%% SCRIPT
close all; 
clear all;

% load configuration file. all parameters such as file directories, toolbox 
% directories etc. shall be specified here. 
confidecid_eeg_explore_subj_conf;

% initialize FieldTrip
addpath(conf.fieldtrip);                                                                                                                                                                                                                                                                                                
ft_defaults;

% getting preprocessed data directories - should be organised as BIDS recommends
% the preprocessed eeg file already contains behavioral data
conf.eegx_figures_dir    = [conf.bids '/derivatives/eeg_explore/figures/' conf.subj ]; % eegx is for eeg eXplore
eegx_stats_dir      = [conf.bids '/derivatives/eeg_explore/stats/' conf.subj ];
eeg_dir             = [conf.bids '/derivatives/eegprep/' conf.subj];
eeg_on_dir          = [conf.bids '/derivatives/eegprep/' conf.subj '/ses-on/eeg'];
eeg_off_dir         = [conf.bids '/derivatives/eegprep/' conf.subj '/ses-off/eeg'];

file_on = [conf.subj '_ses-on_task-confidecid_stg7.mat']; 
file_off = [conf.subj '_ses-off_task-confidecid_stg7.mat'];
conf.stg7_filename_on = [eeg_on_dir '/' file_on];
conf.stg7_filename_off = [eeg_off_dir '/' file_off];

%% LOAD DATA
% check if stg7 on and off files exist, otherwise ask for it
if isfile(conf.stg7_filename_on)
     fprintf(' [ init ] loading %s\n',file_on);
     x = load(conf.stg7_filename_on);
     data_on = x.data; 
     clear x;
else
     fprintf(' [ init ] could not find %s\n',conf.stg7_filename_on);
     fprintf(' [ init ] please proceed to stage 7 preprocessing of %s_ses-on\n',conf.subj);
end

if isfile(conf.stg7_filename_off)
     fprintf(' [ init ] loading %s\n',file_off);
     x = load(conf.stg7_filename_off);
     data_off = x.data; 
     clear x;
%      keepchans = ~contains(data_off.label,{'HEOG','VEOG','DBSArtefact','Photodiode'}); 
%      elecs_on = data.label(keepchans);
else
     fprintf(' [ init ] could not find %s\n',conf.stg7_filename_off);
     fprintf(' [ init ] please proceed to stage 7 preprocessing of %s_ses-off\n',conf.subj);
end

%% EXPLORE DATA

% explore first second of EEG 
if any(contains(conf.explore,'01_firstsec')) 
    fprintf(' [ explore_01_firstsec ] start \n');
    exp1_firstsec_subj_confidecid(data_on, data_off, conf);
    fprintf(' [ explore_01_firstsec ] end \n');
else
    fprintf(' [ explore_01_firstsec ] skipping \n');
end

% explore last second before accept/reject
if any(contains(conf.explore,'02_lastsec')) 
    fprintf(' [ explore_02_end ] start \n');
    % align end of data
    fprintf(' [ explore_02_end ] align end of data \n');
    data_on_end = align_end_of_data(data_on, conf);
    data_off_end = align_end_of_data(data_off, conf);
    exp2_lastsec_subj_confidecid(data_on, data_off, conf);
    fprintf(' [ explore_02_end ] end \n');
else
    fprintf(' [ explore_02_end ] skipping \n');
end

%% power spetrum exploration
% overall power spectrum during trial
if any(contains(conf.explore,'04_overallpower')) 
    fprintf(' [ explore_04_psd_fulllength ] start \n');
    exp4_psd_subj_confidecid(data_on, data_off, conf);
    fprintf(' [ explore_04_psd_fulllength ] end \n');
else
    fprintf(' [ explore_04_psd_fulllength ] skipping \n');
end

% first second power spectrum

% last second power spectrum

%% time-frequency exploration


