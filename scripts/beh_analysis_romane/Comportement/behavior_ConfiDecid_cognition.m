% Analyses comportement CONFIDENCE-DECID COGNITION task

clearvars
close all

%% Param�tres

param.patients = 1; % 0 = healthy subjects   1 = patients

% Define BDD path according to computer name
name = getenv('COMPUTERNAME');
if ismember(name, 'HP-ROMANE')
    init.bdd_path = 'C:\Users\Romane\Documents\MATLAB\BDD_ConfiDecid\Cognitive_task';
elseif ismember(name, 'DESKTOP-IHJ3T8M')
    init.bdd_path = 'C:\Users\Romane\Documents\BDD_ConfiDecid\Cognitive_task';
end

if param.patients == 1 % PATIENTS -----------------------------------------------
    
    init.subj_name = {'GRE_2019_BAUj','GRE_2019_PLEc','GRE_2019_AUBa','GRE_2019_CHRc','GRE_2019_GEOa',...
        'LYO_2019_IBRa','REN_2019_BAIp','REN_2019_LAPp','REN_2019_LEBc','LYO_2019_CAPp',...
        'GRE_2019_BERj','GRE_2020_BERl','GRE_2020_SOLm','GRE_2020_FOUt','PRA_2020_PRAb',...
        'GRE_2020_SULg','PRA_2020_PRAc','GRE_2020_JERr','GRE_2020_GENj','GRE_2020_KAYc',...
        'GRE_2021_GUIl'}; % Ajouter les sujets ici
    
    % Excluded patients : 'PRA_2020_PRGa', 'PRA_2020_PRAd'
    
    init.path = fullfile(init.bdd_path, 'Patients'); % Chemin o� se trouvent les donn�es
    init.pathtofig = fullfile(init.bdd_path, 'Comportement'); % Chemin o� sont enregistr�es les figures g�n�r�es
    
else % SUJETS SAINS -------------------------------------------------------
    
    init.subj_name = {'GRE_2019_ALBi','GRE_2019_BENh','GRE_2019_BENl','GRE_2019_CHAl','GRE_2019_DEPa',...
        'GRE_2019_GOUr','GRE_2019_GROe','GRE_2019_GUEm','GRE_2019_LAQm','GRE_2019_LARa',...
        'GRE_2019_LEMl','GRE_2019_LORd','GRE_2019_MARm','GRE_2019_PONe','GRE_2019_RIBm',...
        'GRE_2019_RODt','GRE_2019_VESa','GRE_2019_ZARa','GRE_2019_ZOUr'}; % Ajouter les sujets ici
    
    init.path = fullfile(init.bdd_path, 'Sujets_sains'); % Chemin o� se trouvent les donn�es
    init.pathtofig = fullfile(init.bdd_path, 'Sujets_sains', 'Comportement_sujets_sains'); % Chemin o� sont enregistr�es les figures g�n�r�es
    
    init.city = 'GRE';
    init.year = '2019';
    
end % ---------------------------------------------------------------------

init.task = 'ConfiDecid';
init.protocol = 'cognition';
init.nb_sess = 4; % Nombre de sessions possible
init.nb_trial_sess = 128; % Nombre d'essais par session

init.group_data = [];

%% Colors definition

init.color.blue = [0.305, 0.656, 0.867];
init.color.purple = [0.590, 0.430, 0.676];
init.color.darkblue = [0.02, 0.447, 0.467];
init.color.agent = init.color.blue;
init.color.non_agent = init.color.purple;

%% Boucle � travers les sujets

for subj_idx = 1:length(init.subj_name)
    
    clearvars -except param init subj_idx choice_data mood_data
    subjName = init.subj_name{subj_idx};
    
    % Boucle � travers les sessions et cr�ation de la matrice "group_data"
    for sess = 1:init.nb_sess
        
        % Load data file
        if param.patients == 1 % PATIENTS
            data_filename = fullfile(init.path, subjName, 'RAW', 'Comportement', sprintf('%s_%s%d_%s_data.mat', subjName, init.task, sess, init.protocol));
        else % SUJETS SAINS
            data_filename = fullfile(init.path, subjName, sprintf('%s_%s%d_%s_data.mat',subjName, init.task, sess, init.protocol));
        end
        
        if exist(data_filename,'file') % Si le fichier de la session existe
            load(data_filename, 'data'); % Charger la matrice data de la session
            
            data(:,1) = subj_idx; % Ajouter l'indice du sujet colonne 1
            init.group_data = [init.group_data ; data]; % Matrice contenant les donn�es de toutes les sessions et de tous les sujets
        end
        
    end % Fin de la boucle � travers les sessions
    
    subj_data = init.group_data(init.group_data(:,1) == subj_idx,:); % All data for one subject
    
    % Cr�ation des dossiers sujets dans le dossier 'Comportement' s'ils n'existent pas
    init.pathtofigsubj{subj_idx} = fullfile(init.pathtofig, subjName);
    if ~exist(init.pathtofigsubj{subj_idx},'dir')
        mkdir(init.pathtofigsubj{subj_idx});
    end
    
    %% CHOICE MODEL
    
    cond = {'agent', 'non_agent'};
    
    % Load choice data of all subjects
    choice_data_fullname = fullfile(init.pathtofig, 'all_choice_data.mat');
    if exist(choice_data_fullname,'file') && ~exist('choice_data', 'var')
        load(choice_data_fullname, 'choice_data'); % Load 'choice_data' structure
    elseif ~exist(choice_data_fullname,'file') % Create an empty 'choice_data' structure
        choice_data = struct(cond{1}, struct(), cond{2}, struct());
    end
    
    for agentivity = 1:length(cond) % Loop over agentivity
        
        agent = agentivity*(-1)+2; % 1 = agent ; 2 = non agent (proba)
        cond_data = subj_data(subj_data(:,11) == agent,:); % All data for one condition (agent/proba)
        trial_idx = find(subj_data(:,11) == agent);
        
        % If subject's name is not a field of 'choice_data' : compute choice model for this subject
        if isempty(fieldnames(choice_data.(cond{agentivity}))) || ~isfield(choice_data.(cond{agentivity}).inversionResult.model(1), subjName)
            
            % Load mean and std of the subject
            if param.patients == 1 % PATIENTS
                param_filename = fullfile(init.path, subjName, 'RAW', 'Comportement', sprintf('%s_estim_param.mat', subjName));
            else % SUJETS SAINS
                param_filename = fullfile(init.path, subjName, sprintf('%s_estim_param.mat', subjName));
            end
            
            load(param_filename);
            
            S = [];
            S.subjname = subjName;
            S.data = cond_data;
            S.moy_estim = moy_estim;
            S.std_estim = std_estim;
            S.trial_idx = trial_idx;
            S.trial_nb = length(subj_data);
            
            choice_data.(cond{agentivity}) = choiceModel_ConfiDecid(S, choice_data.(cond{agentivity})); % Get choice model for the subject
            
            choice_data.nSubject = length(fieldnames(choice_data.(cond{agentivity}).inversionResult.model));
            
            % Save choice data
            save(choice_data_fullname, 'choice_data');
            close all;
        end
        
    end % End of the loop over agentivity
    
    %% MOOD MODEL
    
    % Load mood data of all subjects
    mood_data_fullname = fullfile(init.pathtofig, 'all_mood_data.mat');
    if exist(mood_data_fullname, 'file') && ~exist('mood_data', 'var')
        load(mood_data_fullname, 'mood_data'); % Load 'mood_data' structure
    elseif ~exist(mood_data_fullname, 'file') % Create an empty 'mood_data' structure
        mood_data = struct();
    end
    
    % If subject's name is not a field of 'mood_data' : compute TML for this subject
    if isempty(fieldnames(mood_data)) || ~isfield(mood_data.inversionResult.model(1), subjName)
        
        % Parameters
        S = [];
        S.subjname = subjName;
        S.data = subj_data;
        S.meanTML = 0;
        S.pathtofig = init.pathtofigsubj{subj_idx};
        S.agent = agent;
        
        mood_data = TML_ConfiDecid_VBA(S, mood_data); % Get model datas for the subject
        
        % Save mood data
        save(mood_data_fullname, 'mood_data');
    end
    
end % Fin de la boucle � travers les sujets

%% Mood models comparison

if ~isfield(mood_data, 'best_model')
    
    p = VBA_groupBMC(mood_data.inversionResult.FF); close all;
    best_model = find(p.a == max(p.a));
    null_model = find(ismember({mood_data.inversionResult.param.modelSpace.moodType}, 'null'));
    
    mood_data.best_model = best_model;
    mood_data.null_model = null_model;
    save(mood_data_fullname, 'mood_data');
    
    % Plot best mood model
    plot_best_mood_model(init, mood_data)
    
end

%% ANALYSE 1 : Probabilit� d'accepter en fonction des dimensions de la t�che et de l'agentivit�

% opt.var_column = 7; % Choix (1 = accept ; 0 = reject)
% opt.ylabel = 'P(accept)';
%
% % ANALYSE 1.A : probabilit� d'accepter le d�fi en fonction de chaque dimension de la t�che pour chaque sujet
% opt.YTick = (0:.2:1);
% opt.axis = [0 5 0 1];
% opt.subj_fig_name = '_task_dim_effect_on_choice.tiff';
% opt.all_fig_name = 'p_accept_according2dim.tiff';
%
% task_dim_effect_on_var(init, opt);
%
% % ANALYSE 1.B : corr�lation entre la probabilit� d'accepter le d�fi et chaque dimension de la t�che pour chaque sujets
% opt.subj_fig_name = '_correl_task_dim_and_choice.tiff';
% opt.ylim = [0 1];
%
% beta_corr = corr_task_dim_effect(init, opt);
%
% % ANALYSE 1.C : ttest sur les corr�lations obtenues dans l'analyse B
% opt.ylabel = 'choice';
% opt.DataAspectRatio = [1 0.2 1];
% opt.all_fig_name = 'slope_correl_task_dim_and_choice.tiff';
%
% ttest_beta_corr(beta_corr, init, opt)

%% ANALYSE 2 : Note de confiance en fonction des dimensions de la t�che et de l'agentivit�

% opt.var_column = 9; % Notes de confiance de 1 � 100
% opt.ylabel = 'Confidence';
%
% % ANALYSE 2.A : confiance en fonction des dimensions de la t�che
% opt.YTick = (0:25:100);
% opt.axis = [0 5 0 100];
% opt.subj_fig_name = '_task_dim_effect_on_confidence.tiff';
% opt.all_fig_name = 'confidence_according2dim.tiff';
%
% task_dim_effect_on_var(init, opt);
%
% % ANALYSE 1.B : corr�lation entre la probabilit� d'accepter le d�fi et chaque dimension de la t�che pour chaque sujets
% opt.subj_fig_name = '_correl_task_dim_and_confidence.tiff';
% opt.ylim = [0 100];
%
% beta_corr = corr_task_dim_effect(init, opt);
%
% % ANALYSE 2.C : ttest sur les corr�lations obtenues dans l'analyse B
% opt.ylabel = 'confidence';
% opt.DataAspectRatio = [1 1 1];
% opt.all_fig_name = 'slope_correl_task_dim_and_confidence.tiff';
%
% ttest_beta_corr(beta_corr, init, opt)

%% ANALYSE 3 : Performance according to task dimension and agentivity

% opt.var_column = 12; % Challenge result (1 = success ; 0 = failure)
% opt.ylabel = 'P(success)';
%
% % ANALYSE 3.A : confiance en fonction des dimensions de la t�che
% opt.YTick = (0:.2:1);
% opt.axis = [0 5 0 1];
% opt.subj_fig_name = '_task_dim_effect_on_success.tiff';
% opt.all_fig_name = 'p_success_according2dim.tiff';
%
% task_dim_effect_on_var(init, opt);
%
% % ANALYSE 3.B : corr�lation entre la probabilit� d'accepter le d�fi et chaque dimension de la t�che pour chaque sujets
% opt.subj_fig_name = '_correl_task_dim_and_perf.tiff';
% opt.ylim = [0 1];
%
% beta_corr = corr_task_dim_effect(init, opt);
%
% % ANALYSE 3.C : ttest sur les corr�lations obtenues dans l'analyse B
% opt.ylabel = 'perf';
% opt.DataAspectRatio = [1 0.2 1];
% opt.all_fig_name = 'slope_correl_task_dim_and_perf.tiff';
%
% ttest_beta_corr(beta_corr, init, opt)
%
% % ANALYSE 3.D : Perfomance according to agentivity
% perf_acc2agent(init)

%% ANALYSE 4 : Choice model analysis
% choice_model_analysis(init, choice_data) % Plot of the model

%% ANALYSE 5 : Mood analysis
% mood_model_free_analysis(init) % Model free
% mood_model_analysis(init, mood_data) % Mood model
% effect_of_mood_on_choice(init, choice_data, mood_data)

%% ANALYSE 6 : Correlation mood and confidence
% correl_mood_confid(init)

%% ANALYSE 7 : Correlation between : confidence (confidence & proba) | RT | p(accept) | utility
% correl_analysis(init, choice_data)

%% FIN
return;

%% FUNCTIONS

% ANALYSE A : variation d'une variable en fonction de chaque dimension de la t�che pour chaque sujet
function task_dim_effect_on_var(init, opt)

var_p = cell(length(init.subj_name),4,3,2);
task_dim_column = [4 5 6]; % Gains = colonne 4 ; Pertes = colonne 5 ; Diff = colonne 6 dans la matrice 'group_data'

for subj_idx = 1:length(init.subj_name) % Boucle � travers les sujets
    
    subj_data = init.group_data(init.group_data(:,1) == subj_idx,:); % All data for one subject
    var = subj_data(:,opt.var_column); % Offre accept�e (1) ou rejet�e (0) // Notes de confiance entre 1 et 100
    
    fig = figure;
    hold on;
    
    for agentivity = 1:2 % Boucle � travers l'agentivit�
        agent = agentivity*(-1)+2; % 1 = agent ; 2 = non agent
        
        for task_dim = 1:length(task_dim_column) % task_dim : 1 = gain, 2 = loss, 3 = diff
            
            prospect = subj_data(:,task_dim_column(task_dim)); % prospect de toutes les sessions pour 1 sujet
            id_q1 = prospect < 20 & subj_data(:,11) == agent;
            id_q2 = prospect < 30 & prospect >= 20 & subj_data(:,11) == agent;
            id_q3 = prospect < 40 & prospect >= 30 & subj_data(:,11) == agent;
            id_q4 = prospect < 50 & prospect >= 40 & subj_data(:,11) == agent;
            
            var_p(subj_idx,:,task_dim,agentivity) = {var(id_q1), var(id_q2), var(id_q3), var(id_q4)}; % choice_p dim : subject by task_levels (4 levels) by task dim (gain, loss, challenge_diff)
            
            if agentivity == 1
                plot_color(agentivity,:) = init.color.agent;
            elseif agentivity == 2
                plot_color(agentivity,:) = init.color.non_agent;
            end
            
            %FIGURE : proba d'accepter pour chaque dimension (gain, loss, diff) et chaque sujet
            subplot(3,1,task_dim);
            hold on;
            pbaspect([1,0.5,1]);
            errorbar(cellfun(@mean, var_p(subj_idx,:,task_dim,agentivity)), cellfun(@std, var_p(subj_idx,:,task_dim,agentivity))./sqrt(cellfun(@length, var_p(subj_idx,:,task_dim,agentivity))),...
                'o-', 'Color', plot_color(agentivity,:), 'MarkerSize', 3);
            ylabel(opt.ylabel)
            ax = gca;
            ax.XTick = [1 2 3 4];
            ax.YTick = opt.YTick;
            axis(opt.axis)
            set(gca,'XTickLabel',{'[1-2]',']2-3]',']3-4]',']4-5]'})
            
            switch task_dim
                case 1
                    xlabel('Gain prospect ()')
                case 2
                    xlabel('Loss prospect ()')
                case 3
                    xlabel('Challenge difficulty')
            end
            
        end % Fin de la boucle � travers les dimensions
    end % Fin de la boucle � travers l'agentivit�
    
    legend('agent','non agent','Location','best');
    
    fig.PaperPosition = [0, 0, 7 14];
    fig_name = fullfile(init.pathtofigsubj{subj_idx}, [init.subj_name{subj_idx} opt.subj_fig_name]);
    print(fig, fig_name, '-dtiff', '-r200');
    
    close(fig);
    
end % Fin de la boucle � travers les sujets

% Figure pour tous les sujets
if subj_idx == length(init.subj_name)
    
    fig = figure;
    hold on;
    
    for agentivity = 1:2 % Boucle � travers l'agentivit�
        for task_dim = 1:3 % task_dim : 1 = gain, 2 = loss, 3 = diff
            
            %FIGURE : proba d'accepter pour chaque dimension (gain, loss, diff) tous les sujets
            subplot(3,1,task_dim);
            hold on;
            errorbar(nanmean(cellfun(@nanmean, var_p(:,:,task_dim,agentivity))), nanstd(cellfun(@nanmean, var_p(:,:,task_dim,agentivity)))/sqrt(size(var_p,1)),...
                'o-', 'Color', plot_color(agentivity,:), 'MarkerSize', 3);
            ylabel(opt.ylabel)
            ax = gca;
            ax.XTick = [1 2 3 4];
            ax.YTick = opt.YTick;
            axis(opt.axis)
            set(gca,'XTickLabel',{'[1-2]',']2-3]',']3-4]',']4-5]'})
            
            switch task_dim
                case 1
                    xlabel('Gain prospect ()')
                case 2
                    xlabel('Loss prospect ()')
                case 3
                    xlabel('Challenge difficulty')
            end
            
        end
    end % Fin de la boucle � travers l'agentivit�
    
    legend('agent','non agent','Location','best');
    
    fig.PaperPosition = [0, 0, 7 14];
    fig_name = fullfile(init.pathtofig, opt.all_fig_name);
    print(fig, fig_name, '-dtiff', '-r200');
    
    close(fig);
end

end

% ANALYSE B : corr�lation entre une variable et chaque dimension de la t�che pour chaque sujet
function beta_corr = corr_task_dim_effect(init, opt)

task_dim_column = [4 5 6]; % Gains = colonne 4 ; Pertes = colonne 5 ; Diff = colonne 6 dans la matrice 'group_data'

for subj_idx = 1:length(init.subj_name) % Boucle � travers les sujets
    
    subj_data = init.group_data(init.group_data(:,1) == subj_idx,:); % All data for one subject
    
    for agentivity = 1:2 % Boucle � travers l'agentivit�
        
        voi(subj_idx,1,agentivity) = {subj_data(subj_data(:,11) == agentivity*(-1)+2, task_dim_column)}; % Col 1 = Prospects
        voi(subj_idx,2,agentivity) = {subj_data(subj_data(:,11) == agentivity*(-1)+2, opt.var_column)}; % Col 2 = Variable d'int�r�t (choix, confiance...)
        
        % Corr�lation entre la variable d'int�r�t et toutes les dimensions (gain, loss, diff) pour chaque sujet
        
        x = voi{subj_idx,1,agentivity}; % Predictors
        y = voi{subj_idx,2,agentivity}; % Responses
        
        if VBA_isBinary(y) == true
            b = glmfit(x,y,'binomial');
            blink = 'logit';
        else
            b = glmfit(x,y);
            blink = 'identity';
        end
        
        intercept(subj_idx, agentivity) = b(1); % Intercept (utile pour le plot)
        beta_corr(subj_idx, agentivity, :) = b(2:end); % Pente de la corr�lation
        
    end % Fin de la boucle � travers l'agentivit�
    
    fig = figure;
    hold on;
    
    for task_dim = 1:length(task_dim_column) % task_dim : 1 = gain, 2 = loss, 3 = diff
        for agentivity = 1:2 % Boucle � travers l'agentivit�
            
            prdct = voi{subj_idx,1,agentivity}(:,task_dim);
            resp = voi{subj_idx,2,agentivity};
            beta = [intercept(subj_idx, agentivity); beta_corr(subj_idx, agentivity, task_dim)];
            
            if agentivity == 1
                plot_color = init.color.agent;
            elseif agentivity == 2
                plot_color = init.color.non_agent;
            end
            
            % FIGURE : Plot de la corr�lation entre la variable d'int�r�t et l'une des dimensions
            subplot(3,1,task_dim);
            hold on;
            pbaspect([1,0.5,1]);
            
            yfit = glmval(beta, prdct, blink);
            st = sortrows([prdct, yfit]);
            p(agentivity) = plot(st(:,1), st(:,2), 'LineWidth', 2, 'Color', plot_color);
            scatter(prdct, resp, 10, 'filled', 'MarkerFaceColor', p(agentivity).Color, 'MarkerFaceAlpha', 0.3);
            
            ylabel(opt.ylabel)
            ylim(opt.ylim)
            
            switch task_dim
                case 1
                    xlabel('Gain prospect ()')
                case 2
                    xlabel('Loss prospect ()')
                case 3
                    xlabel('Challenge difficulty')
            end
            
        end % Fin de la boucle � travers l'agentivit�
    end % Fin de la boucle � travers les dimensions de la t�che
    
    legend(p, 'agent','non agent','Location','best');
    
    fig.PaperPosition = [0, 0, 7 14];
    fig_name = fullfile(init.pathtofigsubj{subj_idx}, [init.subj_name{subj_idx} opt.subj_fig_name]);
    print(fig, fig_name, '-dtiff', '-r200');
    
    close(fig);
    
end % Fin de la boucle � travers les sujets

% T-test agent vs. non agent (identique pour chaque dimensions de la t�che)
[h,p,~,stats] = ttest(cellfun(@mean, voi(:,2,1)), cellfun(@mean, voi(:,2,2)));

end

% ANALYSE C : ttest de la pente des corr�lations obtenues dans l'analyse B
function ttest_beta_corr(beta_corr, init, opt)

fig = figure;
hold on;

for task_dim = 1:size(beta_corr,3) % task_dim : 1 = gain, 2 = loss, 3 = diff
    for agentivity = 1:2 % Boucle � travers l'agentivit�
        
        if agentivity == 1
            plot_color = init.color.agent;
        elseif agentivity == 2
            plot_color = init.color.non_agent;
        end
        
        % FIGURE
        subplot(3,1,task_dim);
        hold on;
        pbaspect([1,0.5,1]);
        xlim([0 3])
        xticks([1 2])
        
        bar(agentivity, mean(beta_corr(:, agentivity, task_dim)), 'FaceColor', plot_color);
        errorbar(agentivity, mean(beta_corr(:, agentivity, task_dim)), std(beta_corr(:, agentivity, task_dim))/sqrt(length(beta_corr)), 'Color', 'black')
        scatter(repmat(agentivity,1,size(beta_corr,1)), beta_corr(:, agentivity, task_dim), 15, 'MarkerFaceColor', [0.5 0.5 0.5], 'MarkerFaceAlpha', 0.3, 'MarkerEdgeColor', 'none')
        
        % T-test
        [h,p,~,stats] = ttest(beta_corr(:, agentivity, task_dim));
        if h == 1 % Mettre une �toile au dessus de la bar si c'est significaivement diff�rent de z�ro
            y = mean(beta_corr(:, agentivity, task_dim)) + std(beta_corr(:, agentivity, task_dim))/sqrt(length(beta_corr))*sign(mean(beta_corr(:, agentivity, task_dim)))*4;
            scatter(agentivity, y, '*k', 'sizeData',50,'linewidth',0.7)
        end
        
    end % Fin de la boucle � travers l'agentivit�
    
    xticklabels({'agent','non agent'})
    
    switch task_dim
        case 1
            xlabel('Gain prospect')
        case 2
            xlabel('Loss prospect')
        case 3
            xlabel('Challenge difficulty')
    end
    
    set(gca,'DataAspectRatio',opt.DataAspectRatio)
    ylabel(sprintf('Slope of correl\nw/ %s', opt.ylabel));
    
end % Fin de la boucle � travers les dimensions de la t�che

fig.PaperPosition = [0, 0, 7 14];
fig_name = fullfile(init.pathtofig, opt.all_fig_name);
print(fig, fig_name, '-dtiff', '-r200');

close(fig);

end

% ANALYSE 3 : performance en fonction de l'agentivit�
function perf_acc2agent(init)

for subj_idx = 1:length(init.subj_name) % Boucle � travers les sujets
    for agentivity = 1:2 % Boucle � travers l'agentivit�
        
        agent = agentivity*(-1)+2; % 1 = agent ; 2 = non agent
        
        subj_data = init.group_data(init.group_data(:,1) == subj_idx,:); % All data for one subject
        agent_data = subj_data(subj_data(:,11) == agent,:); % All data for one condition (agent/non agent)
        
        % Performance all trials
        perf(subj_idx, agentivity) = mean(agent_data(:,12)); % nSubj * Agentivity
        err_perf(subj_idx, agentivity) = std(agent_data(:,12))/sqrt(length(agent_data(:,12)));
        
        % Proba accept
        pAccept(subj_idx, agentivity) = mean(agent_data(:,7)); % nSubj * Agentivity
        
        % Confidence
        z_confi = zscore(subj_data(:,9));
        confi(subj_idx, agentivity) = mean(z_confi(subj_data(:,11) == agent)); % nSubj * Agentivity
        
        % Loop over sessions
        sess_id = unique(subj_data(:,2));
        for sess = 1:length(sess_id) % Loop over sessions
            perf_sess(subj_idx, agentivity, sess) = mean(agent_data(agent_data(:,2) == sess_id(sess), 12));
        end % End of the loop over sessions
        
    end % Fin de la boucle � travers l'agentivit�
end % Fin de la boucle � travers les sujets

% FIGURE (for all subjects)

fig = figure;
hold on;

subgrid = [2, 3];

% All subjects
subplot(subgrid(1), subgrid(2), 1:3);
hold on;

bar(perf)

ngroups = size(perf, 1);
nbars = size(perf, 2);
% Calculating the width for each bar group
groupwidth = min(0.8, nbars/(nbars + 1.5));
for i = 1:nbars
    x = (1:ngroups) - groupwidth/2 + (2*i-1) * groupwidth / (2*nbars);
    errorbar(x, perf(:,i), err_perf(:,i), 'k', 'LineStyle', 'none');
end

legend('agent', 'non agent', 'Location', 'best')

xlabel('Subjects')
ylabel('% success')

% Mean
subplot(subgrid(1), subgrid(2), 4);
hold on;

b = bar(mean(perf), 'FaceColor', 'flat');
b.CData(2,:) = [0.85,0.33,0.10];
errorbar(mean(perf), std(perf)/sqrt(length(perf)), 'k', 'LineStyle', 'none')

ylim([0 1])
[~,p] = ttest(perf(:,1),perf(:,2));
TextLocation(sprintf('p = %.2f', p), 'Location', 'best');

xticks([1,2])
xticklabels({'agent','non agent'})
ylabel('% success')
title('Across subjects')

% Correlation with p(accept)
subplot(subgrid(1), subgrid(2), 5);
hold on;

x = perf(:,1)-perf(:,2);
y = pAccept(:,1)-pAccept(:,2);
n = length(y);

scatter(x, y, 'filled', 'MarkerEdgeColor', [0 0 0])
line([-1 1], [0 0], 'Color', 'k', 'LineStyle', '--')
line([0 0], [-1 1], 'Color', 'k', 'LineStyle', '--')

[beta, ~, stats] = glmfit(x, y); % Correlation pAccept according to performance
yfit = glmval(beta, x, 'identity', 'size', n);
plot(x, yfit./n, '-', 'LineWidth', 2)

TextLocation(sprintf('p = %.2f', stats.p(end)), 'Location', 'best');

xlabel('perf_{agent} - perf_{proba}')
ylabel('p(accept)_{agent} - p(accept)_{proba}')
title('Choice')

% Correlation with confidence
subplot(subgrid(1), subgrid(2), 6);
hold on;

x = perf(:,1)-perf(:,2);
y = confi(:,1)-confi(:,2);
n = length(y);

scatter(x, y, 'filled', 'MarkerEdgeColor', [0 0 0])
line([-1 1], [0 0], 'Color', 'k', 'LineStyle', '--')
line([0 0], [-1.5 1.5], 'Color', 'k', 'LineStyle', '--')

[beta, ~, stats] = glmfit(x, y); % Correlation confidence according to performance
yfit = glmval(beta, x, 'identity', 'size', n);
plot(x, yfit./n, '-', 'LineWidth', 2)

TextLocation(sprintf('p = %.2f', stats.p(end)), 'Location', 'best');

xlabel('perf_{agent} - perf_{proba}')
ylabel('confidence_{agent} - confidence_{proba}')
title('Confidence')

% Save
fig_name = fullfile(init.pathtofig, 'perf_acc2agent');
% print(fig, [fig_name '.tiff'], '-dtiff', '-r200');
savefig(fig, fig_name,'compact'); % Matlab format

close(fig);

end

% ANALYSE 4 : choice model analysis
function choice_model_analysis(init, choice_data)

task_dim_column = [4 5 6]; % Gains = colonne 4 ; Pertes = colonne 5 ; Diff = colonne 6 dans la matrice 'group_data'

for subj_idx = 1:length(init.subj_name) % Boucle � travers les sujets
    
    subj_data = init.group_data(init.group_data(:,1) == subj_idx,:); % All data for one subject
    cond = {'agent', 'non_agent'};
    
    fig = figure;
    hold on;
    
    for agentivity = 1:2 % Boucle � travers l'agentivit�
        
        ag = agentivity*(-1)+2; % 1 = agent ; 2 = non agent
        agent_data = subj_data(subj_data(:,11) == ag,:); % All data for one subject and one condition (agent/non agent)
        
        choices = agent_data(:,7);
        proba_accept = choice_data.(cond{agentivity}).inversionResult.model.(init.subj_name{subj_idx}).posterior.muX(2,:)'; % Also in out.suffStat.gx
        utility_accept = choice_data.(cond{agentivity}).inversionResult.model.(init.subj_name{subj_idx}).posterior.muX(1,:);
        
        for task_dim = 1:length(task_dim_column) % task_dim : 1 = gain, 2 = loss, 3 = diff
            
            prospect = agent_data(:,task_dim_column(task_dim)); % prospect de toutes les sessions pour 1 sujet
            id_q1 = prospect < 20;
            id_q2 = prospect < 30 & prospect >= 20;
            id_q3 = prospect < 40 & prospect >= 30;
            id_q4 = prospect < 50 & prospect >= 40;
            
            choice_bin(subj_idx, :, task_dim, agentivity) = {choices(id_q1), choices(id_q2), choices(id_q3), choices(id_q4)}; % choice_bin dim : subject by task_levels (4 levels) by task dim (gain, loss, challenge_diff)
            model_bin(subj_idx, :, task_dim, agentivity) = {proba_accept(id_q1), proba_accept(id_q2), proba_accept(id_q3), proba_accept(id_q4)};
            
            %FIGURE : proba d'accepter pour chaque dimension (gain, loss, diff) et chaque sujet
            subplot(2, 2, task_dim);
            hold on;
            pbaspect([1, 0.5, 1]);
            
            % Model
            mean_model = cellfun(@mean, model_bin(subj_idx, :, task_dim, agentivity));
            err_model = cellfun(@std, model_bin(subj_idx, :, task_dim, agentivity))./sqrt(cellfun(@length, model_bin(subj_idx, :, task_dim, agentivity)));
            patch([1:length(mean_model) length(mean_model):-1:1], mean_model([1:end end:-1:1]) + cat(2, err_model(1:end), -err_model(end:-1:1)),...
                init.color.(cond{agentivity})-0.1, 'EdgeColor', 'none', 'FaceAlpha', .2);
            l(agentivity) = plot(1:length(mean_model), mean_model, '--', 'LineWidth', 1, 'Color', init.color.(cond{agentivity})-0.1);
            
            % Data
            e(agentivity) = errorbar(cellfun(@mean, choice_bin(subj_idx,:,task_dim,agentivity)), cellfun(@std, choice_bin(subj_idx,:,task_dim,agentivity))./sqrt(cellfun(@length, choice_bin(subj_idx,:,task_dim,agentivity))),...
                'o', 'Color', init.color.(cond{agentivity}), 'MarkerFaceColor', init.color.(cond{agentivity}), 'MarkerSize', 3); % Choices
            
            ylabel('P(accept)')
            ax = gca;
            ax.XTick = [1 2 3 4];
            ax.YTick = (0:.2:1);
            axis([0 5 0 1])
            set(gca,'XTickLabel',{'[1-2]',']2-3]',']3-4]',']4-5]'})
            
            switch task_dim
                case 1
                    xlabel('Gain prospect ()')
                case 2
                    xlabel('Loss prospect ()')
                case 3
                    xlabel('Challenge difficulty')
            end
            
        end % Fin de la boucle � travers les dimensions
        
        % Plot also utility
        subplot(2, 2, 4);
        hold on;
        pbaspect([1, 0.5, 1]);
        
        group = linspace(-8,7,10); % A modifier si on veut regrouper plus ou moins d'utilit�
        
        % Make bins
        for ig = 1:length(group)-1
            if ig == 1 % First bin
                itmp = utility_accept < group(ig+1);
            elseif ig == length(group)-1 % Last bin
                itmp = utility_accept >= group(ig);
            else
                itmp = utility_accept >= group(ig) & utility_accept < group(ig+1);
            end
            choice_utility(subj_idx, ig, agentivity) = {choices(itmp)}; % Data
            model_utility(subj_idx, ig, agentivity) = {proba_accept(itmp)}; % Model
        end
        
        % Model
        mean_model = cellfun(@mean, model_utility(subj_idx, :, agentivity));
        err_model = cellfun(@std, model_utility(subj_idx, :, agentivity))./sqrt(cellfun(@length, model_utility(subj_idx, :, agentivity)));
        patch([1:length(mean_model) length(mean_model):-1:1], mean_model([1:end end:-1:1]) + cat(2, err_model(1:end), -err_model(end:-1:1)),...
            init.color.(cond{agentivity})-0.1, 'EdgeColor', 'none', 'FaceAlpha', .2);
        l(agentivity) = plot(1:length(mean_model), mean_model, '--', 'LineWidth', 1, 'Color', init.color.(cond{agentivity})-0.1);
        
        % Data
        e(agentivity) = errorbar(cellfun(@mean, choice_utility(subj_idx,:,agentivity)), cellfun(@std, choice_utility(subj_idx,:,agentivity))./sqrt(cellfun(@length, choice_utility(subj_idx,:,agentivity))),...
            'o', 'Color', init.color.(cond{agentivity}), 'MarkerFaceColor', init.color.(cond{agentivity}), 'MarkerSize', 3); % Choices
        
        ylabel('P(accept)')
        xlabel('Utility(accept)')
        
        xticks(1:length(group)-1)
        for i = 1:length(group)-1
            Xtick{i} = sprintf('(n = %d) [%.1f : %.1f[',length(choice_utility{subj_idx,i,agentivity}),group(i),group(i+1));
        end
        xticklabels(Xtick);
        xtickangle(45);
        xlim([1 length(group)-1])
        ylim([0 1])
        
    end % Fin de la boucle � travers l'agentivit�
    
    leg = legend([e,l], 'agent', 'non agent', 'model agent', 'model non agent', 'Location','best');
    leg.Position = [.5-(leg.Position(3)/2) .5-(leg.Position(4)/2) leg.Position(3:4)]; % [left bottom width height]
    
    fig.PaperPosition = [0, 0, 15 13]; % [left bottom width height]
    fig_name = fullfile(init.pathtofigsubj{subj_idx}, [init.subj_name{subj_idx} '_task_dim_effect_on_choice_+model.tiff']);
    print(fig, fig_name, '-dtiff', '-r200');
    
    close(fig);
    
end % Fin de la boucle � travers les sujets

% Figure pour tous les sujets
if subj_idx == length(init.subj_name)
    
    fig = figure;
    hold on;
    
    for agentivity = 1:2 % Boucle � travers l'agentivit�
        for task_dim = 1:3 % task_dim : 1 = gain, 2 = loss, 3 = diff
            
            %FIGURE : proba d'accepter pour chaque dimension (gain, loss, diff) tous les sujets
            subplot(2,2,task_dim);
            hold on;
            
            % Model
            mean_model_all = mean(cellfun(@mean, model_bin(:, :, task_dim, agentivity)));
            err_model_all = std(cellfun(@mean, model_bin(:, :, task_dim, agentivity)))./sqrt(length(cellfun(@mean, model_bin(:, :, task_dim, agentivity))));
            patch([1:length(mean_model_all) length(mean_model_all):-1:1], mean_model_all([1:end end:-1:1]) + cat(2, err_model_all(1:end), -err_model_all(end:-1:1)),...
                init.color.(cond{agentivity})-0.1, 'EdgeColor', 'none', 'FaceAlpha', .2);
            l(agentivity) = plot(1:length(mean_model_all), mean_model_all, '--', 'LineWidth', 1, 'Color', init.color.(cond{agentivity})-0.1);
            
            % Data
            e(agentivity) = errorbar(mean(cellfun(@mean, choice_bin(:,:,task_dim,agentivity))), std(cellfun(@mean, choice_bin(:,:,task_dim,agentivity)))./sqrt(length(cellfun(@mean, choice_bin(:,:,task_dim,agentivity)))),...
                'o', 'Color', init.color.(cond{agentivity}), 'MarkerFaceColor', init.color.(cond{agentivity}), 'MarkerSize', 3); % Choices
            
            ylabel('P(accept)')
            ax = gca;
            ax.XTick = [1 2 3 4];
            ax.YTick = (0:.2:1);
            axis([0 5 0 1])
            set(gca,'XTickLabel',{'[1-2]',']2-3]',']3-4]',']4-5]'})
            
            switch task_dim
                case 1
                    xlabel('Gain prospect ()')
                case 2
                    xlabel('Loss prospect ()')
                case 3
                    xlabel('Challenge difficulty')
            end
            
        end % Fin de la boucle � travers les dimensions
        
        % Plot also utility
        subplot(2, 2, 4);
        hold on;
        
        % Model
        mean_model_all = nanmean(cellfun(@mean, model_utility(:, :, agentivity)));
        err_model_all = nanstd(cellfun(@mean, model_utility(:, :, agentivity)))./sqrt(length(cellfun(@mean, model_utility(:, :, agentivity))));
        patch([1:length(mean_model_all) length(mean_model_all):-1:1], mean_model_all([1:end end:-1:1]) + cat(2, err_model_all(1:end), -err_model_all(end:-1:1)),...
            init.color.(cond{agentivity})-0.1, 'EdgeColor', 'none', 'FaceAlpha', .2);
        l(agentivity) = plot(1:length(mean_model_all), mean_model_all, '--', 'LineWidth', 1, 'Color', init.color.(cond{agentivity})-0.1);
        
        % Data
        e(agentivity) = errorbar(nanmean(cellfun(@mean, choice_utility(:,:,agentivity))), nanstd(cellfun(@mean, choice_utility(:,:,agentivity)))./sqrt(length(cellfun(@mean, choice_utility(:,:,agentivity)))),...
            'o', 'Color', init.color.(cond{agentivity}), 'MarkerFaceColor', init.color.(cond{agentivity}), 'MarkerSize', 3); % Choices
        
        ylabel('P(accept)')
        xlabel('Utility(accept)')
        
        xticks(1:length(group)-1)
        for i = 1:length(group)-1
            Xtick{i} = sprintf('(n = %d) [%.1f : %.1f[',sum(cellfun(@length, choice_utility(:,i,agentivity))),group(i),group(i+1));
        end
        xticklabels(Xtick);
        xtickangle(45);
        xlim([1 length(group)-1])
        ylim([0 1])
        
    end % Fin de la boucle � travers l'agentivit�
    
    leg = legend([e,l], 'agent', 'non agent', 'model agent', 'model non agent');
    leg.Position = [.5-(leg.Position(3)/2) .5-(leg.Position(4)/2) leg.Position(3:4)]; % [left bottom width height]
    
    % Save
    fig.PaperPosition = [0, 0, 15 13]; % [left bottom width height];
    fig_name = fullfile(init.pathtofig, 'p_accept_according2dim_+model.tiff');
    print(fig, fig_name, '-dtiff', '-r200');
    % savefig(fig, [fig_name(1:end-5), '.fig'], 'compact');
    
    close(fig);
    
end

end

% ANALYSE 5 : mood analysis
function mood_model_free_analysis(init)

for subj_idx = 1:length(init.subj_name) % Boucle � travers les sujets
    
    subj_data = init.group_data(init.group_data(:,1) == subj_idx,:); % All data for one subject
    
    % Interpolate real mood ratings
    mood_pos = find(~isnan(subj_data(:,14))); % Mood position
    mood_val = zscore(subj_data(mood_pos, 14)); % Mood value (zscored)
    mood_nb = 1:length(subj_data); % Number of mood ratings
    interp_mood = interp1(mood_pos, mood_val, mood_nb, 'linear', 'extrap'); % Interpolated moods
    
    % Delta mood value
    delta_mood = diff(mood_val);
    delta_interp = diff(interp_mood);
    
    % Split according to condition (agent/non agent)
    agent_mood(subj_idx) = mean(delta_mood(subj_data(mood_pos(2:end),11) == 1));
    proba_mood(subj_idx) = mean(delta_mood(subj_data(mood_pos(2:end),11) == 0));
    
    agent_interp(subj_idx) = mean(delta_interp(subj_data(2:end,11) == 1));
    proba_interp(subj_idx) = mean(delta_interp(subj_data(2:end,11) == 0));
    
end % End of the loop over subjects

% Plot
fig = figure;
hold on

mean_plot = [mean(agent_mood) mean(proba_mood) ; mean(agent_interp) mean(proba_interp)];
err_plot = [std(agent_mood)/sqrt(length(agent_mood)) std(proba_mood)/sqrt(length(proba_mood)) ;...
    std(agent_interp)/sqrt(length(agent_interp)) std(proba_interp)/sqrt(length(proba_interp))];

bar(mean_plot)

ngroups = size(mean_plot, 1);
nbars = size(mean_plot, 2);
% Calculating the width for each bar group
groupwidth = min(0.8, nbars/(nbars + 1.5));
for i = 1:nbars
    x = (1:ngroups) - groupwidth/2 + (2*i-1) * groupwidth / (2*nbars);
    errorbar(x, mean_plot(:,i), err_plot(:,i), 'k', 'LineStyle', 'none');
end

xticks([1 2])
xticklabels({'Mood ratings', 'Interpolated mood'})

legend('agent', 'non agent')

% Stats
h.c1 = ttest(agent_mood, proba_mood);
h.c2 = ttest(agent_interp, proba_interp);

Ylim = ylim;
for i = 1:2
    if h.(sprintf('c%d',i)) == 1
        line([i-groupwidth/4, i+groupwidth/4], [Ylim(2) Ylim(2)], 'Color', 'k')
        scatter(i, Ylim(2)+0.1*Ylim(2), '*')
    end
end

% Save
fig_name = 'Effect_of_agentivity_on_mood_ratings';
fig_path = fullfile(init.pathtofig, fig_name);
print(fig, [fig_path '.tiff'], '-dtiff', '-r200');

close(fig)

end

function mood_model_analysis(init, mood_data)

% Initialize variable
param = NaN(length(init.subj_name), 3);

for subj_idx = 1:length(init.subj_name) % Boucle � travers les sujets
    
    best_model = mood_data.best_model;
    null_model = mood_data.null_model;
    
    % 1 = Delta model evidence
    param(subj_idx, 1) = mood_data.inversionResult.model(best_model).(init.subj_name{subj_idx}).out.F - ...
        mood_data.inversionResult.model(null_model).(init.subj_name{subj_idx}).out.F;
    
    % Find index of the subject in 'mood_data' structure
    idx = strcmp(fieldnames(mood_data.inversionResult.model), init.subj_name{subj_idx});
    
    % 2 = kF = feedback weight in agent trials
    param(subj_idx, 2) = mood_data.inversionResult.freeParameter(best_model).kF(idx);
    
    % 3 = kFp = feedback weight in non agent (proba) trials
    param(subj_idx, 3) = mood_data.inversionResult.freeParameter(best_model).kFp(idx);
    
    % 4 = R
    param(subj_idx, 4) = mood_data.inversionResult.freeParameter(best_model).R(idx);
    
    param(subj_idx, 5) = subj_idx; % Index of the subject
    
end % Fin de la boucle � travers les sujets

%% Histogramme des Delta model evidence
fig = figure;
hold on

sorted_param = sortrows(param, 1); % Sorted according to first column (= delta model evidence)
bar(sorted_param(:,1), 'FaceColor', [.8 .8 .8]) % Delta model evidence

ylabel('{\Delta}model evidence');
xlabel('Subjects');

% Save
fig_name = fullfile(init.pathtofig, 'delta_model_evidence.tiff');
print(fig, fig_name, '-dtiff', '-r200');

close(fig);

%% Ttest kF accross conditions

if ismember(mood_data.inversionResult.param.modelSpace(best_model).agentivityInclusion, '2kF')
    fig = figure;
    hold on
    
    fb_weight = sorted_param(:,2:3);
    
    b = bar(mean(fb_weight), 'FaceColor', 'flat');
    b.CData = [init.color.agent; init.color.non_agent];
    errorbar(mean(fb_weight), std(fb_weight)/sqrt(size(fb_weight,1)), 'LineStyle', 'none', 'Color', 'black')
    
    xticks([1 2])
    xticklabels({'Agent', 'Non agent'})
    ylabel('kF (feedback weight)');
    
    % Stats
    Ylim = ylim;
    [h,p] = ttest(fb_weight(:,1), fb_weight(:,2));
    text(1.5, max([b.YData])/2, sprintf('p = %.3g',p),'HorizontalAlignment', 'center', 'FontSize', 12)
    
    if h == 1 % Significant
        
        maxid = find(mean(fb_weight) == max(mean(fb_weight)));
        y = mean(fb_weight(:,maxid)) + std(fb_weight(:,maxid))/sqrt(length(fb_weight(:,maxid))) + diff(Ylim)*0.1;
        line([1,2], repmat(y, 1, 2), 'Color', 'k')
        
        y = y + diff(Ylim)*0.05;
        scatter(1.5, y, 'k*')
        
    end
    
    % Save
    fig_name = fullfile(init.pathtofig, 'kF_acc2condi.tiff');
    print(fig, fig_name, '-dtiff', '-r200');
    
    close(fig);
end

end

function effect_of_mood_on_choice(init, choice_data, mood_data)

% Initialisation
beta_mood = NaN(length(init.subj_name), 2);
beta_tml = NaN(length(init.subj_name), 2);

for subj_idx = 1:length(init.subj_name) % Boucle � travers les sujets
    
    subj_data = init.group_data(init.group_data(:,1) == subj_idx,:); % All data for one subject
    cond = {'agent', 'non_agent'};
    
    z_resid_all = NaN(length(subj_data), 1);
    
    for agentivity = 1:length(cond) % Boucle � travers l'agentivit�
        
        ag = agentivity*(-1)+2; % 1 = agent ; 2 = non agent
        trial_idx = subj_data(:,11) == ag;
        
        % z_resid = zscore(choice_data.(cond{agentivity}).inversionResult.model.(init.subj_name{subj_idx}).out.suffStat.dy); % R�sidus du mod�le de choix z-scor�s par condition
        z_resid = choice_data.(cond{agentivity}).inversionResult.model.(init.subj_name{subj_idx}).out.suffStat.dy; % Residues non z-scored
        z_resid_all(trial_idx) = z_resid;
        
    end % End of the loop over agentivity
    
    mood_pos = find(~isnan(subj_data(:,14))); % Mood position
    z_mood = zscore(subj_data(mood_pos, 14)); % Z-scored mood value
    
    next_mood_idx = mood_pos + 1; % Index of trials after mood ratings
    rmv_out = next_mood_idx <= length(subj_data); % If mood rating on the last trial
    
    z_tml = zscore(mood_data.inversionResult.model(mood_data.best_model).(init.subj_name{subj_idx}).out.suffStat.gx);
    
    beta_mood(subj_idx,:) = glmfit(z_mood(rmv_out), z_resid_all(next_mood_idx(rmv_out))); % Does mood predict the choice of next trial ?
    beta_tml(subj_idx,:) = glmfit(z_tml, [z_resid_all(2:end); NaN]); % Does TML predict the choice of next trial ?
    
end % End of the loop over subjects

% Plot
fig = figure;
hold on

Xlim = [0.5 2.5];
xlim(Xlim);
violinplot([beta_mood(:,2), beta_tml(:,2)], [], 'ShowMean', true);
line(Xlim, [0 0], 'Color', 'black', 'LineStyle', '--');

[~,p] = ttest([beta_mood(:,2), beta_tml(:,2)]);
text(1, max(ylim)-diff(ylim)/1.2, sprintf('p = %.3g', p(1)), 'HorizontalAlignment', 'center', 'FontSize', 12)
text(2, max(ylim)-diff(ylim)/1.2, sprintf('p = %.3g', p(2)), 'HorizontalAlignment', 'center', 'FontSize', 12)

xticks([1 2])
xticklabels({'Mood ratings', 'TML'});
ylabel('Regression estimate')
title('Mood/TML predicting choice residues')

fig_name = fullfile(init.pathtofig, 'mood_predicting_choice_resid.tiff');
print(fig, fig_name, '-dtiff', '-r200');

close(fig);

end

% ANALYSE 6 : Correlation mood and confidence
function correl_mood_confid(init)

fig = figure;
hold on

nCol = 6;
nLine = ceil((length(init.subj_name)+1)/nCol);

for subj_idx = 1:length(init.subj_name) % Boucle � travers les sujets
    
    subj_data = init.group_data(init.group_data(:,1) == subj_idx,:); % All data for one subject
    
    mood = zscore(subj_data(~isnan(subj_data(:,14)),14));
    confid_and_proba = zscore(subj_data(~isnan(subj_data(:,14)),9));
    
    % Do the correlation
    prdct = confid_and_proba;
    resp = mood;
    b(subj_idx,:) = glmfit(prdct, resp); % Does confidence predicts mood
    
    % Plot the correlation for each patient
    subplot(nLine, nCol, subj_idx); hold on;
    scatter(confid_and_proba, mood, 11, 'filled', 'MarkerFaceAlpha', 0.3);
    
    yfit = glmval(b(subj_idx,:)', prdct, 'identity');
    st = sortrows([prdct, yfit]);
    plot(st(:,1), st(:,2), 'LineWidth', 2);
    
    xlabel('Confidence')
    ylabel('Mood')
    title(init.subj_name{subj_idx}, 'Interpreter', 'none')
    
end % End of the loop over subjects

% Plot mean betas for all patients
subplot(nLine, nCol, length(init.subj_name)+1); hold on;
title('All')
bar(mean(b))
errorbar(mean(b), std(b)/sqrt(length(b)), 'LineStyle', 'none', 'Color', 'black')

Xticks = 1:size(b,2);
xticks(Xticks)
xticklabels({'Intercept', '\beta'})

% Stats
h = ttest(b);
Ylim = ylim;

for i = 1:length(h)
    if h(i) == 1
        scatter(Xticks(i), Ylim(2) + diff(Ylim)*0.1, 'k*');
    end
end

% Save
fig.PaperPosition = [0, 0, 30 20]; % [left bottom width height]
fig_name = fullfile(init.pathtofig, 'correl-confid-mood.tiff');
print(fig, fig_name, '-dtiff', '-r200');

close(fig);

end

% ANALYSE 7 : Quadratic correlations analysis
function correl_analysis(init, choice_data)

% In this function :
%   - Confidence = Subjective probability of success (agent trials)
%   - Probability = Probability of success (non agent trials)
%   - Confidence_proxy = Probability that the decision is correct = (p(accept)centr�)�

fig = figure;
hold on

nCol = 6;
nLine = ceil((length(init.subj_name)+1)/nCol);

for subj_idx = 1:length(init.subj_name) % Boucle � travers les sujets
    
    subj_data = init.group_data(init.group_data(:,1) == subj_idx,:); % All data for one subject
    cond = {'agent', 'non_agent'};
    
    % Delete reaction time outliers
    rt = subj_data(:,8);
    [~,out] = rmoutliers(rt, 'mean');
    subj_data(out,8) = NaN;
    
    for agentivity = 1:2 % Boucle � travers l'agentivit�
        
        ag = agentivity*(-1)+2; % 1 = agent ; 2 = non agent
        agent_data = subj_data(subj_data(:,11) == ag,:); % All data for one subject and one condition (agent/non agent)
        
        confid_rating = zscore(agent_data(:,9)); % Confidence rating in agent trials | Proba rating in non agent trials
        proba_accept = choice_data.(cond{agentivity}).inversionResult.model.(init.subj_name{subj_idx}).posterior.muX(2,:)'; % Also in out.suffStat.gx
        utility_accept = choice_data.(cond{agentivity}).inversionResult.model.(init.subj_name{subj_idx}).posterior.muX(1,:);
        proxy_confid = (proba_accept - mean(proba_accept)).^2; % Proba that the decision is correct
        corrected_confid = (confid_rating - mean(confid_rating)).^2; % Confidence ratings corrected
        choice_rt = agent_data(:,8);
        
        prdct = zscore(utility_accept)';
        resp = choice_rt; % zscore(choice_rt);
        
        prdct_name = 'utility';
        resp_name = 'response-time';
        
        % Do the correlations
        % b(subj_idx,:,agentivity) = polyfit(prdct, resp, 2);
        b(subj_idx,:,agentivity) = glmfit([prdct, prdct.^2], resp);
        
        % Plot the correlation for each patient
        subplot(nLine, nCol, subj_idx); hold on;
        scatter(prdct, resp, 11, 'filled', 'MarkerFaceColor', init.color.(cond{agentivity}), 'MarkerFaceAlpha', 0.3, 'HandleVisibility', 'off');
        
        % yfit = polyval(b(subj_idx,:,agentivity), prdct);
        yfit = glmval(b(subj_idx,:,agentivity)', [prdct, prdct.^2], 'identity');
        st = sortrows([prdct, yfit]);
        plot(st(:,1), st(:,2), 'LineWidth', 2, 'Color', init.color.(cond{agentivity}));
        
        xlabel(prdct_name)
        ylabel(resp_name)
        title(init.subj_name{subj_idx}, 'Interpreter', 'none')
        
        % Make bins
        group = linspace(-3,3,6);
        for ig = 1:length(group)-1
            if ig == 1 % First bin
                itmp = prdct < group(ig+1);
            elseif ig == length(group)-1 % Last bin
                itmp = prdct >= group(ig);
            else
                itmp = prdct >= group(ig) & prdct < group(ig+1);
            end
            bin_resp(subj_idx, ig, agentivity) = {resp(itmp)}; % Data
        end
        
    end % End of the loop over agentivity
    
    if subj_idx == 1 % First subject
        legend(cond, 'Interpreter', 'none')
    end
    
end % End of the loop over subjects

% Save
fig.PaperPosition = [0, 0, 40 20]; % [left bottom width height]
fig_name = fullfile(init.pathtofig, sprintf('correl-%s-%s.tiff', prdct_name, resp_name));
print(fig, fig_name, '-dtiff', '-r200');

close(fig);

%% New figure with mean of all patients

fig = figure;
hold on

% SUBPLOT 1
subplot(1,2,1)
hold on

for agentivity = 1:2 % Boucle � travers l'agentivit�
    
    e(agentivity) = errorbar(nanmean(cellfun(@nanmean, bin_resp(:,:,agentivity))), nanstd(cellfun(@nanmean, bin_resp(:,:,agentivity)))./sqrt(sum(~isnan(cellfun(@nanmean, bin_resp(:,:,agentivity))))),...
        '-o', 'Color', init.color.(cond{agentivity}), 'MarkerFaceColor', init.color.(cond{agentivity}), 'MarkerSize', 3); % Choices
    
end % End of the loop over agentivity

xlabel(prdct_name)
ylabel(resp_name)

xticks(1:length(group)-1)
for i = 1:length(group)-1
    Xtick{i} = sprintf('(n = %d) [%.1f : %.1f[', sum(cellfun(@length, bin_resp(:,i,agentivity))), group(i), group(i+1));
end
xticklabels(Xtick);
xtickangle(45);
xlim([1 length(group)-1])
legend(cond, 'Interpreter', 'none', 'Location', 'best')

% SUBPLOT 2 : Plot mean betas for all patients
subplot(1,2,2); hold on;
mean_betas = squeeze(mean(b,1));
err_betas = squeeze(std(b,[],1))/sqrt(size(b,1));

b_plot = bar(mean_betas, 'FaceColor', 'flat');
for k = 1:size(mean_betas,2)
    b_plot(k).CData = init.color.(cond{k});
end

h = logical(squeeze(ttest(b)));
Ylim = ylim;

% Plot error bar
ngroups = size(mean_betas, 1);
nbars = size(mean_betas, 2);
groupwidth = min(0.8, nbars/(nbars + 1.5));
for i = 1:nbars
    x = (1:ngroups) - groupwidth/2 + (2*i-1) * groupwidth / (2*nbars);
    errorbar(x, mean_betas(:,i), err_betas(:,i), 'k', 'LineStyle', 'none');
    sign_mean = sign(mean_betas(h(:,i),i));
    scatter(x(h(:,i)), mean_betas(h(:,i),i) + (err_betas(h(:,i),i) .* sign_mean) + (diff(Ylim) * 0.1 .* sign_mean), 'k*')
end

Xticks = 1:size(b,2);
xticks(Xticks)
xticklabels({'Intercept', 'Linear', 'Quadratic'})
ylabel('betas')

fig.PaperPosition = [0, 0, 17 10]; % [left bottom width height]
fig_name = fullfile(init.pathtofig, sprintf('correl-%s-%s-all.tiff', prdct_name, resp_name));
print(fig, fig_name, '-dtiff', '-r200');

close(fig);

end
