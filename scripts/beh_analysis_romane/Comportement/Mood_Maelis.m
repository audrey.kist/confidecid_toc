clear
% Matrice avec toutes les donn�es de tous les sujets :
load('C:\Users\Romane\Desktop\Mood_Mealis\init.mat', 'init');
% init.subj_data(essais, donn�es, sujets)

for num_sujet = 1:size(init.subj_data,3)
    
    subjdata = init.subj_data(:,:,num_sujet); % Matrice pour 1 sujet a chaque fois 
    
    %-------------------------------------------------------------------------%
    % 1 : Trouver l'indice de tous les essais o� il y a eu une notation de l'humeur (non NaN)
    % et r�cup�rer les notes de l'humeur et les choix associ�s dans une matrice (colonne 1 = note de l'humeur, colonne 2 = choix)
    
    MOCH = subjdata(~isnan(subjdata(:,14)),[14,7]);
    
    % 2 : Trouver les choix associ�s � une note haute ou basse (avec la
    % m�diane) et faire la moyenne de ces choix dans deux matrices diff�rentes
    M=median(MOCH(:,1));
    
    % trouver la proba d'accepter quanf humeur basse--------------------------------
    LB = find(MOCH(:,1)<M);  % Lignes correspondant � l'humeur basse
    CB = MOCH(LB,2);    % les choix d'humeur basses
    PHB(num_sujet)= mean(CB);    %proba d'accepter quand lhumeur est basse
    
    % trouver la proba d'accepter quanf humeur haute--------------------------------
    LH = find(MOCH(:,1)>M);  % Lignes correspondant � l'humeur basse
    CH = MOCH(LH,2);    % les choix d'humeur basses
    PHH(num_sujet) = mean(CH);    %proba d'accepter quand lhumeur est basse
    
end % Fin de la boucle � travers les sujets

% Figure

moodfig = figure;
hold on;

y = nanmean(PHH-PHB);
sem = nanstd(PHB-PHH)/sqrt(length(PHH)); % Erreur moyenne humeur basse

bar(y)
errorbar(y,sem,'LineStyle','none','Color','black')

xticks(1);
xticklabels('High - Low mood')
ylabel('p(accept)')

% Test statistique
[h,p] = ttest(PHH-PHB);

if h == 1
    scatter(1, 0.17, 'k*') 
end

ylim([0 0.18])
