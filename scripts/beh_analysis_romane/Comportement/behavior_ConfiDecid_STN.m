% Analyses comportement CONFIDENCE-DECID patients STN (stage Jos�phine)

clearvars
close all

%% Param�tres

param.behav.choice = 1; % 0 = don't   1 = analyse choice behavior

% ------------------------------------------------------------------------%

init.path = 'D:\BDD_ConfiDecid\Cognitive_task_STN\Patients'; % Chemin o� se trouvent les donn�es
init.pathtofig = 'D:\BDD_ConfiDecid\Cognitive_task_STN\Comportement'; % Chemin o� sont enregistr�es les figures g�n�r�es

% Find patients' names
filelist = dir(init.path);
name = {filelist.name};
init.subj_name = name(~strncmp(name, '.', 1)); % No files starting with '.'

init.nb_trial_sess = 128; % Nombre d'essais par session

%% Colors definition

init.color.blue = [0.212, 0.624, 0.761];
init.color.orange = [1, 0.475, 0.278];
init.color.darkblue = [0.02, 0.447, 0.467];

%% Initialisation (avant boucle � travers les sujets)

init.group_data = [];

%% Boucle � travers les sujets

for subj_idx = 1:length(init.subj_name)
    
    subjName = init.subj_name{subj_idx};
    
    % Trouver les sessions du sujet
    behav_folder = fullfile(init.path, subjName, 'RAW', 'Behavior', 'Postsurgery');
    filelist = dir(behav_folder); % Liste de tous les fichiers dans le dossier "Postsurgery"
    name = {filelist.name};
    sess_name = name(~cellfun(@isempty, regexp(name, '[1-9]_cognition_data.mat', 'match'))); % On ne prend pas les sessions 0 car elles correspondent � l'entra�nement
    
    % Boucle � travers les sessions et cr�ation de la matrice "group_data"
    for sess = 1:length(sess_name)
        
        % Load data file
        data_filename = fullfile(behav_folder, sess_name{sess});
        load(data_filename, 'data'); % Charger la matrice data de la session
        
        data(:,1) = subj_idx; % Ajouter l'indice du sujet colonne 1
        init.group_data = [init.group_data ; data]; % Matrice contenant les donn�es de toutes les sessions et de tous les sujets
        
    end % Fin de la boucle � travers les sessions
    
    subj_data = init.group_data(init.group_data(:,1) == subj_idx,:); % All data for one subject
    
    % Cr�ation des dossiers sujets dans le dossier 'Comportement' s'ils n'existent pas
    init.pathtofigsubj{subj_idx} = fullfile(init.pathtofig,subjName);
    if ~exist(init.pathtofigsubj{subj_idx},'dir')
        mkdir(init.pathtofigsubj{subj_idx});
    end
    
    %% CHOICE MODEL
    
    % Load choice data of all subjects
    choice_data_fullname = fullfile(init.pathtofig, 'all_choice_data.mat');
    if exist(choice_data_fullname,'file')
        load(choice_data_fullname, 'choice_data'); % Load 'choice_data' structure
    else % Create an empty 'mood_data' structure
        choice_data = struct();
    end
    
    if isempty(fieldnames(choice_data)) || ~isfield(choice_data.inversionResult.model(1), subjName) % If subject's name is not a field of 'choice_data' : compute TML for this subject
        
        % Load mean and std of the subject
        param_filename = name(~cellfun(@isempty, regexp(name, '_estim_param', 'match')));
        load(fullfile(behav_folder, param_filename{:}));
        
        S = [];
        S.subjname = subjName;
        S.data = subj_data;
        S.moy_estim = moy_estim;
        S.std_estim = std_estim;
        choice_data = choiceModel_ConfiDecid(S, choice_data); % Get choice model for the subject
        
        choice_data.nSubject = length(fieldnames(choice_data.inversionResult.model));
        
        % Save choice data
        save(choice_data_fullname, 'choice_data');
        close all;
    end
    
end % Fin de la boucle � travers les sujets

%% CHOICE BEHAVIOR

if param.behav.choice == 1
    
    %% ANALYSE 1 : Probabilit� d'accepter en fonction des dimensions de la t�che et de l'agentivit�
    
    opt.var_column = 7; % Choix (1 = accept ; 0 = reject)
    opt.ylabel = 'P(accept)';
    
    % ANALYSE 1.A : probabilit� d'accepter le d�fi en fonction de chaque dimension de la t�che pour chaque sujet
    opt.YTick = (0:.2:1);
    opt.axis = [0 5 0 1];
    opt.subj_fig_name = '_task_dim_effect_on_choice.tiff';
    opt.all_fig_name = 'p_accept_according2dim.tiff';
    
    task_dim_effect_on_var(init, opt);
    
    % ANALYSE 1.B : corr�lation entre la probabilit� d'accepter le d�fi et chaque dimension de la t�che pour chaque sujets
    opt.subj_fig_name = '_correl_task_dim_and_choice.tiff';
    opt.ylim = [0 1];
    
    beta_corr = corr_task_dim_effect(init, opt);
    
    % ANALYSE 1.C : ttest sur les corr�lations obtenues dans l'analyse B
    opt.ylabel = 'choice';
    opt.DataAspectRatio = [1 0.2 1];
    opt.all_fig_name = 'slope_correl_task_dim_and_choice.tiff';
    
    ttest_beta_corr(beta_corr, init, opt)
    
    %% ANALYSE 2 : Note de confiance en fonction des dimensions de la t�che et de l'agentivit�
    
    opt.var_column = 9; % Notes de confiance de 1 � 100
    opt.ylabel = 'Confidence';
    
    % ANALYSE 2.A : confiance en fonction des dimensions de la t�che
    opt.YTick = (0:25:100);
    opt.axis = [0 5 0 100];
    opt.subj_fig_name = '_task_dim_effect_on_confidence.tiff';
    opt.all_fig_name = 'confidence_according2dim.tiff';
    
    task_dim_effect_on_var(init, opt);
    
    % ANALYSE 2.B : corr�lation entre la probabilit� d'accepter le d�fi et chaque dimension de la t�che pour chaque sujets
    opt.subj_fig_name = '_correl_task_dim_and_confidence.tiff';
    opt.ylim = [0 100];
    
    beta_corr = corr_task_dim_effect(init, opt);
    
    % ANALYSE 2.C : ttest sur les corr�lations obtenues dans l'analyse B
    opt.ylabel = 'confidence';
    opt.DataAspectRatio = [1 1 1];
    opt.all_fig_name = 'slope_correl_task_dim_and_confidence.tiff';
    
    ttest_beta_corr(beta_corr, init, opt)
    
    %% ANALYSE 3 : Probabilit� d'accepter le d�fi en fonction de l'utilit�
    
    p_accept_utility(init, choice_data)
    
end

%% FIN
return;

%% CHOICE BEHAVIOR

% ANALYSE A : variation d'une variable en fonction de chaque dimension de la t�che pour chaque sujet
function task_dim_effect_on_var(init, opt)

var_p = cell(length(init.subj_name),4,3,2);
task_dim_column = [4 5 6]; % Gains = colonne 4 ; Pertes = colonne 5 ; Diff = colonne 6 dans la matrice 'group_data'

for subj_idx = 1:length(init.subj_name) % Boucle � travers les sujets
    
    subj_data = init.group_data(init.group_data(:,1) == subj_idx,:); % All data for one subject
    var = subj_data(:,opt.var_column); % Offre accept�e (1) ou rejet�e (0) // Notes de confiance entre 1 et 100
    
    fig = figure;
    hold on;
    
    for agentivity = 1:2 % Boucle � travers l'agentivit�
        agent = agentivity*(-1)+2; % 1 = agent ; 2 = non agent
        
        for task_dim = 1:length(task_dim_column) % task_dim : 1 = gain, 2 = loss, 3 = diff
            
            prospect = subj_data(:,task_dim_column(task_dim)); % prospect de toutes les sessions pour 1 sujet
            id_q1 = prospect < 20 & subj_data(:,11) == agent;
            id_q2 = prospect < 30 & prospect >= 20 & subj_data(:,11) == agent;
            id_q3 = prospect < 40 & prospect >= 30 & subj_data(:,11) == agent;
            id_q4 = prospect < 50 & prospect >= 40 & subj_data(:,11) == agent;
            
            var_p(subj_idx,:,task_dim,agentivity) = {var(id_q1), var(id_q2), var(id_q3), var(id_q4)}; % choice_p dim : subject by task_levels (4 levels) by task dim (gain, loss, challenge_diff)
            
            %FIGURE : proba d'accepter pour chaque dimension (gain, loss, diff) et chaque sujet
            subplot(3,1,task_dim);
            hold on;
            pbaspect([1,0.5,1]);
            errorbar(cellfun(@mean, var_p(subj_idx,:,task_dim,agentivity)), cellfun(@std, var_p(subj_idx,:,task_dim,agentivity))./sqrt(cellfun(@length, var_p(subj_idx,:,task_dim,agentivity))),'o-','MarkerFaceColor',[0 0.4470 0.7410],'MarkerSize',3);
            ylabel(opt.ylabel)
            ax = gca;
            ax.XTick = [1 2 3 4];
            ax.YTick = opt.YTick;
            axis(opt.axis)
            set(gca,'XTickLabel',{'[1-2]',']2-3]',']3-4]',']4-5]'})
            
            switch task_dim
                case 1
                    xlabel('Gain prospect ()')
                case 2
                    xlabel('Loss prospect ()')
                case 3
                    xlabel('Challenge difficulty')
            end
            
        end % Fin de la boucle � travers les dimensions
    end % Fin de la boucle � travers l'agentivit�
    
    legend('agent','non agent','Location','best');
    
    fig.PaperPosition = [0, 0, 7 14];
    fig_name = fullfile(init.pathtofigsubj{subj_idx}, [init.subj_name{subj_idx} opt.subj_fig_name]);
    print(fig, fig_name, '-dtiff', '-r200');
    
    close(fig);
    
end % Fin de la boucle � travers les sujets

% Figure pour tous les sujets
if subj_idx == length(init.subj_name)
    
    fig = figure;
    hold on;
    
    for agentivity = 1:2 % Boucle � travers l'agentivit�
        for task_dim = 1:3 % task_dim : 1 = gain, 2 = loss, 3 = diff
            
            %FIGURE : proba d'accepter pour chaque dimension (gain, loss, diff) tous les sujets
            subplot(3,1,task_dim);
            hold on;
            errorbar(nanmean(cellfun(@nanmean, var_p(:,:,task_dim,agentivity))),nanstd(cellfun(@nanmean, var_p(:,:,task_dim,agentivity)))/sqrt(size(var_p,1)),'o-','MarkerFaceColor',[0 0.4470 0.7410],'MarkerSize',3);
            ylabel(opt.ylabel)
            ax = gca;
            ax.XTick = [1 2 3 4];
            ax.YTick = opt.YTick;
            axis(opt.axis)
            set(gca,'XTickLabel',{'[1-2]',']2-3]',']3-4]',']4-5]'})
            
            switch task_dim
                case 1
                    xlabel('Gain prospect ()')
                case 2
                    xlabel('Loss prospect ()')
                case 3
                    xlabel('Challenge difficulty')
            end
            
        end
    end % Fin de la boucle � travers l'agentivit�
    
    legend('agent','non agent','Location','best');
    
    fig.PaperPosition = [0, 0, 7 14];
    fig_name = fullfile(init.pathtofig, opt.all_fig_name);
    print(fig, fig_name, '-dtiff', '-r200');
    
    close(fig);
end

end

% ANALYSE B : corr�lation entre une variable et chaque dimension de la t�che pour chaque sujet
function beta_corr = corr_task_dim_effect(init, opt)

task_dim_column = [4 5 6]; % Gains = colonne 4 ; Pertes = colonne 5 ; Diff = colonne 6 dans la matrice 'group_data'

for subj_idx = 1:length(init.subj_name) % Boucle � travers les sujets
    
    subj_data = init.group_data(init.group_data(:,1) == subj_idx,:); % All data for one subject
    
    fig = figure;
    hold on;
    
    for task_dim = 1:length(task_dim_column) % task_dim : 1 = gain, 2 = loss, 3 = diff
        for agentivity = 1:2 % Boucle � travers l'agentivit�
            
            voi(subj_idx,1,task_dim,agentivity) = {subj_data(subj_data(:,11) == agentivity*(-1)+2, task_dim_column(task_dim))}; % Col 1 = Prospects
            voi(subj_idx,2,task_dim,agentivity) = {subj_data(subj_data(:,11) == agentivity*(-1)+2, opt.var_column)}; % Col 2 = Variable d'int�r�t (choix, confiance...)
            
            % FIGURE : Corr�lation entre la variable d'int�r�t et chaque dimension (gain, loss, diff) et chaque sujet
            
            x = voi{subj_idx,1,task_dim,agentivity}; % Predictors
            y = voi{subj_idx,2,task_dim,agentivity}; % Responses
            if VBA_isBinary(y) == true
                [b,~,stats] = glmfit(x,y,'binomial');
                blink = 'logit';
            else
                [b,~,stats] = glmfit(x,y);
                blink = 'identity';
            end
            
            beta_corr(subj_idx, agentivity, task_dim) = b(2); % Pente de la corr�lation
            stats_corr(subj_idx, agentivity, task_dim) = stats.p(2); % P-value de la corr�lation
            
            subplot(3,1,task_dim);
            hold on;
            pbaspect([1,0.5,1]);
            
            yfit = glmval(b,x,blink);
            st = sortrows([x,yfit]);
            p(agentivity) = plot(st(:,1),st(:,2),'LineWidth',2);
            scatter(x,y,10,'filled','MarkerFaceColor',p(agentivity).Color,'MarkerFaceAlpha',0.3);
            
            ylabel(opt.ylabel)
            ylim(opt.ylim)
            
            switch task_dim
                case 1
                    xlabel('Gain prospect ()')
                case 2
                    xlabel('Loss prospect ()')
                case 3
                    xlabel('Challenge difficulty')
            end
            
        end % Fin de la boucle � travers l'agentivit�
    end % Fin de la boucle � travers les dimensions de la t�che
    
    legend(p, 'agent','non agent','Location','best');
    
    fig.PaperPosition = [0, 0, 7 14];
    fig_name = fullfile(init.pathtofigsubj{subj_idx}, [init.subj_name{subj_idx} opt.subj_fig_name]);
    print(fig, fig_name, '-dtiff', '-r200');
    
    close(fig);
    
    % Save stats
    t.agent = table(squeeze(beta_corr(subj_idx,1,:)),squeeze(stats_corr(subj_idx,1,:)),'VariableNames',{'beta','pvalue'},'RowNames',{'Gain';'Loss';'Diff'});
    t.non_agent = table(squeeze(beta_corr(subj_idx,2,:)),squeeze(stats_corr(subj_idx,2,:)),'VariableNames',{'beta','pvalue'},'RowNames',{'Gain';'Loss';'Diff'});
    [~,na] = fileparts(opt.subj_fig_name);
    save(fullfile(init.pathtofigsubj{subj_idx}, [init.subj_name{subj_idx} na '.mat']),'t');
    
end % Fin de la boucle � travers les sujets

% T-test agent vs. non agent (identique pour chaque dimensions de la t�che)
[h,p,~,stats] = ttest(cellfun(@mean, voi(:,2,1,1)), cellfun(@mean, voi(:,2,1,2)));

end

% ANALYSE C : ttest de la pente des corr�lations obtenues dans l'analyse B
function ttest_beta_corr(beta_corr, init, opt)

fig = figure;
hold on;

for task_dim = 1:size(beta_corr,3) % task_dim : 1 = gain, 2 = loss, 3 = diff
    for agentivity = 1:2 % Boucle � travers l'agentivit�
        
        % FIGURE
        subplot(3,1,task_dim);
        hold on;
        pbaspect([1,0.5,1]);
        xlim([0 3])
        xticks([1 2])
        
        bar(agentivity, mean(beta_corr(:, agentivity, task_dim)));
        errorbar(agentivity, mean(beta_corr(:, agentivity, task_dim)), std(beta_corr(:, agentivity, task_dim))/sqrt(length(beta_corr)), 'Color', 'black')
        scatter(repmat(agentivity,1,size(beta_corr,1)), beta_corr(:, agentivity, task_dim), 15, 'MarkerFaceColor', [0.5 0.5 0.5], 'MarkerFaceAlpha', 0.3, 'MarkerEdgeColor', 'none')
        
        % T-test
        [h,p,~,stats] = ttest(beta_corr(:, agentivity, task_dim));
        if h == 1 % Mettre une �toile au dessus de la bar si c'est significaivement diff�rent de z�ro
            y = mean(beta_corr(:, agentivity, task_dim)) + std(beta_corr(:, agentivity, task_dim))/sqrt(length(beta_corr))*sign(mean(beta_corr(:, agentivity, task_dim)))*4;
            scatter(agentivity, y, '*k', 'sizeData',50,'linewidth',0.7)
        end
        
    end % Fin de la boucle � travers l'agentivit�
    
    xticklabels({'agent','non agent'})
    
    switch task_dim
        case 1
            xlabel('Gain prospect')
        case 2
            xlabel('Loss prospect')
        case 3
            xlabel('Challenge difficulty')
    end
    
    set(gca,'DataAspectRatio',opt.DataAspectRatio)
    ylabel(sprintf('Slope of correl\nw/ %s', opt.ylabel));
    
end % Fin de la boucle � travers les dimensions de la t�che

fig.PaperPosition = [0, 0, 7 14];
fig_name = fullfile(init.pathtofig, opt.all_fig_name);
print(fig, fig_name, '-dtiff', '-r200');

close(fig);

end

% ANALYSE 3 : Probabilit� d'accepter le d�fi en fonction de l'utilit�
function p_accept_utility(init, choice_data)

for subj_idx = 1:length(init.subj_name) % Boucle � travers les sujets
    
    subj_data = init.group_data(init.group_data(:,1) == subj_idx,:); % All data for one subject
    
    % Utility = ps*kg*gain - (1-ps)*kl*loss [ps = proba of sucess ; k = weight]
    utility_accept{subj_idx,:} = choice_data.inversionResult.model.(init.subj_name{subj_idx}).posterior.muX(1,:);
    proba_accept{subj_idx,:} = choice_data.inversionResult.model.(init.subj_name{subj_idx}).posterior.muX(2,:); % Also in out.suffStat.gx
    choices{subj_idx,:} = subj_data(:,7)';
    
    group = linspace(min(utility_accept{subj_idx,:}),max(utility_accept{subj_idx,:}),10);
    
    % Make bins
    for ig = 1:length(group)-1
        if ig == 1 % First bin
            itmp = utility_accept{subj_idx,:} < group(ig+1);
        elseif ig == length(group)-1 % Last bin
            itmp = utility_accept{subj_idx,:} >= group(ig);
        else
            itmp = utility_accept{subj_idx,:} >= group(ig) & utility_accept{subj_idx,:} < group(ig+1);
        end
        proba_model(subj_idx,ig) = {proba_accept{subj_idx,:}(itmp)}; % Model
        proba_choice(subj_idx,ig) = {choices{subj_idx,:}(itmp)}; % Data
    end
    
    % Figure pour chaque sujet
    fig = figure;
    hold on;
    
    % Model
    plot(1:length(group)-1, cellfun(@nanmean, proba_model(subj_idx,:)), '--', 'LineWidth', 1, 'Color', init.color.darkblue)
    
    % Data
    errorbar(1:length(group)-1, cellfun(@nanmean, proba_choice(subj_idx,:)),...
        cellfun(@nanstd, proba_choice(subj_idx,:))./sqrt(cellfun(@length, proba_choice(subj_idx,:))),...
        'o', 'LineStyle','none', 'MarkerFaceColor', init.color.orange, 'MarkerEdgeColor', init.color.orange);
    
    ylabel('P(accept)')
    xlabel('Utility(accept)')
    
    legend('Model','Data','Location','best');
    
    xticks(1:length(group)-1)
    for i = 1:length(group)-1
        Xtick{i} = sprintf('(n = %d) [%.1f : %.1f[',length(proba_choice{subj_idx,i}),group(i),group(i+1));
    end
    xticklabels(Xtick);
    xtickangle(45);
    xlim([1 length(group)-1])
    ylim([0 1])
    
    fig_name = fullfile(init.pathtofigsubj{subj_idx}, [init.subj_name{subj_idx} '_utility_effect_on_choice.tiff']);
    print(fig, fig_name, '-dtiff', '-r200');
    
    close(fig);
    
end % Fin de la boucle � travers les sujets

group = linspace(-7.5,4.5,10); % A modifier si on veut regrouper plus ou moins d'utilit�

% Plot mean utility
fig = figure;
hold on;

% Model
mean_model = nanmean(cellfun(@nanmean, proba_model));
err_model = nanstd(cellfun(@nanmean, proba_model))./sqrt(sum(~isnan(cellfun(@nanmean, proba_model))));
patch([1:length(group)-1 length(group)-1:-1:1], mean_model([1:end end:-1:1]) + cat(2, err_model(1:end), -err_model(end:-1:1)),...
    init.color.darkblue, 'EdgeColor', 'none', 'FaceAlpha', .1);
l(1) = plot(1:length(group)-1, mean_model, '--', 'LineWidth', 1, 'Color', init.color.darkblue);

% Data
l(2) = errorbar(1:length(group)-1, nanmean(cellfun(@nanmean, proba_choice)),...
    nanstd(cellfun(@nanmean, proba_choice))./sqrt(sum(~isnan(cellfun(@nanmean, proba_choice)))),...
    'o', 'LineStyle', 'none', 'MarkerFaceColor', init.color.orange, 'MarkerEdgeColor', init.color.orange);

ylabel('P(accept)')
xlabel('Utility(accept)')

legend(l, 'Model','Data','Location','best');

xticks(1:length(group)-1)
for i = 1:length(group)-1
    Xtick{i} = sprintf('[%.1f : %.1f[',group(i),group(i+1));
end
xticklabels(Xtick);
xtickangle(45);
xlim([1 length(group)-1])
ylim([0 1])

fig_name = fullfile(init.pathtofig, 'utility_effect_on_choice.tiff');
print(fig, fig_name, '-dtiff', '-r200');
savefig(fig, fig_name(1:end-5),'compact'); % Matlab format

close(fig);

end
