% Analyses comportement ConfiDecid STIM task

clearvars
close all

%% Param�tres

init.parcels = {'PFCvm','aINS_dors','aINS_vent'}; % Parcels to analyse (e.g: 'PFCvm', 'aINS') ,'PFCvm','aINS_dors','aINS_vent'
init.rmv_badchan = 1;

% rmv_badchan options :
%   - 0 = No channels removed
%   - 1 = Remove channels with problems from analysis (badchan)
%   - 2 = Remove channels according to sEEG activity (evocked resp to feedback) +

% Use parameters from the cognition dataset and let fluctuate only kg and kl (if set to 1)
init.use_cog_choice_model = 0;

%-------------------------------------------------------------------------%

init.subj_name = {'GRE_2018_BOIl','GRE_2019_BAUj','GRE_2019_CHRc','GRE_2019_BERj','GRE_2020_BERl',...
    'GRE_2020_SOLm','GRE_2020_FOUt','GRE_2020_SULg','GRE_2020_KAYc','GRE_2021_GUIl',...
    'GRE_2021_GROg','GRE_2021_BUIm','GRE_2021_GUIa'}; % Ajouter les sujets ici

% All subjects
% 'GRE_2018_BOIl','GRE_2019_BAUj','GRE_2019_CHRc','GRE_2019_BERj','GRE_2020_BERl',...
%     'GRE_2020_SOLm','GRE_2020_FOUt','GRE_2020_SULg','GRE_2020_KAYc','GRE_2021_GUIl',...
%     'GRE_2021_GROg','GRE_2021_BUIm'}

name = getenv('COMPUTERNAME');
if ismember(name, 'HP-ROMANE')
    init.bdd_path = 'C:\Users\Romane\Documents\MATLAB\BDD_ConfiDecid';
elseif ismember(name, 'DESKTOP-IHJ3T8M')
    init.bdd_path = 'C:\Users\Romane\Documents\BDD_ConfiDecid';
end

init.path = fullfile(init.bdd_path, 'Stim_task', 'Patients'); % Chemin o� se trouvent les donn�es
init.pathtofig = fullfile(init.bdd_path, 'Stim_task', 'Comportement'); % Chemin o� sont enregistr�es les figures g�n�r�es

switch init.rmv_badchan
    case 0
        init.pathtofig_sub = fullfile(init.pathtofig, 'noChanRmv');
    case 1
        init.pathtofig_sub = fullfile(init.pathtofig, 'rmv_badchan');
    case 2
        init.pathtofig_sub = fullfile(init.pathtofig, 'rmv_acc2sEEG');
end

init.conditions = {'no_stim','stim'};
init.group_data = [];

%% Colors definition

init.color.blue = [0.212, 0.624, 0.761];
init.color.orange = [1, 0.475, 0.278];
init.color.darkblue = [0.02, 0.447, 0.467];

%% Load things

% Load choice data from the COGNITION task
load(fullfile(init.bdd_path, 'Cognitive_task', 'Comportement', 'all_choice_data.mat'), 'choice_data');
cog_choice_data = choice_data;
clear choice_data

% Load choice data of all subjects
choice_data_fullname = fullfile(init.pathtofig, 'all_choice_data_stim.mat');
if exist(choice_data_fullname,'file') && ~exist('choice_data', 'var')
    load(choice_data_fullname, 'choice_data'); % Load 'choice_data' structure
elseif ~exist(choice_data_fullname,'file') % Create an empty 'choice_data' structure
    choice_data.no_stim = struct();
    choice_data.stim = struct();
end

%% Boucle � travers les parcelles

for parcl = 1:length(init.parcels)
    for subj_idx = 1:length(init.subj_name) % Boucle � travers les sujets
        
        clearvars -except init parcl subj_idx choice_data cog_choice_data choice_data_fullname
        subjName = init.subj_name{subj_idx};
        
        %% Trouver les �lectrodes du sujet
        
        behav_folder = fullfile(init.path, subjName, 'RAW', 'Comportement');
        filelist = dir(behav_folder); % Liste de tous les fichiers dans le dossier "Postsurgery"
        name = {filelist.name};
        sess_name = name(~cellfun(@isempty, regexp(name, '(.+)mA_data.mat', 'match'))); % All sessions
        
        subj_elec = regexp(sess_name, '[a-zA-Z](p|\'')?\d+-\d+', 'match'); % Electrodes names
        subj_elec = cat(1, subj_elec{:}); % Convert into one cell array
        
        format_elec = format_chanlabels(subj_elec); % Electrode name formatted
        
        elec_intensity = regexp(sess_name, '\d(?=mA)', 'match'); % Electrodes intensity (in mA)
        elec_intensity = str2double([elec_intensity{:}]); % Convert into double
        
        %% Find electrodes within the parcel
        
        % Get parcel infos for each sEEG channel from .CSV file
        csvfile = fullfile(fullfile(init.bdd_path, 'Anatomie'), init.subj_name{subj_idx}, sprintf('%s.csv', init.subj_name{subj_idx}));
        csv = csv2anat2(csvfile, format_elec);
        
        % Specify all the contraints that will be used to define a given ROI
        constraints = anat_constraints(init.parcels{parcl});
        ROI_chan_idx = test_chan_parcel(csv, constraints); % Index of electrodes in the parcel for the subject
        
        %% Loop over electrodes
        
        for elec = 1:length(ROI_chan_idx)
            
            elec_idx = ROI_chan_idx(elec);
            
            % Load data file
            data_filename = fullfile(init.path, subjName, 'RAW', 'Comportement', sess_name{elec_idx});
            load(data_filename, 'data'); % Charger la matrice data de la session
            
            data(:,1) = subj_idx; % Ajouter l'indice du sujet colonne 1
            data(:,14) = parcl; % Ajouter l'indice de la parcelle colonne 14
            data(:,15) = elec; % Ajouter l'indice de la session colonne 15 (un num�ro par session, m�me si m�me �lectrode)
            data(:,16) = elec_intensity(elec_idx); % Ajouter l'intensit� de stimulation colonne 16
            data(:,17) = find(strcmp(format_elec{elec_idx}, unique(format_elec))); % Ajouter l'indice de l'�lectrode colonne 17 (m�me num�ro si m�me �lectrode avec intensit�s diff�rentes)
            init.group_data = [init.group_data ; data]; % Matrice contenant les donn�es de toutes les sessions et de tous les sujets
            
            %% Save some interesting data
            
            init.elec_id{subj_idx, elec, parcl} = sprintf('%s_%s_%dmA', subjName, format_elec{elec_idx}, elec_intensity(elec_idx));
            init.elec_MNI{subj_idx, elec, parcl} = csv.MNI(elec_idx,:);
            init.elec_freesurfer{subj_idx, elec, parcl} = csv.freelabels(elec_idx,:);
            
            %% Temp : Extract all csv columns
            
            %             opts = detectImportOptions(csvfile);
            %             csv_data = readtable(csvfile, opts); % Open .CSV file in table
            %
            %             % Find electrodes names in .CSV
            %             col = strcmp(csv_data.Properties.VariableNames,'contact');
            %             contact = table2cell(csv_data(:,col));
            %             contact = regexprep(contact,'-',''); % Remove hyphen ('-') if any
            %
            %             chanidx = find(strcmp(sort(format_elec{elec_idx}), cellfun(@sort,contact,'UniformOutput',false))); % Compare sorted string
            %             init.elec_csv{subj_idx, elec, parcl} = table2cell(csv_data(chanidx,:));
            
            %% CHOICE MODEL
            
            for condi = 1:length(init.conditions) % Loop over conditions (stim/no stim)
                if isempty(fieldnames(choice_data.(init.conditions{condi}))) || ~isfield(choice_data.(init.conditions{condi}).inversionResult.model(1), init.elec_id{subj_idx, elec, parcl}) % If subject's name is not a field of 'choice_data' : compute TML for this subject
                    
                    % Load mean and std of the subject
                    param_filename = fullfile(init.bdd_path, 'Cognitive_task', 'Patients', subjName, 'RAW', 'Comportement', sprintf('%s_estim_param.mat', subjName));
                    try
                        load(param_filename, 'moy_estim', 'std_estim');
                    catch
                        load(fullfile(behav_folder, sprintf('%s_estim_param.mat', subjName)), 'moy_estim', 'std_estim');
                    end
                    
                    S = [];
                    S.subjname = init.elec_id{subj_idx, elec, parcl};
                    S.data = data(data(:,2) == 2+(condi-1)*198, :); % 1 = no stim (2) ; 2 = stim (200)
                    S.moy_estim = moy_estim;
                    S.std_estim = std_estim;
                    S.trial_idx = find(data(:,2) == 2+(condi-1)*198);
                    S.trial_nb = size(data, 1);
                    
                    if init.use_cog_choice_model == 1 % Use parameters from the cognition dataset and let fluctuate only kg and kl
                        
                        % Retrieve choice model weights for the subject
                        fld = fieldnames(cog_choice_data.agent.inversionResult.freeParameter.model);
                        s_idx = ismember(fieldnames(cog_choice_data.agent.inversionResult.model), subjName);
                        
                        if ~isempty(find(s_idx, 1)) || ismember(subjName, {'GRE_2018_BOIl'}) % If the patient performed the cognition task
                            
                            for f = 1:length(fld)
                                S.priors.(fld{f}).mean = cog_choice_data.agent.inversionResult.freeParameter.model.(fld{f})(s_idx);
                                S.priors.(fld{f}).var = 0; % Variance = 0 = param�tre fixe
                            end
                            
                            if ismember(subjName, {'GRE_2018_BOIl'}) % Exception for this patient who is not part of the ConfiDecid dataset
                                S.priors.FF.mean = -81.5820;
                                S.priors.sigma.mean = 0.2567;
                                S.priors.kG.mean = 25.3010;
                                S.priors.kL.mean = 11.1398;
                                S.priors.kT.mean = -0.9202;
                                S.priors.C.mean = 1.6241;
                                [S.priors.gamma.mean, S.priors.delta.mean] = deal(1);
                                [S.priors.kE.mean, S.priors.e1.mean, S.priors.e2.mean, S.priors.kMP.mean,...
                                    S.priors.kMG.mean, S.priors.kML.mean, S.priors.kMC.mean] = deal(0);
                            end
                            
                            % Parameters that can be adjusted
                            S.priors.kG.var = 125;
                            S.priors.kL.var = 125;
                            % S.priors.C.var = 5;
                            
                        end
                    end
                    
                    choice_data.(init.conditions{condi}) = choiceModel_ConfiDecid(S, choice_data.(init.conditions{condi})); % Get choice model for the subject
                    
                    choice_data.nSubject = length(fieldnames(choice_data.(init.conditions{condi}).inversionResult.model));
                    
                    init.tmp(subj_idx).moy_estim = moy_estim;
                    init.tmp(subj_idx).std_estim = std_estim;
                    
                    % Save choice data
                    save(choice_data_fullname, 'choice_data');
                    close all;
                end
            end % End of the loop over conditions
            
        end % Fin de la boucle � travers les �lectrodes
    end % End of the loop over subjects
    
    % Cr�ation des dossiers parcelles dans le dossier 'Comportement' s'ils n'existent pas
    init.pathtofig_parcl{parcl} = fullfile(init.pathtofig_sub, init.parcels{parcl});
    if ~exist(init.pathtofig_parcl{parcl},'dir')
        mkdir(init.pathtofig_parcl{parcl});
    end
    
end % Fin de la boucle � travers les parcelles

%% REMOVE BAD ELECTRODES

% Load sEEG responses
if init.rmv_badchan == 2
    load(fullfile(init.pathtofig, 'sEEG_resp.mat'), 'sEEG_resp'); % Load 'sEEG_resp' table
end

if init.rmv_badchan ~= 0
    
    for parcl = 1:length(init.parcels) % Boucle � travers les parcelles
        
        idx = 0;
        parcl_data = init.group_data(init.group_data(:,14) == parcl,:); % All data for one parcelle
        
        for subj = 1:length(init.subj_name) % Loop over subjects
            
            subj_data = parcl_data(parcl_data(:,1) == subj, :);
            
            for sess = 1:length(unique(subj_data(:,15))) % Loop over sessions of the subject
                
                idx = idx +1;
                sess_data = subj_data(subj_data(:,15) == sess, :);
                
                for stim = 1:length(init.conditions) % Loop over stim condition
                    
                    stim_state = 2+(stim-1)*198; % 1 = no stim (2) ; 2 = stim (200)
                    stim_data = sess_data(sess_data(:,2) == stim_state, :);
                    
                    p_accept(stim) = mean(stim_data(:,7));
                    
                    if stim_state == 2 % No stim
                        
                        task_dim_column = [4 5 6]; % Gains ; Losses ; Diff
                        prdct = stim_data(:, task_dim_column); % Prospects
                        resp = stim_data(:, 7); % Choices
                        b = glmfit(prdct, resp, 'binomial');
                        
                        beta_corr = b(2:end); % Slope
                    end
                    
                end % End of the loop over the stim condition
                
                % Delete electrode if :
                if init.rmv_badchan == 1
                    
                    if all(p_accept == 0) || ... % All reject
                            all(p_accept == 1) % || ... %% All accept
                            % (beta_corr(1) < 0 && beta_corr(2) > 0) % If the slopes of the correlation with gains AND losses are reversed
                        
                        % (any(p_accept == (length(resp)-1)/length(resp)) && any(p_accept == 1)) || ... % Only 1 reject in one condition
                        % (any(p_accept == 1/length(resp)) && any(p_accept == 0)) || ... % Only 1 accept in one condition
                        % all(p_accept == (length(resp)-1)/length(resp)) || ... % Only 1 reject in both conditions
                        % all(p_accept == 1/length(resp)) || ... % Only 1 accept in both conditions
                        
                        init.group_data(init.group_data(:,14) == parcl & init.group_data(:,1) == subj & init.group_data(:,15) == sess, :) = [];
                        init.elec_id{subj, sess, parcl} = [];
                    end
                    
                elseif init.rmv_badchan == 2
                    
                    elec_id = ismember(sEEG_resp.names, init.elec_id{subj, sess, parcl}(1:end-4));
                    
                    if sEEG_resp.evocked_resp2fb(elec_id) == 0 || ... % No response to feedback
                            all(p_accept == 0) || ... % All reject
                            all(p_accept == 1) %% All accept
                        
                        init.group_data(init.group_data(:,14) == parcl & init.group_data(:,1) == subj & init.group_data(:,15) == sess, :) = [];
                        init.elec_id{subj, sess, parcl} = [];
                        
                    end
                end
                
            end % End of the loop over sessions
        end % End of the loop over subjects
    end % End of the loop over parcels
end

%% Effet de la stim sur une variable d'int�r�t

var_of_int = 'p(accept)'; % 'p(accept)' | 'RT' | 'Confidence'

if ismember(var_of_int, {'p(accept)'})
    
    opt.var_column = 7; % Choix (1 = accept ; 0 = reject)
    opt.ylabel = 'p(accept)';
    opt.ylim = [0 1];
    opt.YTick = (0:.2:1);
    
elseif ismember(var_of_int, {'RT'})
    
    opt.var_column = 8; % Choice RT (in sec)
    opt.ylabel = 'RT';
    opt.ylim = [-0.7 0.7];
    opt.YTick = (-0.7:0.7:0.7);
    
elseif ismember(var_of_int, {'Confidence'})
    
    opt.var_column = 9; % Confidence (1 to 100)
    opt.ylabel = 'Confidence';
    opt.ylim = [-0.5 0.5];
    opt.YTick = (-0.5:0.5);
end

for parcl = 1:length(init.parcels)
    if ~exist(fullfile(init.pathtofig_parcl{parcl}, opt.ylabel), 'dir')
        mkdir(fullfile(init.pathtofig_parcl{parcl}, opt.ylabel));
    end
end

%-------------------------------------------------------------------------%

% ANALYSE 1 : Effet de la stim sur la probabilit� d'accepter
stim_effect_on_var_bar(init, opt)
% stim_effect_on_var_acc2rt(init, opt) % S�parer les essais stim en fonction du temps de r�action (< ou > � 4 sec)

%-------------------------------------------------------------------------%

% ANALYSE 2 : Effet de la stim sur la probabilit� d'accepter
% en fonction du Dv (gains - losses)

opt.plot_sess_level = 1;
opt.param = 'Dv';
opt.bins = [-40, 0, 40];

% stim_effect_on_var_acc2mult_param(init, opt)

%-------------------------------------------------------------------------%

% ANALYSE 3 : Effet de la stim sur la probabilit� d'accepter
% en fonction de l'utilit�

% opt.plot_sess_level = 1;
% opt.param = 'Utility';
% opt.prct = 0:25:100;
% 
% stim_effect_on_var_acc2mult_param(init, opt, choice_data)

%-------------------------------------------------------------------------%

% ANALYSE 4 : Effet de la stim sur la probabilit� d'accepter
% en fonction des gains, pertes et difficult�

opt.plot_sess_level = 1;
opt.param = 'Gains+Losses'; % 'Gains+Losses+Diff' | 'Gains+Losses'
opt.bins = 10:10:50;

% stim_effect_on_var_acc2mult_param(init, opt)

%-------------------------------------------------------------------------%

% ANALYSE 5 : Effet de la stim sur la probabilit� d'accepter
% en fonction de la valeur absolue du Dv (gains - losses)

% opt.plot_sess_level = 1;
% opt.param = 'abs(Dv)';
% opt.bins = [0, 5, 10, 20, 40];
% 
% stim_effect_on_var_acc2mult_param(init, opt)

%-------------------------------------------------------------------------%

% ANALYSE 6 : Effet de la stim sur les param�tres du mod�le de choix
% stim_effect_on_choice_model_free_param(init, choice_data)

%-------------------------------------------------------------------------%

%% FIN
return;

%% FUNCTIONS

function stim_effect_on_var_bar(init, opt)

for parcl = 1:length(init.parcels) % Boucle � travers les parcelles
    
    clearvars -except init opt parcl
    parcl_data = init.group_data(init.group_data(:,14) == parcl,:); % All data for one parcelle
    
    % Figure for all sessions
    fig = figure;
    hold on;
    
    nCol = 6;
    nLine = ceil((length(find(~cellfun(@isempty, init.elec_id(:,:,parcl)))))/nCol);
    idx = [0 0 0];
    
    for subj = 1:length(init.subj_name) % Loop over subjects
        
        subj_data = parcl_data(parcl_data(:,1) == subj, :);
        u_elec = unique(subj_data(:,17));
        
        for elec = 1:length(u_elec) % Loop over electrodes of the subject
            
            elec_data = subj_data(subj_data(:,17) == u_elec(elec), :);
            
            intensity_data(1) = {elec_data(elec_data(:,16) == max(elec_data(:,16)), :)}; % Max intensity
            intensity_data(2) = {elec_data(elec_data(:,16) ~= max(elec_data(:,16)), :)}; % Sub max intensity
            intensity_data(3) = {elec_data}; % All intensity
            
            for intens = 1:size(intensity_data, 2)
                
                intens_data = intensity_data{intens};
                u_sess = unique(intens_data(:,15));
                
                for sess = 1:length(u_sess) % Loop over sessions of the subject
                    
                    idx(intens) = idx(intens) + 1;
                    sess_data = intens_data(intens_data(:,15) == u_sess(sess), :);
                    
                    sorted_sess_data = sortrows(sess_data, [4,5]); % Sort according to gain and loss columns
                    voi = sorted_sess_data(:,opt.var_column);
                    
                    if VBA_isBinary(voi(~isnan(voi))) == true
                        z_voi = voi;
                    else
                        z_voi = zscore(voi);
                    end
                    
                    no_stim_data = z_voi(sorted_sess_data(:,2) == 2, :);
                    stim_data = z_voi(sorted_sess_data(:,2) == 200, :);
                    
                    all_stim = [no_stim_data, stim_data] ; % [No stim (2), Stim (200)]
                    
                    mean_var{intens}(idx(intens), :) = mean(all_stim);
                    elec_id{intens, idx(intens)} = init.elec_id{subj, u_sess(sess), parcl};
                    elec_mni{intens}(idx(intens), :) = init.elec_MNI{subj, u_sess(sess), parcl};
                    elec_free{intens}(idx(intens), :) = init.elec_freesurfer{subj, u_sess(sess), parcl};
                    % elec_csv{intens}(idx(intens), :) = {init.elec_csv{subj, u_sess(sess), parcl}};
                    
                    % Plot the proba for each session
                    if intens == 3 % Only for all intensity
                        
                        subplot(nLine, nCol, idx(intens)); hold on;
                        
                        for stim = 1:length(init.conditions)
                            b(stim) = bar(stim, mean(all_stim(:,stim)));
                            errorbar(stim, mean(all_stim(:,stim)), std(all_stim(:,stim))/sqrt(length(all_stim(:,stim))), 'Color', 'black');
                        end
                        
                        % Stats
                        h = ttest(no_stim_data - stim_data);
                        
                        if h == 1 % Significatif
                            line([1 2], [max([b.YData]) + 0.2, max([b.YData]) + 0.2], 'Color', 'black')
                            scatter(1.5, max([b.YData]) + 0.25, '*k', 'sizeData', 40, 'linewidth', 0.7);
                        end
                        
                        % Title & axes
                        title(init.elec_id{subj, u_sess(sess), parcl}, 'Interpreter', 'none')
                        ylabel(opt.ylabel);
                        ylim(opt.ylim);
                        
                    end % End of 'if intens = 3'
                    
                    %% GLM
                    
                    prdct = sess_data(:,2); % Stim condition
                    prdct(prdct == 2) = -1;
                    prdct(prdct == 200) = 1;
                    resp = sess_data(:, opt.var_column);
                    
                    if VBA_isBinary(resp(~isnan(resp))) == true
                        distr = 'binomial';
                        blink = 'logit';
                    else
                        distr = 'normal';
                        blink = 'identity';
                        resp = zscore(resp);
                    end
                    
                    betas{intens}(idx(intens),:) = glmfit(prdct, resp, distr);
                    
                    % Create the database for glme
                    if intens == 1 % Only for max intensity
                        glme_data.predictors{idx(intens)} = prdct;
                        glme_data.responses{idx(intens)} = resp;
                        glme_data.electrodes{idx(intens)} = repmat(init.elec_id(subj, u_sess(sess), parcl), length(resp), 1);
                        glme_data.patients{idx(intens)} = repmat(init.subj_name(subj), length(resp), 1);
                    end
                    
                end % End of the loop over sessions
            end % End of the loop over intensities
        end % End of the loop over electrodes
    end % End of the loop over subjects
    
    legend(b, init.conditions, 'Location', 'best', 'Interpreter', 'none')
    legend('AutoUpdate', 'off')
    
    % Save
    fig_name = fullfile(init.pathtofig_parcl{parcl}, opt.ylabel, 'sess_level_all_intens.tiff');
    fig.PaperPosition = [0, 0, 50 nLine*10]; % [left bottom width height]
    print(fig, fig_name, '-dtiff', '-r200');
    % savefig(fig, fig_name(1:end-5),'compact'); % Matlab format
    
    close(fig);
    
    init.data_parcel{parcl} = mean_var{intens};
    
    %% Figure for all electrodes of a ROI
    
    fig = figure;
    hold on
    
    for intens = 1:size(mean_var, 2)
        
        subplot(1, size(mean_var, 2), intens); hold on;
        
        bar(nanmean(mean_var{intens}))
        errorbar(nanmean(mean_var{intens}), nanstd(mean_var{intens})./sqrt(sum(~isnan(mean_var{intens}))), 'Color', 'black', 'LineStyle', 'none');
        
        % Stats
        [~,p] = ttest(mean_var{intens}(:,1), mean_var{intens}(:,2));
        TextLocation(sprintf('n = %g\np = %.3g', size(mean_var{intens}, 1), p), 'Location', 'north')
        
        % Axes
        xticks(1:length(init.conditions))
        xticklabels(init.conditions)
        ylabel(opt.ylabel);
        ylim(opt.ylim);
        
        switch intens
            case 1
                inty = 'max';
            case 2
                inty = 'sub';
            case 3
                inty = 'all';
        end
        title(sprintf('%s - %s intensity', init.parcels{parcl}, inty), 'Interpreter', 'none')
        
    end % End of the loop over intensity
    
    % Save
    fig.PaperPosition = [0, 0, 40 10]; % [left bottom width height]
    fig_name = fullfile(init.pathtofig_parcl{parcl}, opt.ylabel, 'ROI_level.tiff');
    print(fig, fig_name, '-dtiff', '-r200');
    % savefig(fig, fig_name(1:end-5),'compact'); % Matlab format
    
    close(fig);
    
    %% 3D plot (to see if there is a gradient or clusters)
    % Only for max intensity
    
    fig = figure;
    hold on
    
    intens = 1; % Max intensity
    diff_mean = diff(mean_var{intens},1,2); % stim - no stim
    
    % OPTION 1 :
    % s1 = diff_mean > -1 & diff_mean < 0;
    % scatter3(elec_mni{intens}(s1,1), elec_mni{intens}(s1,2), elec_mni{intens}(s1,3), [], [255, 162, 64]/255, 'filled')
    
    % s2 = diff_mean == 0;
    % scatter3(elec_mni{intens}(s2,1), elec_mni{intens}(s2,2), elec_mni{intens}(s2,3), [], [53, 211, 106]/255, 'filled')
    
    % s3 = diff_mean > 0 & diff_mean < 1;
    % scatter3(elec_mni{intens}(s3,1), elec_mni{intens}(s3,2), elec_mni{intens}(s3,3), [], [54, 159, 194]/255, 'filled')
    
    % legend({'Negative', 'Zero', 'Positive'})
    
    % OPTION 2 :
    scatter3(elec_mni{intens}(:,1), elec_mni{intens}(:,2), elec_mni{intens}(:,3), [], diff_mean, 'filled')
    load(fullfile(init.pathtofig, 'colorbar_3D_fig.mat'), 'cbar', 'cmap')
    colorbar;
    caxis(cbar.Limits);
    colormap(cmap)
    
    xlabel('X')
    ylabel('Y')
    zlabel('Z')
    
    if ismember(init.parcels{parcl}, {'PFCvm'})
        view(0,0);
    elseif ismember(init.parcels{parcl}, {'aINS', 'aINS_dors', 'aINS_vent'})
        view(90,0);
    end
    
    title({sprintf('{%s%s}', '\bf', init.parcels{parcl}) ; sprintf('%s_{stim - no stim}', opt.ylabel)}, 'FontWeight', 'Normal');
    
    % Save
    fig_name = fullfile(init.pathtofig_parcl{parcl}, opt.ylabel, '3D_p(accept)_max_intens.tif');
    savefig(fig, fig_name(1:end-4),'compact'); % Matlab format
    
    close(fig);
    
    %% GLM fit according to electrode position
    
    fig = figure;
    hold on
    
    subl = 1;
    subc = 4;
    
    intens = 1; % Max intensity
    diff_mean = diff(mean_var{intens},1,2); % stim - no stim
    
    for dim = 1:3
        
        [b,~,stats] = glmfit(elec_mni{intens}(:,dim), diff_mean);
        yfit = glmval(b, elec_mni{intens}(:,dim), 'identity');
        
        subplot(subl,subc,dim); hold on
        scatter(elec_mni{intens}(:,dim), diff_mean, 'filled')
        plot(elec_mni{intens}(:,dim), yfit, 'LineWidth', 2)
        
        TextLocation(sprintf('p = %.2g', stats.p(2)), 'best')
        
        if dim == 1
            xlabel('X')
        elseif dim == 2
            xlabel('Y')
        elseif dim == 3
            xlabel('Z')
        end
        
        ylabel(sprintf('%s_{stim - no stim}', opt.ylabel))
        
    end
    
    % Subplot Y * Z
    subplot(subl,subc,4); hold on
    
    [b,~,stats] = glmfit(elec_mni{intens}(:,2) .* elec_mni{intens}(:,3), diff_mean);
    yfit = glmval(b, elec_mni{intens}(:,2) .* elec_mni{intens}(:,3), 'identity');
    
    scatter(elec_mni{intens}(:,2) .* elec_mni{intens}(:,3), diff_mean, 'filled')
    plot(elec_mni{intens}(:,2) .* elec_mni{intens}(:,3), yfit, 'LineWidth', 2)
    
    TextLocation(sprintf('p = %.2g', stats.p(2)), 'best')
    
    xlabel('Y * Z')
    ylabel(sprintf('%s_{stim - no stim}', opt.ylabel))
    
    % Save
    fig.PaperPosition = [0, 0, 50 10]; % [left bottom width height]
    fig_name = fullfile(init.pathtofig_parcl{parcl}, opt.ylabel, 'p(accept)_acc2dim.tif');
    print(fig, fig_name, '-dtiff', '-r200');
    
    close(fig);
    
    %% GLM stats
    
    % GLM
    [~,p] = ttest(betas{1}(:,end));
    
    % GLME ---------------------------------------------------------------%
    
    if ismember(opt.ylabel, 'p(accept)')
        resp_name = 'choice';
    else
        resp_name = opt.ylabel;
    end
    prdct_name = 'stim_condi';
    
    % Get responses and predictors
    responses = cat(1, glme_data.responses{:});
    predictors = cat(1, glme_data.predictors{:});
    
    % Convert ELECTRODES and PATIENTS to categorical
    ELECTRODES = categorical(cat(1, glme_data.electrodes{:}));
    PATIENTS = categorical(cat(1, glme_data.patients{:}));
    
    % Convert data to table
    tbl_data = splitvars(table(responses, predictors, ELECTRODES, PATIENTS));
    tbl_data.Properties.VariableNames = [resp_name, prdct_name, {'ELECTRODES', 'PATIENTS'}];
    
    % Multilevel (Hierarchical) Model : ELECTRODES nested in PATIENTS
    glme = fitglme(tbl_data, sprintf('%1$s ~ 1 + %2$s + (1 + %2$s|PATIENTS) + (1 + %2$s|ELECTRODES:PATIENTS)', resp_name, prdct_name),...
        'Distribution', distr, 'FitMethod', 'Laplace');
    
    % Result
    glme
    double(glme.Coefficients(:,2)); % Estimate
    double(glme.Coefficients(:,6)); % p-value
    
end % End of the loop over parcels
end

function stim_effect_on_var_acc2rt(init, opt)

for parcl = 1:length(init.parcels) % Boucle � travers les parcelles
    
    clearvars -except init opt parcl
    parcl_data = init.group_data(init.group_data(:,14) == parcl,:); % All data for one parcelle
    idx = 0;
    
    for subj = 1:length(init.subj_name) % Loop over subjects
        
        subj_data = parcl_data(parcl_data(:,1) == subj, :);
        u_elec = unique(subj_data(:,17));
        
        for elec = 1:length(u_elec) % Loop over electrodes of the subject
            
            elec_data = subj_data(subj_data(:,17) == u_elec(elec), :);
            
            intens_data = elec_data(elec_data(:,16) == max(elec_data(:,16)), :); % Max intensity
            u_sess = unique(intens_data(:,15));
            
            for sess = 1:length(u_sess) % Loop over sessions of the subject
                
                idx = idx + 1;
                sess_data = intens_data(intens_data(:,15) == u_sess(sess), :);
                
                sorted_sess_data = sortrows(sess_data, [4,5]); % Sort according to gain and loss columns
                voi = sorted_sess_data(:,opt.var_column);
                choice_rt = sorted_sess_data(:,8); % Choice reaction time
                
                if VBA_isBinary(voi(~isnan(voi))) == true
                    z_voi = voi;
                else
                    z_voi = zscore(voi);
                end
                
                no_stim_data = z_voi(sorted_sess_data(:,2) == 2, :);
                stim_data = z_voi(sorted_sess_data(:,2) == 200, :);
                
                all_stim = [no_stim_data, stim_data] ; % [No stim (2), Stim (200)]
                
                % Find choice RT > X sec
                rt_stim = choice_rt(sorted_sess_data(:,2) == 200, :); % Stim (200)
                stim_end = 4; % End of the stim after choice onset (in sec)
                row_inf = rt_stim <= stim_end; % RT <= X sec
                
                all_stim_rt_inf = all_stim(row_inf,:); % Stim data for stim trials with reaction times < X sec
                all_stim_rt_sup = all_stim(~row_inf,:); % Stim data for stim trials with reaction times > X sec
                
                mean_var{1}(idx, :) = mean(all_stim_rt_inf, 1);
                mean_var{2}(idx, :) = mean(all_stim_rt_sup, 1);
                
                elec_id{idx} = init.elec_id{subj, u_sess(sess), parcl};
                elec_mni(idx, :) = init.elec_MNI{subj, u_sess(sess), parcl};
                elec_free(idx, :) = init.elec_freesurfer{subj, u_sess(sess), parcl};
                
            end % End of the loop over sessions
        end % End of the loop over electrodes
    end % End of the loop over subjects
    
    %% Figure for all electrodes of a ROI
    
    fig = figure;
    hold on
    
    for rt_cond = 1:size(mean_var, 2)
        
        subplot(1, size(mean_var, 2)+1, rt_cond); hold on;
        
        bar(nanmean(mean_var{rt_cond}))
        errorbar(nanmean(mean_var{rt_cond}), nanstd(mean_var{rt_cond})./sqrt(sum(~isnan(mean_var{rt_cond}))), 'Color', 'black', 'LineStyle', 'none');
        
        % Stats
        [~,p] = ttest(mean_var{rt_cond}(:,1), mean_var{rt_cond}(:,2));
        TextLocation(sprintf('n = %g\np = %.3g', size(mean_var{rt_cond}, 1), p), 'Location', 'north')
        
        % Axes
        xticks(1:length(init.conditions))
        xticklabels(init.conditions)
        ylabel(opt.ylabel);
        ylim(opt.ylim);
        
        switch rt_cond
            case 1
                cond_name = sprintf('Stim trials with RT <= %d sec', stim_end);
            case 2
                cond_name = sprintf('Stim trials with RT > %d sec', stim_end);
        end
        title(sprintf('%s - %s', init.parcels{parcl}, cond_name), 'Interpreter', 'none')
        
    end % End of the loop over intensity
    
    % Figure to compare stim trials
    subplot(1, size(mean_var, 2)+1, rt_cond+1); hold on;
    
    stim_cond = [mean_var{1}(:,2), mean_var{2}(:,2)];
    bar(nanmean(stim_cond))
    errorbar(nanmean(stim_cond), nanstd(stim_cond)./sqrt(sum(~isnan(stim_cond))), 'Color', 'black', 'LineStyle', 'none');
    
    % Stats
    [~,p] = ttest(stim_cond(:,1), stim_cond(:,2));
    TextLocation(sprintf('n = %g\np = %.3g', size(mean_var{rt_cond}, 1), p), 'Location', 'north')
    
    % Axes
    xticks(1:size(stim_cond,2))
    xticklabels({sprintf('<= %d sec', stim_end),sprintf('> %d sec', stim_end)})
    ylabel(opt.ylabel);
    ylim(opt.ylim);
    
    title(sprintf('%s - Stim trials', init.parcels{parcl}), 'Interpreter', 'none')
    
    % Save
    fig.PaperPosition = [0, 0, (rt_cond+1)*10+10 10]; % [left bottom width height]
    fig_name = fullfile(init.pathtofig_parcl{parcl}, opt.ylabel, 'ROI_level_acc2rt_max_intens.tiff');
    print(fig, fig_name, '-dtiff', '-r200');
    % savefig(fig, fig_name(1:end-5),'compact'); % Matlab format
    
    close(fig);
    
    %% 3D plot (to see if there is a gradient or clusters)
    % Only for max intensity
    
    fig = figure;
    hold on
    
    for rt_cond = 1:size(mean_var, 2)
        
        subplot(1, size(mean_var, 2), rt_cond); hold on;
        
        diff_mean = diff(mean_var{rt_cond},1,2); % stim - no stim
        
        % OPTION 2 :
        scatter3(elec_mni(:,1), elec_mni(:,2), elec_mni(:,3), [], diff_mean, 'filled')
        load(fullfile(init.pathtofig, 'colorbar_3D_fig.mat'), 'cbar', 'cmap')
        colorbar;
        caxis(cbar.Limits);
        colormap(cmap)
        
        xlabel('X')
        ylabel('Y')
        zlabel('Z')
        
        if ismember(init.parcels{parcl}, {'PFCvm'})
            view(0,0);
        elseif ismember(init.parcels{parcl}, {'aINS', 'aINS_dors', 'aINS_vent'})
            view(90,0);
        end
        
        switch rt_cond
            case 1
                cond_name = sprintf('Stim trials with RT <= %d sec', stim_end);
            case 2
                cond_name = sprintf('Stim trials with RT > %d sec', stim_end);
        end
        title({sprintf('{%s%s}', '\bf', init.parcels{parcl}) ; sprintf('%s_{stim - no stim}', opt.ylabel) ; cond_name}, 'FontWeight', 'Normal');
        
    end
    
    % Save
    fig_name = fullfile(init.pathtofig_parcl{parcl}, opt.ylabel, '3D_p(accept)_acc2rt_max_intens.tif');
    savefig(fig, fig_name(1:end-4),'compact'); % Matlab format
    
    close(fig);
    
end % End of the loop over parcels

end

function stim_effect_on_var_acc2mult_param(init, opt, choice_data)

for parcl = 1:length(init.parcels) % Boucle � travers les parcelles
    
    clearvars -except init opt choice_data parcl
    
    idx = [0 0 0];
    parcl_data = init.group_data(init.group_data(:,14) == parcl,:); % All data for one parcelle
    
    prdct_name = strsplit(opt.param, '+'); % 'param must be separated by '+' characters
    
    if opt.plot_sess_level == 1
        
        % Figure for all sessions
        nfig = 3;
        for f = 1:nfig
            for pr = 1:length(prdct_name)
                fig(pr, f) = figure; ax(pr, f) = axes; hold(ax(pr, f), 'on');
            end
        end
        
        nCol = 6;
        nLine = ceil((length(find(~cellfun(@isempty, init.elec_id(:,:,parcl)))))/nCol);
        
    end
    
    for subj = 1:length(init.subj_name) % Loop over subjects
        
        subj_data = parcl_data(parcl_data(:,1) == subj, :);
        u_elec = unique(subj_data(:,17));
        
        for elec = 1:length(u_elec) % Loop over electrodes of the subject
            
            elec_data = subj_data(subj_data(:,17) == u_elec(elec), :);
            
            intensity_data(1) = {elec_data(elec_data(:,16) == max(elec_data(:,16)), :)}; % Max intensity
            intensity_data(2) = {elec_data(elec_data(:,16) ~= max(elec_data(:,16)), :)}; % Sub max intensity
            intensity_data(3) = {elec_data}; % All intensity
            
            for intens = 1:size(intensity_data, 2)
                
                intens_data = intensity_data{intens};
                u_sess = unique(intens_data(:,15));
                
                for sess = 1:length(u_sess)
                    
                    idx(intens) = idx(intens) + 1;
                    sess_data = intens_data(intens_data(:,15) == u_sess(sess), :);
                    
                    elec_id{intens, idx(intens)} = init.elec_id{subj, u_sess(sess), parcl};
                    elec_mni{intens}(idx(intens), :) = init.elec_MNI{subj, u_sess(sess), parcl};
                    elec_free{intens}(idx(intens), :) = init.elec_freesurfer{subj, u_sess(sess), parcl};
                    
                    % Z-score before separating the conditions
                    if VBA_isBinary(sess_data(~isnan(sess_data(:,opt.var_column)),opt.var_column)) == false % If non binary
                        sess_data(:,opt.var_column) = zscore(sess_data(:,opt.var_column));
                    end
                    
                    for stim = 1:length(init.conditions) % Loop over stim condition
                        
                        stim_state = 2+(stim-1)*198; % 1 = no stim (2) ; 2 = stim (200)
                        stim_data = sess_data(sess_data(:,2) == stim_state, :);
                        
                        if ismember(opt.param, {'Dv'})
                            param = stim_data(:,4) - stim_data(:,5);
                        elseif ismember(opt.param, {'Utility'})
                            param = choice_data.(init.conditions{stim}).inversionResult.model.(init.elec_id{subj, u_sess(sess), parcl}).posterior.muX(1,:)';
                        elseif ismember(opt.param, {'Gains+Losses+Diff'})
                            param = stim_data(:,4:6);
                        elseif ismember(opt.param, {'Gains+Losses'})
                            param = stim_data(:,4:5);
                        elseif ismember(opt.param, {'abs(Dv)'})
                            param = abs(stim_data(:,4) - stim_data(:,5));
                        end
                        
                        %% GLM
                        
                        prdct = param;
                        resp = stim_data(:, opt.var_column);
                        
                        if VBA_isBinary(resp(~isnan(resp))) == true
                            distr = 'binomial';
                            blink = 'logit';
                        else
                            distr = 'normal';
                            blink = 'identity';
                        end
                        
                        % RELATIVE value
                        b = glmfit(prdct, resp, distr);
                        
                        for pr = 1:length(prdct_name) % Loop over predictors
                            
                            betas{intens, pr}(idx(intens), stim) = b(pr+1);
                            
                            if opt.plot_sess_level == 1
                                
                                % Set current figure
                                set(0,'CurrentFigure', fig(pr, intens));
                                sax(pr, intens) = subplot(nLine, nCol, idx(intens)); hold(sax(pr, intens), 'on');
                                
                                % Plot
                                yfit = glmval(b([1,pr+1]), prdct(:,pr), blink);
                                sorted = sortrows([prdct(:,pr), yfit]);
                                plot(sax(pr, intens), sorted(:,1), sorted(:,2) ,'-', 'LineWidth', 2);
                                
                                % Title & axes
                                title(init.elec_id{subj, u_sess(sess), parcl}, 'Interpreter', 'none')
                                
                                if ismember(opt.param, {'Dv', 'Gains+Losses+Diff', 'Gains+Losses', 'abs(Dv)'})
                                    xticks(opt.bins)
                                elseif ismember(opt.param, {'Utility'})
                                    xticks(min(param):max(param))
                                end
                                
                                ylabel(opt.ylabel);
                                ylim(opt.ylim);
                                
                            end
                            
                            % --------------------------------------------%
                            % Compute indifference point
                            
                            if ismember(distr, {'binomial'})
                                
                                % Binomial equation :
                                % logit(Y) = log(Y/(1-Y)) = b0 + b1 * X
                                % Thus : X = (log(Y/(1-Y)) - b0) / b1
                                proba = 0.5;
                                indif_pt{intens, pr}(idx(intens), stim) = (log(proba/(1-proba)) - b(1)) / b(pr+1);
                                
                            elseif ismember(distr, {'normal'})
                                
                                % Calculate indifference point :
                                % Normal equation : Y = b0 + b1 * X
                                % Thus : X = (Y - b0) / b1
                                proba = 0;
                                indif_pt{intens, pr}(idx(intens), stim) = (proba - mdl.Coefficients.Estimate(1)) / mdl.Coefficients.Estimate(2);
                                
                            end
                            %---------------------------------------------%
                            
                        end % End of the loop over predictors
                        
                        %% Bins
                        
                        % RELATIVE value
                        if ismember(opt.param, {'Utility'})
                            opt.bins = prctile(param, opt.prct);
                        end
                        
                        dv_lim_rel = opt.bins;
                        
                        for pr = 1:length(prdct_name)
                            for bin = 1:length(dv_lim_rel)-1
                                if bin == 1
                                    id = param(:,pr) >= dv_lim_rel(bin) & param(:,pr) <= dv_lim_rel(bin+1);
                                else
                                    id = param(:,pr) > dv_lim_rel(bin) & param(:,pr) <= dv_lim_rel(bin+1);
                                end
                                mean_var{intens, pr}(stim, bin, idx(intens)) = mean(stim_data(id, opt.var_column));
                                sem_var{intens, pr}(stim, bin, idx(intens)) = std(stim_data(id, opt.var_column))/sqrt(length(find(id)));
                            end
                        end
                        
                    end % End of the loop over the stim condition
                end % End of the loop over sessions
            end % End of the loop over intensity condition
        end % End of the loop over electrodes
    end % End of the loop over subjects
    
    % Save
    for intens = 1:size(intensity_data, 2)
        for pr = 1:length(prdct_name)
            legend(sax(pr, intens), init.conditions, 'Location', 'best', 'Interpreter', 'none')
            
            switch intens
                case 1
                    name = sprintf('sess_level_%s_max_intens.tiff', prdct_name{pr});
                case 2
                    name = sprintf('sess_level_%s_sub_intens.tiff', prdct_name{pr});
                case 3
                    name = sprintf('sess_level_%s_all_intens.tiff', prdct_name{pr});
            end
            
            fig_name = fullfile(init.pathtofig_parcl{parcl}, opt.ylabel, name);
            fig(pr, intens).PaperPosition = [0, 0, 50 nLine*10]; % [left bottom width height]
            print(fig(pr, intens), fig_name, '-dtiff', '-r200');
            % savefig(fig, fig_name(1:end-5),'compact'); % Matlab format
            
            close(fig(pr, intens));
        end
    end
    
    % Save for figure over parcels
    for pr = 1:length(prdct_name)
        init.data_parcel{parcl,pr} = squeeze(mean_var{1,pr}(1,:,:))'; % Only no stim data for each predictor and max intensity
    end
    
    %% GLM : Figure for all electrodes of the parcel
    
    fig = figure;
    hold on;
    
    for pr = 1:length(prdct_name) % Loop over predictors
        
        subplot(1, length(prdct_name), pr); hold on;
        
        for intens = 1:size(mean_var, 1)
            
            bar_mean(intens,:) = nanmean(betas{intens, pr});
            bar_err(intens,:) = nanstd(betas{intens, pr}./sqrt(sum(~isnan(betas{intens, pr}), 1)));
            
            % Stats
            h(intens) = ttest(betas{intens, pr}(:,1), betas{intens, pr}(:,2));
        end
        
        bar(bar_mean)
        
        ngroups = size(bar_mean, 1);
        nbars = size(bar_mean, 2);
        % Calculating the width for each bar group
        groupwidth = min(0.8, nbars/(nbars + 1.5));
        for i = 1:nbars
            x = (1:ngroups) - groupwidth/2 + (2*i-1) * groupwidth / (2*nbars);
            errorbar(x, bar_mean(:,i), bar_err(:,i), 'k', 'LineStyle', 'none');
        end
        
        % Stats
        star_pos = find(h == 1);
        Ylim = ylim;
        x_pos = 1:size(mean_var, 1);
        y_pos = Ylim(2) - diff(Ylim)*0.1;
        scatter(x_pos(star_pos), repmat(y_pos, length(x_pos(star_pos)), 1), 'k*')
        
        % Title & Axes
        title(['Choice = \alpha + \beta ', sprintf('%s\n%s', opt.param, prdct_name{pr})])
        xticks(1:size(mean_var, 1))
        xticklabels({'Max','Sub','All'})
        xlabel('Intensity')
        ylabel('Regression estimate');
        
    end % End of the loop over predictors
    
    % Save
    fig.PaperPosition = [0, 0, 10*length(prdct_name)+10 10]; % [left bottom width height]
    fig_name = fullfile(init.pathtofig_parcl{parcl}, opt.ylabel, sprintf('ROI_level_%s.tiff', opt.param));
    print(fig, fig_name, '-dtiff', '-r200');
    % savefig(fig, fig_name(1:end-5),'compact'); % Matlab format
    
    close(fig);
    
    %% 3D plot of regression estimates (to see if there is a gradient or clusters)
    % Only for max intensity
    
    fig = figure;
    hold on
    
    intens = 1; % Max intensity
    
    for pr = 1:length(prdct_name) % Loop over predictors
        
        subplot(1, length(prdct_name), pr); hold on;
        diff_mean = diff(betas{intens, pr},1,2); % stim - no stim
        
        scatter3(elec_mni{intens}(:,1), elec_mni{intens}(:,2), elec_mni{intens}(:,3), [], diff_mean, 'filled')
        load(fullfile(init.pathtofig, 'colorbar_3D_fig.mat'), 'cbar', 'cmap')
        colorbar;
        caxis(cbar.Limits);
        colormap(cmap)
        
        xlabel('X')
        ylabel('Y')
        zlabel('Z')
        
        if ismember(init.parcels{parcl}, {'PFCvm'})
            view(0,0);
        elseif ismember(init.parcels{parcl}, {'aINS', 'aINS_dors', 'aINS_vent'})
            view(90,0);
        end
        
        title({sprintf('{%s%s}', '\bf', init.parcels{parcl}) ; sprintf('beta %s_{stim - no stim}', prdct_name{pr})}, 'FontWeight', 'Normal');
        
    end
    
    % Save
    fig_name = fullfile(init.pathtofig_parcl{parcl}, opt.ylabel, sprintf('3D_%s_max_intens.tif', opt.param));
    savefig(fig, fig_name(1:end-4),'compact'); % Matlab format
    
    close(fig);
    
    %% Bins : Figure for all electrodes of the parcel
    
    fig = figure;
    hold on;
    p_idx = 1;
    
    for pr = 1:length(prdct_name) % Loop over predictors
        for intens = 1:size(mean_var, 1)
            
            sub = subplot(length(prdct_name), size(mean_var,1), p_idx); hold on;
            
            plot_mean = squeeze(nanmean(mean_var{intens, pr}, 3));
            plot_err = squeeze(nanstd(mean_var{intens, pr}, [], 3)./sqrt(sum(~isnan(mean_var{intens, pr}), 3)));
            
            % No stim
            p(1) = plot(plot_mean(1,:), '-o');
            errorbar(plot_mean(1,:), plot_err(1,:), 'k', 'LineStyle', 'none')
            
            % Stim
            p(2) = plot(plot_mean(2,:), '-o');
            errorbar(plot_mean(2,:), plot_err(2,:), 'k', 'LineStyle', 'none')
            
            % Stats ------------------------------------------------------%
            % T-test
            % h = ttest(squeeze(mean_var{intens, cond}(1,:,:))', squeeze(mean_var{intens, cond}(2,:,:))');
            % star_pos = find(h == 1);
            % m = find(plot_mean(:, star_pos) == max(plot_mean(:, star_pos)));
            % y_pos = plot_mean(m, star_pos) + plot_err(m, star_pos).*sign(plot_mean(m, star_pos)) + (diff(ylim)*0.1*sign(plot_mean(m, star_pos)));
            % if y_pos > opt.ylim(end)
            %     y_pos = opt.ylim(end);
            % end
            % scatter(star_pos, y_pos, 'k*')
            
            % 2 ways ANOVA (method 1) : Doesn't work if there are NaN in the data
            % y = [squeeze(mean_var{intens, cond}(1,:,:))'; squeeze(mean_var{intens, cond}(2,:,:))'];
            % [p_anova,~,stats] = anova2(y, size(y,1)/2);
            
            % 2 ways ANOVA (method 2)
            y = [];
            stim_cond = [];
            dv_bin = [];
            
            for st = 1:size(mean_var{intens, pr}, 1) % Loop over stim condition
                for bin = 1:size(mean_var{intens, pr}, 2) % Loop over bins
                    y = cat(1, y, squeeze(mean_var{intens, pr}(st, bin, :)));
                    stim_cond = cat(1, stim_cond, repmat(st, size(mean_var{intens, pr}(st, bin, :), 3), 1));
                    dv_bin = cat(1, dv_bin, repmat(bin, size(mean_var{intens, pr}(st, bin, :), 3), 1));
                end
            end
            
            [p_anova,~,stats] = anovan(y, {dv_bin, stim_cond}, 'model', 'full', 'varnames', {'dv_bin', 'stim_cond'}, 'display', 'off');
            inter_effect = multcompare(stats, 'Dimension', [1 2], 'Display', 'off');
            
            if ismember(prdct_name{pr}, {'Losses', 'Diff'})
                annotation('textbox', 'String', sprintf('2-ways ANOVA\n%s : %.3g\nStim : %.3g\nInteraction : %.3g', prdct_name{pr}, p_anova), 'Position', sub.Position, 'Vert', 'bottom', 'FitBoxToText', 'on')
            else
                annotation('textbox', 'String', sprintf('2-ways ANOVA\n%s : %.3g\nStim : %.3g\nInteraction : %.3g', prdct_name{pr}, p_anova), 'Position', sub.Position, 'Vert', 'top', 'FitBoxToText', 'on')
            end
            
            pairs2test = [1:size(mean_var{intens, pr}, 2); size(mean_var{intens, pr}, 2)+1:size(mean_var{intens, pr}, 2)*size(mean_var{intens, pr}, 1)];
            for pa = 1:size(pairs2test, 2)
                test_line = inter_effect(:,1) == pairs2test(1,pa) & inter_effect(:,2) == pairs2test(2,pa);
                if ~isempty(find(test_line, 1))
                    star_pos(pa) = inter_effect(test_line, 6) <= 0.05;
                else
                    star_pos(pa) = 0;
                end
            end
            
            star_pos = find(star_pos);
            m = find(plot_mean(:, star_pos) == max(plot_mean(:, star_pos)));
            y_pos = plot_mean(m, star_pos) + plot_err(m, star_pos).*sign(plot_mean(m, star_pos)) + (diff(ylim)*0.1*sign(plot_mean(m, star_pos)));
            if y_pos > opt.ylim(end)
                y_pos = opt.ylim(end);
            end
            scatter(star_pos, y_pos, 'k*')
            
            %-------------------------------------------------------------%
            
            if ismember(opt.param, {'Dv', 'Gains+Losses+Diff', 'Gains+Losses', 'abs(Dv)'})
                lim = dv_lim_rel;
                xlabel(prdct_name{pr})
            elseif ismember(opt.param, {'Utility'})
                lim = opt.prct;
                xlabel(['% ', opt.param])
            end
            
            xlim([0.5 length(lim)-0.5])
            xticks(1:length(lim)-1)
            for i = 1:length(lim)-1
                if i == 1
                    Xtick{i} = sprintf('[%d:%d]', lim(i), lim(i+1));
                else
                    Xtick{i} = sprintf(']%d:%d]', lim(i), lim(i+1));
                end
            end
            xticklabels(Xtick);
            
            ylim(opt.ylim);
            yticks(opt.YTick)
            ylabel(opt.ylabel);
            
            if intens == 1
                title(sprintf('%s - max intensity', init.parcels{parcl}), 'Interpreter', 'none')
            elseif intens == 2
                title(sprintf('%s - sub intensity', init.parcels{parcl}), 'Interpreter', 'none')
            elseif intens == 3
                title(sprintf('%s - all intensity', init.parcels{parcl}), 'Interpreter', 'none')
                legend(p, init.conditions, 'Location', 'southeast', 'Interpreter', 'none')
            end
            
            p_idx = p_idx + 1;
            
        end % End of the loop over intensity condition
    end % End of the loop over predictors
    
    fig.PaperPosition = [0, 0, 40 10*length(prdct_name)]; % [left bottom width height]
    fig_name = fullfile(init.pathtofig_parcl{parcl}, opt.ylabel, sprintf('ROI_level_%s_binned.tiff', opt.param));
    
    print(fig, fig_name, '-dtiff', '-r200');
    % savefig(fig, fig_name(1:end-5),'compact'); % Matlab format
    
    close(fig);
    
    %% Indifference point : Figure for all electrodes of the parcel
    
    fig = figure;
    hold on
    
    for pr = 1:length(prdct_name) % Loop over predictors
        
        subplot(1, length(prdct_name), pr); hold on;
        
        for intens = 1:size(mean_var, 1)
            
            diff_pt = diff(indif_pt{intens, pr}, [], 2);
            
            % Remove abberant data point
            if ismember(opt.param, 'Dv')
                diff_pt(diff_pt > 80 | diff_pt < -80) = [];
            elseif ismember(opt.param, 'Gains+Losses+Diff')
                diff_pt(diff_pt > 500 | diff_pt < -500) = [];
            elseif ismember(opt.param, 'abs(Dv)')
                diff_pt(diff_pt > 80 | diff_pt < -80) = [];
            elseif ismember(opt.param, 'Utility')
                diff_pt(diff_pt > 15 | diff_pt < -15) = [];
            end
            
            bar(intens, mean(diff_pt))
            errorbar(intens, mean(diff_pt), std(diff_pt)/sqrt(length(diff_pt)), 'k', 'LineStyle', 'none')
            scatter(repmat(intens, length(diff_pt), 1), diff_pt, 'MarkerFaceColor', [.5 .5 .5], 'MarkerFaceAlpha', 0.3, 'MarkerEdgeColor', 'none')
            
            Ylim = ylim;
            
            % Stats
            [~,p] = ttest(diff_pt);
            text(intens, mean(Ylim), sprintf('p = %.2g', p), 'HorizontalAlignment', 'center')
            
        end % End of the loop over intensity condition
        
        % Title & Axes
        title(sprintf('%s - %s', init.parcels{parcl}, prdct_name{pr}), 'Interpreter', 'none')
        xticks(1:size(mean_var, 1))
        xticklabels({'Max','Sub','All'})
        xlabel('Intensity')
        ylabel('Indifference point_{stim - no stim}')
        
    end % End of the loop over predictors
    
    fig.PaperPosition = [0, 0, 10*length(prdct_name)+10 10]; % [left bottom width height]
    fig_name = fullfile(init.pathtofig_parcl{parcl}, opt.ylabel, sprintf('ROI_level_%s_indiff_point.tiff', opt.param));
    
    print(fig, fig_name, '-dtiff', '-r200');
    % savefig(fig, fig_name(1:end-5),'compact'); % Matlab format
    
    close(fig);
    
    %% GLM fit according to electrode position
    
    fig = figure;
    hold on
    
    subl = length(prdct_name);
    subc = 3;
    
    intens = 1; % Max intensity
    idx = 0;
    
    for pr = 1:length(prdct_name) % Loop over predictors
        
        diff_mean = diff(indif_pt{intens, pr}, [], 2); % stim - no stim
        
        % Remove abberant data point
        if ismember(opt.param, 'Dv')
            rmv_idx = diff_mean > 80 | diff_mean < -80;
        elseif ismember(opt.param, 'Gains+Losses+Diff')
            rmv_idx = diff_mean > 500 | diff_mean < -500;
        elseif ismember(opt.param, 'abs(Dv)')
            rmv_idx = diff_mean > 80 | diff_mean < -80;
        elseif ismember(opt.param, 'Utility')
            rmv_idx = diff_mean > 15 | diff_mean < -15;
        end
        
        for dim = 2:3
            
            idx = idx + 1;
            subplot(subl, subc, idx); hold on;
            
            [b,~,stats] = glmfit(elec_mni{intens}(~rmv_idx,dim), diff_mean(~rmv_idx));
            yfit = glmval(b, elec_mni{intens}(~rmv_idx,dim), 'identity');
            
            scatter(elec_mni{intens}(~rmv_idx,dim), diff_mean(~rmv_idx), 'filled')
            plot(elec_mni{intens}(~rmv_idx,dim), yfit, 'LineWidth', 2)
            
            TextLocation(sprintf('p = %.2g', stats.p(2)), 'best')
            
            if dim == 1
                xlabel('X')
            elseif dim == 2
                xlabel('Y')
            elseif dim == 3
                xlabel('Z')
            end
            
            ylabel(sprintf('%s_{stim - no stim}', opt.ylabel))
            title({sprintf('{%s%s}', '\bf', init.parcels{parcl}) ; sprintf('beta %s_{stim - no stim}', prdct_name{pr})}, 'FontWeight', 'Normal');
            
        end
        
        idx = idx + 1;
        
        % Subplot Y + Z
        subplot(subl,subc,idx); hold on
        
        [b,~,stats] = glmfit(elec_mni{intens}(~rmv_idx,2) + elec_mni{intens}(~rmv_idx,3), diff_mean(~rmv_idx));
        yfit = glmval(b, elec_mni{intens}(~rmv_idx,2) + elec_mni{intens}(~rmv_idx,3), 'identity');
        
        scatter(elec_mni{intens}(~rmv_idx,2) + elec_mni{intens}(~rmv_idx,3), diff_mean(~rmv_idx), 'filled')
        plot(elec_mni{intens}(~rmv_idx,2) + elec_mni{intens}(~rmv_idx,3), yfit, 'LineWidth', 2)
        
        TextLocation(sprintf('p = %.2g', stats.p(2)), 'best')
        
        xlabel('Y + Z')
        ylabel('Indifference point_{stim - no stim}')
        
    end
    
    % Save
    fig.PaperPosition = [0, 0, 40 10*subl]; % [left bottom width height]
    fig_name = fullfile(init.pathtofig_parcl{parcl}, opt.ylabel, sprintf('indiff_pt_acc2%s_and_dim.tif', opt.param));
    print(fig, fig_name, '-dtiff', '-r200');
    
    close(fig);
    
end % End of the loop over parcels

end

function stim_effect_on_choice_model_free_param(init, choice_data)

for parcl = 1:length(init.parcels) % Boucle � travers les parcelles
    
    clearvars -except init choice_data parcl
    parcl_data = init.group_data(init.group_data(:,14) == parcl,:); % All data for one parcelle
    
    idx = [0 0 0];
    
    for subj = 1:length(init.subj_name) % Loop over subjects
        
        subj_data = parcl_data(parcl_data(:,1) == subj, :);
        u_elec = unique(subj_data(:,17));
        
        for elec = 1:length(u_elec) % Loop over electrodes of the subject
            
            elec_data = subj_data(subj_data(:,17) == u_elec(elec), :);
            
            intensity_data(1) = {elec_data(elec_data(:,16) == max(elec_data(:,16)), :)}; % Max intensity
            intensity_data(2) = {elec_data(elec_data(:,16) ~= max(elec_data(:,16)), :)}; % Sub max intensity
            intensity_data(3) = {elec_data}; % All intensity
            
            for intens = 1:size(intensity_data, 2)
                
                intens_data = intensity_data{intens};
                u_sess = unique(intens_data(:,15));
                
                for sess = 1:length(u_sess) % Loop over sessions of the subject
                    
                    idx(intens) = idx(intens) + 1;
                    sess_name = init.elec_id{subj, u_sess(sess), parcl};
                    
                    % No stim
                    subj_idx = find(strcmp(fieldnames(choice_data.no_stim.inversionResult.model), sess_name));
                    free_param.no_stim.kG{intens}(idx(intens),:) = choice_data.no_stim.inversionResult.freeParameter.model.kG(subj_idx);
                    free_param.no_stim.kL{intens}(idx(intens),:) = choice_data.no_stim.inversionResult.freeParameter.model.kL(subj_idx);
                    
                    % Stim
                    subj_idx = find(strcmp(fieldnames(choice_data.stim.inversionResult.model), sess_name));
                    free_param.stim.kG{intens}(idx(intens),:) = choice_data.stim.inversionResult.freeParameter.model.kG(subj_idx);
                    free_param.stim.kL{intens}(idx(intens),:) = choice_data.stim.inversionResult.freeParameter.model.kL(subj_idx);
                    
                end % End of the loop over sessions
            end % End of the loop over intensities
        end % End of the loop over electrodes
    end % End of the loop over subjects
    
    %% Figure for all electrodes of a ROI
    
    fig = figure;
    hold on
    
    for intens = 1:size(intensity_data, 2)
        
        subplot(1, size(intensity_data, 2), intens); hold on;
        
        kG_data = free_param.stim.kG{intens} - free_param.no_stim.kG{intens};
        kL_data = free_param.stim.kL{intens} - free_param.no_stim.kL{intens};
        
        plot_mean = mean([kG_data, kL_data]);
        plot_err = [std(kG_data)/sqrt(length(kG_data)), std(kL_data)/sqrt(length(kL_data))];
        cate = categorical({'kG', 'kL'});
        
        for k = 1:size(plot_mean,2)
            bar(cate(k), plot_mean(k));
        end
        errorbar(plot_mean, plot_err, 'k', 'LineStyle', 'none')
        
        % Stats
        [h(1),p(1)] = ttest(kG_data);
        [h(2),p(2)] = ttest(kL_data);
        TextLocation(sprintf('n = %g\nkG : p = %.3g\nkL : p = %.3g', size(kG_data, 1), p), 'Location', 'north')
        
        % Axes
        ylabel('stim - no stim');
        
        switch intens
            case 1
                inty = 'max';
            case 2
                inty = 'sub';
            case 3
                inty = 'all';
        end
        title(sprintf('%s - %s intensity', init.parcels{parcl}, inty), 'Interpreter', 'none')
        
    end % End of the loop over intensity
    
    % Save
    fig_folder = fullfile(init.pathtofig_parcl{parcl}, 'choice_model');
    if ~exist(fig_folder, 'dir')
        mkdir(fig_folder)
    end
    
    fig.PaperPosition = [0, 0, 40 10]; % [left bottom width height]
    fig_name = fullfile(fig_folder, 'stim_effect_on_kG_kL.tiff');
    print(fig, fig_name, '-dtiff', '-r200');
    % savefig(fig, fig_name(1:end-5),'compact'); % Matlab format
    
    close(fig);
    
end % End of the loop over parcels
end

