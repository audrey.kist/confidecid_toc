% This script makes the regression of mean activity in a given time window
% divided according to a variable of interest for a specific ROI
% Example :
%   - Mean activity = b0 + b1 * VoI(low)
%   - Mean activity = b0 + b1 * VoI(high)
%
% It also plots the power (binned) according to the VoI
%
% It requires that each frequency of interest has been extracted and time-locked on the events of interest

clear;
close all;

%% Options

init.response    = 'gains-agent'; % Choose data to regress (VoI) : 'confidence', 'probability'
init.div         = 'bins'; % Division method : 'percentile', 'bins';
init.parcel_name = 'aINS'; % Parcel to take into account (e.g: 'PFCvm', 'aINS')
init.mean_interv = [0, 3]; % Interval in which the activity is averaged (choice onset locked);

if ismember(init.div, {'percentile'}) % If init.div = 'percentile'
    init.perct = 50; % 1 to 50 ; 50 = median
elseif ismember(init.div, {'bins'}) % If init.div = 'bins'
    init.bins = 5; % Number of bins ; 2 = 50/50 ; 3 = 1/3, 2/3, 3/3...
end

init.freq_band = [50,150]; % Choose ONE frequency bands to analyse ([4,8], [8,13], [13,33], [33,49], [50,150])
init.smoothing = 250;   % Choose ONE smoothing to analyse
init.fsample   = 100;   % Sample ONE frequency of files to analyse

%% Initialisation

% Patients :
% 'GRE_2019_BAUj','GRE_2019_PLEc','GRE_2019_AUBa','GRE_2019_CHRc','GRE_2019_GEOa',...
% 'LYO_2019_IBRa','REN_2019_BAIp','REN_2019_LAPp','REN_2019_LEBc','LYO_2019_CAPp',...
% 'GRE_2019_BERj','GRE_2020_BERl','GRE_2020_SOLm','GRE_2020_FOUt',...'PRA_2020_PRGa',...
% 'PRA_2020_PRAb','GRE_2020_SULg','PRA_2020_PRAc','GRE_2020_JERr','GRE_2020_GENj',...
% 'GRE_2020_KAYc'

%-------------------------------------------------------------------------%

init.pat_names = {'REN_2019_BAIp'}; % Ajouter les sujets ici

init.bdd_path = 'C:\Users\Romane\Documents\BDD_ConfiDecid';

init.task_path = fullfile(init.bdd_path, 'Cognitive_task');
init.subj_path = fullfile(init.task_path, 'Patients');
init.behav_path = fullfile(init.task_path, 'Comportement'); % Path where model files are located
init.anat_path = fullfile(init.bdd_path, 'Anatomie'); % Path where .CSV files are located
init.backuproot = fullfile(init.task_path, 'BPF_analyzes', 'ROI_analyses_BPF', sprintf('sf%d', init.fsample)); % Path where new files will be saved

%-------------------------------------------------------------------------%

init.task = 'ConfiDecid';

% Load theoretical mood data
mood_data_fullname = fullfile(init.behav_path,'all_mood_data.mat');
load(mood_data_fullname, 'mood_data'); % Load 'mood_data' structure

%% Initialization

init.chan_nb = 0;

%% Boucle � travers les sujets

for pat_idx = 1:length(init.pat_names)
    tic;
    
    clearvars -except init mood_data pat_idx store
    
    subjName = init.pat_names{pat_idx};
    patfile = fullfile(init.subj_path, subjName);
    
    removebadchannels = 1;
    
    %% STEP 1: LOAD BEHAVIORAL DATA
    
    % Trouver les sessions du sujet
    raw_behav_folder = fullfile(patfile, 'RAW', 'Comportement');
    filelist = dir(raw_behav_folder); % Liste de tous les fichiers du dossier
    name = {filelist.name};
    
    all_sess_name = name(~cellfun(@isempty, regexp(name, sprintf('%s_%s[1-9]_cognition_data.mat', subjName, init.task), 'match'))); % Sessions de 1 � X
    
    % Load data matrix
    subjdata = [];
    for sess = 1:length(all_sess_name) % Loop over sessions
        
        data_filename = fullfile(raw_behav_folder, all_sess_name{sess});
        load(data_filename, 'data'); % Charger la matrice data de la session
        subjdata = [subjdata ; data]; % Matrice contenant les donn�es de toutes les sessions pour un sujet
        
    end % Fin de la boucle � travers les sessions
    
    %% STEP 2: LOAD EEG DATA
    
    f_range = init.freq_band; % Frequency range
    f_range_name = sprintf('f%df%d', min(f_range), max(f_range));
    smoothing = sprintf('sm%d',init.smoothing);
    
    bpffilename = fullfile(patfile, 'Analyses', 'BPF', sprintf('%s_%s_f%df%d_sf%d_%s', subjName, init.task, min(f_range), max(f_range), init.fsample, smoothing));
    D = spm_eeg_load(bpffilename);
    
    % Load data
    alldata = permute(D(:,:,:), [3 2 1]); % Ntrials x Ntimes x Nchan
    
    m_cond = str2double(D.conditions'); % Events order used to create the BPF matrix
    timelist = D.time;
    chanlabels = D.chanlabels;
    
    % We will use 'alldata' as the data matrix to perform single channel statistics
    
    %% STEP 3: APPLY SOME OPTIONS TO EEG DATA
    
    if removebadchannels == 1
        badchan = [];
        sess = unique(subjdata(:,2));
        for sbc = 1:length(sess) % Loop over sessions
            bcfilename = fullfile(patfile, 'Analyses', 'BadChannel', sprintf('sess%d', sess(sbc)), sprintf('bc_sess%d.mat', sess(sbc)));
            BC = spm_eeg_load(bcfilename);
            badchan = [badchan BC.badchannels];
        end % End of the loop over sessions
        badchan = unique(badchan); % Delete the channel if it's bad in at least one session
        alldata(:,:,badchan) = [];
        chanlabels(badchan) = [];
    end
    
    %% Find channels in the specified parcel
    
    % Get parcel infos for each sEEG channel from .CSV file
    csvfile = fullfile(init.anat_path, subjName, sprintf('%s.csv', subjName));
    csv = csv2anat2(csvfile, chanlabels);
    
    % Specify all the contraints that will be used to define a given ROI
    constraints = anat_constraints(init.parcel_name);
    ROI_chan_idx = test_chan_parcel(csv, constraints);
    
    %% STEP 4: Select data to regress
    
    z_idx = find(ismember(m_cond, [10:13, 20:23])); % Onset du choix
    
    switch init.response
        
        case 'confidence'
            confi_rat = zscore(subjdata(:,9)); % Confidence ratings all trials (specific to the subject)
            prdct = confi_rat(subjdata(:,11) == 1); % Confidence ratings when AGENT
            idx = find(ismember(m_cond, 10:13)); % Choice onset of AGENT trials
        case 'probability'
            confi_rat = zscore(subjdata(:,9)); % Confidence ratings all trials (specific to the subject)
            prdct = confi_rat(subjdata(:,11) == 0); % Confidence ratings when NON AGENT
            idx = find(ismember(m_cond, 20:23)); % Choice onset of NON AGENT trials
        case 'gains-agent'
            prdct = subjdata(subjdata(:,11) == 1, 4); % Gains when AGENT
            idx = find(ismember(m_cond, 10:13)); % Choice onset of AGENT trials
        case 'gains-proba'
            prdct = subjdata(subjdata(:,11) == 0, 4); % Gains when NON AGENT
            idx = find(ismember(m_cond, 10:13)); % Choice onset of NON AGENT trials
            
    end % End of the switch over response
    
    switch init.div
        
        case 'percentile' % Split trials according to a given percentile
            
            percentilm = prctile(prdct, [init.perct, 100-init.perct]);
            low_idx = prdct < percentilm(1);
            high_idx = prdct > percentilm(2);
            
            % Above
            Aidx{1} = find(high_idx);
            predictors{1} = prdct(high_idx);
            
            % Below
            Aidx{2} = find(low_idx);
            predictors{2} = prdct(low_idx);
            
            % All
            Aidx{3} = find(prdct);
            predictors{3} = prdct;
            
            beg_contrastname = sprintf('%d-%d', init.perct, 100-init.perct); % Beginning of contrast name
            
        case 'bins'
            
            percentilm = prctile(prdct, [0, ((1:init.bins)./init.bins)*100]);
            perct_idx = discretize(prdct, percentilm);
            
            for i = 1:length(percentilm) - 1
                Aidx{i} = find(perct_idx == i);
                predictors{i} = prdct(perct_idx == i);
            end
            
            beg_contrastname = sprintf('bins-%d', init.bins); % Beginning of contrast name
            
    end % End of the switch over division method
    
    contrastname = sprintf('%s-%s-choice-onset-locked-all-trials', beg_contrastname, init.response);
    
    %%  Moyenne � travers le temps pour chaque contacts de la parcelle
    
    filterin = timelist >= init.mean_interv(1) & timelist <= init.mean_interv(2);
    z_alldata = zscore(alldata(z_idx,:,:)); % Trial x Time x Chan
    
    [~,ia] = intersect(z_idx, idx);
    z_alldata = z_alldata(ia,:,:); % Trial x Time x Chan
    
    if ~isempty(ROI_chan_idx)
        obs_data = z_alldata(:,filterin, ROI_chan_idx); % Trial x Time matrix x ROI chan
        data2regress = squeeze(mean(obs_data, 2)); % Moyenne � travers le temps de la parcelle pour le patient (Trial x ROI chan)
    end
    
    %% Do the regression
    
    for group = 1:length(predictors) % Loop over groups
        for chan = 1:length(ROI_chan_idx) % Loop over channels
            
            resp = data2regress(Aidx{group}, chan);
            
            % GLM
            [b,~,stats] = glmfit(predictors{group}, resp);
            dots = b(2:end)'; % Regression coefficient (= slope) of the regression line
            t = stats.t(2:end)';
            dots_err = stats.p(2:end)';
            
            store.dots(init.chan_nb + chan, group) = dots; % We store regression coefficients for each channel (Channel * Conditions)
            store.tstat(init.chan_nb + chan, group) = t; % On stock les t-values
            store.pval(init.chan_nb + chan, group) = dots_err; % On stock les p-values
            
            %% Store data used for the regression
            
            store.predictors{init.chan_nb + chan, group} = predictors{group}; % Conditions * ROI chan
            store.responses{init.chan_nb + chan, group} = resp; % Conditions * ROI chan
            
        end % Fin de la boucle � travers les channels de la parcelle pour le patient
    end % End of the loop over conditions
    init.chan_nb = init.chan_nb + length(ROI_chan_idx);
    
    fprintf('Elapsed time for patient #%d (%s) is %.2f seconds\n', pat_idx, init.pat_names{pat_idx}, toc);
    
end % Fin de la boucle � travers les sujets

store.contrastname = contrastname;
store.filtered_timelist = timelist(filterin);
store.f_range_name = f_range_name;
store.smoothing = smoothing;
store.parcel = init.parcel_name;
store.init = init;

store_folder = fullfile(init.backuproot, f_range_name, smoothing, 'glm_mean_ROI_against_binned_VoI', sprintf('[%.2g_%.2g]', init.mean_interv(1), init.mean_interv(2)));
if ~exist(store_folder, 'dir')
    mkdir(store_folder)
end
save(fullfile(store_folder, sprintf('%s_%s.mat', init.parcel_name, store.contrastname)), 'store');

%% Plots

switch init.div
    
    case 'percentile'
        
        % Estimates
        fig = figure;
        
        mean_dots = mean(store.dots);
        
        bar(mean_dots)
        hold on
        errorbar(mean_dots, std(store.dots)/sqrt(length(store.dots)), 'Color', 'black', 'LineStyle', 'none');
        
        ticksLabel{1} = sprintf('Low (%d%%)', init.perct);
        ticksLabel{2} = sprintf('High (%d%%)', 100-init.perct);
        ticksLabel{3} = sprintf('All');
        
        xticklabels(ticksLabel)
        xlabel(init.response)
        ylabel('Estimate')
        title(init.parcel_name)
        
        Ylim = ylim;
        if sign(mean_dots(max(abs(mean_dots)) == abs(mean_dots))) == 1 % Positif
            ylim([Ylim(1) Ylim(2) + diff(ylim)*0.2])
            Y1 = Ylim(2) + diff(ylim)*0.05;
            Y2 = Y1 + diff(ylim)*0.05;
        else % Negatif
            ylim([Ylim(1) - diff(ylim)*0.2 Ylim(2)])
            Y1 = Ylim(1) - diff(ylim)*0.05;
            Y2 = Y1 - diff(ylim)*0.05;
        end
        
        [~,p] = ttest(store.dots(:,1), store.dots(:,2));
        line([1 2], [Y1 Y1], 'Color', 'black')
        text(1.5, Y2, sprintf('p = %.4f', p), 'HorizontalAlignment', 'center');
        
    case 'bins'
        
        % BGA
        bga = cellfun(@nanmean, store.responses);
        
        fig = figure;
        hold on
        
        % bar(mean(bga))
        errorbar(nanmean(bga), nanstd(bga)./sqrt(sum(~isnan(bga))), '--o');
        
        for i = 1:init.bins
            ticksLabel{i} = sprintf('%d/%d', i, init.bins);
        end
        
        xticks(1:round(init.bins/10):init.bins);
        xticklabels(ticksLabel(1:round(init.bins/10):init.bins))
        xlabel(sprintf('increasing %s', init.response))
        ylabel(f_range_name)
        title(init.parcel_name)
        
        % [~,p] = ttest(bga(:,1), bga(:,end));
        % text(mean(xlim), mean(ylim), sprintf('p = %.4f', p), 'HorizontalAlignment', 'center');
        
end

% Save
fig_name = fullfile(store_folder, sprintf('%s_%s', init.parcel_name, store.contrastname));
savefig(fig, fig_name,'compact'); % Matlab format

