% This script makes regressions at the contact level of :
%   - activity (in a specific frequency band) against one or multiple variable of interest (VoI) : activity = b0 + b1 * VoI (VoI predicts activity)
%   - a variable of interest against activity : VoI = b0 + b1 * activity (activity predicts VoI)
%
% It also perform regressions with randomized predictors for multiple
% comparisons correction
%
% It requires that each frequency of interest has been extracted and time-locked on the events of interest
%
% To make ROI level stats and figure see : D_BPF_group_level_on_parcels

clear;
close all;

%% Contrastes

% 1  = Regression of activity against mood ratings, choice onset locked
% 2  = Regression of activity against mood ratings, mood onset locked
% 3  = Regression of activity against mood ratings, mood resp locked
% 4  = Regression of activity against prospective confidence ratings (agent trials) choice onset locked
% 5  = Regression of activity against prospective confidence ratings (agent trials) choice offset locked
% 6  = Regression of activity against prospective probability ratings (non agent trials) choice onset locked
% 7  = Regression of activity against prospective probability ratings (non agent trials) choice offset locked
% 8  = Regression of activity against proxy of retrospective confidence (agent trials) choice onset locked
% 9  = Regression of activity against proxy of retrospective confidence (agent trials) choice offset locked
% 10 = Regression of activity against proxy of retrospective probability (non agent trials) choice onset locked
% 11 = Regression of activity against proxy of retrospective probability (non agent trials) choice offset locked
% 12 = Regression of activity against TML, feedback onset locked
% 13 = Regression of activity against choice variables (agent trials), choice onset locked
% 14 = Regression of activity against choice variables (non agent trials), choice onset locked
% 15 = Regression of activity against choice variables (agent trials), choice offset locked
% 16 = Regression of activity against choice variables (non agent trials), choice offset locked
% 17 = T-test agent vs. proba trials, choice onset locked (T-test script)
% 18 = T-test feedback (+) vs. (-) trials, feedback onset locked (T-test script)
% 19 = Regression of activity against utility (all trials), choice onset locked
% 20 = Regression of activity against utility (all trials), choice offset locked
% 21 = Regression of activity against utility (agent trials), choice onset locked
% 22 = Regression of activity against utility (non agent trials), choice onset locked
% 23 = Regression of activity against utility (agent trials), choice offset locked
% 24 = Regression of activity against utility (non agent trials), choice offset locked
% 25 = Regression of activity against feedbacks (all trials), feedback onset locked

%% Options

init.cont      = 25;         % Choose contrasts to run
init.freq_band = {[50,150]}; % Choose frequency bands to analyse ({[4,8], [8,13], [13,33], [33,49], [50,150]})
init.smoothing = 250;        % Choose smoothing to analyse
init.fsample   = 100;        % Sample frequency of files to analyse
init.parcels   = {'PFCvm', 'aINS'};    % Name of specific parcels or 'all'

init.perm_test = 0;          % Perform permutation tests (1 = yes / 0 = no)
init.perm_nb   = 300;        % Number of permutations

% All parcels
% {'PFCvm','aINS_dors','aINS_vent','aINS','pINS','Thalamus','Caudate','Putamen','Pallidum','Hippocampus','Amygdala','Accumbens',...
% 'OFCv','OFCvm','OFCvl','Pfrdls','Pfrdli','VCl','VCcm','ICC','ITCm','VCs','Cu','VCrm','ITCr','MTCc','MTCr','STCc','STCr','IPCv','IPCd',...
% 'SPC','SPCm','PCm','Sv','Sdl','Sdm','PCC','MCC','ACC','Mv','Mdl','Mdm','PMrv','PMdl','PMdm','PFcdl','PFcdm','PFrvl','PFrd','PFrm'};

%% Initialisation

% Patients :
% 'GRE_2019_BAUj','GRE_2019_PLEc','GRE_2019_AUBa','GRE_2019_CHRc','GRE_2019_GEOa',...
% 'LYO_2019_IBRa','REN_2019_BAIp','REN_2019_LAPp','REN_2019_LEBc','LYO_2019_CAPp',...
% 'GRE_2019_BERj','GRE_2020_BERl','GRE_2020_SOLm','GRE_2020_FOUt','PRA_2020_PRAb',...
% 'GRE_2020_SULg','PRA_2020_PRAc','GRE_2020_JERr','GRE_2020_GENj','GRE_2020_KAYc'

% Excluded patients : 'PRA_2020_PRGa', 'PRA_2020_PRAd'

%-------------------------------------------------------------------------%

init.pat_names = {'GRE_2019_BAUj','GRE_2019_PLEc','GRE_2019_AUBa','GRE_2019_CHRc','GRE_2019_GEOa',...
    'LYO_2019_IBRa','REN_2019_BAIp','REN_2019_LAPp','REN_2019_LEBc','LYO_2019_CAPp',...
    'GRE_2019_BERj','GRE_2020_BERl','GRE_2020_SOLm','GRE_2020_FOUt','PRA_2020_PRAb',...
    'GRE_2020_SULg','PRA_2020_PRAc','GRE_2020_JERr','GRE_2020_GENj','GRE_2020_KAYc',...
    'GRE_2021_GUIl'}; % Ajouter les sujets ici

% Define BDD path according to computer name
name = getenv('COMPUTERNAME');
if ismember(name, 'HP-ROMANE')
    init.bdd_path = 'C:\Users\Romane\Documents\MATLAB\BDD_ConfiDecid';
elseif ismember(name, 'DESKTOP-IHJ3T8M')
    init.bdd_path = 'C:\Users\Romane\Documents\BDD_ConfiDecid';
end

init.task_path = fullfile(init.bdd_path, 'Cognitive_task');
init.subj_path = fullfile(init.task_path, 'Patients');
init.behav_path = fullfile(init.task_path, 'Comportement'); % Path where model files are located
init.anat_path = fullfile(init.bdd_path, 'Anatomie'); % Path where .CSV files are located
init.backuproot = fullfile(init.task_path, 'BPF_analyzes', 'Group_stat_BPF'); % Path where new files will be saved
%-------------------------------------------------------------------------%

init.task = 'ConfiDecid';

% Load theoretical mood data
mood_data_fullname = fullfile(init.behav_path,'all_mood_data.mat');
load(mood_data_fullname, 'mood_data'); % Load 'mood_data' structure

% Load choice model data
choice_data_fullname = fullfile(init.behav_path,'all_choice_data.mat');
load(choice_data_fullname, 'choice_data'); % Load 'choice_data' structure

%% Boucle � travers les sujets

for pat_idx = 1:length(init.pat_names)
    tic;
    
    clearvars -except init mood_data choice_data pat_idx
    
    subjName = init.pat_names{pat_idx};
    patfile = fullfile(init.subj_path, subjName);
    
    backup_data       = 1;
    removebadchannels = 1;
    
    %% Load EPI_BPF_stat.mat of the subject if exist (the file saved at the end of this script)
    
    filename = sprintf('%s_%s_BPF_stat.mat', subjName, init.task);
    filebackup = fullfile(init.backuproot, sprintf('sf%d', init.fsample), filename);
    if exist(filebackup, 'file')
        load(filebackup, 'store');
    end
    
    if ~exist(fileparts(filebackup), 'dir') % Si le dossier n'existe pas on le cr��
        mkdir(fileparts(filebackup));
    end
    
    %% STEP 1: LOAD BEHAVIORAL DATA
    
    % Trouver les sessions du sujet
    raw_behav_folder = fullfile(patfile, 'RAW', 'Comportement');
    filelist = dir(raw_behav_folder); % Liste de tous les fichiers du dossier
    name = {filelist.name};
    
    all_sess_name = name(~cellfun(@isempty, regexp(name, sprintf('%s_%s[1-9]_cognition_data.mat', subjName, init.task), 'match'))); % Sessions de 1 � X
    
    % Load data matrix
    subjdata = [];
    for sess = 1:length(all_sess_name) % Loop over sessions
        
        data_filename = fullfile(raw_behav_folder, all_sess_name{sess});
        load(data_filename, 'data'); % Charger la matrice data de la session
        subjdata = [subjdata ; data]; % Matrice contenant les donn�es de toutes les sessions pour un sujet
        
    end % Fin de la boucle � travers les sessions
    
    %% Loop over frequency bands and smoothings
    
    for bpf = 1:length(init.freq_band) % Loop over filtered frequency bands
        
        f_range = init.freq_band{bpf}; % Frequency range
        f_range_name = sprintf('f%df%d', min(f_range), max(f_range));
        
        for smooth = 1:length(init.smoothing) % Loop over smoothings
            
            smoothing = sprintf('sm%d',init.smoothing(smooth));
            
            %% STEP 2: LOAD EEG DATA
            
            bpffilename = fullfile(patfile, 'Analyses', 'BPF', sprintf('%s_%s_f%df%d_sf%d_sm%d', subjName, init.task, min(f_range), max(f_range), init.fsample, init.smoothing(smooth)));
            D = spm_eeg_load(bpffilename);
            
            % Load data
            alldata = permute(D(:,:,:), [3 2 1]); % Ntrials x Ntimes x Nchan
            
            m_cond = str2double(D.conditions'); % Events order used to create the BPF matrix
            timelist = D.time;
            chanlabels = D.chanlabels;
            
            % We will use 'alldata' as the data matrix to perform single channel statistics
            
            %% STEP 3: APPLY SOME OPTIONS TO EEG DATA
            
            if removebadchannels == 1
                badchan = [];
                sess = unique(subjdata(:,2));
                for sbc = 1:length(sess) % Loop over sessions
                    bcfilename = fullfile(patfile, 'Analyses', 'BadChannel', sprintf('sess%d', sess(sbc)), sprintf('bc_sess%d.mat', sess(sbc)));
                    BC = spm_eeg_load(bcfilename);
                    badchan = [badchan BC.badchannels];
                end % End of the loop over sessions
                badchan = unique(badchan); % Delete the channel if it's bad in at least one session
                alldata(:,:,badchan) = [];
                chanlabels(badchan) = [];
            end
            
            %% Find channels in specified parcel
            
            if ismember(init.parcels, 'all')
                ROI_chan_idx = 1:length(chanlabels);
            else
                ROI_chan_idx = [];
                
                % Get parcel infos for each sEEG channel from .CSV file
                csvfile = fullfile(init.anat_path, subjName, sprintf('%s.csv', subjName));
                csv = csv2anat2(csvfile, chanlabels);
                
                for p = 1:length(init.parcels)
                    % Specify all the contraints that will be used to define a given ROI
                    constraints = anat_constraints(init.parcels{p});
                    idx = test_chan_parcel(csv, constraints);
                    ROI_chan_idx = [ROI_chan_idx ; idx];
                end
                ROI_chan_idx = unique(ROI_chan_idx);
            end
            
            %% STEP 4: LOOP OVER CONTRASTS
            
            for tidx = 1:length(init.cont) % Loop over contrasts
                
                clear opt
                
                % Clear data if contrasts already existed
                
                if exist('store','var') && isfield(store, f_range_name) && isfield(store.(f_range_name), smoothing) &&...
                        init.cont(tidx) <= length(store.(f_range_name).(smoothing)) && ~isempty(store.(f_range_name).(smoothing)(init.cont(tidx)).options) % If the contrast already exist
                    store.(f_range_name).(smoothing)(init.cont(tidx)) = structfun(@(x) [], store.(f_range_name).(smoothing)(init.cont(tidx)), 'UniformOutput', false); % Empty it
                end
                
                %% Load Permutation_tests\XXX_20XX_NNNp\contrastX.mat if exists
                
                if init.perm_test == 1 % Permutation tests
                    
                    permbackup = fullfile(init.backuproot, sprintf('sf%d', init.fsample), 'Permutation_tests', subjName, sprintf('contrast%d.mat', init.cont(tidx)));
                    
                    if exist(permbackup, 'file') % Si le contrast existe d�j�
                        load(permbackup, 'perm_tests')
                        if isfield(perm_tests, f_range_name) && isfield(perm_tests.(f_range_name), smoothing) && isfield(perm_tests.(f_range_name).(smoothing), 'dots') % if perm_test for the freq and smoothing considered already exist, empty it
                            if pat_idx == 1 && size(perm_tests.(f_range_name).(smoothing).dots{1},3) > init.perm_nb
                                answer = questdlg(sprintf('Attention : Le nombre de permutation est inf�rieur � celui existant.\nLancer quand m�me l''analyse ?'), 'Warning', 'Oui', 'Non', 'Non');
                                if strcmp(answer, 'Non')
                                    return
                                end
                            end
                            perm_tests.(f_range_name).(smoothing).dots = {};
                        end
                    end
                end
                
                %% Define contrasts
                
                switch init.cont(tidx)
                    
                    case {1,2,3} % Does mood ratings predict activity (Activity = a + b * Mood ratings)
                        
                        opt.contrastname = 'BPF = a + b*Mood rating';
                        opt.var_of_interest = zscore(subjdata(~isnan(subjdata(:,14)),14)); % Mood ratings z-scored by subjects
                        opt.voi_name = 'mood-rating';
                        opt.glm_predictor = 'voi';
                        
                        if init.cont(tidx) == 1
                            opt.time_lock = 'next-choice-onset-baseline';
                        elseif init.cont(tidx) == 2
                            opt.time_lock = 'mood-onset';
                        elseif init.cont(tidx) == 3
                            opt.time_lock = 'mood-resp';
                        end
                        
                        if ~isempty(regexp(opt.time_lock, 'next-choice', 'once')) && ~isnan(subjdata(end,14)) % If mood rating at last trial
                            opt.var_of_interest(end) = []; % Delete it because there is no trial after
                        end
                        
                    case {4,5} % Does prospective confidence (AGENT) predicts activity (Activity = a + b * Confidence)
                        
                        opt.contrastname = 'BPF = a + b*Prospective Confidence';
                        opt.var_of_interest = zscore(subjdata(subjdata(:,11) == 1,9)); % Confidence ratings when AGENT
                        opt.voi_name = 'prospective-confidence';
                        opt.glm_predictor = 'voi';
                        
                        if init.cont(tidx) == 4
                            opt.time_lock = 'choice-onset';
                        elseif init.cont(tidx) == 5
                            opt.time_lock = 'choice-resp';
                        end
                        
                    case {6,7} % Does prospective probability (NON AGENT) predicts activity (Activity = a + b * Probability)
                        
                        opt.contrastname = 'BPF = a + b*Prospective Probability';
                        opt.var_of_interest = zscore(subjdata(subjdata(:,11) == 0,9)); % Confidence ratings when NON AGENT
                        opt.voi_name = 'prospective-probability';
                        opt.glm_predictor = 'voi';
                        
                        if init.cont(tidx) == 6
                            opt.time_lock = 'choice-onset';
                        elseif init.cont(tidx) == 7
                            opt.time_lock = 'choice-resp';
                        end
                        
                    case {8, 9} % Does proxy of retrospective confidence (AGENT) predicts activity (Activity = a + b * Proxy of confidence)
                        
                        opt.contrastname = 'BPF = a + b*Retrospective Confidence';
                        
                        proba_accept = choice_data.agent.inversionResult.model.(init.pat_names{pat_idx}).posterior.muX(2,:)'; % Also in out.suffStat.gx
                        
                        opt.var_of_interest = zscore((proba_accept - mean(proba_accept)).^2); % Proxy of retrospective confidence when AGENT (proba that the decision is correct)
                        opt.voi_name = 'retrospective-confidence-proxy';
                        opt.glm_predictor = 'voi';
                        
                        if init.cont(tidx) == 8
                            opt.time_lock = 'choice-onset';
                        elseif init.cont(tidx) == 9
                            opt.time_lock = 'choice-resp';
                        end
                        
                    case {10, 11} % Does proxy of retrospective probability (NON AGENT) predicts activity (Activity = a + b * Proxy of probability)
                        
                        opt.contrastname = 'BPF = a + b*Retrospective Probability';
                        
                        proba_accept = choice_data.non_agent.inversionResult.model.(init.pat_names{pat_idx}).posterior.muX(2,:)'; % Also in out.suffStat.gx
                        
                        opt.var_of_interest = zscore((proba_accept - mean(proba_accept)).^2); % Proxy of confidence when NON AGENT (proba that the decision is correct)
                        opt.voi_name = 'retrospective-probability-proxy';
                        opt.glm_predictor = 'voi';
                        
                        if init.cont(tidx) == 10
                            opt.time_lock = 'choice-onset';
                        elseif init.cont(tidx) == 11
                            opt.time_lock = 'choice-resp';
                        end
                        
                    case {12} % Does TML predict activity (Activity = a + b * TML)
                        
                        opt.contrastname = 'BPF = a + b*TML';
                        opt.var_of_interest = zscore(mood_data.inversionResult.model(mood_data.best_model).(init.pat_names{pat_idx}).out.suffStat.gx'); % TML z-scored by subjects
                        % opt.var_of_interest(end) = []; % Delete last trial because there is no trial after
                        opt.voi_name = 'tml';
                        opt.glm_predictor = 'voi';
                        
                        opt.time_lock = 'feedback-onset'; %'next-choice-onset-baseline';
                        
                    case {13, 14, 15, 16} % Does choice variables predict activity (Activity = a + b * Gains + c * Losses + d * Diff + e * Confidence + f * p(accept))
                        
                        opt.contrastname = 'BPF = a + b*Choice Variables';
                        opt.var_of_interest(:,1) = subjdata(:,4); % Perspective of gain ALL trials
                        opt.var_of_interest(:,2) = subjdata(:,5); % Perspective of loss ALL trials
                        opt.var_of_interest(:,3) = subjdata(:,6); % Perspective of difficulty ALL trials
                        opt.var_of_interest(:,4) = subjdata(:,9); % Confidence ratings ALL trials
                        
                        proba_accept(subjdata(:,11) == 1) = choice_data.agent.inversionResult.model.(init.pat_names{pat_idx}).posterior.muX(2,:)';
                        proba_accept(subjdata(:,11) == 0) = choice_data.non_agent.inversionResult.model.(init.pat_names{pat_idx}).posterior.muX(2,:)';
                        opt.var_of_interest(:,5) = proba_accept; % Proba that the decision is correct; % p(accept)
                        % opt.var_of_interest(:,5) = subjdata(:,7); % Choice (0|1)
                        
                        opt.var_of_interest = zscore(opt.var_of_interest);
                        
                        % Remove trials with long choice RT
                        % opt.var_of_interest(subjdata(:,8) > 50,:) = NaN;
                        
                        if ismember(init.cont(tidx), [13,15])
                            opt.contrastname = ['(agent)', opt.contrastname];
                            opt.var_of_interest = opt.var_of_interest(subjdata(:,11) == 1,:);
                            opt.voi_name = 'gains+losses+diff+confidence+p(accept)';
                        elseif ismember(init.cont(tidx), [14,16])
                            opt.contrastname = ['(computer)', opt.contrastname];
                            opt.var_of_interest = opt.var_of_interest(subjdata(:,11) == 0,:);
                            opt.voi_name = 'gains+losses+diff+probability+p(accept)';
                        end
                        
                        opt.glm_predictor = 'voi';
                        
                        if ismember(init.cont(tidx), [13,14])
                            opt.time_lock = 'choice-onset';
                        elseif ismember(init.cont(tidx), [15,16])
                            opt.time_lock = 'choice-resp';
                        end
                        
                    case {19, 20, 21, 22, 23, 24} % Does utility predict activity (Activity = a + b * Utility)
                        
                        opt.contrastname = 'BPF = a + b*Utility';
                        
                        utility(subjdata(:,11) == 1) = choice_data.agent.inversionResult.model.(init.pat_names{pat_idx}).posterior.muX(1,:);
                        utility(subjdata(:,11) == 0) = choice_data.non_agent.inversionResult.model.(init.pat_names{pat_idx}).posterior.muX(1,:);
                        
                        opt.var_of_interest(:,1) = zscore(utility); % Utility z-scored by subjects
                        
                        if ismember(init.cont(tidx), [21,23])
                            opt.contrastname = ['(agent)', opt.contrastname];
                            opt.var_of_interest = opt.var_of_interest(subjdata(:,11) == 1,:);
                        elseif ismember(init.cont(tidx), [22,24])
                            opt.contrastname = ['(computer)', opt.contrastname];
                            opt.var_of_interest = opt.var_of_interest(subjdata(:,11) == 0,:);
                        end
                        
                        opt.voi_name = 'utility';
                        opt.glm_predictor = 'voi';
                        
                        if ismember(init.cont(tidx), [19,21,22])
                            opt.time_lock = 'choice-onset';
                        elseif ismember(init.cont(tidx), [20,23,24])
                            opt.time_lock = 'choice-resp';
                        end
                        
                    case {25}
                        
                        opt.contrastname = 'BPF = a + b*Feedback';
                        
                        fb = subjdata(:,12);
                        fb(fb == 0) = -1;
                        
                        opt.var_of_interest(:,1) = fb; % Feedbacks (-1|1)
                        
                        opt.voi_name = 'feedback';
                        opt.glm_predictor = 'voi';
                        opt.time_lock = 'feedback-onset';
                        
                end
                
                %% Define time-lock
                % Aidx = index of trials used to perform the correlation
                % opt.z_trial = index of trials used to z-score alldata -> if don't exist, Aidx is used
                
                switch opt.time_lock
                    case 'choice-onset'
                        
                        % Define trials used to perform the correlation
                        Aidx = find(ismember(m_cond, [10:13,20:23])); % Choice onset
                        opt.z_trial = Aidx;
                        
                        if ~isempty(regexp(opt.voi_name, 'confidence', 'once')) || ismember(char(extractBetween(opt.contrastname, '(', ')')), {'agent'})
                            Aidx = find(ismember(m_cond, 10:13)); % Choice onset of AGENT trials
                        elseif ~isempty(regexp(opt.voi_name, 'probability', 'once'))  || ismember(char(extractBetween(opt.contrastname, '(', ')')), {'computer'})
                            Aidx = find(ismember(m_cond, 20:23)); % Choice onset of NON AGENT trials
                        end
                        
                        opt.exploretw = [-1 5]; % Time window for data exploration/visualisation
                        opt.xlabel = 'time (s) relative to choice onset';
                        
                    case 'next-choice-onset-baseline'
                        
                        % Define trials used to perform the correlation
                        choice_onset = find(ismember(m_cond, [10:13,20:23])); % Choice onset
                        
                        if ismember(opt.voi_name, {'mood-rating'})
                            
                            Bidx = find(ismember(m_cond, 170)) + 1; % Choice onset of trials in which a mood rating was performed right before
                            Aidx = intersect(choice_onset, Bidx);
                            
                            if length(Aidx) < length(opt.var_of_interest) % For example in case subject responds too quickly in some trials (170 before 17)
                                Cidx = intersect(find(ismember(m_cond, [10:13,20:23])), find(ismember(m_cond, 170)) + 2);
                                Aidx = sort([Aidx; Cidx]);
                            end
                            
                        elseif ismember(opt.voi_name, {'tml'})
                            Aidx = choice_onset(2:end);
                        end
                        
                        opt.exploretw = [-5 1]; % Time window for data exploration/visualisation
                        opt.xlabel = 'time (s) relative to next choice onset';
                        
                    case 'choice-resp'
                        
                        % Define trials used to perform the correlation
                        Aidx = find(ismember(m_cond, [14,24])); % Choice offset
                        opt.z_trial = Aidx;
                        
                        if ~isempty(regexp(opt.voi_name, 'confidence', 'once')) || ismember(char(extractBetween(opt.contrastname, '(', ')')), {'agent'})
                            Aidx = find(ismember(m_cond, 14)); % Choice offset of AGENT trials
                        elseif ~isempty(regexp(opt.voi_name, 'probability', 'once')) || ismember(char(extractBetween(opt.contrastname, '(', ')')), {'computer'})
                            Aidx = find(ismember(m_cond, 24)); % Choice offset of NON AGENT trials
                        end
                        
                        opt.exploretw = [-6 1]; % Time window for data exploration/visualisation
                        opt.xlabel = 'time (s) relative to choice response';
                        
                    case 'mood-onset'
                        
                        % Define trials used to perform the correlation
                        Aidx = find(ismember(m_cond, 17)); % Mood rating onset
                        
                        opt.exploretw = [-2 5]; % Time window for data exploration/visualisation
                        opt.xlabel = 'time (s) relative to mood rating onset';
                        
                    case 'mood-resp'
                        
                        % Define trials used to perform the correlation
                        Aidx = find(ismember(m_cond, 170)); % Mood rating offset
                        
                        opt.exploretw = [-5 1]; % Time window for data exploration/visualisation
                        opt.xlabel = 'time (s) relative to mood response';
                        
                    case 'feedback-onset'
                        
                        % Define trials used to perform the correlation
                        Aidx = find(ismember(m_cond, 16)); % Feedback onset
                        
                        opt.exploretw = [-0.5 5]; % Time window for data exploration/visualisation
                        opt.xlabel = 'time (s) relative to feedback onset';
                        
                end
                
                %% Loop over ROI's channels
                
                filterin = timelist >= opt.exploretw(1) & timelist <= opt.exploretw(2);
                
                % Z-score activity
                if ~isfield(opt, 'z_trial')
                    % z_alldata = zscore(alldata(Aidx,:,:)); % Ntrials x Ntimes x Nchan
                    z_alldata = alldata(Aidx,:,:); % Ntrials x Ntimes x Nchan
                else
                    % z_alldata = zscore(alldata(opt.z_trial,:,:)); % Ntrials x Ntimes x Nchan
                    z_alldata = alldata(opt.z_trial,:,:); % Ntrials x Ntimes x Nchan
                    
                    % Compute percentage change instead of z-score
                    % trials_of_interest = alldata(opt.z_trial,:,:);
                    % baseline = mean(trials_of_interest(:, timelist >= -0.2 & timelist <= 0, :),2);
                    % z_alldata = 100*((trials_of_interest - baseline) ./ baseline);
                     
                    [~,ia] = intersect(opt.z_trial, Aidx);
                    z_alldata = z_alldata(ia,:,:); % Ntrials x Ntimes x Nchan
                end
                
                for chan = 1:length(ROI_chan_idx) % Loop over selected channels
                    
                    % Identify regression predictors and responses
                    if ismember(opt.glm_predictor, 'voi') % Variable of interest predicts activity
                        
                        predictors = opt.var_of_interest;
                        responses = z_alldata(:, filterin, ROI_chan_idx(chan)); % Trial x Time matrix
                        
                        dots = NaN(size(responses, 2), size(predictors, 2)); % Initialize 'dots' matrix
                        
                    elseif ismember(opt.glm_predictor, 'activity') % Activity predicts variable of interest
                        
                        predictors = z_alldata(:, filterin, ROI_chan_idx(chan)); % Trial x Time matrix
                        responses = opt.var_of_interest;
                        
                        dots = NaN(size(predictors, 2), size(responses, 2)); % Initialize 'dots' matrix
                        
                    end
                    
                    %% Do the regression
                    
                    t = dots;
                    dots_err = dots;
                    
                    for timi = 1:size(dots, 1) % Loop over time points to get regression weights and stats
                        
                        if ismember(opt.glm_predictor, 'voi') % Variable of interest predicts activity
                            prdct = predictors;
                            resp = responses(:,timi);
                        elseif ismember(opt.glm_predictor, 'activity') % Activity predicts variable of interest
                            prdct = predictors(:,timi);
                            resp = responses;
                        end
                        
                        if VBA_isBinary(resp(~isnan(resp))) == true
                            distr = 'binomial';
                            blink = 'logit';
                        else
                            distr = 'normal';
                            blink = 'identity';
                        end
                        
                        [b,~,stats] = glmfit(prdct, resp, distr);
                        dots(timi,:) = b(2:end)'; % Regression coefficient (= slope) of the regression line
                        t(timi,:) = stats.t(2:end)';
                        dots_err(timi,:) = stats.p(2:end)';
                        
                    end % End of the loop over time points
                    
                    %% Permuation tests
                    
                    if init.perm_test == 1
                        
                        % MEX FUNCTION (5 time faster)
                        % Work ONLY for : linear regressions (glmfit) | distr:normal | link:identity
                        [dots_perm_mex, t_perm_mex, dots_err_perm_mex] = permutations_mex(opt.var_of_interest, responses, opt.glm_predictor, size(dots), init.perm_nb);
                        
                        % CODE TO BE USED IF MEX FUNCTION CANNOT BE USED (e.g. logistic regression)
                        % dots_perm = NaN(size(dots, 1), size(dots, 2) , init.perm_nb);
                        % t_perm = dots_perm;
                        % dots_err_perm = dots_perm;
                        %
                        % for perm = 1:init.perm_nb
                        %
                        %     perm_reg = Shuffle(opt.var_of_interest, 2); % Shuffle lines of our variable(s) of interest (matching between VOIs is maintained)
                        %
                        %     for timi = 1:size(dots, 1) % R�gression � chaque temps
                        %
                        %         if ismember(opt.glm_predictor, 'voi') % Variable of interest predicts activity
                        %             prdct = perm_reg;
                        %             resp = responses(:,timi);
                        %         elseif ismember(opt.glm_predictor, 'activity') % Activity predicts variable of interest
                        %             prdct = predictors(:,timi);
                        %             resp = perm_reg;
                        %         end
                        %
                        %         [b_perm,~,stats_perm] = glmfit(prdct, resp);
                        %         dots_perm(timi,:,perm) = b_perm(2:end)'; % Regression coefficient (= slope) of the regression line
                        %         t_perm(timi,:,perm) = stats_perm.t(2:end)';
                        %         dots_err_perm(timi,:,perm) = stats_perm.p(2:end)';
                        %
                        %     end % End of the loop over time points
                        % end % End of the loop over permutation number
                        
                    end % End of the condition "if init.perm_test == 1"
                    
                    %% Store
                    
                    store.(f_range_name).(smoothing)(init.cont(tidx)).dots{ROI_chan_idx(chan)} = dots; % We store regression coefficients for each time point and channel
                    store.(f_range_name).(smoothing)(init.cont(tidx)).tstat{ROI_chan_idx(chan)} = t; % On stock les t-values
                    store.(f_range_name).(smoothing)(init.cont(tidx)).pval{ROI_chan_idx(chan)} = dots_err; % On stock les p-values
                    
                    if init.perm_test == 1 % Permutation tests
                        
                        perm_tests.(f_range_name).(smoothing).dots{ROI_chan_idx(chan)} = dots_perm; % Time x Nb regressor x Nb permutation
                        perm_tests.(f_range_name).(smoothing).tstat{ROI_chan_idx(chan)} = t_perm;
                        perm_tests.(f_range_name).(smoothing).pval{ROI_chan_idx(chan)} = dots_err_perm;
                        
                    end
                    
                end % Fin de la boucle � travers les channels
                
                store.(f_range_name).(smoothing)(init.cont(tidx)).options = opt; % Save contrast options
                store.(f_range_name).(smoothing)(init.cont(tidx)).options.filtered_timelist = timelist(filterin);
                
                % Permutation tests
                if init.perm_test == 1
                    perm_tests.(f_range_name).(smoothing).contrastname = opt.contrastname;
                    
                    if ~exist(fileparts(permbackup), 'dir') % Si le dossier n'existe pas on le cr��
                        mkdir(fileparts(permbackup));
                    end
                    
                    save(permbackup, 'subjName', 'perm_tests', '-v7.3'); % If the file is > 2GB
                end
                
            end % Fin de la boucle � travers les contrastes
        end % Fin de la boucle � travers les smoothings
    end % Fin de la boucle � travers les bandes de fr�quence
    
    % Fill hdr structure
    hdr.dimord = 'trial_time_chan';
    hdr.dimsiz = size(alldata);
    hdr.conditions = m_cond;
    hdr.timelist = timelist;
    hdr.chanlist = chanlabels';
    
    if backup_data % Save intermediate results for group analysis
        if ~exist(fullfile(init.backuproot), 'dir')
            mkdir(fullfile(init.backuproot));
        end
        save(filebackup, 'subjName', 'hdr', 'store');
    end
    
    fprintf('Elapsed time for patient #%d (%s) is %.2f seconds\n', pat_idx, subjName, toc);
    
end % Fin de la boucle � travers les sujets

return % END

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
