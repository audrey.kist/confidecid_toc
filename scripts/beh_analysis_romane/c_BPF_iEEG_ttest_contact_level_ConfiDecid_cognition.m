% This script makes t-tests at the contact level between two conditions
% Be careful to use a different contrast number than in script :
% c_BPF_iEEG_glm_contact_level_ConfiDecid_cognition
%
% It requires that each frequency of interest has been extracted and time-locked on the events of interest
%
% To make ROI level stats and figure see : d_BPF_group_level_on_parcels

clear;
close all;

%% Contrastes

% 17 = T-test agent vs. proba trials, choice onset locked
% 18 = T-test feedback (+) vs. (-) trials, feedback onset locked

%% Options

init.cont      = 18;         % Choose contrasts to run
init.freq_band = {[50,150]}; % Choose frequency bands to analyse ({[4,8], [8,13], [13,33], [33,49], [50,150]})
init.smoothing = 250;        % Choose smoothing to analyse
init.fsample   = 100;        % Sample frequency of files to analyse
init.parcels   = {'all'};    % Name of specific parcels or 'all'

init.perm_test = 0;          % Perform permutation tests (1 = yes / 0 = no)
init.perm_nb   = 300;        % Number of permutations

% All parcels
% {'PFCvm','aINS_dors','aINS_vent','aINS','pINS','Thalamus','Caudate','Putamen','Pallidum','Hippocampus','Amygdala','Accumbens',...
% 'OFCv','OFCvm','OFCvl','Pfrdls','Pfrdli','VCl','VCcm','ICC','ITCm','VCs','Cu','VCrm','ITCr','MTCc','MTCr','STCc','STCr','IPCv','IPCd',...
% 'SPC','SPCm','PCm','Sv','Sdl','Sdm','PCC','MCC','ACC','Mv','Mdl','Mdm','PMrv','PMdl','PMdm','PFcdl','PFcdm','PFrvl','PFrd','PFrm'};

%% Initialisation

% Patients :
% 'GRE_2019_BAUj','GRE_2019_PLEc','GRE_2019_AUBa','GRE_2019_CHRc','GRE_2019_GEOa',...
% 'LYO_2019_IBRa','REN_2019_BAIp','REN_2019_LAPp','REN_2019_LEBc','LYO_2019_CAPp',...
% 'GRE_2019_BERj','GRE_2020_BERl','GRE_2020_SOLm','GRE_2020_FOUt',...'PRA_2020_PRGa',...
% 'PRA_2020_PRAb','GRE_2020_SULg','PRA_2020_PRAc','GRE_2020_JERr','GRE_2020_GENj',...
% 'GRE_2020_KAYc'

%-------------------------------------------------------------------------%

init.pat_names = {'GRE_2019_BAUj','GRE_2019_PLEc','GRE_2019_AUBa','GRE_2019_CHRc','GRE_2019_GEOa',...
    'LYO_2019_IBRa','REN_2019_BAIp','REN_2019_LAPp','REN_2019_LEBc','LYO_2019_CAPp',...
    'GRE_2019_BERj','GRE_2020_BERl','GRE_2020_SOLm','GRE_2020_FOUt',...'PRA_2020_PRGa',...
    'PRA_2020_PRAb','GRE_2020_SULg','PRA_2020_PRAc','GRE_2020_JERr','GRE_2020_GENj',...
    'GRE_2020_KAYc'}; % Ajouter les sujets ici

% Define BDD path according to computer name
name = getenv('COMPUTERNAME');
if ismember(name, 'HP-ROMANE')
    init.bdd_path = 'C:\Users\Romane\Documents\MATLAB\BDD_ConfiDecid';
elseif ismember(name, 'DESKTOP-IHJ3T8M')
    init.bdd_path = 'C:\Users\Romane\Documents\BDD_ConfiDecid';
end

init.task_path = fullfile(init.bdd_path, 'Cognitive_task');
init.subj_path = fullfile(init.task_path, 'Patients');
init.behav_path = fullfile(init.task_path, 'Comportement'); % Path where model files are located
init.anat_path = fullfile(init.bdd_path, 'Anatomie'); % Path where .CSV files are located
init.backuproot = fullfile(init.task_path, 'BPF_analyzes', 'Group_stat_BPF'); % Path where new files will be saved
%-------------------------------------------------------------------------%

init.task = 'ConfiDecid';

% Load theoretical mood data
mood_data_fullname = fullfile(init.behav_path,'all_mood_data.mat');
load(mood_data_fullname, 'mood_data'); % Load 'mood_data' structure

% Load choice model data
choice_data_fullname = fullfile(init.behav_path,'all_choice_data.mat');
load(choice_data_fullname, 'choice_data'); % Load 'choice_data' structure

%% Boucle � travers les sujets

for pat_idx = 1:length(init.pat_names)
    tic;
    
    clearvars -except init mood_data choice_data pat_idx
    
    subjName = init.pat_names{pat_idx};
    patfile = fullfile(init.subj_path, subjName);
    
    backup_data       = 1;
    removebadchannels = 1;
    
    %% Load EPI_BPF_stat.mat of the subject if exist (the file saved at the end of this script)
    
    filename = sprintf('%s_%s_BPF_stat.mat', subjName, init.task);
    filebackup = fullfile(init.backuproot, sprintf('sf%d', init.fsample), filename);
    if exist(filebackup, 'file')
        load(filebackup, 'store');
    end
    
    if ~exist(fileparts(filebackup), 'dir') % Si le dossier n'existe pas on le cr��
        mkdir(fileparts(filebackup));
    end
    
    %% STEP 1: LOAD BEHAVIORAL DATA
    
    % Trouver les sessions du sujet
    raw_behav_folder = fullfile(patfile, 'RAW', 'Comportement');
    filelist = dir(raw_behav_folder); % Liste de tous les fichiers du dossier
    name = {filelist.name};
    
    all_sess_name = name(~cellfun(@isempty, regexp(name, sprintf('%s_%s[1-9]_cognition_data.mat', subjName, init.task), 'match'))); % Sessions de 1 � X
    
    % Load data matrix
    subjdata = [];
    for sess = 1:length(all_sess_name) % Loop over sessions
        
        data_filename = fullfile(raw_behav_folder, all_sess_name{sess});
        load(data_filename, 'data'); % Charger la matrice data de la session
        subjdata = [subjdata ; data]; % Matrice contenant les donn�es de toutes les sessions pour un sujet
        
    end % Fin de la boucle � travers les sessions
    
    %% Loop over frequency bands and smoothings
    
    for bpf = 1:length(init.freq_band) % Loop over filtered frequency bands
        
        f_range = init.freq_band{bpf}; % Frequency range
        f_range_name = sprintf('f%df%d', min(f_range), max(f_range));
        
        for smooth = 1:length(init.smoothing) % Loop over smoothings
            
            smoothing = sprintf('sm%d',init.smoothing(smooth));
            
            %% STEP 2: LOAD EEG DATA
            
            bpffilename = fullfile(patfile, 'Analyses', 'BPF', sprintf('%s_%s_f%df%d_sf%d_sm%d', subjName, init.task, min(f_range), max(f_range), init.fsample, init.smoothing(smooth)));
            D = spm_eeg_load(bpffilename);
            
            % Load data
            alldata = permute(D(:,:,:), [3 2 1]); % Ntrials x Ntimes x Nchan
            
            m_cond = str2double(D.conditions'); % Events order used to create the BPF matrix
            timelist = D.time;
            chanlabels = D.chanlabels;
            
            % We will use 'alldata' as the data matrix to perform single channel statistics
            
            %% STEP 3: APPLY SOME OPTIONS TO EEG DATA
            
            if removebadchannels == 1
                badchan = [];
                sess = unique(subjdata(:,2));
                for sbc = 1:length(sess) % Loop over sessions
                    bcfilename = fullfile(patfile, 'Analyses', 'BadChannel', sprintf('sess%d', sess(sbc)), sprintf('bc_sess%d.mat', sess(sbc)));
                    BC = spm_eeg_load(bcfilename);
                    badchan = [badchan BC.badchannels];
                end % End of the loop over sessions
                badchan = unique(badchan); % Delete the channel if it's bad in at least one session
                alldata(:,:,badchan) = [];
                chanlabels(badchan) = [];
            end
            
            %% Find channels in specified parcel
            
            if ismember(init.parcels, 'all')
                ROI_chan_idx = 1:length(chanlabels);
            else
                ROI_chan_idx = [];
                
                % Get parcel infos for each sEEG channel from .CSV file
                csvfile = fullfile(init.anat_path, subjName, sprintf('%s.csv', subjName));
                csv = csv2anat2(csvfile, chanlabels);
                
                for p = 1:length(init.parcels)
                    % Specify all the contraints that will be used to define a given ROI
                    constraints = anat_constraints(init.parcels{p});
                    idx = test_chan_parcel(csv, constraints);
                    ROI_chan_idx = [ROI_chan_idx ; idx];
                end
                ROI_chan_idx = unique(ROI_chan_idx);
            end
            
            %% STEP 4: LOOP OVER CONTRASTS
            
            for tidx = 1:length(init.cont) % Loop over contrasts
                
                clear opt
                
                % Clear data if contrasts already existed
                
                if exist('store','var') && isfield(store, f_range_name) && isfield(store.(f_range_name), smoothing) &&...
                        init.cont(tidx) <= length(store.(f_range_name).(smoothing)) && ~isempty(store.(f_range_name).(smoothing)(init.cont(tidx)).options) % If the contrast already exist
                    store.(f_range_name).(smoothing)(init.cont(tidx)) = structfun(@(x) [], store.(f_range_name).(smoothing)(init.cont(tidx)), 'UniformOutput', false); % Empty it
                end
                
                %% Load Permutation_tests\XXX_20XX_NNNp\contrastX.mat if exists
                
                if init.perm_test == 1 % Permutation tests
                    
                    permbackup = fullfile(init.backuproot, sprintf('sf%d', init.fsample), 'Permutation_tests', subjName, sprintf('contrast%d.mat', init.cont(tidx)));
                    
                    if exist(permbackup, 'file') % Si le contrast existe d�j�
                        load(permbackup, 'perm_tests')
                        if isfield(perm_tests, f_range_name) && isfield(perm_tests.(f_range_name), smoothing) && isfield(perm_tests.(f_range_name).(smoothing), 'dots') % if perm_test for the freq and smoothing considered already exist, empty it
                            if pat_idx == 1 && size(perm_tests.(f_range_name).(smoothing).dots{1},3) > init.perm_nb
                                answer = questdlg(sprintf('Attention : Le nombre de permutation est inf�rieur � celui existant.\nLancer quand m�me l''analyse ?'), 'Warning', 'Oui', 'Non', 'Non');
                                if strcmp(answer, 'Non')
                                    return
                                end
                            end
                            perm_tests.(f_range_name).(smoothing).dots = {};
                        end
                    end
                end
                
                %% Define contrasts
                
                switch init.cont(tidx)
                    
                    case {17} % T-test agent vs. proba trials
                        
                        opt.contrastname = 'ttest-agent-vs-proba-trials';
                        opt.voi_name = 'agent-vs-proba';
                        
                        % Define trials used to perform the correlation
                        opt.time_lock = 'choice-onset';
                        opt.idx{:,1} = find(ismember(m_cond, 10:13)); % Choice onset of AGENT trials
                        opt.idx{:,2} = find(ismember(m_cond, 20:23)); % Choice onset of NON AGENT trials
                        
                        opt.exploretw = [-1 5]; % Time window for data exploration/visualisation
                        opt.xlabel = 'time (s) relative to choice onset';
                        
                    case {18} % T-test feedback (+) vs. (-) trials
                        
                        opt.contrastname = 'ttest-feedback-pos-vs-neg';
                        opt.voi_name = 'fb-pos-vs-neg';
                        
                        % Define trials used to perform the correlation
                        opt.time_lock = 'feedback-onset';
                        Aidx = find(ismember(m_cond, 16)); % Feedback onset
                        opt.idx{:,1} = Aidx(subjdata(:,12) == 1); % Feedback onset of positive feedbacks
                        opt.idx{:,2} = Aidx(subjdata(:,12) == 0); % Feedback onset of negative feedbacks
                        
                        opt.exploretw = [-0.5 3]; % Time window for data exploration/visualisation
                        opt.xlabel = 'time (s) relative to feedback onset';
                        
                end
                
                %% Loop over ROI's channels
                
                filterin = timelist >= opt.exploretw(1) & timelist <= opt.exploretw(2);
                
                for chan = 1:length(ROI_chan_idx) % Loop over selected channels
                    
                    % Do the t-test
                    
                    opt.data1 = alldata(opt.idx{:,1}, filterin, chan); % Trials x Time
                    opt.data2 = alldata(opt.idx{:,2}, filterin, chan); % Trials x Time
                    
                    dots = (squeeze(mean(opt.data1, 1)) - squeeze(mean(opt.data2, 1)))'; % A - B
                    
                    if size(opt.data1,1) == size(opt.data2,1)
                        [~, p, ~, stats] = ttest(opt.data1, opt.data2); % Calcul des ttest
                    else
                        [~, p, ~, stats] = ttest2(opt.data1, opt.data2); % Calcul des ttest
                    end
                    
                    t = squeeze(stats.tstat)';
                    dots_err = p';
                    
                    opt.var_of_interest = dots; % Kept for compatibility
                    
                    %% Permuation tests
                    
                    if init.perm_test == 1
                        
                        % MEX FUNCTION (5 time faster)
                        % Work ONLY for : linear regressions | distr:normal | link:identity
                        [dots_perm_mex, t_perm_mex, dots_err_perm_mex] = permutations_mex(opt.var_of_interest, responses, opt.glm_predictor, size(dots), init.perm_nb);
                        
                        % CODE TO BE USED IF MEX FUNCTION CANNOT BE USED (e.g. logistic regression)
                        % dots_perm = NaN(size(dots, 1), size(dots, 2) , init.perm_nb);
                        % t_perm = dots_perm;
                        % dots_err_perm = dots_perm;
                        %
                        % for perm = 1:init.perm_nb
                        %
                        %     perm_reg = Shuffle(opt.var_of_interest, 2); % Shuffle lines of our variable(s) of interest (matching between VOIs is maintained)
                        %
                        %     for timi = 1:size(dots, 1) % R�gression � chaque temps
                        %
                        %         if ismember(opt.glm_predictor, 'voi') % Variable of interest predicts activity
                        %             prdct = perm_reg;
                        %             resp = responses(:,timi);
                        %         elseif ismember(opt.glm_predictor, 'activity') % Activity predicts variable of interest
                        %             prdct = predictors(:,timi);
                        %             resp = perm_reg;
                        %         end
                        %
                        %         [b_perm,~,stats_perm] = glmfit(prdct, resp);
                        %         dots_perm(timi,:,perm) = b_perm(2:end)'; % Regression coefficient (= slope) of the regression line
                        %         t_perm(timi,:,perm) = stats_perm.t(2:end)';
                        %         dots_err_perm(timi,:,perm) = stats_perm.p(2:end)';
                        %
                        %     end % End of the loop over time points
                        % end % End of the loop over permutation number
                        
                    end % End of the condition "if init.perm_test == 1"
                    
                    %% Store
                    
                    store.(f_range_name).(smoothing)(init.cont(tidx)).dots{ROI_chan_idx(chan)} = dots; % We store regression coefficients for each time point and channel
                    store.(f_range_name).(smoothing)(init.cont(tidx)).tstat{ROI_chan_idx(chan)} = t; % On stock les t-values
                    store.(f_range_name).(smoothing)(init.cont(tidx)).pval{ROI_chan_idx(chan)} = dots_err; % On stock les p-values
                    
                    if init.perm_test == 1 % Permutation tests
                        
                        perm_tests.(f_range_name).(smoothing).dots{ROI_chan_idx(chan)} = dots_perm; % Time x Nb regressor x Nb permutation
                        perm_tests.(f_range_name).(smoothing).tstat{ROI_chan_idx(chan)} = t_perm;
                        perm_tests.(f_range_name).(smoothing).pval{ROI_chan_idx(chan)} = dots_err_perm;
                        
                    end
                    
                end % Fin de la boucle � travers les channels
                
                store.(f_range_name).(smoothing)(init.cont(tidx)).options = opt; % Save contrast options
                store.(f_range_name).(smoothing)(init.cont(tidx)).options.filtered_timelist = timelist(filterin);
                
                % Permutation tests
                if init.perm_test == 1
                    perm_tests.(f_range_name).(smoothing).contrastname = opt.contrastname;
                    
                    if ~exist(fileparts(permbackup), 'dir') % Si le dossier n'existe pas on le cr��
                        mkdir(fileparts(permbackup));
                    end
                    
                    save(permbackup, 'subjName', 'perm_tests', '-v7.3'); % If the file is > 2GB
                end
                
            end % Fin de la boucle � travers les contrastes
        end % Fin de la boucle � travers les smoothings
    end % Fin de la boucle � travers les bandes de fr�quence
    
    % Fill hdr structure
    hdr.dimord = 'trial_time_chan';
    hdr.dimsiz = size(alldata);
    hdr.conditions = m_cond;
    hdr.timelist = timelist;
    hdr.chanlist = chanlabels';
    
    if backup_data % Save intermediate results for group analysis
        if ~exist(fullfile(init.backuproot), 'dir')
            mkdir(fullfile(init.backuproot));
        end
        save(filebackup, 'subjName', 'hdr', 'store');
    end
    
    fprintf('Elapsed time for patient #%d (%s) is %.2f seconds\n', pat_idx, subjName, toc);
    
end % Fin de la boucle � travers les sujets

return % END

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
