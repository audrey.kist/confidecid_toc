% THEORETICAL MOOD LEVEL COMPUTATION USING THE VBA TOOLBOX FOR ONE SUBJECT
%
% INPUT : Structure
%   - S.subjname      = Subject name
%   - S.data          = 'data' matrix for the subject
%   - S.meanTML :   0 = don't ; 1 = use the mean of all subjects' parameters to compute TML
%   - S.plot :      0 = don't ; 1 = plot and save figure
%   - S.pathtofig     = Path where to save figure (needed only if S.plot = 1)
%   - S.cond          = Condition name (agent / non_agent)
%   - S.agent         = Condition number (1 = agent ; 0 = non agent)
%
% OUTPUT :
%   - mood_data structure

function mood_data = TML_ConfiDecid_VBA(S, mood_data)

if ~isempty(fieldnames(mood_data))
    inversionResult = mood_data.inversionResult;
end

best_model = 4;

% Supprimer notes d'humeur incorrectes
% [...]

% Interpolate real mood ratings
mood_pos = find(~isnan(S.data(:,14))); % Mood position
mood_val = zscore(S.data(mood_pos, 14)); % Mood value (zscored)
mood_nb = 1:length(S.data); % Number of mood ratings
interp_mood = interp1(mood_pos, mood_val, mood_nb, 'linear', 'extrap'); % Interpolated moods

if ismember(S.cond, 'all')
    isYout = zeros(1, length(S.data)); % Fit all trials
else
    isYout = (S.data(:,11) ~= S.agent)'; % Don't fit agent/non agent trials
end

tooFast = (S.data(:,15) < 1)'; % Don't fit trials in which mood RT is < 1 sec
isYout = isYout + tooFast;
isYout(isYout == 2) = 1;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% FABIEN'S CODE TO COMPUTE TML %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Define model space : produced a factorial combination of all possible options

param.modelSpace = VBA_factorial_struct(...
    'moodType', {'quizz'},...                           % Define which task is/are taken into account (quizz | propsect | both)
    'reinforcementType', {'R','combination'},...        % Define what variable is used to update mood (R | RPE | combination):  reward/feedback (R), reward prediction error (difference with mean correct response rate for quizz) or a weighted combination of R and expectation
    'learningRate', {'symetric'},...                    % Not used (kept for compatibility)
    'biasedReward', {'no','yes'},...                    % Define if perception of events is modulated by current mood (e.g R = R + kMood * mood) (yes | no)
    'time', {'outDeltaRule'},...                        % Define to what extent there is an effect of time on mood (no | outDeltaRule). Note that the effect of time is purely additive, and doesn't interact with mood update. In earlier version, there was an inDeltaRule option in which the effect of time was included in update.
    'kMood', {'yes'},...                                % Not used (kept for compatibility)
    'k0', {'yes'},...                                   % Define to what extent there is a free parameter defining a constant component (k0) in mood (yes | no)
    'gainLossAssymetry', {'no','yes'},...               % Define to what extent positive and negative events have separate weights on mood (yes | no)
    'choosenOption', {'no'});                           % Used only when taking into account the prospect (choice task): define to what extent you used the expected value of the prospect ("no") or the expected value of the chosen option (depending on accept or decline) (yes) to update mood

aga = VBA_factorial_struct(... % Second group of models - defined separately as full factorial combination could create non-sens models (e.g. choosenOption has no meaning when only the quizz task is taken into account)
    'moodType', {'prospect'},...
    'reinforcementType', {'R'},...
    'learningRate', {'symetric'},...
    'biasedReward', {'no'},...
    'time', {'outDeltaRule'},...
    'kMood', {'yes'},...
    'k0', {'yes'},...
    'gainLossAssymetry', {'no'},...
    'choosenOption', {'yes'});

aga2 = VBA_factorial_struct(...
    'moodType', {'both'},...
    'reinforcementType', {'R','combination'},...
    'learningRate', {'symetric'},...
    'biasedReward', {'no','yes'},...
    'time', {'outDeltaRule'},...
    'kMood', {'yes'},...
    'k0', {'yes'},...
    'gainLossAssymetry', {'no','yes'},...
    'choosenOption', {'yes'});

aga3 = VBA_factorial_struct(... % Control model (mood is a linear function of time)
    'moodType', {'null'},...
    'reinforcementType', {'no'},...
    'learningRate', {'no'},...
    'biasedReward', {'no'},...
    'time', {'outDeltaRule'},...
    'kMood', {'yes'},...
    'k0', {'yes'},...
    'gainLossAssymetry', {'no'},...
    'choosenOption', {'no'});

% Pool all models
param.modelSpace = [param.modelSpace; aga3]; % [param.modelSpace', aga', aga2', aga3];

% Define the index of the null model
null_model = find(ismember({param.modelSpace.moodType}, 'null'));

% Alternatively, you can select models in order to avoid redundant ones
% isGoodModel = ones(length(param.modelSpace), 1);
% isGoodModel(strcmp({param.modelSpace.moodType}, 'null') == 1 & strcmp({param.modelSpace.learningRate}, 'assymetric') == 1) = 0;
% param.modelSpace = param.modelSpace(isGoodModel == 1);

% Create a table to summarize all properties of model (and create a numeric vector to index options when not already done)
listField = fieldnames(param.modelSpace);
for iField = 1:length(listField)
    [param.modelSpace.(['numeric' listField{iField}])] = deal(param.modelSpace.(listField{iField}));
    param.modelSpace = struct.changeLabel(param.modelSpace, ['numeric' listField{iField}]);
    summary.tab(:,iField) = [param.modelSpace.(['numeric' listField{iField}])]';
end

param.nModel = length(param.modelSpace); % Number of models
summary.tab = [summary.tab (1:param.nModel)']; % Add a numeric index of models (1:nModels)
index = num2cell(1:param.nModel);
[param.modelSpace.index] = deal(index{:});

f_fname = @f_moodRL_keep_eldar_z;   % Evolution fonction
g_fname = @g_fitMood_0003;          % Observation function

%% Define priors

options = struct;
dim = struct('n',0, 'n_theta', 0, 'n_phi', 0);

[dim, options] = setPriors(...
    options, dim, 'X0',...
    'h1', 0, 0,... % A variable keeping trace of the quizz task (accumulating feedbacks or feedback prediction error)
    'h2', 0, 0,... % A variable keeping trace of the choice task
    'mood', 0, 0); % Predicted mood (combination of k0, h1, h2 and time)

% Free parameters of evolution function

if S.meanTML == 0
    [dim, options] = setPriors(...
        options, dim, 'theta',...
        'gamma', 0, 1.7,... % Forgetting factor
        'kTime', 0, 3,...   % Weight of time
        'k0', 0, 3,...      % Constant
        'kF', 0, 3,...      % Weight of the quizz task (feedback)
        'kP', 0, 3,...      % Weight of the choice task (prospect)
        'kE1', 0, 3,...     % Weight of expectations (if combination of feedback and expectation with separate weights is used)
        'kM', 0, 1,...      % kMood : impact of mood on perception of incidental events
        'R', 0, 1);         % Multiplicative factor for positive events (if different weight are used for positive and negative ones)
else
    [dim, options] = setPriors(...
        options, dim, 'theta',...
        'gamma', S.mean.gamma, 0,... % Forgetting factor
        'kTime', S.mean.kTime, 0,... % Weight of time
        'k0', S.mean.k0, 0,...       % Constant
        'kF', S.mean.kF, 0,...       % Weight of the quizz task (feedback)
        'kP', S.mean.kP, 0,...       % Weight of the choice task (prospect)
        'kE1', S.mean.kE1, 0,...     % Weight of expectations (if combination of feedback and expectation with separate weights is used)
        'kM', S.mean.kM, 0,...       % kMood : impact of mood on perception of incidental events
        'R', S.mean.R, 0);           % Multiplicative factor for positive events (if different weight are used for positive and negative ones)
end

inversionResult.FF = NaN(param.nModel,1); % Create a field to store model evidence
inversionResult.param = param;            % Keep trace of model definitions
inversionResult.options = options;        % Keep trace of options

% If you want to include models in which the choice task is taken into account into mood (e.g. expected utility of prospect may impact mood), then you will need the fit of the choice task !
filepath = fileparts(S.pathtofig);
decisionFit = load(fullfile(filepath, 'all_choice_data.mat'));

nTrial = length(interp_mood); % Number of trials

%% Define u (experimental factors)

feedback = S.data(:,12)'; % Feedbacks : 1 = positif ; 0 = n�gatif                                        % Actual feedbacks of the challenge
time = (1:nTrial);                                                                                       % Time (~ iTrial)

EV = NaN(1,length(S.data));                                                                              % Expected value of the prospect (choice task)
choice = EV;                                                                                             % Actual choices (choice task)
isToolate = EV;                                                                                          % Trials for which the participant didn't choose (and therefore got a penalty)

EV(S.data(:,11) == 1) = zscore(decisionFit.choice_data.agent.inversionResult.model(1).(S.subjname).posterior.muX(1,:));
EV(S.data(:,11) == 0) = zscore(decisionFit.choice_data.non_agent.inversionResult.model(1).(S.subjname).posterior.muX(1,:));

choice(S.data(:,11) == 1) = decisionFit.choice_data.agent.inversionResult.model(1).(S.subjname).out.y;
choice(S.data(:,11) == 0) = decisionFit.choice_data.non_agent.inversionResult.model(1).(S.subjname).out.y;

isToolate(S.data(:,11) == 1) = +decisionFit.choice_data.agent.inversionResult.model(1).(S.subjname).out.options.isYout;
isToolate(S.data(:,11) == 0) = +decisionFit.choice_data.non_agent.inversionResult.model(1).(S.subjname).out.options.isYout;

cEV = EV .* choice - 2*isToolate;                                                                        % Expected value of the choosen option (value of "decline" is fixed to zero, as in the best model of choice)

quizzEasyness = zeros(1, nTrial); % EV;                                                                                      % Percentage of correct response (through all subjects)

% EV = zeros(1, nTrial);  % Set to 0 (as we removed all prospect - related models)
% cEV = zeros(1, nTrial); % Set to 0 (as we removed all prospect - related models)

[u, dim, options] = setInput(options, dim,... % Create u
    'feedback',      2*feedback-1,...        % Feedback is either -1 or 1
    'quizzEasyness', (2*quizzEasyness-1),... % Thus, mean correctness is converted to [-1, 1]
    'iTrial',        time,...
    'time',          zscore(time')',...
    'EV',            [0 EV(1:(end-1))],...   % For choice-tasks related variable, each prospect impacts the next trial and not the current one.
    'choosenEV',     [0 cEV(1:(end-1))]);

%% Define y (data to fit)

y = interp_mood; % Behavior to explain

for iModel = [best_model, null_model] % Best model and null model % 1:param.nModel
    
    disp(['Model number ' num2str(iModel) ' : ' strcat(num2str(summary.tab(iModel,:)))]);
    
    % Model details
    
    dim.p = 1;                                          % Number of variable to explain
    options.sources.type = 0;                           % Type of data (binomial or not) (before : options.binomial)
    options.isYout = isYout; % zeros(1, nTrial);        % A binary variable indicating data to not fit
    options.skipf = isnan(feedback);                    % Skip evolution fonction for these trials
    
    options.verbose = 1;                                % Display text in command window
    options.DisplayWin = 1;                             % Plot nice figures
    
    options.inF.modelSpace = param.modelSpace(iModel);  % Options that will be used in evolution function
    options.inG.modelSpace = param.modelSpace(iModel);  % Options that will be used in observation function
    
    options.priors.a_alpha = Inf;                       % Not useful for deterministic models
    options.priors.b_alpha = 0;
    
    [aa,bb] = VBA_guessHyperpriors(y, [0.1, 0.9]);      % Hyperpriors (more or less "prior about noise") may be defined through the getHyperpriors function, that uses priors on the explainable fraction of the total variance of y. Rule of thumb: the influence of p_min / p_max is quite low, you could use the default (between .1 and .9)
    priors.a_sigma = aa;
    priors.b_sigma = bb;
    
    options.dim = dim;
    
    [posterior, out] = VBA_NLStateSpaceModel(y,u,f_fname,g_fname,dim,options);
    assert(~isempty(posterior), 'Empty model'); % Crash if model estimation didn't work
    inversionResult.model(iModel).(S.subjname).posterior = posterior; % Keep posterior
    inversionResult.model(iModel).(S.subjname).out = out; % Keep out
    
end

% Find index of the subject
subj_idx = find(strcmp(fieldnames(inversionResult.model), S.subjname));

%% Model comparison

for iModel = [best_model, null_model] % 1:param.nModel
    inversionResult.FF(iModel, subj_idx) = inversionResult.model(iModel).(S.subjname).out.F; % F is model evidence
end

% summary.tab=[summary.tab sum(FF(:,:,1),2)];
% % cond = summary.tab(:,2) ~= 2 & summary.tab(:,9) == 1
% cond = ones(size(summary.tab, 1), 1);
% op.DisplayWin =0;
% [p, o] = VBA_groupBMC(FF(cond==1,:), op);

%% Keep free parameters

for iModel = [best_model, null_model] % 1:param.nModel
    
    [~,theta,~] = getStateParamInput(inversionResult.model(iModel).(S.subjname).posterior.muX0, inversionResult.model(iModel).(S.subjname).posterior.muTheta, [], inversionResult.options.inF);
    % Transformation to actual free parameters
    theta.R = exp(theta.R);
    theta.gamma = 1./(1+exp(-theta.gamma));
    listField = fields(theta);
    nField = length(listField);
    
    for iField = 1:nField
        inversionResult.freeParameter.model(iModel).(listField{iField})(subj_idx) = theta.(listField{iField});
    end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% FIN DU CODE DE FABIEN %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Save data
mood_data.inversionResult = inversionResult;
mood_data.tested_Model = [best_model, null_model];
mood_data.best_model = best_model;
mood_data.null_model = null_model;

%% PLOT

TML_best = inversionResult.model(best_model).(S.subjname).out.suffStat.gx;

if S.plot == 1
    
    % Define colors
    color.feed.positive = [0.322, 0.808, 0.573];
    color.feed.negative = [1, 0.416, 0.40];
    color.grey = [0.5 0.5 0.5];
    
    fig = figure;
    hold on;
    
    % Plot the color of real data according to feedback
    if ismember(S.cond, 'all')
        posFeedIdx = find(S.data(:,12) == 1);
        negFeedIdx = find(S.data(:,12) == 0);
    else
        posFeedIdx = find(S.data(:,11) == S.agent & S.data(:,12) == 1);
        negFeedIdx = find(S.data(:,11) == S.agent & S.data(:,12) == 0);
    end
    YoutIdx    = find(isYout);
    
    plot(interp_mood, '--', 'LineWidth', 1, 'Color', color.grey, 'HandleVisibility', 'off') % Real data extrapolated
    scatter(posFeedIdx, interp_mood(posFeedIdx), 'o', 'LineWidth', 1, 'MarkerEdgeColor', color.feed.positive) % Positive feedbacks
    scatter(negFeedIdx, interp_mood(negFeedIdx), 'o', 'LineWidth', 1, 'MarkerEdgeColor', color.feed.negative) % Negative feedbacks
    scatter(YoutIdx, interp_mood(YoutIdx), 'o', 'LineWidth', 1, 'MarkerEdgeColor', color.grey) % Non fitted trials
    
    scatter(mood_pos(ismember(mood_pos, posFeedIdx)), mood_val(ismember(mood_pos, posFeedIdx)), 'filled', 'MarkerFaceColor', color.feed.positive); % Color positive feedbacks at actual mood ratings
    scatter(mood_pos(ismember(mood_pos, negFeedIdx)), mood_val(ismember(mood_pos, negFeedIdx)), 'filled', 'MarkerFaceColor', color.feed.negative); % Color negative feedbacks at actual mood ratings
    
    % Plot TML
    plot(TML_best, 'linewidth', 1, 'Color', color.grey) % TML best
    patch([time fliplr(time)], [inversionResult.model(best_model).(S.subjname).out.suffStat.gx - inversionResult.model(best_model).(S.subjname).out.suffStat.vy fliplr(inversionResult.model(best_model).(S.subjname).out.suffStat.gx + inversionResult.model(best_model).(S.subjname).out.suffStat.vy)],...
        color.grey, 'EdgeColor', 'none', 'FaceAlpha', .2, 'HandleVisibility', 'off'); % Confidence interval
    
    % Define ylim
    Ylim = [min([interp_mood, inversionResult.model(best_model).(S.subjname).out.suffStat.gx - inversionResult.model(best_model).(S.subjname).out.suffStat.vy]) - 0.5,...
        max([interp_mood, inversionResult.model(best_model).(S.subjname).out.suffStat.gx + inversionResult.model(best_model).(S.subjname).out.suffStat.vy]) + 0.5];
    
    ylim(Ylim);
    
    legend('Interpolated mood ratings (positive feedback)','Interpolated mood ratings (negative feedback)','Non fitted trials',...
        'Real mood ratings (positive feedback)','Real mood ratings (negative feedback)','TML (vba)','Location','bestoutside');
    xlabel('Trials');
    ylabel('Mood');
    title(S.subjname,'Interpreter', 'none');
    
    % Save figure
    fig_name = fullfile(S.pathtofig, sprintf('%s_mood_data_vs_model_%s.tiff', S.subjname, S.cond));
    fig.PaperPosition = [0 0 40 15]; % Fig size [left bottom width height]
    print(fig, fig_name, '-dtiff', '-r200');
    close(fig);
    
end

close all

end