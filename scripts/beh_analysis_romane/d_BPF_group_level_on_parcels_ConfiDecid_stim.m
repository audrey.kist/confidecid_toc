% Compute second-level stat (across contacts)
% Parcel infos for each sEEG channel are picked up from the CSV file
% and channels are identified as a function of defined constraints

clear;
% close all;

%% Options

init.fsample   = 100;        % Sample frequency of files to analyse
init.freq_band = {[50,150]}; % Choose frequency bands to analyse ({[4,8], [8,13], [13,33], [33,49], [50,150]})
init.smoothing = 250;        % Choose smoothings to analyse
init.contrast  = 1;          % Choose contrast to run
init.timewin   = [0 1];      % Choose time window for statistical analyses
init.exploretw = [-1 2];     % Choose time window for data exploration/visualisation ([-5.6 1])

init.clust_cor = 0;           % Find threshold for cluster correction
init.random_nb = 60000;       % Number of permutation for each parcel
init.elec_cor  = 0;           % Compute cluster correction of each electrode

init.parcel_names = {'aINS'};

% All parcels
% {'PFCvm','aINS_dors','aINS_vent','pINS','Thalamus','Caudate','Putamen','Pallidum','Hippocampus','Amygdala','Accumbens',...
%     'OFCv','OFCvm','OFCvl','Pfrdls','Pfrdli','VCl','VCcm','ICC','ITCm','VCs','Cu','VCrm','ITCr','MTCc','MTCr','STCc','STCr','IPCv','IPCd',...
%     'SPC','SPCm','PCm','Sv','Sdl','Sdm','PCC','MCC','ACC','Mv','Mdl','Mdm','PMrv','PMdl','PMdm','PFcdl','PFcdm','PFrvl','PFrd','PFrm'};

%% Initialisation

init.pat_names = {'GRE_2019_BAUj', 'GRE_2019_CHRc', 'GRE_2019_BERj', 'GRE_2020_BERl',...
    'GRE_2020_SOLm', 'GRE_2020_FOUt', 'GRE_2020_SULg'}; % Ajouter les sujets ici

init.bdd_path = 'C:\Users\Romane\Documents\MATLAB\BDD_ConfiDecid';
init.task_path = fullfile(init.bdd_path, 'Stim_task');

init.path = fullfile(init.task_path, 'Patients');
init.anat_path = fullfile(init.task_path, 'Anatomie'); % Path where are .csv files
init.behav_path = fullfile(init.task_path, 'Comportement'); % Path were are mood model infos
init.first_level_path = fullfile(init.task_path, 'BPF_analyzes', 'Group_stat_BPF', sprintf('sf%d', init.fsample)); % Path where are the first level analyzes (BPF_stat.mat)
init.second_level_path = fullfile(init.task_path, 'BPF_analyzes', 'ROI_analyses_BPF', sprintf('sf%d', init.fsample)); % Path where new files will be saved

init.task = 'ConfiDecid';
init.alpha = 0.05;

%% Load mood model infos

mood_model_fullname = fullfile(init.behav_path, 'all_mood_model_infos.mat');
load(mood_model_fullname, 'mood_model'); % Load 'mood_model' table

init.subj_constraints = mood_model; % No constraints
init.name_ext = '';

%% Colors definition

init.color.positive = [0.212, 0.624, 0.761]; % blue
init.color.negative = [1, 0.804, 0.4]; % yellow

%% Loop over parcels

for parcel = 1:length(init.parcel_names)
    
    close all
    clearvars -except init parcel parcels_data result
    
    [Tall, dots, pstat, tstat, store, chan_idx] = get_parcel_chan_BPF(init, parcel); % Find all the channels in a parcel through the subjects
    
    %% Plot group level on parcels and save parcels infos
    
    for bpf = 1:length(init.freq_band) % Loop over filtered frequency bands
        
        f_range_name = sprintf('f%df%d', min(init.freq_band{bpf}), max(init.freq_band{bpf})); % Ex : f50f150
        
        for smooth = 1:length(init.smoothing) % Loop over smoothings
            
            smoothing = sprintf('sm%d',init.smoothing(smooth));
            
            for contrast = init.contrast % Loop over contrasts
                
                store_folder.(f_range_name).(smoothing) = fullfile(init.second_level_path, f_range_name, smoothing, sprintf('%s%s', store.(f_range_name).(smoothing)(contrast).contrastname, init.name_ext));
                if ~exist(store_folder.(f_range_name).(smoothing), 'dir')
                    mkdir(store_folder.(f_range_name).(smoothing));
                end
                
                if contrast ~= init.no_timeCourse % If the contrast contains a time course
                    
                    if ~isempty(dots.(f_range_name).(smoothing){1,contrast}) % S'il y a des contacts dans la parcelle
                        
                        timelist = store.(f_range_name).(smoothing)(contrast).filtered_timelist; % Time list of the contrast
                        
                        if mean(mean(dots.(f_range_name).(smoothing){1,contrast}(timelist >= init.timewin(1) & timelist <= init.timewin(2),:),1)) > 0
                            plot_color = init.color.positive;
                        else
                            plot_color = init.color.negative;
                        end
                        
                        %% Compute cluster correction threshold
                        
                        if init.clust_cor == 1
                            permbackup = fullfile(init.first_level_path, 'Permutation_stats', sprintf('tw_%.2f_%.2f', init.timewin), sprintf('contrast%d_%s_%s_%s.mat', contrast, init.parcel_names{parcel}, f_range_name, smoothing));
                            
                            if exist(permbackup, 'file')
                                load(permbackup, 'permstat');
                            else
                                permstat.roi = [];
                                permstat.elec = [];
                            end
                            
                            if isempty(permstat.roi) || (init.elec_cor == 1 && isempty(permstat.elec)) % If fields 'roi' or 'elec' are empty
                                contrastname = store.(f_range_name).(smoothing)(contrast).contrastname;
                                permstat = cluster_correction_threshold(init, chan_idx, f_range_name, smoothing, contrast, timelist, permstat);
                                if ~exist(fileparts(permbackup), 'dir')
                                    mkdir(fileparts(permbackup));
                                end
                                save(permbackup, 'permstat', 'contrastname');
                            end
                        else
                            permstat.roi = [];
                            permstat.elec = [];
                        end
                        
                        %% Plot mean time course
                        
                        plot_mean_time_course(init, parcel, dots, store, f_range_name, smoothing, contrast, store_folder, timelist, plot_color, 'permutations', permstat.roi)
                        
                        %% Collect single contact statistics
                        
                        path2plot = fullfile(store_folder.(f_range_name).(smoothing), sprintf('%s_single_chan', init.parcel_names{parcel}));
                        Tall_chan = single_contact_stat_ROI_DECID(Tall, dots.(f_range_name).(smoothing){1,contrast}, pstat.(f_range_name).(smoothing){1,contrast}, tstat.(f_range_name).(smoothing){1,contrast}, init.timewin, timelist, init.alpha, 'permutations', permstat.elec); % , path2plot
                        Tallstat = join(Tall, Tall_chan); % Aggregate tables
                        
                        % Save the table containing the channels info for this ROI
                        xlsfilename = fullfile(store_folder.(f_range_name).(smoothing), sprintf('DECID_ROIs_%s.xlsx',store.(f_range_name).(smoothing)(contrast).contrastname));
                        
                        if ~exist(xlsfilename, 'file') % If the file doesn't exist yet
                            writetable(Tallstat, fullfile(store_folder.(f_range_name).(smoothing), sprintf('DECID_ROIs_%s.xlsx',store.(f_range_name).(smoothing)(contrast).contrastname)), 'Sheet', init.parcel_names{parcel}); % Write data
                        else
                            [~, sheetNames] = xlsfinfo(xlsfilename); % Retrieve sheet names
                            
                            if ~ismember(init.parcel_names(parcel),sheetNames) % If the sheet doesn't already exist
                                writetable(Tallstat, fullfile(store_folder.(f_range_name).(smoothing), sprintf('DECID_ROIs_%s.xlsx',store.(f_range_name).(smoothing)(contrast).contrastname)), 'Sheet', init.parcel_names{parcel}); % Write data
                            else % Clear sheet before writing
                                Excel = actxserver('Excel.Application'); % Open Excel as a COM Automation server
                                Workbook = Excel.Workbooks.Open(xlsfilename); % Open Excel workbook
                                cellfun(@(x) Excel.ActiveWorkBook.Sheets.Item(x).Cells.Clear, init.parcel_names(parcel)); % Clear the content of the sheets
                                Workbook.Save; % Save
                                Excel.Workbook.Close; % Close
                                invoke(Excel, 'Quit'); % Quit
                                delete(Excel) % Delete Excel actxserver
                                writetable(Tallstat, fullfile(store_folder.(f_range_name).(smoothing), sprintf('DECID_ROIs_%s.xlsx',store.(f_range_name).(smoothing)(contrast).contrastname)), 'Sheet', init.parcel_names{parcel}); % Write data
                            end
                        end
                        
                        %% Overall statistics
                        
                        tw_idx = timelist >= init.timewin(1) & timelist <= init.timewin(2);
                        stat_timelist = timelist(tw_idx); % Filtered timelist for statistical analyses
                        
                        for prdct = 1:size(store.(f_range_name).(smoothing)(contrast).regressor, 2) % Loop over predictors
                            
                            % Find best cluster
                            avg_dots = mean(dots.(f_range_name).(smoothing){prdct, contrast}(tw_idx,:),2); % Average of regressor estimates (time course)
                            
                            [h,~,~,stats] = ttest(dots.(f_range_name).(smoothing){prdct, contrast}(tw_idx,:)'); % Ttest on the time window we want to analye
                            
                            L = spm_bwlabel(h); %% Find clusters
                            
                            % Find the biggest cluster
                            stats_summary_tmp = NaN(max(L),4); % Initialization
                            for k = 1:max(L) % Analyse all clusters (numbered in L)
                                tmp = stat_timelist(L == k);
                                stats_summary_tmp(k,:) = [mean(avg_dots(L == k)), tmp(1), tmp(end), sum(stats.tstat(L == k))];
                                % (1) Moyenne des beta du cluster ; (2) Onset du cluster ; (3) Offset du cluster ; (4) Somme des t-values du cluster
                            end
                            
                            % Retrieve the data to put in the table
                            if isempty(stats_summary_tmp) % No cluster
                                best_clust = [0, NaN, NaN, NaN];
                                stats.tstat = NaN;
                                dots_err = NaN;
                                sig_chan = NaN;
                            else
                                best_clust_idx = abs(stats_summary_tmp(:,4)) == max(abs(stats_summary_tmp(:,4))); % Index of the best cluster
                                best_clust = stats_summary_tmp(best_clust_idx,:);
                                
                                if ~isempty(permstat.roi) % Permutation correction
                                    up_thresh = prctile(permstat.roi(:, 1, prdct), 100-init.alpha*100);
                                    down_thresh = prctile(permstat.roi(:, 2, prdct), init.alpha*100);
                                    if stats_summary_tmp(best_clust_idx,4) > up_thresh ||  stats_summary_tmp(best_clust_idx,4) < down_thresh % Best cluster significant
                                        stats.tstat = stats_summary_tmp(best_clust_idx,4); % Somme des t-values du plus gros cluster
                                        if best_clust(4) > 0 % Best cluster positive
                                            dots_err = length(find(permstat.roi(:, 1, prdct) > best_clust(4)))/length(permstat.roi(:, 1, prdct));
                                        elseif best_clust(4) < 0 % Best cluster negative
                                            dots_err = length(find(permstat.roi(:, 2, prdct) < best_clust(4)))/length(permstat.roi(:, 2, prdct));
                                        end
                                    else
                                        stats.tstat = NaN;
                                        dots_err = NaN;
                                    end
                                else % No correction
                                    [~, dots_err, ~, stats] = ttest(mean(dots.(f_range_name).(smoothing){1,contrast}(timelist >= best_clust(2) & timelist <= best_clust(3),:),1)); % Ttest de la moyenne des t-values du cluster � travers les �lectrodes
                                end
                            end
                            
                            % Collect summary info for this ROI and contrast
                            init.tsum.(f_range_name).(smoothing){prdct, contrast}(parcel).parcel_name = init.parcel_names{parcel};
                            init.tsum.(f_range_name).(smoothing){prdct, contrast}(parcel).nb_contacts = length(Tallstat.channel);
                            init.tsum.(f_range_name).(smoothing){prdct, contrast}(parcel).nb_patients = length(unique(Tallstat.subname));
                            init.tsum.(f_range_name).(smoothing){prdct, contrast}(parcel).best_cluster = [best_clust(2),best_clust(3)];
                            init.tsum.(f_range_name).(smoothing){prdct, contrast}(parcel).mean_regression_coef = best_clust(1);
                            init.tsum.(f_range_name).(smoothing){prdct, contrast}(parcel).tvalue = stats.tstat;
                            init.tsum.(f_range_name).(smoothing){prdct, contrast}(parcel).pvalue = dots_err;
                            init.tsum.(f_range_name).(smoothing){prdct, contrast}(parcel).nb_significant_patients = length(unique(Tallstat.subname(Tallstat.significance == 1)));
                            init.tsum.(f_range_name).(smoothing){prdct, contrast}(parcel).significant_contact_prop = mean(Tallstat.significance);
                            init.tsum.(f_range_name).(smoothing){prdct, contrast}(parcel).nb_significant_contact = sum(Tallstat.significance);
                            
                        end % End of the loop over predictors
                        
                    else % There is no contact in the parcel
                        
                        init.tsum.(f_range_name).(smoothing){contrast}(parcel).parcel_name = init.parcel_names{parcel};
                        init.tsum.(f_range_name).(smoothing){contrast}(parcel).nb_contacts = 0;
                        
                    end % End of the 'if there are contacts in the parcel'
                    
                else % Contrasts without time course
                    
                    % Bar plot of each parcel
                    
                    for coef = 1:length(find(~cellfun(@isempty, dots.(f_range_name).(smoothing)(:,contrast)))) % Loop over contrast coefficients
                        if ~isempty(dots.(f_range_name).(smoothing){coef,contrast}) % S'il y a des contacts dans la parcelle
                            
                            init.notc.AVG_dots(parcel,coef) = mean(dots.(f_range_name).(smoothing){coef,contrast},2); % Average of regressor estimates across channels (fot the time interval specified in the contrast)
                            init.notc.SEM_dots(parcel,coef) = 1*(std(dots.(f_range_name).(smoothing){coef,contrast},[],2)./sqrt(size(dots.(f_range_name).(smoothing){coef,contrast},2))); % SEM of regressor estimates
%                             init.notc.AVG_dots(parcel,coef) = mean(dots.meanSubj.(f_range_name).(smoothing){coef,contrast},2); % Average of regressor estimates across channels (fot the time interval specified in the contrast)
%                             init.notc.SEM_dots(parcel,coef) = 1*(std(dots.meanSubj.(f_range_name).(smoothing){coef,contrast},[],2)./sqrt(size(dots.meanSubj.(f_range_name).(smoothing){coef,contrast},2))); % SEM of regressor estimates                            
                            
                            % Statistical test
                            [init.notc.h(parcel,coef),init.notc.p(parcel,coef),~,init.notc.stats(parcel,coef)] = ttest(dots.(f_range_name).(smoothing){coef,contrast});
%                             [init.notc.h(parcel,coef),init.notc.p(parcel,coef),~,init.notc.stats(parcel,coef)] = ttest(dots.meanSubj.(f_range_name).(smoothing){coef,contrast});
                            
                        end
                    end
                    
                    if parcel == length(init.parcel_names)
                        fig = figure;
                        hold on;
                        % grid on;
                        
                        bar(init.notc.AVG_dots);
                        errorbar(init.notc.AVG_dots,init.notc.SEM_dots,'Color','black','LineStyle','none');
                        
                        scatter(find(init.notc.h==1), init.notc.AVG_dots(init.notc.h == 1)+(init.notc.SEM_dots(init.notc.h == 1)+0.01).*sign(init.notc.AVG_dots(init.notc.h == 1)), '*k','sizeData',50,'linewidth',0.7);
                        
                        xticks(1:1:length(init.parcel_names));
                        xticklabels(init.parcel_names);
                        xtickangle(45);
                    end
                    
                    % Test de m�diation
                    [h,p,~,stats] = ttest(dots.(f_range_name).(smoothing){2,contrast}.*dots.(f_range_name).(smoothing){4,contrast});
%                     [h,p,~,stats] = ttest(dots.meanSubj.(f_range_name).(smoothing){2,contrast}.*dots.meanSubj.(f_range_name).(smoothing){4,contrast})
                    
                end % End of 'if the contrast contains time course'
            end % End of the loop across contrasts
        end % End of the loop over smoothings
    end % End of the loop over filtered frequency bands
    
end % Fin de la boucle � travers les parcelles

%% Save the table containing summary info for all ROIs

for bpf = 1:length(init.freq_band) % Loop over filtered frequency bands
    
    f_range_name = sprintf('f%df%d', min(init.freq_band{bpf}), max(init.freq_band{bpf})); % Ex : f50f150
    
    for smooth = 1:length(init.smoothing) % Loop over smoothings
        
        smoothing = sprintf('sm%d',init.smoothing(smooth));
        
        for contrast = init.contrast % Loop over contrasts
            
            if contrast ~= init.no_timeCourse % If the contrast contains a time course
                
                for prdct = 1:size(store.(f_range_name).(smoothing)(contrast).regressor, 2) % Loop over predictors
                    
                    Tsum = struct2table(init.tsum.(f_range_name).(smoothing){prdct, contrast});
                    
                    % Save the table containing infos for all ROIs
                    xlsfilesum = fullfile(store_folder.(f_range_name).(smoothing), sprintf('DECID_ROIs_%s_summary.xlsx',store.(f_range_name).(smoothing)(contrast).contrastname));
                    sheetname = sprintf('best cluster ; prdct = %d', prdct);
                    
                    if ~exist(xlsfilesum, 'file') % If the file doesn't exist yet
                        writetable(Tsum, xlsfilesum, 'Sheet', sheetname); % Write data
                    else
                        [~, sheetNames] = xlsfinfo(xlsfilesum); % Retrieve sheet names
                        
                        if ~ismember(sheetname,sheetNames) % If the sheet doesn't already exist
                            writetable(Tsum, xlsfilesum, 'Sheet', sheetname); % Write data
                        else % Clear sheet before writing
                            Excel = actxserver('Excel.Application'); % Open Excel as a COM Automation server
                            Workbook = Excel.Workbooks.Open(xlsfilesum); % Open Excel workbook
                            cellfun(@(x) Excel.ActiveWorkBook.Sheets.Item(x).Cells.Clear, {sheetname}); % Clear the content of the sheets
                            Workbook.Save; % Save
                            Excel.Workbook.Close; % Close
                            invoke(Excel, 'Quit'); % Quit
                            delete(Excel) % Delete Excel actxserver
                            writetable(Tsum, xlsfilesum, 'Sheet', sheetname); % Write data
                        end
                    end
                    
                end % End of the loop over predictors
            end % End of 'if the contrast contains time course'
        end % Enf of the loop across contrasts
    end % End of the loop over smoothings
end % End of the loop over filtered frequency bands

close all
