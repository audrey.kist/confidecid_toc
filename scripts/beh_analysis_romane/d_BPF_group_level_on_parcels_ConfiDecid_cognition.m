% Compute second-level stat (across contacts)
% Parcel infos for each sEEG channel are picked up from the CSV file
% and channels are identified as a function of defined constraints

clear;
% close all;

%% Options

init.contrast  = 25;         % Choose contrasts to run

init.fsample   = 100;         % Sample frequency of files to analyse
init.freq_band = {[50,150]};  % Choose frequency bands to analyse ({[4,8], [8,13], [13,33], [33,49], [50,150]})
init.smoothing = 250;         % Choose smoothings to analyse

init.clust_cor = 0;           % Find threshold for cluster correction
init.random_nb = 60000;       % Number of permutation for each parcel
init.elec_cor  = 0;           % Compute cluster correction of each electrode (/!\ Not codded for multiple predictors in 'cluster_correction_threshold' = TO DO)

init.parcel_names = {'PFCvm','aINS'}; % Parcel names || 'all'

if ismember(init.parcel_names, {'all'}) % All parcels
    init.parcel_names = {'PFCvm','aINS','aINS_dors','aINS_vent','pINS','Thalamus','Caudate','Putamen','Pallidum','Hippocampus','Amygdala','Accumbens',...
        'OFCv','OFCvm','OFCvl','Pfrdls','Pfrdli','VCl','VCcm','ICC','ITCm','VCs','Cu','VCrm','ITCr','MTCc','MTCr','STCc','STCr','IPCv','IPCd',...
        'SPC','SPCm','PCm','Sv','Sdl','Sdm','PCC','MCC','ACC','Mv','Mdl','Mdm','PMrv','PMdl','PMdm','PFcdl','PFcdm','PFrvl','PFrd','PFrm'};
end

%% Initialisation

init.pat_names = {'GRE_2019_BAUj','GRE_2019_PLEc','GRE_2019_AUBa','GRE_2019_CHRc','GRE_2019_GEOa',...
    'LYO_2019_IBRa','REN_2019_BAIp','REN_2019_LAPp','REN_2019_LEBc','LYO_2019_CAPp',...
    'GRE_2019_BERj','GRE_2020_BERl','GRE_2020_SOLm','GRE_2020_FOUt','PRA_2020_PRAb',...
    'GRE_2020_SULg','PRA_2020_PRAc','GRE_2020_JERr','GRE_2020_GENj','GRE_2020_KAYc',...
    'GRE_2021_GUIl'}; % Ajouter les sujets ici

% Excluded patients : 'PRA_2020_PRGa', 'PRA_2020_PRAd'

% Define BDD path according to computer name
name = getenv('COMPUTERNAME');
if ismember(name, 'HP-ROMANE')
    init.bdd_path = 'C:\Users\Romane\Documents\MATLAB\BDD_ConfiDecid';
elseif ismember(name, 'DESKTOP-IHJ3T8M')
    init.bdd_path = 'C:\Users\Romane\Documents\BDD_ConfiDecid';
end

init.task_path = fullfile(init.bdd_path, 'Cognitive_task');
init.path = fullfile(init.task_path, 'Patients');
init.anat_path = fullfile(init.bdd_path, 'Anatomie'); % Path where .CSV files are located

init.first_level_path = fullfile(init.task_path, 'BPF_analyzes', 'Group_stat_BPF', sprintf('sf%d', init.fsample)); % Path where first level analyzes (BPF_stat.mat) are located
init.second_level_path = fullfile(init.task_path, 'BPF_analyzes', 'ROI_analyses_BPF', sprintf('sf%d', init.fsample)); % Path where new files will be saved

init.task = 'ConfiDecid';
init.alpha = 0.05;

init.badChar = '<>:"/\|?*'; % Forbidden characters for file/folder

%% Loop over parcels

for parcel = 1:length(init.parcel_names)
    
    close all
    clearvars -except init parcel
    
    [Tall, parcel_bpf, contrasts_options, chan_idx] = get_parcel_chan_BPF_ConfiDecid(init, parcel); % Find all the channels in a parcel through the subjects
    
    %% Plot group level on parcels and save parcels infos
    
    for bpf = 1:length(init.freq_band) % Loop over filtered frequency bands
        
        f_range_name = sprintf('f%df%d', min(init.freq_band{bpf}), max(init.freq_band{bpf})); % Ex : f50f150
        
        for smooth = 1:length(init.smoothing) % Loop over smoothings
            
            smoothing = sprintf('sm%d',init.smoothing(smooth));
            
            for contrast = 1:length(init.contrast) % Loop over contrasts
                if ~isempty(parcel_bpf) % S'il y a des contacts dans la parcelle
                    
                    ctrst = init.contrast(contrast);
                    dots = parcel_bpf.(f_range_name).(smoothing)(ctrst).dots;
                    pstat = parcel_bpf.(f_range_name).(smoothing)(ctrst).pval;
                    tstat = parcel_bpf.(f_range_name).(smoothing)(ctrst).tstat;
                    
                    % Define time windows according to contrast options
                    switch contrasts_options(ctrst).time_lock
                        case 'choice-onset'
                            init.timewin   = [0 3]; % Time window for statistical analyses
                            init.exploretw = [-1 3]; % Time window for data exploration/visualisation
                        case 'next-choice-onset-baseline'
                            init.timewin   = [-4 0]; % Time window for statistical analyses
                            init.exploretw = [-5 1]; % Time window for data exploration/visualisation
                        case {'choice-resp', 'mood-resp'}
                            init.timewin   = [-5 0]; % Time window for statistical analyses
                            init.exploretw = [-5 1]; % Time window for data exploration/visualisation
                        case {'mood-onset'}
                            init.timewin   = [0 5]; % Time window for statistical analyses
                            init.exploretw = [-2 5]; % Time window for data exploration/visualisation
                        case {'feedback-onset'}
                            init.timewin   = [0 1]; % Time window for statistical analyses
                            init.exploretw = [-0.5 3]; % Time window for data exploration/visualisation
                    end
                    
                    % Determine the folder name where figures will be saved according to the contrast name
                    ctrst_name = sprintf('%s-%s', replace(contrasts_options(ctrst).contrastname, ' ', ''), contrasts_options(ctrst).time_lock); % Remove white spaces
                    ctrst_name(ismember(ctrst_name, init.badChar)) = ''; % Remove forbidden characters
                    
                    store_folder = fullfile(init.second_level_path, f_range_name, smoothing, ctrst_name);
                    if ~exist(store_folder, 'dir')
                        mkdir(store_folder);
                    end
                    
                    timelist = contrasts_options(ctrst).filtered_timelist; % Time list of the contrast
                    
                    %% Compute cluster correction threshold
                    
                    if init.clust_cor == 1
                        permbackup = fullfile(init.first_level_path, 'Permutation_stats', sprintf('tw_%.2f_%.2f', init.timewin), sprintf('contrast%d_%s_%s_%s.mat', ctrst, init.parcel_names{parcel}, f_range_name, smoothing));
                        
                        if exist(permbackup, 'file')
                            load(permbackup, 'permstat');
                        else
                            permstat.roi = [];
                            permstat.elec = [];
                            
                            % Load permutation tests for all subjects
                            if ~isfield('init', 'perm_tests_all') % If the field "perm_tests_all" does not already exist
                                for pat_idx = 1:length(init.pat_names)
                                    
                                    perm_file = fullfile(init.first_level_path, "Permutation_tests", init.pat_names{pat_idx}, sprintf('contrast%d.mat', init.contrast)); % File containing permutations for 1 patient and 1 contrast
                                    load(perm_file, 'perm_tests'); % Load variables 'perm_tests'
                                    
                                    for freq = 1:length(init.freq_band) % Loop over filtered frequency bands
                                        freq_name = sprintf('f%df%d', min(init.freq_band{freq}), max(init.freq_band{freq})); % Ex : f50f150
                                        for smoo = 1:length(init.smoothing) % Loop over smoothings
                                            sm_name = sprintf('sm%d',init.smoothing(smoo));
                                            init.perm_tests_all(pat_idx).(freq_name).(sm_name) = perm_tests.(freq_name).(sm_name);
                                        end
                                    end
                                    clearvars perm_tests
                                end % End of the loop over subjects
                            end % End of the condition
                            
                        end
                        
                        if isempty(permstat.roi) || (init.elec_cor == 1 && isempty(permstat.elec)) % If fields 'roi' or 'elec' are empty
                            permstat = cluster_correction_threshold(init, chan_idx, init.perm_tests_all(:,contrast), f_range_name, smoothing, timelist, permstat);
                            if ~exist(fileparts(permbackup), 'dir')
                                mkdir(fileparts(permbackup));
                            end
                            save(permbackup, 'permstat', 'folder_name');
                        end
                    else
                        permstat.roi = [];
                        permstat.elec = [];
                    end
                    
                    %% Plot mean time course
                    
                    plot_mean_time_course_ConfiDecid(init, dots, timelist, init.parcel_names{parcel}, contrasts_options(ctrst), store_folder, 'permutations', permstat.roi)
                    
                    %% Collect single contact statistics
                    
                    path2plot = fullfile(store_folder, sprintf('%s_single_chan', init.parcel_names{parcel})); % Path where to plot single contacts
                    Tall_chan = single_contact_stat_ROI_ConfiDecid(Tall, dots, pstat, tstat, init.timewin, timelist, init.alpha, 'permutations', permstat.elec); % , path2plot
                    Tallstat = join(Tall, Tall_chan); % Aggregate tables
                    
                    % Save the table containing the channels info for this ROI
                    xlsfilename = fullfile(store_folder, sprintf('%s_ROIs_%s.xlsx', init.task, ctrst_name));
                    
                    if size(xlsfilename, 2) > 218 % Excel limit
                        xlsfilename = fullfile(store_folder, sprintf('%s_ROIs_%s.xlsx', init.task, ctrst_name(1:end-(size(xlsfilename,2)-218))));
                    end
                    
                    if ~exist(xlsfilename, 'file') % If the file doesn't exist yet
                        writetable(Tallstat, xlsfilename, 'Sheet', init.parcel_names{parcel}); % Write data
                        
                        % Delete the default sheets
                        Excel = actxserver('Excel.Application'); % Open Excel as a COM Automation server
                        Workbook = Excel.Workbooks.Open(xlsfilename); % Open Excel workbook
                        Excel.ActiveWorkBook.Sheets.Item('Feuil1').Delete;
                        Excel.ActiveWorkBook.Sheets.Item('Sheet2').Delete;
                        Excel.ActiveWorkBook.Sheets.Item('Sheet3').Delete;
                        Workbook.Save; % Save
                        Excel.Workbook.Close; % Close
                        invoke(Excel, 'Quit'); % Quit
                        delete(Excel) % Delete Excel actxserver
                        
                    else % The Excel file already exists
                        [~, sheetNames] = xlsfinfo(xlsfilename); % Retrieve sheet names
                        
                        if ~ismember(init.parcel_names(parcel),sheetNames) % If the sheet doesn't already exist
                            writetable(Tallstat, xlsfilename, 'Sheet', init.parcel_names{parcel}); % Write data
                        else % Clear sheet before writing
                            Excel = actxserver('Excel.Application'); % Open Excel as a COM Automation server
                            Workbook = Excel.Workbooks.Open(xlsfilename); % Open Excel workbook
                            cellfun(@(x) Excel.ActiveWorkBook.Sheets.Item(x).Cells.Clear, init.parcel_names(parcel)); % Clear the content of the sheets
                            Workbook.Save; % Save
                            Excel.Workbook.Close; % Close
                            invoke(Excel, 'Quit'); % Quit
                            delete(Excel) % Delete Excel actxserver
                            writetable(Tallstat, xlsfilename, 'Sheet', init.parcel_names{parcel}); % Write data
                        end
                    end
                    
                    %% Overall statistics
                    
                    tw_idx = timelist >= init.timewin(1) & timelist <= init.timewin(2);
                    stat_timelist = timelist(tw_idx); % Filtered timelist for statistical analyses
                    
                    for prdct = 1:size(dots, 2) % Loop over predictors
                        
                        % Find best cluster
                        avg_dots = mean(dots{prdct}(tw_idx,:), 2); % Average of regressor estimates (time course)
                        
                        [h,~,~,stats] = ttest(dots{prdct}(tw_idx,:)'); % Ttest on the time window we want to analye
                        
                        L = spm_bwlabel(h); %% Find clusters
                        
                        % Find the biggest cluster
                        stats_summary_tmp = NaN(max(L),4); % Initialization
                        for k = 1:max(L) % Analyse all clusters (numbered in L)
                            tmp = stat_timelist(L == k);
                            stats_summary_tmp(k,:) = [mean(avg_dots(L == k)), tmp(1), tmp(end), sum(stats.tstat(L == k))];
                            % (1) Moyenne des beta du cluster ; (2) Onset du cluster ; (3) Offset du cluster ; (4) Somme des t-values du cluster
                        end
                        
                        % Retrieve the data to put in the table
                        if isempty(stats_summary_tmp) % No cluster
                            best_clust = [0, NaN, NaN, NaN];
                            tvalue = NaN;
                            dots_err = NaN;
                            sig_chan = NaN;
                        else
                            best_clust_idx = abs(stats_summary_tmp(:,4)) == max(abs(stats_summary_tmp(:,4))); % Index of the best cluster
                            best_clust = stats_summary_tmp(best_clust_idx,:); % (1) Moyenne des beta ; (2) Onset ; (3) Offset ; (4) Somme des t-values
                            
                            if ~isempty(permstat.roi) % Permutation correction
                                
                                abs_thresh = prctile(permstat.roi(:, 1, prdct), 100-init.alpha*100);
                                
                                if abs(best_clust(4)) > abs_thresh % Best cluster significant
                                    tvalue = best_clust(4); % Somme des t-values du plus gros cluster
                                    dots_err = length(find(permstat.roi(:, 1, prdct) > abs(best_clust(4)))) / length(permstat.roi(:, 1, prdct)); 
                                else
                                    tvalue = NaN;
                                    dots_err = NaN;
                                end
                                
                            else % No correction
                                tvalue = best_clust(4);
                                dots_err = NaN;
                            end
                        end
                        
                        % Collect summary info for this ROI and contrast
                        init.tsum.(f_range_name).(smoothing){prdct, ctrst}(parcel).parcel_name = init.parcel_names{parcel};
                        init.tsum.(f_range_name).(smoothing){prdct, ctrst}(parcel).nb_contacts = length(Tallstat.channel);
                        init.tsum.(f_range_name).(smoothing){prdct, ctrst}(parcel).nb_patients = length(unique(Tallstat.subname));
                        init.tsum.(f_range_name).(smoothing){prdct, ctrst}(parcel).best_cluster = [best_clust(2),best_clust(3)];
                        init.tsum.(f_range_name).(smoothing){prdct, ctrst}(parcel).mean_regression_coef = best_clust(1);
                        init.tsum.(f_range_name).(smoothing){prdct, ctrst}(parcel).tvalue = tvalue;
                        init.tsum.(f_range_name).(smoothing){prdct, ctrst}(parcel).pvalue = dots_err;
                        init.tsum.(f_range_name).(smoothing){prdct, ctrst}(parcel).nb_significant_patients = length(unique(Tallstat.subname(cellfun(@(x) x(prdct), Tallstat.significance) == 1)));
                        init.tsum.(f_range_name).(smoothing){prdct, ctrst}(parcel).significant_contact_prop = nanmean(cellfun(@(x) x(prdct), Tallstat.significance));
                        init.tsum.(f_range_name).(smoothing){prdct, ctrst}(parcel).nb_significant_contact = sum(cellfun(@(x) x(prdct), Tallstat.significance));
                        
                        %% Statistics on the average across the window of interest
                        avg_time = mean(dots{prdct}(tw_idx,:)); % Average over time (nChan)
                        [h,p,~,stats] = ttest(avg_time); % Ttest over channels
                        
                        % Collect summary info for this ROI and contrast
                        init.tsum.(f_range_name).(smoothing){prdct, ctrst}(parcel).tw_average_mean = mean(avg_time);
                        init.tsum.(f_range_name).(smoothing){prdct, ctrst}(parcel).tw_average_sem = std(avg_time)/sqrt(length(avg_time));
                        init.tsum.(f_range_name).(smoothing){prdct, ctrst}(parcel).tw_average_pval = p;
                        init.tsum.(f_range_name).(smoothing){prdct, ctrst}(parcel).tw_average_tval = stats.tstat;
                        init.tsum.(f_range_name).(smoothing){prdct, ctrst}(parcel).tw_average_df = stats.df;
                        
                    end % End of the loop over predictors
                    
                else % There is no contact in the parcel
                    
                    for prdct = 1:size(contrasts_options(init.contrast(contrast)).var_of_interest, 2) % Loop over predictors
                        init.tsum.(f_range_name).(smoothing){prdct, init.contrast(contrast)}(parcel).parcel_name = init.parcel_names{parcel};
                        init.tsum.(f_range_name).(smoothing){prdct, init.contrast(contrast)}(parcel).nb_contacts = 0;
                    end % End of the loop over predictors
                    
                end % End of the condition if there are contacts in the parcel
                
                %% Save the table containing summary info for all ROIs
                
                if parcel == length(init.parcel_names) % Last parcel
                    for prdct = 1:size(dots, 2) % Loop over predictors
                        
                        Tsum = struct2table(init.tsum.(f_range_name).(smoothing){prdct, ctrst});
                        
                        % Save the table containing infos for all ROIs
                        xlsfilesum = fullfile(store_folder, sprintf('%s_ROIs_summary_%s.xlsx', init.task, ctrst_name));
                        
                        if size(xlsfilesum,2) > 218 % Excel limit
                            xlsfilesum = fullfile(store_folder, sprintf('%s_ROIs_summary_%s.xlsx', init.task, ctrst_name(1:end-(size(xlsfilesum,2)-218))));
                        end
                        
                        if size(dots, 2) > 1
                            prdct_name = strsplit(contrasts_options(ctrst).voi_name, '+'); % 'voi names must be separated by '+' characters
                            sheetname = sprintf('best cluster ; prdct = %s', prdct_name{prdct});
                            if length(sheetname) > 31
                                sheetname = sheetname(1:31); % Sheet name must be text and contain 1-31 characters, excluding :, \, /, ?, *, [, and ]
                            end
                        else
                            sheetname = 'best cluster';
                        end
                        
                        if ~exist(xlsfilesum, 'file') % If the file doesn't exist yet
                            writetable(Tsum, xlsfilesum, 'Sheet', sheetname); % Write data
                            
                            % Delete the default sheets
                            Excel = actxserver('Excel.Application'); % Open Excel as a COM Automation server
                            Workbook = Excel.Workbooks.Open(xlsfilesum); % Open Excel workbook
                            Excel.ActiveWorkBook.Sheets.Item('Feuil1').Delete;
                            Excel.ActiveWorkBook.Sheets.Item('Sheet2').Delete;
                            Excel.ActiveWorkBook.Sheets.Item('Sheet3').Delete;
                            Workbook.Save; % Save
                            Excel.Workbook.Close; % Close
                            invoke(Excel, 'Quit'); % Quit
                            delete(Excel) % Delete Excel actxserver
                        else
                            [~, sheetNames] = xlsfinfo(xlsfilesum); % Retrieve sheet names
                            
                            if ~ismember(sheetname,sheetNames) % If the sheet doesn't already exist
                                writetable(Tsum, xlsfilesum, 'Sheet', sheetname); % Write data
                            else % Clear sheet before writing
                                Excel = actxserver('Excel.Application'); % Open Excel as a COM Automation server
                                Workbook = Excel.Workbooks.Open(xlsfilesum); % Open Excel workbook
                                cellfun(@(x) Excel.ActiveWorkBook.Sheets.Item(x).Cells.Clear, {sheetname}); % Clear the content of the sheets
                                Workbook.Save; % Save
                                Excel.Workbook.Close; % Close
                                invoke(Excel, 'Quit'); % Quit
                                delete(Excel) % Delete Excel actxserver
                                writetable(Tsum, xlsfilesum, 'Sheet', sheetname); % Write data
                            end
                        end
                        
                    end % End of the loop over predictors 
                end % End of the condition if last parcel
                
            end % End of the loop across contrasts
        end % End of the loop over smoothings
    end % End of the loop over filtered frequency bands
end % Fin de la boucle � travers les parcelles

close all
