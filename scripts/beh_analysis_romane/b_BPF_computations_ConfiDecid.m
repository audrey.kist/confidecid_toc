% Band-pass filter computations from SPM files for the ConfiDecid task
%
% (1) Filtre passe-bande sur les donn�es :
%	- Session par session
%   - A partir de fichiers SPM en montage bipolaire, NON sous-�chantillonn�s et NON �poch�s
%
% (2) Epoching des nouveaux fichiers filtr�s
% (3) Concat�nation des sessions
% (4) Suppression des fichiers interm�diaires

clear;
close all;

% Patients faits :

%-------------------------------------------------------------------------%

% CHOOSE FREQUENCY BANDS TO FILTER
init.freq_band = {50:10:150}; % {50:10:150, 4:1:8, 8:1:13, 13:5:33, 33:4:49}
init.comp_freq = 512; % Frequency on which bpf computation is performed
init.ds_freq   = 100; % Down-sampled frequency
init.smoothing = 250; % Smoothings : [0 250 500 1000]

init.protocol  = 'cognition'; % 'cognition', 'stim'

%-------------------------------------------------------------------------%

name = getenv('COMPUTERNAME');
if ismember(name, 'HP-ROMANE')
    init.bdd_path = 'C:\Users\Romane\Documents\MATLAB\BDD_ConfiDecid';
elseif ismember(name, 'DESKTOP-IHJ3T8M')
    init.bdd_path = 'C:\Users\Romane\Documents\BDD_ConfiDecid';
end

if ismember(init.protocol, 'cognition')
    
    init.concatsess = 1;
    init.convert2elan = 0; % Convert files to Elan format (0|1)
    
    init.path = fullfile(init.bdd_path, 'Cognitive_task', 'Patients');
    
    init.pat_names = {'GRE_2021_GUIl'}; % Ajouter les sujets ici
    
    init.task = 'ConfiDecid';
    
elseif ismember(init.protocol, 'stim')
    
    init.concatsess = 0; % No concatenation  of sessions at the end
    init.path = fullfile(init.bdd_path, 'Stim_task', 'Patients');
    
    init.pat_names = {'GRE_2019_BAUj', 'GRE_2019_CHRc', 'GRE_2019_BERj', 'GRE_2020_BERl',...
        'GRE_2020_SOLm', 'GRE_2020_FOUt', 'GRE_2020_SULg'}; % Ajouter les sujets ici
    
end

% Epoching info
init.TimeWin = [-6000 6000]; % Epoched time window
load(fullfile(fileparts(which(mfilename)), sprintf('a_epoch_events_%s.mat', init.protocol)), 'epoch_events'); % Charge les �v�nements �poch�s 'epoch_events' (from a_preprocess_eeg_DECID.m)
init.epoch_events = epoch_events;

for pat_idx = 1:length(init.pat_names) % Boucle � travers les sujets
    
    clearvars -except init pat_idx
    subjName = init.pat_names{pat_idx};
    
    % Chemin du dossier patient
    patfile = fullfile(init.path, subjName);
    
    % Trouver les sessions du sujet
    bpf_folder = fullfile(patfile, 'Analyses', 'BPF', 'RAW');
    filelist = dir(bpf_folder); % Liste de tous les fichiers dans le dossier "Postsurgery"
    name = {filelist.name};
    
    if ismember(init.protocol, 'cognition')
        all_sess_name = name(~cellfun(@isempty, regexp(name, sprintf('%s_%s[1-9]_cognition_BPF.mat', subjName, init.task), 'match'))); % Sessions de 1 � X
    elseif ismember(init.protocol, 'stim')
        all_sess_name = name(~cellfun(@isempty, regexp(name, '(.+)mA_BPF.mat', 'match'))); % All electrodes and intensity
    end
    
    for sess = 1:length(all_sess_name) % Boucle � travers les sessions
        
        % Chemin des fichiers SPM (montage bipolaire, non �poch�s, non sous-�chantillonn�s cr��s dans a_preprocess_eeg_DECID.m)
        spmfile = fullfile(bpf_folder, all_sess_name{sess}); % Full path of SPM file
        
        % Load data
        D = spm_eeg_load(spmfile); % SPM struct
        
        % Down-sample to 512 Hz if this is a 1024 Hz file (or more) : (create d.mat)
        if D.fsample > init.comp_freq
            S = [];
            S.D = D.fullfile; % Not down-sampled and not epoched file
            S.fsample_new = init.comp_freq;
            D = spm_eeg_downsample(S);
        end
        
        %% Loop over frequency band
        
        if ismember(init.protocol, 'cognition')
            fileout = fullfile(patfile, 'Analyses', 'BPF', sprintf('%s_%s%d', subjName, init.task, sess)); % Full path of the output file WITHOUT extension
        elseif ismember(init.protocol, 'stim')
            fileout = fullfile(patfile, 'Analyses', 'BPF', char(regexp(all_sess_name{sess}, sprintf('%s_(.+)mA', subjName), 'match'))); % Full path of the output file WITHOUT extension
        end
        
        for bpf = 1:length(init.freq_band)
            
            f_range = init.freq_band{bpf}; % Frequency range
            smoothing = init.smoothing; % Smoothing windows in ms
            newfiles = spm2env(D, f_range, fileout, smoothing); % Band-pass filtering
            
            % Epoch new SPM files
            for sm = 1:length(smoothing) % Loop over smoothings
                
                newfilename = newfiles{sm};
                
                % Down-sample at a defined frequency
                S = [];
                S.D = newfilename; % BPF file
                S.fsample_new = init.ds_freq;
                E = spm_eeg_downsample(S);
                
                % Change file name
                newfilename = sprintf('%s_f%df%d_sf%d_sm%d', fileout, min(f_range), max(f_range), E.fsample, smoothing(sm));
                Dnew = copy(E, newfilename);
                save(Dnew);
                
                % Save file name before epoching for Elan conversion
                filesForElan{sess, bpf, sm} = newfilename;
                
                % Epoching
                S = [];
                S.D = newfilename; % Nom du fichier filtr� et sous-�chantillonn�
                S.bc = 0;
                S.timewin = init.TimeWin;
                
                for i = 1:length(init.epoch_events)
                    S.trialdef(i).conditionlabel = num2str(init.epoch_events(i));
                    S.trialdef(i).eventtype = num2str(init.epoch_events(i));
                    S.trialdef(i).eventvalue = init.epoch_events(i);
                end
                
                S.fsample = E.fsample;
                S.timeonset = 0;
                S.inputformat = [];
                S.reviewtrials = 0;
                S.save = 1;
                S.epochinfo.padding = 0;
                
                F = spm_eeg_epochs(S); % channels x temps x trial (pr�fixe e)
                
                epochfiles{sess, bpf, sm} = F.fullfile;
                
            end % End of the loop over smoothings
            
        end % End of the loop over filtered frequency band
        
    end % Fin de la boucle � travers les sessions
    
    %% CONCATENATION OF ALL SESSIONS
    
    if init.concatsess == 1
        for bpf = 1:length(init.freq_band) % Loop over frequency band
            for sm = 1:length(smoothing) % Loop over smoothings
                
                clear concatfiles;
                concatfiles(:,:) = char(epochfiles(~cellfun(@isempty, epochfiles(:,bpf,sm)),bpf,sm)); % Don't work if filenames are not the same length...
                
                S = [];
                S.D = concatfiles;
                S.recode = 'same';
                S.fileOut = sprintf('%s_%s%s', subjName, init.task, concatfiles(1,regexp(concatfiles(1,:),'_f\d'):end-4));
                S.prefix = '';
                D = spm_eeg_merge_RC(S);
                
            end % End of the loop over smoothings
        end % End of the loop over filtered frequency band
    end
    
    close all;
    
    %% SPM to ELAN conversion
    
    if ismember(init.protocol, {'cognition'}) && init.convert2elan == 1
        for bpf = 1:length(init.freq_band) % Loop over frequency band
            for sm = 1:length(smoothing) % Loop over smoothings
                
                clear spmFiles;
                spmFiles(:,:) = char(filesForElan(~cellfun(@isempty, filesForElan(:,bpf,sm)),bpf,sm)); % Don't work if filenames are not the same length...
                
                elan_folder = fullfile(patfile, 'Analyses', 'ELAN');
                if ~exist(elan_folder, 'dir')
                    mkdir(elan_folder)
                end
                
                [~, elanFile] = fileparts(spmFiles(1,:));
                delIdx = regexp(elanFile, sprintf('(?<=%s)[0-9]+', init.task));
                elanFile(delIdx) = [];
                elanFullPath = fullfile(elan_folder, elanFile);
                
                Spm2ElanContinuous(spmFiles, elanFullPath); % Convert continuous spm files into elan format
                
            end % End of the loop over smoothings
        end % End of the loop over filtered frequency band
    end
    
    %% DELETE ALL INTERMEDIATE FILES
    
    files = dir(E.path);
    fold = files.folder;
    filenames = {files.name};
    
    % Delete intermediate files for each session
    if ismember(init.protocol, 'cognition')
        expr = sprintf('%s%s', init.task, '\d'); % Find all filenames containing DECIDx (x = a number from 0-9)
    elseif ismember(init.protocol, 'stim')
        expr = '^[^.e(RAW)]'; % Find all filenames that do not start with an e
    end
    
    delfiles = regexp(filenames(1,:), expr, 'match');
    delfiles = filenames(~cellfun(@isempty, delfiles));
    
    % Delete files
    for i = 1:length(delfiles)
        delete(fullfile(fold, delfiles{i}));
    end
    
    % Delete also downsampled files in the RAW folder
    files = dir(fullfile(fold, 'RAW'));
    fold = files.folder;
    filenames = {files.name};
    
    delfiles2 = regexp(filenames(1,:), sprintf('d%s', subjName), 'match');
    delfiles2 = filenames(~cellfun(@isempty, delfiles2));
    
    % Delete files
    for i = 1:length(delfiles2)
        delete(fullfile(fold, delfiles2{i}));
    end
    
end % Fin de la boucle � travers les sujets
