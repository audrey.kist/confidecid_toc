% Compute iEEG contact level BPF maps
% It also generates an important file with contact-level T-values + statistics

clear;
close all;

%% Contrastes

% 1  = Correlation activity with feedback (feedback onset locked + all trials)

%% Options

init.cont      = 1;          % Choose contrasts to run
init.freq_band = {[50,150]}; % Choose frequency bands to analyse (ex : {[50,150],[12,30]} for gamma and beta analyses)
init.smoothing = 250;        % Choose smoothing to analyse
init.fsample   = 100;        % Sample frequency of files to analyse
init.parcels   = {'aINS'};   % Name of specific parcels or 'all'

% All parcels
% {'PFCvm','aINS_dors','aINS_vent','pINS','Thalamus','Caudate','Putamen','Pallidum','Hippocampus','Amygdala','Accumbens',...
% 'OFCv','OFCvm','OFCvl','Pfrdls','Pfrdli','VCl','VCcm','ICC','ITCm','VCs','Cu','VCrm','ITCr','MTCc','MTCr','STCc','STCr','IPCv','IPCd',...
% 'SPC','SPCm','PCm','Sv','Sdl','Sdm','PCC','MCC','ACC','Mv','Mdl','Mdm','PMrv','PMdl','PMdm','PFcdl','PFcdm','PFrvl','PFrd','PFrm'};

%% Initialisation

% Patients :
% 'GRE_2018_BOIl', 'GRE_2019_BAUj', 'GRE_2019_CHRc', 'GRE_2019_BERj', 'GRE_2020_BERl',...
% 'GRE_2020_SOLm', 'GRE_2020_FOUt', 'GRE_2020_SULg'

%-------------------------------------------------------------------------%

init.pat_names = {'GRE_2019_BAUj', 'GRE_2019_CHRc', 'GRE_2019_BERj', 'GRE_2020_BERl',...
    'GRE_2020_SOLm', 'GRE_2020_FOUt', 'GRE_2020_SULg'}; % Ajouter les sujets ici

init.bdd_path = 'C:\Users\Romane\Documents\MATLAB\BDD_ConfiDecid';
init.task_path = fullfile(init.bdd_path, 'Stim_task');

init.path = fullfile(init.task_path, 'Patients');
init.backuproot = fullfile(init.task_path, 'BPF_analyzes');

%% Boucle � travers les sujets

for pat_idx = 1:length(init.pat_names)
    
    clearvars -except init mood_data choice_data pat_idx
    subjName = init.pat_names{pat_idx};
    
    backup_data       = 1;
    removebadchannels = 0;
    baseline_grdavg   = 0;
    
    %% Find sess name
    
    patfile = fullfile(init.path, subjName);
    
    % Trouver les sessions du sujet
    bpf_folder = fullfile(patfile, 'Analyses', 'BPF');
    filelist = dir(bpf_folder); % Liste de tous les fichiers dans le dossier "Postsurgery"
    name = {filelist.name};
    
    all_sess_name = name(~cellfun(@isempty, regexp(name, sprintf('^e%s.*mat', subjName), 'match'))); % All electrodes and intensity
    
    %% Loop over sessions
    
    for sess = 1:length(all_sess_name) % Boucle � travers les sessions
        
        sess_name = regexp(all_sess_name{sess}, sprintf('(?<=(^(e))).*(?=(_f%d))', init.freq_band{1}(1)), 'match');
        sess_name = sess_name{:};
        
        %% Load EPI_BPF_stat.mat of the session if exist
        
        filename = sprintf('%s_BPF_stat.mat', sess_name);
        filebackup = fullfile(init.backuproot, 'Group_stat_BPF', sprintf('sf%d', init.fsample), filename);
        if exist(filebackup, 'file')
            load(filebackup, 'store');
        end
        
        %% STEP 1: LOAD BEHAVIORAL DATA
        
        % Load data matrix
        data_filename = fullfile(patfile, 'RAW', 'Comportement', sprintf('%s_data.mat', sess_name));
        load(data_filename, 'data'); % Charger la matrice data de la session
        sessdata = data; % Matrice contenant les donn�es de toutes les sessions pour un sujet
        
        %% Loop over frequency bands and smoothings
        
        for bpf = 1:length(init.freq_band) % Loop over filtered frequency bands
            
            f_range = init.freq_band{bpf}; % Frequency range
            f_range_name = sprintf('f%df%d', min(f_range), max(f_range));
            
            for smooth = 1:length(init.smoothing) % Loop over smoothings
                
                smoothing = sprintf('sm%d', init.smoothing(smooth));
                
                %% STEP 2: LOAD EEG DATA
                tic;
                
                bpffilename = fullfile(patfile, 'Analyses', 'BPF', sprintf('e%s_f%df%d_sf%d_sm%d', sess_name, min(f_range), max(f_range), init.fsample, init.smoothing(smooth)));
                D = spm_eeg_load(bpffilename);
                
                % Load data
                alldata = permute(D(:,:,:), [3 2 1]); % Ntrials x Ntimes x Nchan
                
                m_cond = str2double(D.conditions'); % Events order used to create the TF matrix
                timelist = D.time;
                chanlabels = D.chanlabels;
                
                % We will use 'alldata' as the data matrix to perform single channel statistics
                
                toc;
                %% STEP 3: APPLY SOME OPTIONS
                
                if removebadchannels == 1
                    badchan = [];
                    bcfilename = fullfile(patfile, 'Analyses', 'BadChannel', sprintf('sess%d', sess(sbc)), sprintf('bc_sess%d.mat', sess(sbc)));
                    BC = spm_eeg_load(bcfilename);
                    badchan = unique(badchan); % Delete the channel if it's bad in at least one session
                    alldata(:,:,badchan) = [];
                    chanlabels(badchan) = [];
                end
                
                if baseline_grdavg == 1
                    alldata = bsxfun(@minus, alldata, nanmean(alldata, 2)); % grand average / if large epoch
                end
                
                if ismember(init.parcels, 'all')
                    ROI_chan_idx = 1:length(chanlabels);
                else
                    ROI_chan_idx = [];
                    
                    % Get parcel infos for each sEEG channel from .CSV file
                    csvfile = fullfile(fullfile(init.bdd_path, 'Anatomie'), init.pat_names{pat_idx}, sprintf('%s.csv', init.pat_names{pat_idx}));
                    csv = csv2anat2(csvfile,chanlabels);
                    
                    for p = 1:length(init.parcels)
                        % Specify all the contraints that will be used to define a given ROI
                        constraints = anat_constraints(init.parcels{p});
                        idx = test_chan_parcel(csv, constraints);
                        ROI_chan_idx = [ROI_chan_idx ; idx];
                    end
                    ROI_chan_idx = unique(ROI_chan_idx);
                end
                
                % Exceptions for some patients
                sessdata = exception_patients_ConfiDecid_stim(sess_name, sessdata, m_cond);
                
                %% STEP 4: DEFINE CONTRAST BETWEEN CONDITIONS
                close all;
                
                for tidx = 1:length(init.cont) % Loop over contrasts
                    
                    if exist('store', 'var') && isfield(store, f_range_name) && isfield(store.(f_range_name), smoothing) && init.cont(tidx) <= length(store.(f_range_name).(smoothing)) && ~isempty(store.(f_range_name).(smoothing)(init.cont(tidx)).contrastname) % If the contrast already exist
                        store.(f_range_name).(smoothing)(init.cont(tidx)) = structfun(@(x) [], store.(f_range_name).(smoothing)(init.cont(tidx)), 'UniformOutput', false); % Empty it
                    end
                    
                    switch init.cont(tidx) % Switch between contrasts
                        
                        case 1 % Correlation activity with feedback (feedback onset locked + all trials)
                            
                            contrastname = 'resp-to-fb';
                            Aidx = find(ismember(m_cond, 16)); % Onset du feedback
                            predictors = sessdata(:,12); % Choice feedback (0 or 1)
                            
                            % Fen�tre que l'on veut regarder autour de notre �v�nement
                            tmin = -2;
                            tmax = 2;
                            
                            label = 'time (s) relative to feedback onset';
                            
                    end
                    
                    store.(f_range_name).(smoothing)(init.cont(tidx)).contrastname = contrastname;
                    store.(f_range_name).(smoothing)(init.cont(tidx)).xlabel = label;
                    
                    filterin = timelist >= tmin & timelist <= tmax;
                    
                    %% PERFORM THE CORRELATIONS
                    
                    for chan = 1:length(chanlabels) % Boucle � travers les channels (calcul stats + figures)
                        if ismember(chan, ROI_chan_idx) % If the channel is one of the selected parcels
                            
                            switch init.cont(tidx) % Switch between contrasts
                                
                                case 1 % Power = a + b.Predictors
                                    
                                    z_alldata = zscore(alldata(Aidx,:,:)); % Z-score over trials
                                    responses = z_alldata(:, filterin, chan); % Trial x Time matrix
                                    
                                    dots = NaN(size(responses, 2), size(predictors, 2), 2);
                                    t = dots;
                                    dots_err = dots;
                                    
                                    for cond = 1:2 % Loop over stim condition (non-stim or stim)
                                        
                                        stim_state = 2+(cond-1)*198; % 1 = no stim (2) ; 2 = stim (200)
                                        cond_idx = sessdata(:,2) == stim_state;
                                        
                                        prdct = predictors(cond_idx,:);
                                        resp = responses(cond_idx,:);
                                        
                                        for timi = 1:size(resp, 2) % Loop over time points to get regression weights and stats
                                            [b,~,stats] = glmfit(prdct, resp(:,timi));
                                            dots(timi,:,cond) = b(2:end)'; % Regression coefficient (= slope) of the regression line
                                            t(timi,:,cond) = stats.t(2:end)'; % Time * Nb of regressors * Conditions
                                            dots_err(timi,:,cond) = stats.p(2:end)';
                                        end
                                    end % End of the loop over stim condition
                                    
                                    store.(f_range_name).(smoothing)(init.cont(tidx)).predictors = predictors;
                            end
                            
                            store.(f_range_name).(smoothing)(init.cont(tidx)).dots{chan} = dots; % We store regression coefficients for each time point
                            store.(f_range_name).(smoothing)(init.cont(tidx)).tstat{chan} = t; % On stock les t-values
                            store.(f_range_name).(smoothing)(init.cont(tidx)).pval{chan} = dots_err; % On stock les p-values
                            
                        end % Fin de la contrainte "si la channel est dans une parcelle"
                    end % Fin de la boucle � travers les channels
                    
                    store.(f_range_name).(smoothing)(init.cont(tidx)).filtered_timelist = timelist(filterin);
                    
                end % Fin de la boucle � travers les contrastes
                
            end % Fin de la boucle � travers les smoothings
            
        end % Fin de la boucle � travers les bandes de fr�quence
        
        % Fill hdr structure
        hdr.dimord = 'trial_time_chan';
        hdr.dimsiz = size(alldata);
        hdr.conditions = m_cond;
        hdr.timelist = timelist;
        hdr.chanlist = chanlabels';
        
        if backup_data % Save intermediate results for group analysis
            if ~exist(fullfile(init.backuproot, 'Group_stat_BPF'), 'dir')
                mkdir(fullfile(init.backuproot, 'Group_stat_BPF'));
            end
            save(filebackup, 'subjName', 'hdr', 'store');
        end
        
    end % End of the loop over sessions
end % Fin de la boucle � travers les sujets

toc
return % END

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
