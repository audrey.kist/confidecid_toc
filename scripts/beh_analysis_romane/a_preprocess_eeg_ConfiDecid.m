% ConfiDecid task data preprocessing :
% (1) Conversion des .TRC au format SPM
% (2) V�rification des triggers et des temps
% (3) Import de la position et du nom des �lectrodes
% (4) Montage bipolaire
% (5) Cr�er un fichier SPM en montage bipolaire, NON sous-�chantillonn� et NON �poch� (for BPF computations)
% (6) Spikes detection
% (7) Bad channels detection
% (8) Epoching
% (9) Concat�nation des sessions
% (10) Suppression des fichiers interm�diaires

clear;
close all;
dbstop if warning

% OPTIONS ----------------------------------------------------------------%

init.saveforbpf    = 1; % (5) Cr�er un fichier SPM en montage bipolaire, NON sous-�chantillonn� et NON �poch� (for BPF computations)
init.spikedetect   = 0; % (6) Spikes detection -> DON'T WORK FOR NOW WITH NEW IMAGIN2 FUNCTIONS = to do
init.badchandetect = 1; % (7) Bad channels detection
init.epoch         = 0; % (8) Epoching = Cr�ation des fichiers pour l'analyse temps/fr�quence
init.concatsess    = 0; % (9) Concat�nation de toutes les sessions d'un sujet
init.delete_inter  = 1; % (10) Suppression des fichiers interm�diaires

init.protocol      = 'cognition'; % 'cognition', 'stim'

%-------------------------------------------------------------------------%

% Define BDD path according to computer name
name = getenv('COMPUTERNAME');
if ismember(name, 'HP-ROMANE')
    init.bdd_path = 'C:\Users\Romane\Documents\MATLAB\BDD_ConfiDecid';
elseif ismember(name, 'DESKTOP-IHJ3T8M')
    init.bdd_path = 'C:\Users\Romane\Documents\BDD_ConfiDecid';
end

init.anat_path = fullfile(init.bdd_path, 'Anatomie');

if ismember(init.protocol, 'cognition')
    
    init.path = fullfile(init.bdd_path, 'Cognitive_task', 'Patients');
    init.pat_names = {'GRE_2021_GUIl'}; % Ajouter les sujets ici
    
    % ALL PATIENTS
    % 'GRE_2019_BAUj','GRE_2019_PLEc','GRE_2019_AUBa','GRE_2019_CHRc','GRE_2019_GEOa',...
    % 'LYO_2019_IBRa','REN_2019_BAIp','REN_2019_LAPp','REN_2019_LEBc','LYO_2019_CAPp',...
    % 'GRE_2019_BERj','GRE_2020_BERl','GRE_2020_SOLm','GRE_2020_FOUt',...'PRA_2020_PRGa',...
    % 'PRA_2020_PRAb','GRE_2020_SULg','PRA_2020_PRAc','GRE_2020_JERr','GRE_2020_GENj',...
    % 'GRE_2020_KAYc','PRA_2020_PRAd','GRE_2021_GUIl'
    
    init.trial_sess = 128; % Nombre d'essais par session
    init.mood_sess = 32; % Nombre de mood rating par session
    init.total_events = 705; % Nombre total d'�v�nements par session
    
    init.task = 'ConfiDecid';
    
    init.epoch_events = [10 11 12 13 20 21 22 23 14 24 15 150 16 17 170]; % Epoching : Choice onset & offset agent/no agent, Confidence onset & offset, Feedback onset, Mood onset & offset
    
elseif ismember(init.protocol, 'stim')
    
    init.concatsess = 0; % No concatenation  of sessions at the end
    init.path = fullfile(init.bdd_path, 'Stim_task', 'Patients');
    
    init.pat_names = {'GRE_2019_BAUj'}; % Ajouter les sujets ici
    
    % ALL PATIENTS
    % 'GRE_2018_BOIl', 'GRE_2019_BAUj', 'GRE_2019_CHRc', 'GRE_2019_BERj', 'GRE_2020_BERl',...
    % 'GRE_2020_SOLm', 'GRE_2020_FOUt', 'GRE_2020_SULg'
    
    init.trial_sess = 28; % Nombre d'essais par session
    init.total_events = 183; % Nombre total d'�v�nements par session (1 + 28*6 + 14)
    
    init.epoch_events = [10 11 12 13 14 15 150 16 2 200 201]; % Epoching : Choice onset & offset agent, Confidence onset & offset, Feedback onset, No stim/stim trigger, Stim onset
    
end

save(fullfile(fileparts(which(mfilename)),sprintf('a_epoch_events_%s.mat', init.protocol)), '-struct', 'init', 'epoch_events'); % Save epoched events array
init.TimeWin = [-6000 6000]; % Epoched time window

for pat_idx = 1:length(init.pat_names) % Boucle � travers les sujets
    
    clearvars -except init pat_idx
    close
    
    subjName = init.pat_names{pat_idx};
    
    % Chemin du dossier patient
    patfile = fullfile(init.path, subjName);
    
    % Dossiers o� sont stock�s les nouveaux fichiers
    analysesdir = fullfile(patfile, 'Analyses'); % Dossier o� sont stock�es les analyses
    bpfdir = fullfile(analysesdir, 'BPF', 'RAW'); % Dossier o� sont stock�es les fichiers filtr�s par fr�quence (band-pass filter)
    
    if ~exist(analysesdir,'dir') % Si le dossier d'analyses n'existe pas on le cr��
        mkdir(analysesdir);
    end
    
    if ~exist(bpfdir,'dir') % Si le dossier d'analyses n'existe pas on le cr��
        mkdir(bpfdir);
    end
    
    % Trouver les sessions du sujet
    behav_folder = fullfile(patfile, 'RAW', 'Comportement');
    filelist = dir(behav_folder); % Liste de tous les fichiers dans le dossier "Postsurgery"
    name = {filelist.name};
    
    if ismember(init.protocol, 'cognition')
        all_sess_name = name(~cellfun(@isempty, regexp(name, sprintf('%s_%s[1-9]_cognition.mat', subjName, init.task), 'match'))); % Sessions de 1 � X
    elseif ismember(init.protocol, 'stim')
        all_sess_name = name(~cellfun(@isempty, regexp(name, '(.+)mA.mat', 'match'))); % All electrodes and intensity
    end
    
    for sess = 1:length(all_sess_name) % Boucle � travers les sessions
        
        [filepath, sess_name, ext] = fileparts(all_sess_name{sess});
        spmfile = fullfile(analysesdir, sprintf('%s.mat', sess_name)); % Fichier spm de la session
        
        %% CONVERTION TO SPM FORMAT
        
        if ismember(subjName(1:3), {'PRA'}) % Prague
            
            logfile = fullfile(patfile, 'RAW', 'Comportement', sprintf('%s.mat', sess_name)); % Fichier .mat cr�� lors de la passation
            if exist(logfile,'file')
                D = spm_eeg_convert_Prague_data(patfile,spmfile,sess);
                fprintf(' Session %d of %s has been converted into spm', sess, subjName);
            end
            
        else
            if ismember(subjName(1:3), {'GRE','LYO','REN','TOU','NAN'}) % Grenoble, Lyon, Rennes, Toulouse, Nancy
                eegext = 'TRC';
            elseif ismember(subjName(1:3), {'MAR'}) % Marseille
                eegext = 'edf';
            end
            
            raweegfile = fullfile(patfile, 'RAW', 'sEEG', sprintf('%s.%s', sess_name, eegext)); % Fichier EEG (.TRC ou .edf)
            
            if ~exist(spmfile, 'file') && exist(raweegfile, 'file') % Si le fichier spm n'existe pas (et que le fichier eeg existe) : conversion au format spm
                
                % Exceptions pour r�gler des probl�mes de channel detection
%                 if ismember(subjName(end-3:end), {'SULg'})
%                     SelectChannels = [1:64,66:125];
%                 else
                    SelectChannels = []; % Empty = auto-detection
%                 end
                
                S = [];
                S.dataset = raweegfile; % Full path of the file to convert
                S.SelectChannels = SelectChannels; % Empty = auto-detection
                S.FileOut = spmfile(1:end-4); % Full path to the output .mat/.dat file (without file extension)
                S.isSEEG = 1; % 1 if importing SEEG/ECOG
                ImaGIN_spm_eeg_converteeg2mat_RC(S);
                
                fprintf(' Session %d of %s has been converted into spm', sess, subjName);
                
            end
        end
        
        %% CHECK OF TRIALS : comparaison donn�es sEEG et donn�es comportement
        
        logfile = fullfile(behav_folder, all_sess_name{sess}); % Fichier .mat cr�� lors de la passation
        
        if exist(spmfile, 'file') && ~exist(logfile, 'file') % Fichier comportement manquant
            errordlg(sprintf('Fichier comportement session %d manquant !', sess));
        elseif ~exist(spmfile, 'file') && exist(logfile, 'file') % Fichier sEEG manquant
            errordlg(sprintf('Fichier sEEG session %d manquant !', sess));
        elseif exist(spmfile, 'file') && exist(logfile, 'file') % Si la session existe (et aucun fichier manquant)
            
            % Load data
            D = spm_eeg_load(spmfile); % SPM struct
            
            % Comparaison donn�es sEEG et donn�es comportement
            % (Faite par des fonctions diff�rentes en fonction de la ville d'enregistrement, mais outputs identiques)
            if ismember(init.protocol, 'cognition')
                
                if ismember(subjName(1:3), {'PRA'}) % Prague
                    [checked_eegevents, report] = check_trial_ConfiDecid_Prague(D, init, logfile, sess);
                else % Toutes les autres villes
                    [checked_eegevents, report] = check_trial_ConfiDecid_cognition(D, init, pat_idx, logfile, sess);
                end
                
            elseif ismember(init.protocol, 'stim')
                [checked_eegevents, report] = check_trial_ConfiDecid_stim(D, init, pat_idx, logfile, sess);
            end
            
            % Cr�er l'objet spm interm�diaire (pp = preprocessed)
            D = events(D,1,checked_eegevents); % Changer les �v�nements de D
            
            ppfile = [spmfile(1:end-4),'_pp.mat']; % Nom du fichier pr�-trait� (pp)
            D = fname(D,ppfile); % Changer le nom de D
            D.save();
            
            % Sauvegarder les changements apport�s � D.events (stock�s dans 'report')
            fileID = fopen([spmfile(1:end-4), '_pp.txt'],'w');
            
            B = [{'Line'},{'Old code'},{'New code'},{'Old time'},{'New time'},{'Time correction (sec)'}];
            fprintf(fileID, '%s\t%s\t%s\t%s\t%s\t%s\r\n', B{:});
            
            formatSpec = sprintf('%%-%dd\t%%-%dd\t%%-%dd\t%%-%d.4f\t%%-%d.4f\t%%-%d.4f\r\n', cellfun(@length,B));
            fprintf(fileID, formatSpec, report');
            
            fclose(fileID);
            
            %% SET ELECTRODES NAME AND POSITION
            % Modifier le nom des �lectrodes pour �tre identique au .csv
            % Position n�c�ssaire � la concat�nation
            % Lecture du fichier .csv in Anatomie\Participant_name\Participant_name.csv
            
            % Read .CSV file to export channels name and position
            csvfile = fullfile(init.anat_path, subjName, [subjName '.csv']);
            [Name, Position] = read_elec_data_csv(csvfile);
            
            % Import electrodes position
            S = [];
            S.Fname = D.fullfile;
            S.filenameName = csvfile;
            S.Position = Position; % Position des �lectrodes sous forme d'une matrice nbr_elec*3 (x,y,z)
            ImaGIN_Electrode(S); % Save electrode positions in D, but there is no output
            D = spm_eeg_load(D.fullfile); % So we need to reload D
            
            %% BIPOLAR MONTAGE
            
            S = [];
            S.Filename = D.fullfile; % Nom du fichier.mat pr�-trait� (pp)
            S.FileOut = S.Filename; % On �crase le fichier monopolaire avec les donn�es bipolaires
            D = ImaGIN_BipolarMontage_JB(S);
            
            %% Save at this point for band-pass filtering
            
            if init.saveforbpf == 1
                [~, name] = fileparts(S.Filename);
                copy(D,fullfile(bpfdir,[name(1:end-3), '_BPF']));
            end
            
            %% SPIKES DETECTION
            
            if init.spikedetect == 1
                S = [];
                dataset = D.fullfile;
                S.dataset = dataset(1:end-4);
                S.FileOut = fullfile(D.path, 'SpikesDetection');
                if ~exist(S.FileOut,'dir') % Si le dossier n'existe pas on le cr��
                    mkdir(S.FileOut);
                end
                if ~exist(fullfile(S.FileOut,sprintf('SPK_%s',D.fname)),'file') % Si le fichier de la session n'existe pas on ex�cute ImaGIN_SpikesDetection
                    ImaGIN_SpikesDetection_RC(S);
                end
            end
            
            %% BAD CHANNELS
            % /!\ Need to use randsample function from Matlab, not from spm
            
            if init.badchandetect == 1
                S = [];
                dataset = D.fullfile;
                S.dataset = dataset(1:end-4);
                S.FileOut = fullfile(D.path, 'BadChannel', sprintf('sess%d', sess), sprintf('bc_sess%d', sess));
                if ~exist(S.FileOut(1:max(strfind(S.FileOut,filesep))-1),'dir') % Si le dossier n'existe pas on le cr�� et on ex�cute ImaGIN_BadChannel
                    mkdir(S.FileOut(1:max(strfind(S.FileOut,filesep))-1));
                    D = ImaGIN_BadChannel(S);
                end
            end
            
            %% EPOCHING
            
            if init.epoch == 1
                S = [];
                S.D = D.fullfile; % Nom du fichier.mat pr�-trait� (pp)
                S.bc = 0;
                S.timewin = init.TimeWin;
                
                for i = 1:length(init.epoch_events)
                    S.trialdef(i).conditionlabel = num2str(init.epoch_events(i));
                    S.trialdef(i).eventtype = num2str(init.epoch_events(i));
                    S.trialdef(i).eventvalue = init.epoch_events(i);
                end
                
                S.fsample = D.fsample;
                S.timeonset = 0;
                S.inputformat = [];
                S.reviewtrials = 0;
                S.save = 1;
                S.epochinfo.padding = 0;
                
                D = spm_eeg_epochs(S); % channels x temps x trial (pr�fixe e)
                
                % Down-sample to 512 Hz if this is a 1024 Hz file : (create de.mat)
                if D.fsample > 512
                    S = [];
                    S.D = D.fullfile; % Epoched file
                    S.fsample_new = 512;
                    D = spm_eeg_downsample(S);
                end
            end
            
            %% IF "STIM" : DELETE ALL INTERMEDIATE FILES AT THIS POINT
            
            if ismember(init.protocol, {'stim'})
                files = dir(D.path);
                fold = files.folder;
                filenames = {files.name};
                filenames = filenames(~[files.isdir]); % Don't take folders
                
                dsfile = sprintf('de%s_pp.[md]at', sess_name); % If downsampled
                
                if ~exist(fullfile(analysesdir, [dsfile(1:end-6) 'mat']), 'file')
                    dsfile = sprintf('e%s_pp.[md]at', sess_name); % If downsampled
                end
                
                delfiles = filenames(cellfun(@isempty, regexp(filenames, dsfile, 'match'))); % Keep the 'deXXX_..._pp.mat' file
                delfiles = delfiles(cellfun(@isempty, regexp(delfiles, '_pp.txt', 'match'))); % Except the report file
                for i = 1:length(delfiles)
                    delete(fullfile(analysesdir, delfiles{i}));
                end
            end
            
        end
        
    end % Fin de la boucle � travers les sessions
    
    %% CONCATENATION OF ALL SESSIONS
    if init.concatsess == 1
        
        clear concatfiles;
        
        % Liste des fichiers dans le dossier 'Analyses'
        filelist = dir(analysesdir); % Liste de tous les fichiers dans le dossier "Postsurgery"
        name = {filelist.name};
        
        % Trouver les fichiers correspondants aux sessions pr�-trait�s
        concatidx = regexp(name, sprintf('de%s_%s[1-9]+_%s_pp.mat', subjName, init.task, init.protocol), 'match'); % If files have been downsampled
        if isempty(concatidx)
            concatidx = regexp(name, sprintf('e%s_%s[1-9]+_%s_pp.mat', subjName, init.task, init.protocol), 'match'); % If files haven't been downsampled
        end
        concatfiles = name(~cellfun(@isempty, concatidx))';
        concatfiles = char(cellfun(@(a) fullfile(analysesdir,a), concatfiles, 'UniformOutput', 0));
        
        S = [];
        S.D = concatfiles;
        S.recode = 'same';
        Fname = D.fname;
        S.fileOut = sprintf('%s_%s_%s', subjName, init.task, init.protocol);
        D = spm_eeg_merge_RC(S); % Pr�fixe 'c'
        
    end
    
    %% DELETE ALL INTERMEDIATE FILES
    
    if init.delete_inter == 1
        
        files = dir(D.path);
        filenames = {files.name};
        filenames = filenames(~[files.isdir]); % Don't take folders
        
        % Delete intermediate files for all sessions
        % delfiles = setdiff(filenames, {['c', S.fileOut, '.mat'], ['c', S.fileOut, '.dat']}); % Keep the concatenate file
        delfiles = filenames; % TEMPORARY
        delfiles = delfiles(~ismember(delfiles, delfiles(~cellfun(@isempty, regexp(delfiles, '._(pp|log).txt', 'match'))))); % Keep the report files
        
        for i = 1:length(delfiles)
            delete(fullfile(analysesdir, delfiles{i}));
        end
    end
end % Fin de la boucle � travers les sujets
