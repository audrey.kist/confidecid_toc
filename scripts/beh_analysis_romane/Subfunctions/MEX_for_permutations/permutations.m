function [dots_perm, t_perm, dots_err_perm] = permutations(var_of_interest, neural_data, glm_predictor, size_dots, perm_nb)
%#codegen

% Code used to generate MEX function :
% codegen -report permutations -args {coder.typeof(0,[Inf Inf],1), coder.typeof(0,[Inf Inf],1), coder.typeof('a',[1 Inf],[0 1]), [0 0], 0}

% coder.typeof(X,Y,Z)
%   - X = Class of the argument (0 : double | 'a' : char)
%   - Y = Size of the argument
%   - Z = Size variability of the argument (1 = variable size)

prdct = [];
coder.varsize('prdct');

resp = [];
coder.varsize('resp');

% Permuation tests
dots_perm = NaN(size_dots(1), size_dots(2) , perm_nb);
t_perm = dots_perm;
dots_err_perm = dots_perm;

for perm = 1:perm_nb
    
    % Shuffle lines of our variable(s) of interest (matching between VOIs is maintained)
    % perm_reg = Shuffle(var_of_interest, 2); % Not supported by codegen
    r = randperm(size(var_of_interest,1));
    perm_reg = var_of_interest;
    perm_reg(r,:) = var_of_interest;
    
    for timi = 1:size_dots(1) % R�gression � chaque temps
        
        if ismember(glm_predictor, 'voi') % Variable of interest predicts activity
            prdct = perm_reg;
            resp = neural_data(:,timi);
        elseif ismember(glm_predictor, 'activity') % Activity predicts variable of interest
            prdct = neural_data(:,timi);
            resp = perm_reg;
        end
        
        [coef, t_poly, p_poly] = polyfitn_for_mex(prdct, resp , 1); % Linear regression
        dots_perm(timi,:,perm) = coef(end-1:-1:1); % Regression coefficient (= slope) of the regression line
        t_perm(timi,:,perm) = t_poly(end-1:-1:1);
        dots_err_perm(timi,:,perm) = real(p_poly(end-1:-1:1));
        
    end % End of the loop over time points
end % End of the loop over permutation number

end