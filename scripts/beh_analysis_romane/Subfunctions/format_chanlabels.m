% Formats a character string according to the same scheme as imaGIN:
% - A02A01
% - Ap02Ap01 (Turn ' into "p")
%
% INPUT :
%   - elec : cell array of channel labels we want to format
%
% OUTPUT :
%   - format_elec : cell array of formatted name

function format_elec = format_chanlabels(elec)

format_elec = cell(1, length(elec));

for chan = 1:length(elec)
    
    nbrs = str2double(regexp(elec{chan}, '\d+', 'match')); % Bipole numbers (double)
    prime = char(regexp(elec{chan}, '(p|[\''])', 'match')); % p or ' (if left channel)
    
    if isempty(prime)
        letter = char(regexp(elec{chan}, '[a-zA-Z]+', 'match')); % Channel name
    elseif ismember(prime, '''')
        letter = char(regexp(elec{chan}, '[a-zA-Z]+', 'match')); % Channel name
        prime = 'p';
    elseif ismember(prime, 'p')
        letter = char(regexp(elec{chan}, '[A-Z]+', 'match')); % Channel name
    end
    
    format_elec{chan} = sprintf('%s%s%02.f%s%s%02.f', letter, prime, max(nbrs), letter, prime, min(nbrs));
    
end

end