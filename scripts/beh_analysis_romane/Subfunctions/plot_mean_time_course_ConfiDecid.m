% Plot mean time course of a contrast
% In "d_BPF_group_level_on_parcels"
%
% INPUTS :
%   - init = Structure containing the following fields :
%       - init.exploretw = Time window for visualisation
%       - init.timewin = Time window for statistical analyses
%       - init.alpha = Significance level
%   - dots = Cell array containing all the regression coefficients for one parcel and one contrast (size = nb of glm predictors || predictor matrix = timelist x nb of contact in the ROI)
%   - timelist = Time list of the contrast
%   - parcel_name = Name of the parcel (char array)
%   - contrasts_options = Structure array containing the following fields :
%       - contrastname = Name of the contrast
%       - xlabel = X label for the plot
%       - voi_name = Names of glm variable of interest (separated by '+' characters if more than one)
%   - store_folder = Name of the folder where to store the results
%   - correction = Type of correction applied : 'none', 'fdr', 'permutations'
%   - permstat = N permutations * 2 * coef matrix (obtained from 'cluster_correction_threshold' function) // Can be empty if permutations have not been made
%
% OUTPUT :
% Figure of the mean time course of the considered contrast, stored in 'store_folder'

function plot_mean_time_course_ConfiDecid(init, dots, timelist, parcel_name, contrasts_options, store_folder, correction, permstat)

fig = figure;

for prdct = 1:size(dots, 2) % Loop over glm predictors
    
    AVG_dots = mean(dots{prdct},2); % Average of regressor estimates (time course)
    SEM_dots = std(dots{prdct},[],2)./sqrt(size(dots{prdct},2)); % SEM of regressor estimates (time course)
    
    if prdct == 1
        if~isempty(regexp(contrasts_options.voi_name, 'confidence', 'once'))
            plot_color = [0.305, 0.656, 0.867]; % blue
        elseif ~isempty(regexp(contrasts_options.voi_name, 'probability', 'once'))
            plot_color = [0.590, 0.430, 0.676]; % purple
        elseif mean(AVG_dots) > 0
            plot_color = [0.212, 0.624, 0.761]; % blue
        elseif mean(AVG_dots) <= 0
            plot_color = [1, 0.804, 0.4]; % yellow
        end
    end
    
    hold on;
    
    timl_st = find(timelist >= init.exploretw(1), 1);
    timl_end = find(timelist >= init.exploretw(2), 1);
    
    patch(timelist([timl_st:timl_end timl_end:-1:timl_st]),...
        AVG_dots([timl_st:timl_end timl_end:-1:timl_st]) + cat(1,SEM_dots(timl_st:timl_end), -SEM_dots(timl_end:-1:timl_st)),...
        plot_color,'EdgeColor','none','FaceAlpha',.4)
    
    plt(prdct) = plot(timelist(timl_st:timl_end), AVG_dots(timl_st:timl_end), 'Color', plot_color, 'linewidth', 2);
    
    %% Statistical test
    
    tw_idx = timelist >= init.timewin(1) & timelist <= init.timewin(2);
    stat_timelist = timelist(tw_idx); % Filtered timelist for statistical analyses
    
    [h,p,~,stats] = ttest(dots{prdct}(tw_idx,:)'); % Ttest on the time window we want to analye
    
    if ismember(correction, {'fdr'}) % FDR correction
        
        [~, ~, hfdr] = fdr_BH(p, init.alpha);
        tmp_tl = zeros(1, length(timelist));
        tmp_tl(tw_idx) = hfdr;
        scatter(timelist(tmp_tl == 1), AVG_dots(tmp_tl == 1), 'filled', 'SizeData', 20, 'MarkerFaceColor', plot_color-0.1);
        
    elseif ismember(correction, {'permutations'}) && ~isempty(permstat) % Permutation correction
        
        L = spm_bwlabel(h); %% Find clusters
        
        % Retrieve the sum of t-values of clusters
        stats_summary_tmp = NaN(max(L),1); % Initialization
        for k = 1:max(L) % Analyse all clusters (numbered in L)
            stats_summary_tmp(k) = sum(stats.tstat(L==k)); % Somme des t-values du cluster
        end
        
        % Draw significant cluster in color and non significant in grey
        if ~isempty(stats_summary_tmp)
            
            abs_thresh = prctile(permstat(:,1,prdct), 100-init.alpha*100);
            sig_clust_idx = find(abs(stats_summary_tmp) > abs_thresh); % Index of significant clusters
            
            s(1) = scatter(stat_timelist(ismember(L, sig_clust_idx)), AVG_dots(ismember(timelist, stat_timelist(ismember(L, sig_clust_idx)))), 'filled', 'SizeData', 20, 'MarkerFaceColor', plot_color-0.1); % Significant clusters (color)
            s(2) = scatter(stat_timelist(~ismember(L, [sig_clust_idx;0])), AVG_dots(ismember(timelist, stat_timelist(~ismember(L, [sig_clust_idx;0])))), 'filled', 'SizeData', 20, 'MarkerFaceColor', [.6 .6 .6]);% Non significant clusters (grey)
            
            legend(s(1:2),'significant clusters', 'non significant clusters', 'Location', 'best')
        end
        
        % Draw the best cluster if significant (thicker)
        best_clust_idx = find(abs(stats_summary_tmp) == max(abs(stats_summary_tmp))); % Index of the best cluster
        
        if ~isempty(stats_summary_tmp) && abs(stats_summary_tmp(best_clust_idx)) > abs_thresh % Best cluster significant
            s(3) = scatter(stat_timelist(L == best_clust_idx), AVG_dots(ismember(timelist, stat_timelist(L == best_clust_idx))),...
                'filled', 'd', 'SizeData', 40, 'MarkerFaceColor', plot_color-0.1);
            legend(s,'significant clusters', 'non significant clusters', 'best significant cluster', 'Location', 'best')
        end
        
    else % No correction
        scatter(stat_timelist(h==1), AVG_dots(ismember(timelist, stat_timelist(h==1))), 'filled', 'SizeData', 20, 'MarkerFaceColor', plot_color-0.1);
    end
    
    % Change plot color for next coef
    plot_color = rand(1, 3, 'double');
    while any(plot_color > 1) || any(plot_color-0.1 < 0)
        plot_color = rand(1, 3, 'double');
    end
    
end % End of the loop over contrast coefficients

[filepath, filename] = fileparts(store_folder);
[filepath, smoothing] = fileparts(filepath);
[~, f_range_name] = fileparts(filepath);

title({sprintf('{%s%s - %s - %s}', '\bf', char(join(parcel_name,'-')), smoothing, f_range_name) ; contrasts_options.contrastname}, 'FontWeight', 'Normal');
ylabel('Regression estimates')
xlabel(contrasts_options.xlabel)

line([0 0], ylim, 'Color', 'black', 'LineStyle', '--');
line(xlim, [0 0], 'Color', 'black', 'LineStyle', '--');

xlim([init.exploretw(1) init.exploretw(2)])

prdct_name = strsplit(contrasts_options.voi_name, '+'); % 'voi names must be separated by '+' characters

if size(dots, 2) > 1 % If more than one predictor
    if exist('s', 'var') && length(s) == 2
        legend([s, plt], [{'significant clusters', 'non significant clusters'} prdct_name{:}])
    elseif exist('s', 'var') && length(s) == 3
        legend([s, plt], [{'significant clusters', 'non significant clusters', 'best significant cluster'} prdct_name{:}])
    else
        legend(plt, prdct_name{:})
    end
end

%% Save figure
savefig(fig, fullfile(store_folder, sprintf('%s_%s', char(join(parcel_name, '-')), filename)), 'compact'); % Matlab format

end