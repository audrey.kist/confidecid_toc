% CHECK OF TRIALS
% Comparaison donn�es sEEG et donn�es comportement pour la t�che ConfiDecid
% OUTPUTS :
%	- checked_eegevents = Structure identique � D.events
%   - report = Matrice des changements effectu�s sur les �v�nements (Line|Old code|New code|Time correction (sec))

function [checked_eegevents, report] = check_trial_ConfiDecid_cognition(D, init, pat_idx, logfile, sess)

eegeventsnum = {};
logdata = [];
new_eegevents = [];
report = [];

%% FICHIER EEG

eegevents = struct2cell(D.events')'; % Liste des �v�nements de D

if ismember(init.pat_names{pat_idx}(1:3), {'GRE','LYO','REN','NAN'}) % Grenoble, Lyon, Rennes, Nancy
    
    eegevents(:,1) = regexprep(eegevents(:,1),'t',''); % Supprimer le terme 't'
    eegevents = eegevents(~contains(eegevents(:,1), {'EPIC','avoir'}),:); % Supprimer le marqueur 'EPIC' ou 'avoir' (?) (Rennes)
    
elseif ismember(init.pat_names{pat_idx}(1:3), {'MAR'}) % Marseille (Deltamed)
    
    eegevents(:,1) = regexprep(eegevents(:,1),'MARQUEUR ',''); % Supprimer le terme 'MARQUEUR'
    eegevents = eegevents(~contains(eegevents(:,1), 'DEBUT'),:); % Supprimer le marqueur DEBUT
    eegevents = eegevents(~contains(eegevents(:,1), 'FIN'),:); % Supprimer le marqueur FIN
    eegevents = eegevents(~contains(eegevents(:,1), '255'),:); % Supprimer le marqueur 255 (?)
    
    % Remplacer les codes modifi�s par Deltamed
    eegevents(strcmp(eegevents(:,1),'6'),1) = {'70'};
    eegevents(strcmp(eegevents(:,1),'7'),1) = {'71'};
    eegevents(strcmp(eegevents(:,1),'8'),1) = {'72'};
    eegevents(strcmp(eegevents(:,1),'9'),1) = {'73'};
    eegevents(strcmp(eegevents(:,1),'36'),1) = {'100'};
    
end

% Supprimer les triggers avant le d�but de session (code : 5) (si session lanc�e plusieurs fois par ex)
start = find(strcmp(eegevents(:,1),'5'));
if length(start) > 1 && start(end) < init.total_events % S'il y a plusieurs triggers de d�but de session, on supprime tous les triggers avant le dernier 5, sauf si le dernier 5 est en fin de session (d�but de session suivante par ex)
    eegevents(1:max(start)-1,:) = [];
elseif start ~= 1 % S'il y a des triggers avant le d�but on les supprime
    eegevents(1:max(start)-1,:) = [];
end

% Passage � une matrice
eegeventsnum(:,:) = [num2cell(str2double(eegevents(:,1))), eegevents(:,2)]; % Colonne 1 = codes ; Colonne 2 = temps
eegeventsnum = cell2mat(eegeventsnum);
eegeventsnum = sortrows(eegeventsnum,2); % Dans certains cas, matrice class�e par codes croissants : on reclasse par temps croissants

% Suppression des NaN = tout ce qui n'est pas un nombre (ex: 'crise')
eegeventsnum(isnan(eegeventsnum(:,1)),:) = [];

% On v�rifie que les fichiers commencent bien par un 5 (= d�but de session) et un 10 (= onset du quizz)
if eegeventsnum(1,1) ~= 5
    fprintf(sprintf('First trigger ("5") is missing in session %d : we add it\n', sess));
    eegeventsnum = [[5,0];eegeventsnum];
elseif ~ismember(eegeventsnum(2,1),[10:13,20:23])
    errordlg(sprintf('First quizz onset is missing in session %d', sess));
    return;
end

%% LOGFILE

load(logfile,'passation'); % "passation" struct

% Re-cr�ation du log data sous forme de liste
logdata(1,:) = [5, passation.logfile.start];
line = 2;
for t = 1:init.trial_sess
    
    logdata(line,:) = [passation.logfile.choice.config(t), passation.logfile.choice.onset(t)]; % Choice onset
    line = line + 1;
    
    if passation.logfile.choice.agentivity(t) == 1 % Agent
        choice.offset_trig = 14;
    else % No agent
        choice.offset_trig = 24;
    end
    
    logdata(line,:) = [choice.offset_trig, passation.logfile.choice.resp_press(t)]; % Choice offset
    line = line + 1;
    
    logdata(line,:) = [15, passation.logfile.confidence.onset(t)]; % Confidence onset
    line = line + 1;
    
    logdata(line,:) = [150, passation.logfile.confidence.resp_press(t)]; % Confidence offset
    line = line + 1;
    
    logdata(line,:) = [16, passation.logfile.challenge.feed_onset(t)]; % Feedback onset
    line = line + 1;
    
    if ~isnan(passation.logfile.mood.onset(t))
        logdata(line,:) = [17, passation.logfile.mood.onset(t)]; % Mood onset
        line = line + 1;
        
        logdata(line,:) = [170, passation.logfile.mood.resp_press(t)]; % Mood offset
        line = line + 1;
    end
    
end

% V�rification de logdata
if length(logdata) ~= init.total_events
    errordlg(sprintf('logfile session %d corrompu ?', sess));
    return;
end

% Ranger logdata par ordre de temps croissant
% (Il peut y avoir des probl�mes si le patient valide une note trop vite par exemple)
logdata = sortrows(logdata,2);

%% Comparaison du LOGFILE et du fichier EEG

% On compl�te eegeventsnum avec des z�ros (s'il y a des triggers manquant par exemple)
% avant la comparaison ligne par ligne
eegeventsnum(length(eegeventsnum)+1:length(logdata),:) = zeros(length(length(eegeventsnum)+1:length(logdata)),size(eegeventsnum,2));

% Comparaison ligne par ligne : start onset = r�f�rence de temps
new_eegevents(1,:) = eegeventsnum(1,:);
eegline = 2;

for logline = 2:length(logdata)
    log_interv = logdata(logline, 2) - logdata(logline - 1, 2);
    eeg_interv = eegeventsnum(eegline, 2) - eegeventsnum(eegline - 1, 2);
    
    if logdata(logline, 1) == eegeventsnum(eegline, 1) && abs(log_interv - eeg_interv) < 0.01 && abs((eegeventsnum(eegline, 2) - new_eegevents(logline - 1, 2)) - log_interv) < 0.01 % Lignes identiques (codes identiques + diff�rence d'intervalle < 0.01)
        new_eegevents(logline,:) = eegeventsnum(eegline,1:2);
        eegline = eegline + 1;
        
    else % Probl�me (temps non identiques, missing trigger...)
        new_eegevents(logline,:) = [logdata(logline, 1), new_eegevents(end, 2) + log_interv];
        report(logline,:) = [logline, eegeventsnum(eegline, 1), logdata(logline, 1), eegeventsnum(eegline, 2), new_eegevents(end, 2), abs(log_interv - eeg_interv)];
        
        if (length(logdata) >= (logline + 1) && logdata(logline + 1, 1) == eegeventsnum(eegline, 1) && abs((eegeventsnum(eegline, 2) - (logdata(logline + 1, 2) - logdata(logline, 2))) - eegeventsnum(eegline - 1, 2)) < 0.01) ||... % Lignes identiques mais il manque un trigger
                (length(logdata) >= (logline + 2) && logdata(logline + 2, 1) == eegeventsnum(eegline, 1) && abs((eegeventsnum(eegline, 2) - (logdata(logline + 2, 2) - logdata(logline, 2))) - eegeventsnum(eegline - 2, 2)) < 0.01) % Lignes identiques mais il manque deux triggers
            % Do nothing
        else
            eegline = eegline + 1;
        end
    end
    
end

% Transformer 'checked_eegevents' en structure identique � D.events
new_eegevents(:,3) = new_eegevents(:,1); % 'Value' des �v�nements = 'Type' -> n�c�ssaire pour l'�poching
new_eegevents(:,4) = 0;
checked_eegevents(1:length(new_eegevents),1:5) = [cellstr(num2str(new_eegevents(:,1),'%-d')), num2cell(new_eegevents(:,2)), num2cell(new_eegevents(:,3:4)), cell(length(new_eegevents),1)]; % Faire les modifications
checked_eegevents = cell2struct(checked_eegevents, fieldnames(D.events), 2)';

% Enlever les lignes vides de 'report'
if ~isempty(report)
    report = report(~report(:,1) == 0,:);
end

% V�rifier qu'il n'y a pas de probl�me
lasteegeventsnum = max(eegeventsnum(~ismember(eegeventsnum(:,2),0),2));
if abs(new_eegevents(end,2) - lasteegeventsnum) > 0.01
    file = [mfilename('fullpath') '.m'];
    eval(['dbstop in ' file ' at 157'])
end

end