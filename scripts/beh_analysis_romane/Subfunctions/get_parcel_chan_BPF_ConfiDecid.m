% Get all channels of a specified parcel and the associated informations
%
% INPUTS :
%   - init : structure
%       - init.parcel_names = cell array with parcels name
%       - init.pat_names = cell array with patients name
%       - init.task = name of the task analysed (e.g. 'DEDID')
%       - init.first_level_path = path where are the first level analyzes (BPF_stat.mat)
%       - init.anat_path = path where are .csv files
%       - init.freq_band = frequency bands to analyse
%       - init.smoothing = smoothings to analyse
%   - parcel : index of parcel in init.parcel_names
%
% OUTPUTS :
%   - Tall = table with informations about the selected channels
%   - parcel_bpf = structure containing the following fields :
%       - dots = structure for each contrast of all regression coefficients for each channels (timelist x nb of contact in the ROI)
%       - pval = structure for each contrast of all p-value for each channels (timelist x nb of contact in the ROI)
%       - tstat = structure for each contrast of all t-value for each channels (timelist x nb of contact in the ROI)
%   - contrast_options = structure containing infos about contrasts
%   - chan_idx (optional) = cell containing electrodes number in the parcel for each patient

function [Tall, parcel_bpf, contrasts_options, chan_idx] = get_parcel_chan_BPF_ConfiDecid(init, parcel)

% Specify all the contraints that will be used to define a given ROI
constraints = anat_constraints(init.parcel_names{parcel});

% Initialize variables
Tall = [];
idx = 0;

%% Loop over subjects

for pat_idx = 1:length(init.pat_names)
    
    subid = [init.pat_names{pat_idx} '_' init.task];
    
    %% STEP 1 : Get chanlabels from BPF file
    
    eegfilename = sprintf('%s_BPF_stat.mat', subid);
    load(fullfile(init.first_level_path, eegfilename), 'hdr', 'store'); % Load variables 'hdr' and 'store'
    chanlabels = hdr.chanlist;
    
    %% STEP 2 : Get parcel infos for each sEEG channel from .CSV file
    
    csvfile = fullfile(init.anat_path, init.pat_names{pat_idx}, sprintf('%s.csv', init.pat_names{pat_idx}));
    csv = csv2anat2(csvfile,chanlabels);
    
    %% Test if there is an sEEG contact within current parcel
    
    ROI_chan_idx = test_chan_parcel(csv, constraints);
    
    %% Define channels idx
    
    if isempty(idx) % No electrodes in the previous patient
        idx = size(Tall, 1);
    end
    idx = idx(end)+1:idx(end) + length(ROI_chan_idx);
    
    %% Fill table with ROI infos
    
    t_roi.subname = repmat({subid(1:end-length(init.task)-1)}, length(ROI_chan_idx), 1);
    t_roi.channel = chanlabels(ROI_chan_idx);
    t_roi.marsatlas = csv.marslabels(ROI_chan_idx);
    t_roi.freesurfer = csv.freelabels(ROI_chan_idx);
    t_roi.aal = csv.aallabels(ROI_chan_idx);
    t_roi.MNIx = csv.MNI(ROI_chan_idx,1);
    t_roi.MNIy = csv.MNI(ROI_chan_idx,2);
    t_roi.MNIz = csv.MNI(ROI_chan_idx,3);
    t_roi.marsatlasfull = csv.marslabelsfull(ROI_chan_idx);
    t_roi.greywhite = csv.greywhite(ROI_chan_idx);
    T = struct2table(t_roi);
    
    display(T); % Display final electrodes selected in the ROI for this patient
    Tall = [Tall; T]; % Aggregate tables
    
    if nargout > 3
        chan_idx(pat_idx) = {ROI_chan_idx}; % Retrieves electrodes number in the parcel for each patient
    end
    
    %% Pick-up BPF data and relevent regressors
    
    for bpf = 1:length(init.freq_band) % Loop over filtered frequency bands
        
        f_range_name = sprintf('f%df%d', min(init.freq_band{bpf}), max(init.freq_band{bpf})); % Ex : f50f150
        
        for smooth = 1:length(init.smoothing) % Loop over smoothings
            
            smoothing = sprintf('sm%d',init.smoothing(smooth));
            
            for contrast = 1:length(init.contrast) % Loop over contrasts
                
                store_tmp = store.(f_range_name).(smoothing)(init.contrast(contrast)); % "store" struct for one contrast and one frequency band
                contrasts_options(init.contrast(contrast)) = store_tmp.options;
                
                for chan = 1:length(ROI_chan_idx) % Loop over selected channels in the parcel for the patient
                    
                    for prdct = 1:size(store_tmp.options.var_of_interest, 2) % Loop over glm predictors
                        
                        % Aggregate regression estimates across selected channels
                        parcel_bpf.(f_range_name).(smoothing)(init.contrast(contrast)).dots{prdct}(:,idx(chan)) = store_tmp.dots{ROI_chan_idx(chan)}(:,prdct); % Tous les plots d'une parcelle pour tous les patients (timelist x nb of contact in the ROI)
                        parcel_bpf.(f_range_name).(smoothing)(init.contrast(contrast)).pval{prdct}(:,idx(chan)) = store_tmp.pval{ROI_chan_idx(chan)}(:,prdct); % timelist x nb of contact in the ROI
                        parcel_bpf.(f_range_name).(smoothing)(init.contrast(contrast)).tstat{prdct}(:,idx(chan)) = store_tmp.tstat{ROI_chan_idx(chan)}(:,prdct); % timelist x nb of contact in the ROI
                        
                    end % End of the loop over contrast coefficients
                end % End of the loop over selected channels
            end % End of the loop across contrasts
        end % End of the loop over smoothings
    end % End of the loop over filtered frequency bands
end % End of the loop across subjects

if ~exist('parcel_bpf', 'var') % No contact in the parcel
    parcel_bpf = {};
end

end