% CHECK OF TRIALS PRAGUE
% D�tection des triggers et ajout des �v�nements pour la t�che ConfiDecid
% OUTPUTS :
%   - checked_eegevents = Structure identique � D.events
%   - report = Matrice des changements effectu�s sur les �v�nements (Line|Old code|New code|Time correction (sec))

function [checked_eegevents, report] = check_trial_ConfiDecid_Prague(D, init, logfile, sess)

report = [];

%% LOGFILE

% Load behavior data
load(logfile, 'passation'); % Variable "passation"

% Re-cr�ation du log data sous forme de liste
logdata(1,:) = [5, passation.logfile.start];
line = 2;
for t = 1:init.trial_sess
    
    logdata(line,:) = [passation.logfile.choice.config(t), passation.logfile.choice.onset(t)]; % Choice onset
    line = line + 1;
    
    if passation.logfile.choice.agentivity(t) == 1 % Agent
        choice.offset_trig = 14;
    else % No agent
        choice.offset_trig = 24;
    end
    
    logdata(line,:) = [choice.offset_trig, passation.logfile.choice.resp_press(t)]; % Choice offset
    line = line + 1;
    
    logdata(line,:) = [15, passation.logfile.confidence.onset(t)]; % Confidence onset
    line = line + 1;
    
    logdata(line,:) = [150, passation.logfile.confidence.resp_press(t)]; % Confidence offset
    line = line + 1;
    
    logdata(line,:) = [16, passation.logfile.challenge.feed_onset(t)]; % Feedback onset
    line = line + 1;
    
    if ~isnan(passation.logfile.mood.onset(t))
        logdata(line,:) = [17, passation.logfile.mood.onset(t)]; % Mood onset
        line = line + 1;
        
        logdata(line,:) = [170, passation.logfile.mood.resp_press(t)]; % Mood offset
        line = line + 1;
    end
    
end

% V�rification de logdata
if length(logdata) ~= init.total_events
    errordlg(sprintf('logfile session %d corrompu ?', sess));
    return;
end

% Ranger logdata par ordre de temps croissant
% (Il peut y avoir des probl�mes si le patient valide une note trop vite par exemple)
logdata = sortrows(logdata,2);

%% sEEG : Detect triggers

trigchan = find(not(~contains(upper(D.chanlabels),{'TRIG'}))); % Triggers = usually on the last channel
plot(D(trigchan,:,:)); % Plot of trigger channel

% Peak detection
datas.threshold_up = 1;
datas.threshold_down = -1;
datas.peak_duration = 10;

v_signal = D(trigchan,:,:);
v_time = 1:length(D(trigchan,:,:));
v_pos_peak_up_long = detection_peak_RC(v_signal, v_time, datas);

triggers = v_pos_peak_up_long(1,:); % triggers = INDEX ! On prend le premier signal d�tect� pour chaque pic (montant ou descendant)
triggers = triggers - 1; % Trick to have the real beginning

% Exception
if ismember(passation.participant.identifier, {'PRA_2020_PRAb'}) && sess == 2
    diff = logdata(2,2)-logdata(1,2);
    N = D.time(triggers(2))-diff;
    [~, closestIndex] = min(abs(D.time-N));
    triggers(1) = closestIndex;
end

sEEGcodes = v_pos_peak_up_long(3,:); % Peak amplitude = trigger code

hold on;
plot(triggers, 0, 'r-o'); % Visualiser les triggers d�tect�s

% V�rifier que le nombre de triggers d�tect�s correspond au nombre total d'�v�nements
if length(triggers) > init.total_events
    warndlg(sprintf('Number of triggers > total number of events\nWrong beginning?'));
    return;
end

% V�rifier que le premier trigger d�tect� est correct
if isequal(sEEGcodes(1:2)', logdata(1:2,1))
    
    btn1 = 'ok';
    btn2 = 'not ok (end)';
    check = nonmodalquestdlg('Check that the first two triggers detected are correct before proceeding', btn1, btn2);
    
    if strcmp(check, btn2)
        return;
    end
    
else
    warndlg(sprintf('First two triggers do not match the logfile'));
    return;
end

%% Comparaison donn�es sEEG et donn�es comportement

eegeventsnum = zeros(length(logdata),2); % On pr�-rempli eegeventsnum avec des z�ros (si triggers manquant par ex.)
eegeventsnum(:,1) = sEEGcodes; % Colonne 1 = codes
eegeventsnum(1:length(triggers),2) = D.time(triggers)'; % Colonne 2 = temps

% Comparaison ligne par ligne : start onset = r�f�rence de temps
new_eegevents(1,:) = eegeventsnum(1,:);
eegline = 2;

for logline = 2:length(logdata)
    log_interv = logdata(logline, 2) - logdata(logline - 1, 2);
    eeg_interv = eegeventsnum(eegline, 2) - eegeventsnum(eegline - 1, 2);
    
    if logdata(logline, 1) == eegeventsnum(eegline, 1) && abs(log_interv - eeg_interv) < 0.01 && abs((eegeventsnum(eegline, 2) - new_eegevents(logline - 1, 2)) - log_interv) < 0.01 % Lignes identiques (codes identiques + diff�rence d'intervalle < 0.01)
        new_eegevents(logline,:) = eegeventsnum(eegline,1:2);
        eegline = eegline + 1;
    elseif length(logdata) >= (logline + 1) && logdata(logline + 1, 1) == eegeventsnum(eegline, 1) && abs((logdata(logline + 1, 2) - logdata(logline - 1, 2)) - eeg_interv) < 0.01 % Lignes identiques mais il manque un trigger
        new_eegevents(logline,:) = [logdata(logline, 1), new_eegevents(end, 2) + log_interv];
        report(logline,:) = [logline, eegeventsnum(eegline, 1), logdata(logline, 1), eegeventsnum(eegline, 2), new_eegevents(end, 2), abs(log_interv - eeg_interv)];
    else % Autre probl�me (temps non identiques par ex.)
        new_eegevents(logline,:) = [logdata(logline, 1), new_eegevents(end, 2) + log_interv];
        report(logline,:) = [logline, eegeventsnum(eegline, 1), logdata(logline, 1), eegeventsnum(eegline, 2), new_eegevents(end, 2), abs(log_interv - eeg_interv)];
        eegline = eegline + 1;
    end
    
end

% Transformer 'checked_eegevents' en structure identique � D.events
new_eegevents(:,3) = new_eegevents(:,1); % 'Value' des �v�nements = 'Type' -> n�c�ssaire pour l'�poching
new_eegevents(:,4) = 0;
checked_eegevents(1:length(new_eegevents),1:5) = [cellstr(num2str(new_eegevents(:,1),'%-d')), num2cell(new_eegevents(:,2)), num2cell(new_eegevents(:,3:4)), cell(length(new_eegevents),1)]; % Faire les modifications
Devents = struct('type',[],'time',[],'value',[],'offset',[],'duration',[]);
checked_eegevents = cell2struct(checked_eegevents, fieldnames(Devents), 2)';

% Enlever les lignes vides de 'report'
if ~isempty(report)
    report = report(~report(:,1) == 0,:);
end

% V�rifier qu'il n'y a pas de probl�me
lasteegeventsnum = max(eegeventsnum(~ismember(eegeventsnum(:,2),0),2));
if abs(new_eegevents(end,2) - lasteegeventsnum) > 0.01
    file = [mfilename('fullpath') '.m'];
    eval(['dbstop in ' file ' at 149'])
end

% Plot to check new triggers
scatter(new_eegevents(:,2)*D.fsample, zeros(1,length(new_eegevents)), 'filled');

% Check triggers
btn1 = 'ok';
btn2 = 'not ok (end)';
check = nonmodalquestdlg('Check triggers before proceeding',btn1,btn2);

if strcmp(check, btn2)
    return;
else
    close;
end

close; % Close figure

end