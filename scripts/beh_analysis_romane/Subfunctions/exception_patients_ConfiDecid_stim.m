
function behav_data = exception_patients_ConfiDecid_stim(sessName, behav_data, seeg_data)

% Exceptions for patients with problems
%
% INPUTS :
% - behav_data = Behavioral data (1st dim = nb trials)
% - seeg_data = sEEG data (1st dim = nb events)
%
% OUTPUTS :
% - behav_data = with the same number of trials as the brain data

if ismember(sessName, {'GRE_2019_BAUj_ConfiDecid_stim_X4-5_2mA'})
    % 2 trials were deleted in seeg_data as there was 1 excess stimulation
    behav_data(isnan(behav_data(:,2)),:) = []; % Remove these trials from behav_data
    
elseif ismember(sessName, {'GRE_2020_SOLm_ConfiDecid_stim_Qp1-2_2mA'})
    % Last feedback onset (event 16) is missing in seeg_data (out of bounds)
    % So we delete the trial
    fb_onset = find(seeg_data == 16);
    if size(behav_data,1) - size(fb_onset,1) == 1
        behav_data(end,:) = [];
    end
end

end