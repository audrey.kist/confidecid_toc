% CHECK OF TRIALS
% Comparaison donn�es sEEG et donn�es comportement pour la t�che ConfiDecid
% OUTPUTS :
%	- checked_eegevents = Structure identique � D.events
%   - report = Matrice des changements effectu�s sur les �v�nements (Line|Old code|New code|Time correction (sec))

function [checked_eegevents, report] = check_trial_ConfiDecid_stim(D, init, pat_idx, logfile, sess)

eegeventsnum = {};
logdata = [];
new_eegevents = [];
report = [];

%% FICHIER EEG

eegevents = struct2cell(D.events')'; % Liste des �v�nements de D

if ismember(init.pat_names{pat_idx}(1:3), {'GRE','LYO','REN','NAN'}) % Grenoble, Lyon, Rennes, Nancy
    
    eegevents(:,1) = regexprep(eegevents(:,1),'t',''); % Supprimer le terme 't'
    eegevents = eegevents(~contains(eegevents(:,1), {'EPIC','avoir'}),:); % Supprimer le marqueur 'EPIC' ou 'avoir' (?) (Rennes)
    
end

% Supprimer les triggers avant le d�but de session (code : 5) (si session lanc�e plusieurs fois par ex)
start = find(strcmp(eegevents(:,1),'5'));
if length(start) > 1 && start(end) < init.total_events % S'il y a plusieurs triggers de d�but de session, on supprime tous les triggers avant le dernier 5, sauf si le dernier 5 est en fin de session (d�but de session suivante par ex)
    eegevents(1:max(start)-1,:) = [];
elseif start ~= 1 % S'il y a des triggers avant le d�but on les supprime
    eegevents(1:max(start)-1,:) = [];
end

% Passage � une matrice
eegeventsnum(:,:) = [num2cell(str2double(eegevents(:,1))), eegevents(:,2)]; % Colonne 1 = codes ; Colonne 2 = temps
eegeventsnum = cell2mat(eegeventsnum);
eegeventsnum = sortrows(eegeventsnum,2); % Dans certains cas, matrice class�e par codes croissants : on reclasse par temps croissants

% Trouver les triggers correspondant aux stims et changer en '201'
elec.name = char(regexp(D.fname, '[a-zA-Z](p|\'')?\d+-\d+', 'match')); % Electrode name
elec.letter = char(regexp(elec.name, '[a-zA-Z]', 'match')); % Electrode letter
stim_idx = contains(lower(eegevents(:,1)), lower(elec.letter(1)));
eegeventsnum(stim_idx, 1) = 201;

% Si deux 201 � la suite, supprimer le premier (stim avec essai non d�clench�)
fidx = find(stim_idx);
eegeventsnum(fidx(diff(fidx) == 1),:) = [];

% S'il y a un 201 apr�s un 2 (stim apr�s un essai non stim), supprimer la stim
nostim_idx = find(eegeventsnum(:,1) == 2);
eegeventsnum(fidx(ismember(fidx, nostim_idx + 1)),:) = [];

% On v�rifie que les fichiers commencent bien par un 5 (= d�but de session) et un 2 ou 200 (= no stim/stim trigger)
if eegeventsnum(1,1) ~= 5
    fprintf(sprintf('First trigger ("5") is missing in session %d : we add it\n', sess));
    eegeventsnum = [[5,0];eegeventsnum];
elseif ~ismember(eegeventsnum(2,1),[2, 200])
    errordlg(sprintf('First quizz onset is missing in session %d', sess));
    return;
end

%% LOGFILE

load(logfile,'passation'); % "passation" struct

% Re-cr�ation du log data sous forme de liste
logdata(1,:) = [5, passation.logfile.start];
line = 2;
for t = 1:init.trial_sess
    
    logdata(line,:) = [passation.logfile.stim(t), NaN]; % Stim condition
    line = line + 1;
    
    if passation.logfile.stim(t) == 200 % Stim
        logdata(line,:) = [201, NaN]; % Stim onset
        line = line + 1;
    end
    
    logdata(line,:) = [passation.logfile.choice.config(t), passation.logfile.choice.onset(t)]; % Choice onset
    line = line + 1;
    
    if passation.logfile.choice.agentivity(t) == 1 % Agent
        choice.offset_trig = 14;
    elseif passation.logfile.choice.agentivity(t) == 0 % No agent
        choice.offset_trig = 24;
    end
    
    logdata(line,:) = [choice.offset_trig, passation.logfile.choice.resp_press(t)]; % Choice offset
    line = line + 1;
    
    logdata(line,:) = [15, passation.logfile.confidence.onset(t)]; % Confidence onset
    line = line + 1;
    
    logdata(line,:) = [150, passation.logfile.confidence.resp_press(t)]; % Confidence offset
    line = line + 1;
    
    logdata(line,:) = [16, passation.logfile.challenge.feed_onset(t)]; % Feedback onset
    line = line + 1;
    
end

% V�rification de logdata
if ~ismember(logfile(end-41:end), {'GRE_2019_BAUj_ConfiDecid_stim_X4-5_2mA.mat'}) % Exceptions (problem with some stims)
    if length(logdata) ~= init.total_events
        errordlg(sprintf('logfile session %d corrompu ?', sess));
        return;
    end
end

%% Exceptions for patients with more stim or other problems

if ismember(logfile(end-41:end), {'GRE_2019_BAUj_ConfiDecid_stim_X4-5_2mA.mat'})
    eegeventsnum([9:14,132:138],:) = [];
    logdata([9:14,132:137],:) = [];
elseif ismember(logfile(end-41:end), {'GRE_2019_BAUj_ConfiDecid_stim_X2-3_1mA.mat'})
    eegeventsnum = [eegeventsnum(1:5,:); NaN(2); eegeventsnum(6:end,:)];
elseif ismember(logfile(end-41:end), {'GRE_2020_SULg_ConfiDecid_stim_O1-2_3mA.mat'})
    stim_idx = contains(lower(eegevents(:,1)), '02-01');
    eegeventsnum(stim_idx, 1) = 201;
end

%% Comparaison du LOGFILE et du fichier EEG

% On compl�te eegeventsnum avec des z�ros (s'il y a des triggers manquant par exemple)
% avant la comparaison ligne par ligne
eegeventsnum(length(eegeventsnum)+1:length(logdata),:) = zeros(length(length(eegeventsnum)+1:length(logdata)),size(eegeventsnum,2));

% Comparaison ligne par ligne : start onset = r�f�rence de temps
new_eegevents(1,:) = eegeventsnum(1,:);
eegline = 2;

for logline = 2:length(logdata)
    
    if ismember(logdata(logline,1), [2, 200, 201]) && ismember(eegeventsnum(eegline,1), [2, 200, 201]) % No stim/stim trigger or Stim onset
        new_eegevents(logline,:) = eegeventsnum(eegline,1:2);
        eegline = eegline + 1;
    else
        if ismember(logdata(logline,1), 10:13) && ismember(logdata(logline-1,1), 2) % Choice onset after no stim
            prev_tr = 2;
        elseif ismember(logdata(logline,1), 10:13) && ismember(logdata(logline-1,1), 201) % Choice onset after stim
            prev_tr = 3;
        else
            prev_tr = 1;
        end
        
        log_interv = logdata(logline,2) - logdata(logline - prev_tr, 2);
        eeg_interv = eegeventsnum(eegline,2) - eegeventsnum(eegline - prev_tr, 2);
        
        if logdata(logline,1) == eegeventsnum(eegline,1) && abs(log_interv - eeg_interv) < 0.01 && abs((eegeventsnum(eegline,2) - new_eegevents(logline - prev_tr, 2)) - log_interv) < 0.01 % Lignes identiques (codes identiques + diff�rence d'intervalle < 0.01)
            new_eegevents(logline,:) = eegeventsnum(eegline,1:2);
            eegline = eegline + 1;
        elseif length(logdata) >= (logline + 1) && logdata(logline + 1, 1) == eegeventsnum(eegline,1) && abs((logdata(logline + 1, 2) - logdata(logline - prev_tr, 2)) - eeg_interv) < 0.01 % Lignes identiques mais il manque un trigger
            new_eegevents(logline,:) = [logdata(logline,1), new_eegevents(end,2) + log_interv];
            report(logline,:) = [logline, eegeventsnum(eegline,1), logdata(logline,1), eegeventsnum(eegline,2), new_eegevents(end,2), abs(log_interv - eeg_interv)];
        else % Autre probl�me (temps non identiques par ex.)
            new_eegevents(logline,:) = [logdata(logline,1), new_eegevents(end - prev_tr + 1, 2) + log_interv];
            report(logline,:) = [logline, eegeventsnum(eegline,1), logdata(logline,1), eegeventsnum(eegline,2), new_eegevents(end,2), abs(log_interv - eeg_interv)];
            eegline = eegline + 1;
        end
    end
    
end

% Transformer 'checked_eegevents' en structure identique � D.events
new_eegevents(:,3) = new_eegevents(:,1); % 'Value' des �v�nements = 'Type' -> n�c�ssaire pour l'�poching
new_eegevents(:,4) = 0;
checked_eegevents(1:length(new_eegevents),1:5) = [cellstr(num2str(new_eegevents(:,1),'%-d')), num2cell(new_eegevents(:,2)), num2cell(new_eegevents(:,3:4)), cell(length(new_eegevents),1)]; % Faire les modifications
checked_eegevents = cell2struct(checked_eegevents, fieldnames(D.events), 2)';

% Enlever les lignes vides de 'report'
if ~isempty(report)
    report = report(~report(:,1) == 0,:);
end

% V�rifier qu'il n'y a pas de probl�me
lasteegeventsnum = max(eegeventsnum(~ismember(eegeventsnum(:,2),0),2));
if abs(new_eegevents(end,2) - lasteegeventsnum) > 0.01
    dbstop in C:\Users\Romane\Documents\MATLAB\PhD-codes\ConfiDecid\Subfunctions\check_trial_ConfiDecid_stim.m at 174
end

end