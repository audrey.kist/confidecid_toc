% Create folders for a new patient in BDD

function bdd_new_patient(bdd_path, task_name, patient_name, eye_tracker)

if nargin == 0
    
    bdd_path = 'C:\Users\Romane\Documents\MATLAB\BDD_ConfiDecid';
    task_name = 'Cognitive_task';
    patient_name = 'GRE_2019_CHRc';
    eye_tracker = 'yes';
    
end

new_patient_folder = fullfile(bdd_path, task_name, 'Patients', patient_name, 'RAW');
mkdir(new_patient_folder);

mkdir(fullfile(new_patient_folder, 'Comportement'));
mkdir(fullfile(new_patient_folder, 'sEEG'));
mkdir(fullfile(new_patient_folder, 'Training'));

if strcmp(eye_tracker, 'yes')
    mkdir(fullfile(new_patient_folder, 'Eye_tracker'));
end

end