% This function test each individual sEEG contact-pair and use an
% FDR-corrected or PERMUTATION-corrected threshold to identify onsets and offsets of the encoding
%
% INPUTS :
%   - Tall = Table containing fields : subname, channel
%   - dots = Matrix containing the regression coefficients of all channels for one ROI and one contrast (time x nb of channel in the ROI)
%   - pstat = Matrix containing the p-value of all channels for one ROI and one contrast (time x nb of channel in the ROI)
%   - tstat = Matrix containing the t-value of all channels for one ROI and one contrast (time x nb of channel in the ROI)
%   - time_win = Time window in which the stat will be performed (ex : [0 1])
%   - timelist = Time list for one contrast
%   - alpha = Significance level
%   - path2plot (optional) = path to save plots of single channel regression coefficient in the selected time window
%
% OUTPUT :
%   - Tall_chan = table with stat infos of each channels in the ROI for the selected contrast and time window
% ------------------------------------------------------------------------

function Tall_chan = single_contact_stat_ROI_ConfiDecid(Tall, dots, pstat, tstat, time_win, timelist, alpha, correction, elecpermstat, path2plot)

% Variables initialisation
Tall_chan = table('Size', [height(Tall) 6], 'VariableTypes', {'cell', 'cell', 'cell', 'cell', 'cell', 'cell'},...
    'VariableNames', {'subname', 'channel', 'significance', 'significance_onset', 'significant_offset', 'sum_tvalue'});

tw_idx = timelist >= time_win(1) & timelist <= time_win(2);
tw_timelist = timelist(tw_idx); % Filtered timelist

subjplot = '';

for chan = 1:height(Tall) % Loop over channels
    
    clearvars chan_stats
    chan_stats = NaN(size(dots, 2), 4);
    
    for prdct = 1:size(dots, 2) % Loop over glm predictors
        
        prdct_dots = dots{prdct};
        prdct_pstat = pstat{prdct};
        prdct_tstat = tstat{prdct};
        
        Tall_chan(chan,1:2) = {Tall.subname{chan}, Tall.channel{chan}};
        
        tw_chan_dots = prdct_dots(tw_idx, chan); % Regession coefficients for the specified time window, channel and predictor
        tw_chan_pval = prdct_pstat(tw_idx, chan); % p-value for the specified time window, channel and predictor
        tw_chan_tval = prdct_tstat(tw_idx, chan); % t-value for the specified time window, channel and predictor
        
        if ismember(correction, {'none', 'permutations'}) % No correction or Permutation correction
            h = double(tw_chan_pval <= alpha);
            L = spm_bwlabel(squeeze(h)); %% Now we would like the cluster t-value
        elseif ismember(correction, {'fdr'}) % FDR correction
            [~, ~, h] = fdr_BH(tw_chan_pval, alpha);
            L = spm_bwlabel(double(h)); %% Now we would like the cluster t-value
        end
        
        %% Extract mean T value of the longest cluster
        
        stats_summary_tmp = NaN(max(L),4); % Initialization
        clear filter
        
        for k = 1:max(L) % Analyse all clusters (numbered in L)
            tmp = tw_timelist(L == k);
            stats_summary_tmp(k,:) = [mean(tw_chan_dots(L==k)), tmp(1), tmp(end), sum(tw_chan_tval(L==k))];
            % (1) Moyenne des beta du cluster ; (2) Onset du cluster ; (3) Offset du cluster ; (4) Somme des tvalue du cluster
        end
        
        %% Statistical analysis of the longest cluster
        
        if isempty(stats_summary_tmp) % No cluster
            
            chan_stats(prdct,:) = [0, NaN, NaN, NaN];
            
        else % At least one cluster
            
            best_clust_idx = abs(stats_summary_tmp(:,4)) == max(abs(stats_summary_tmp(:,4))); % Index of the best cluster
            best_clust = stats_summary_tmp(best_clust_idx,:);
            
            if ismember(correction, {'fdr'}) % FDR correction
                chan_stats(prdct,:) = [1, best_clust(2), best_clust(3), best_clust(4)];
                
            elseif ismember(correction, {'permutations'}) && ~isempty(elecpermstat) % Permutation correction
                
                abs_thresh = prctile(elecpermstat(:,1,chan), 100-alpha*100);
                
                if abs(best_clust(4)) > abs_thresh % Best cluster significant
                    chan_stats(prdct,:) = [1, best_clust(2), best_clust(3), best_clust(4)];
                else % No significant cluster
                    chan_stats(prdct,:) = [0, NaN, NaN, NaN];
                end
            else % No correction
                chan_stats(prdct,:) = [NaN, best_clust(2), best_clust(3), best_clust(4)];
            end
        end
        
        %% Plot of each channel for each subj in the selected time window
        
        if exist('path2plot', 'var')
            
            if ~ismember({subjplot}, Tall_chan.subname(chan)) % New subject
                
                subjplot = Tall_chan.subname{chan};
                subjchan = 0;
                fig = figure;
                
                for p = 1:size(dots, 2)
                    subplot(size(dots, 2), 4, p*4-3:p*4-1)
                    hold on;
                    
                    subplot(size(dots, 2), 4, p*4)
                    hold on;
                    text(1.1, 1, sprintf('%10s%9s%8s%8s', 'channel', 'signif', 'dur', 'sum-tval'),...
                        'Interpreter', 'none', 'HorizontalAlignment', 'right', 'FontSize', 7);
                    axis off;
                end
                
            end % End of 'if new subject'
            
            if prdct == 1
                subjchan = subjchan + 1;
            end
            
            subplot(size(dots, 2), 4, prdct*4-3:prdct*4-1)
            hold on
            p = plot(tw_timelist, tw_chan_dots, 'DisplayName', Tall_chan.channel{chan}, 'LineWidth', 1);
            line(xlim, [0 0], 'Color', 'black', 'LineStyle', '--', 'HandleVisibility', 'off');
            
            if ismember(correction, {'none', 'fdr'}) % No correction & FDR correction
                scatter(tw_timelist(h==1), tw_chan_dots(h==1), 5, p.Color, 'filled', 'HandleVisibility', 'off');
            elseif ismember(correction, {'permutations'}) && ~isempty(elecpermstat) && ~isempty(stats_summary_tmp) % Permutation correction
                % Plot all significant clusters
                all_sig_clust = find(abs(stats_summary_tmp(:,4)) > abs_thresh);
                scatter(tw_timelist(ismember(L,all_sig_clust)), tw_chan_dots(ismember(L,all_sig_clust)), 5, p.Color, 'filled', 'HandleVisibility', 'off');
            end
            
            subplot(size(dots, 2), 4, prdct*4)
            text(1.1, 1-(subjchan*0.11), sprintf('%10s : %3.0f%10.3f%8.3f', Tall_chan.channel{chan},...
                chan_stats(prdct,1), chan_stats(prdct,3) - chan_stats(prdct,2), chan_stats(prdct,4)), 'Interpreter', 'none', 'HorizontalAlignment', 'right', 'FontSize', 6);
            
            if (chan == height(Tall) || ~ismember({subjplot}, Tall.subname(chan+1))) && prdct == size(dots, 2) % Last channel OR Last channel of the subject
                if exist('fig','var') % Save previous figure before changing subject
                    
                    subplot(size(dots, 2), 4, 1:3)
                    legend('Location','bestoutside');
                    
                    if ~exist(path2plot,'dir')
                        mkdir(path2plot);
                    end
                    
                    title(sprintf('%s', Tall_chan.subname{chan}), 'Interpreter', 'none');
                    
                    % Save figure
                    % savefig(fig,fullfile(path2plot, sprintf('%s',subjplot)),'compact'); % Matlab format
                    fig.PaperPosition = [0 0 22 11]; % Fig size [left bottom width height]
                    print(fig, fullfile(path2plot, sprintf('%s',subjplot)), '-dtiff', '-r200');
                    close(fig);
                end
            end
            
        end % End of the if 'toplot'
        
    end % End of the loop over glm predictors
    
    % Fill 'Tall_chan' table
    Tall_chan(chan,3:end) = table({chan_stats(:,1)}, {chan_stats(:,2)}, {chan_stats(:,3)}, {chan_stats(:,4)});
    
end % End of the loop over channels


end