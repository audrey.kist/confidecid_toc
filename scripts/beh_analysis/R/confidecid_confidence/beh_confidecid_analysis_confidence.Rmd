---
title: "Confidecid confidence"
author: "Audrey KIST"
date: "`r Sys.Date()`"
output: 
  html_document : 
    toc: true
    number_sections: true
    code_folding: hide
    keep_md: yes
---

# History


2024-06-17 AK 
Selecting only analyses and plots needed for articles.
Still some cleaning to do on the post-hoc tests. 


2024-06-17 AK 
Save before cleaning for article

2024-06-19 AK 
Cleaning for article + marginal effects plots



# Setup 

## R version
```{r, message=FALSE, warning=FALSE} 
R.version
```

## R and KnitR setup

```{r setup, echo=FALSE, include=FALSE, message=FALSE, warning=FALSE }
rm(list=ls())

knitr::opts_chunk$set(include=TRUE, message=FALSE, warning=FALSE)

libs2load = c('broom','dplyr','emmeans','ggeffects', 'ggplot2','ggpubr','ggsignif','ggtext','gridExtra',
              'Hmisc','knitr','lme4', 'outliers','purrr','R.matlab', 'rstatix','sjPlot',
              'stringr','tidyverse','webshot','lmerTest')


lapply(libs2load, library, character.only = TRUE)
```

List of libraries : `r libs2load`.

## Plot options

Setting default figure size, colors, theme. 

```{r plot setup }

knitr::opts_chunk$set(dev = "svg",
                      dpi = 300,out.height=400, out.width=800, fig.width=8, fig.height=4)

theme_set(theme_bw()) 

off_on_colors = c("#619CFF", "#F8766D", "#00BA38")
agent_nonagent_colors =  c("#2596be", "#ac45e2","#4b6187")

```

# Load and clean data

## Load configuration parameters

Subject DOSd will be removed because of G, L, D not taken into account for choice in any of the conditions.

```{r load_conf}
# loading paths
#source("beh_confidecid_conf_binreg.R")
  patients_datapath = "C:/Users/Audrey/OneDrive/Documents/01 data CONFIDECID-BDD/Patients"
  controls_datapath = "C:/Users/Audrey/OneDrive/Documents/01 data CONFIDECID-BDD/Controls"
  # create rderivpath folder to save figures of the day
rderivpath = "C:/Users/Audrey/OneDrive/Documents/01 data-bids/derivatives/beh_analysis"
rderivpath <- paste(rderivpath,"/",format(Sys.time(),"%Y%m%d"))
rderivpath <-   gsub(" / ", "/", rderivpath, fixed = TRUE)
if (!file.exists(rderivpath)) {
  dir.create(file.path(rderivpath))
  message("Created folder of the day in R-analysis derivatives folder")
} else {
  message("(Over)writing existing folder of the day in R-analysis derivatives folder")
}

# rt_lim_accept = 15 
# rt_lim_confidence = 15


```

```{r}
# exclude subjects and sessions :
rm_subj = c("GRE_2019_ALBi","GRE_2019_BENh","GRE_2019_BENl","GRE_2019_CECr","GRE_2019_CHAl","GRE_2019_DEPa","GRE_2019_GOUr","GRE_2019_GROe","GRE_2019_GUEm","GRE_2019_JEDm","GRE_2019_LAQm","GRE_2019_LARa","GRE_2019_LEMl","GRE_2019_LORd","GRE_2019_MARm","GRE_2019_PONe","GRE_2019_RIBm","GRE_2019_RODt","GRE_2019_SZCr","GRE_2019_TESt","GRE_2019_VESa","GRE_2019_ZARa","GRE_2019_ZOUr","GRE_2020_COUg","GRE_2020_OLIe","GRE_2020_RIEj","GRE_2020_SANr","GRE_2020_TESt","GRE_2021_BEDn","GRE_2020_DOSd")
```


```{r}
dfconfi_raw <- read.csv(file = '/Users/audreykist/Documents/73 phd follow-up/confidecid/data/dfconfi_raw_binreg_allsubj_fromsanitychecksscript.csv')

dfconfi_raw$gr<-as.factor(dfconfi_raw$gr)
dfconfi_raw$subj<-as.factor(dfconfi_raw$subj)
dfconfi_raw$agentivity_factor<-as.factor(dfconfi_raw$agentivity_factor)
dfconfi_raw$ses <- as.character(dfconfi_raw$ses)
dfconfi_raw$ses <- factor(dfconfi_raw$ses, levels=c("off", "on","ctr"))

summary(dfconfi_raw)
```


Subjects to remove are: `r rm_subj`.

They are either tests/pilots, data from an older dataset, or re-recorded data of the same subject. 

Raw data stored as default Confidecid filename.

## Load raw data

```{r load_raw}

# getting matlab files
confi_behfiles_patients <- list.files(path = patients_datapath, 
                                  pattern = '.*cognition_data.mat', all.files = FALSE,
                                  full.names = TRUE, recursive = TRUE,
                                  ignore.case = TRUE, include.dirs = TRUE, no.. = FALSE)
confi_behfiles_controls <- list.files(path = controls_datapath, 
                                  pattern = '.*cognition_data.mat', all.files = FALSE,
                                  full.names = TRUE, recursive = TRUE,
                                  ignore.case = TRUE, include.dirs = TRUE, no.. = FALSE)

confi_behfiles <- cbind(confi_behfiles_patients,confi_behfiles_controls)

# exclude some files
confi_excludefiles_patients <- list.files(path = patients_datapath, 
                             pattern = '.*0_cognition_data.mat', all.files = FALSE,
                             full.names = TRUE, recursive = TRUE,
                             ignore.case = TRUE, include.dirs = TRUE, no.. = FALSE)
confi_excludefiles_controls <- list.files(path = controls_datapath, 
                             pattern = '.*0_cognition_data.mat', all.files = FALSE,
                             full.names = TRUE, recursive = TRUE,
                             ignore.case = TRUE, include.dirs = TRUE, no.. = FALSE)
confi_excludefiles <- cbind(confi_excludefiles_patients,confi_excludefiles_controls)
confi_behfiles <-setdiff(confi_behfiles,confi_excludefiles) 

# loading files
for (name in confi_behfiles) {
  
  # read matlab file and convert to dataframe
  tempf = readMat(name)
  tempdf <- as.data.frame(tempf$data)
  
  tempdf$file = name
    
  # fill subject column
  tempdf$V1 = str_sub(name, -44,-32)
  
  # fill ses column 

  if (str_detect(name, "Controls" )) {
      tempdf$ses = "ctr"
  } else { 
  # "(on)|(off)" 
      if (str_detect(name, "(on)|(off)" )) {
        tempses = str_sub(name, -48,-46)
        tempdf$ses = str_extract(tempses, "[a-z]+" )
      }
  }
  
  # fill trial number ??? 
  # binding tables  
  if (exists('dfconfi_raw')) {
    dfconfi_raw = rbind(dfconfi_raw, tempdf)
  } else {
    dfconfi_raw = tempdf
  }
}
  
# rename variables 
dfconfi_raw <- dfconfi_raw %>% 
  rename(
    subj = V1, 
    run = V2,
    spaceconf = V3,
    gain = V4,
    loss = V5,
    diffic = V6,
    accept = V7,
    rt_accept = V8,
    confidence = V9,
    rt_confidence = V10,
    agentivity = V11, # tbd : as factor
    success = V12,
    rt_stopball = V13,
    mood = V14,
    rt_mood = V15
  ) 

# subj as factor
dfconfi_raw$subj<-as.factor(dfconfi_raw$subj)

#create agentivity factor
dfconfi_raw$agentivity_factor <- "agent"
dfconfi_raw[dfconfi_raw$agentivity==0,]$agentivity_factor = "non agent"
dfconfi_raw$agentivity_factor<-as.factor(dfconfi_raw$agentivity_factor)

# create gr parameter
dfconfi_raw$gr = "patients"
dfconfi_raw$gr[dfconfi_raw$ses=='ctr'] = "controls"
dfconfi_raw$gr<-as.factor(dfconfi_raw$gr)

# ses : as factor - order parameter 
dfconfi_raw$ses <- as.character(dfconfi_raw$ses)
dfconfi_raw$ses <- factor(dfconfi_raw$ses, levels=c("off", "on","ctr"))


summary(dfconfi_raw)


```

Add z-scored confidence (across subjects)
```{r}
# compute confidence z-score for each subject, across on and on conditions alltogether
# and across agentivity
dfconfi_raw <- dfconfi_raw %>%
  group_by(subj) %>%
  mutate(z_confidence = unlist(scale(confidence))) 

summary(dfconfi_raw)
```

Save dataframe
```{r}
# saving raw dataframe
dfconfirawfilename <- paste(rderivpath,"/dfconfi_raw_confidence.csv")
dfconfirawfilename <-   gsub(" /", "/", dfconfirawfilename, fixed = TRUE)
write.csv(dfconfi_raw, dfconfirawfilename, row.names = TRUE)

subjlist <- unique(dfconfi_raw$subj)
subjnum <- length(subjlist)
trialnum <- nrow(dfconfi_raw)
```


In the raw  data there are `r subjnum` subjects: `r subjlist` 

and `r trialnum` trials.

Removing bad subjects. 
```{r }
df <- dfconfi_raw
df <- df[!df$subj %in% rm_subj,]
```

Getting metrics.
```{r }
subjlist <- unique(df$subj)
subjnum <- length(subjlist)
trialnum <- nrow(df)

# * for acceptance, remove trial if RT > `r rt_lim_accept`.
# * for confidence, remove trial if RT > `r rt_lim_confidence`.
```

In the working df there are `r subjnum` subjects: `r subjlist` 

and `r trialnum` trials. 


## Select on-off data

Selecting.
```{r}
df_onoff <- subset(df, ses %in% c("on", "off"))
df_onoff$accept <- as.factor(df_onoff$accept)
df_onoff$success <- as.factor(df_onoff$success)
```

On-off data summary:
```{r}
summary(df_onoff)
```

# RAW confidence: check effect of agentivity

## Compare agent to non-agent, in both stimulation groups

### Plot Data

```{r figure3_a, fig.width=3, fig.height=4}
# compte mean and std conf, per participant and per condition
mydf <- df_onoff %>% group_by(subj,ses,agentivity_factor) %>% 
  summarise(confid_mean = mean(as.numeric(confidence)))

# create plot
confidence_agentivity_plot_on  <- ggplot(mydf[mydf$ses == "on",],
                                     aes(x = agentivity_factor, y = confid_mean))+
  geom_bar(aes(x = agentivity_factor,
                y = confid_mean,
                alpha = agentivity_factor),
            stat = "summary",
            fun.y = "mean_se",
            size = 0.8,
            width = 0.6,
           color =  c("#F8766D"),
           fill = c("#F8766D"))+
  geom_errorbar(aes(x = agentivity_factor,
                    y = confid_mean),
                color = "#F8766D", size = 1.2, width = 0.4, 
                  stat = "summary", fun.y = "mean_se")+
  geom_point( aes(group = subj),  color = "#F8766D") +
  geom_line(aes(group = subj), color = "black")+
  scale_alpha_manual(values = c(0.6, 0.2))+
  theme_bw()+
  ylim(0,60)+
  guides(color = FALSE, fill = FALSE, alpha = FALSE)+
    theme(axis.title.x=element_blank(),
        axis.text.x=element_blank(),
        axis.ticks.x=element_blank(),
        axis.title.y=element_blank())
confidence_agentivity_plot_on

confidence_agentivity_plot_off  <- ggplot(mydf[mydf$ses == "off",],
                                     aes(x = agentivity_factor, y = confid_mean))+
  geom_bar(aes(x = agentivity_factor,
                y = confid_mean,
                alpha = agentivity_factor),
            stat = "summary",
            fun.y = "mean_se",
            size = 0.8,
            width = 0.6,
           color =  "#619CFF",
           fill = "#619CFF" )+
  geom_errorbar(aes(x = agentivity_factor,
                    y = confid_mean),
                color =  "#619CFF", size = 1.2, width = 0.4, 
                  stat = "summary", fun.y = "mean_se")+
  geom_point( aes(group = subj),  color =  "#619CFF") +
  geom_line(aes(group = subj), color = "black")+
  scale_alpha_manual(values = c(0.8, 0.2))+
  theme_bw()+
  ylim(0,60)+
  guides(color = FALSE, fill = FALSE, alpha = FALSE)+
  theme(axis.title.x=element_blank(),
        axis.text.x=element_blank(),
        axis.ticks.x=element_blank(),
        axis.title.y=element_blank())
confidence_agentivity_plot_off

ggarrange(confidence_agentivity_plot_off, confidence_agentivity_plot_on)
```

### Paired T-tests

```{r}

# do paired t-test between agent and non-agent
t_test_results_pconfid <- mydf %>%
  group_by(ses) %>%
  do(tidy(t.test(.$confid_mean ~ .$agentivity_factor, paired = TRUE)))


kable(t_test_results_pconfid, caption = "Paired T-test between agent and non-agent for Confidence")
```


## Compare ON to OFF, in both agentivity groups

### Plot Data

```{r fig.width=3, fig.height=4}
# compte mean and std conf, per participant and per condition
mydf <- df_onoff %>% group_by(subj,ses,agentivity_factor) %>% 
  summarise(confid_mean = mean(as.numeric(confidence)))

# create plot
confidence_stim_plot_agent  <- ggplot(mydf[mydf$agentivity_factor == "agent",],
                                     aes(x = ses, y = confid_mean))+
  geom_bar(aes(x = ses,
                y = confid_mean, color = ses, fill = ses),
            stat = "summary",
            fun.y = "mean_se",
            size = 0.8,
            width = 0.6,
           alpha = 0.8)+
  geom_errorbar(aes(x = ses,
                    y = confid_mean, 
                    color = ses),
                size = 1.2, width = 0.4, 
                stat = "summary", fun.y = "mean_se")+
  geom_point( aes(group = subj, color = ses)) +
  geom_line(aes(group = subj), color = "black")+
  scale_color_manual(values = off_on_colors)+
  scale_fill_manual(values = off_on_colors)+
  theme_bw()+
  ylim(0,60)+
  guides(color = FALSE, fill = FALSE, alpha = FALSE)+
    theme(axis.title.x=element_blank(),
        axis.text.x=element_blank(),
        axis.ticks.x=element_blank(),
        axis.title.y=element_blank())
confidence_stim_plot_agent

confidence_stim_plot_nonagent  <- ggplot(mydf[mydf$agentivity_factor == "non agent",],
                                     aes(x = ses, y = confid_mean))+
  geom_bar(aes(x = ses,
              y = confid_mean, color = ses, fill = ses),
            stat = "summary",
            fun.y = "mean_se",
            size = 0.8,
            width = 0.6,
           alpha = 0.2)+
  geom_errorbar(aes(x = ses,
                    y = confid_mean, 
                    color = ses),
                size = 1.2, width = 0.4, 
                stat = "summary", fun.y = "mean_se")+
  geom_point( aes(group = subj, color = ses)) +
  geom_line(aes(group = subj), color = "black")+
  scale_color_manual(values = off_on_colors)+
  scale_fill_manual(values = off_on_colors)+
  theme_bw()+
  ylim(0,60)+
  guides(color = FALSE, fill = FALSE, alpha = FALSE)+
    theme(axis.title.x=element_blank(),
        axis.text.x=element_blank(),
        axis.ticks.x=element_blank(),
        axis.title.y=element_blank())
confidence_stim_plot_nonagent

ggarrange(confidence_stim_plot_agent, confidence_stim_plot_nonagent)
```

### Paired T-tests

```{r}
# compte mean and std p(accept), per participant and per condition
mydf <- df_onoff %>% group_by(subj,ses,agentivity_factor) %>% 
  summarise(confidence_mean = mean(as.numeric(confidence)))

# do paired t-test between agent and non-agent
t_test_results_pconfid <- mydf %>%
  group_by(agentivity_factor) %>%
  do(tidy(t.test(.$confidence_mean ~ .$ses, paired = TRUE)))

kable(t_test_results_pconfid, caption = "Paired T-test between ON and OFF for confidence, in each agentivity condition")
```


## COmpare on to off without accounting for agentivity

```{r}
df_onoff %>%
  group_by(subj, ses) %>%
  group_by(ses)%>%
  get_summary_stats(confidence, type = "mean_se") %>%
  dplyr::select(-variable) %>%
  kable(caption = "Mean and SE for all parameters, all sessions, ignoring agentivity")

mydf <- df_onoff %>%
  group_by(subj, ses) %>%
  summarise(confidence = mean(confidence))

t.test(mydf$confidence[mydf$ses == 'on'], mydf$confidence[mydf$ses == 'off'], paired = TRUE)
```


# z-Confidence - GLMM 


## descriptive statistics

```{r}
df_onoff %>%
  group_by(ses)%>%
  get_summary_stats(z_confidence, type = "mean_se") %>%
  dplyr::select(-variable) %>%
  kable(caption = "Mean and SE for all parameters, all sessions, ignoring agentivity")

mydf <- df_onoff %>%
  group_by(subj, ses) %>%
  summarise(z_confidence = mean(z_confidence))

t.test(mydf$z_confidence[mydf$ses == 'on'], mydf$z_confidence[mydf$ses == 'off'])
```


## GLMM z_confidence ~ (gain + loss + diffic) * ses * accept + (1|subj)

Computing model.
```{r}
m_zconfidence <-lmer(z_confidence ~ (gain + loss + diffic) * ses * accept + (1|subj), df_onoff)
summary(m_zconfidence) 
```

Fixed effects: 

* loss                  4.108e-03  1.952e-03  6.832e+03   2.104 0.035404 *  
* diffic               -5.386e-03  2.412e-03  6.832e+03  -2.233 0.025563 *  
* seson                 3.319e-01  1.593e-01  6.832e+03   2.084 0.037177 *  
* accept1               1.412e+00  1.631e-01  6.832e+03   8.655  < 2e-16 ***
* loss:seson           -6.477e-03  2.823e-03  6.832e+03  -2.295 0.021775 *  
* diffic:seson         -9.212e-03  3.456e-03  6.832e+03  -2.666 0.007702 ** 
* loss:accept1         -1.149e-02  2.909e-03  6.832e+03  -3.952 7.84e-05 ***
* diffic:accept1       -1.384e-02  3.715e-03  6.832e+03  -3.726 0.000196 ***
* seson:accept1        -7.236e-01  2.274e-01  6.832e+03  -3.181 0.001472 ** 

Effects of stimulation only:

* significant effect of stimulation p = 0.037177 *  
* significant interaction stimulation x loss p = 0.021775 *  
* very significant interaction stimulation x difficulty p= 0.007702 ** 
* very significant interaction stimulation x choice p = 0.001472 ** 

Models coefficients for each subj:
```{r}
subj_coeff <- coef(m_zconfidence)$subj
subj_coeff
```


## DATA PLOT z-scored confidence ~ (g+l+d) x choice x stimulation + (1|subj) 

```{r figure3_B_1, fig.height=4, fig.width=6}

df_stack_gld2 <- data.frame(df_onoff[1], df_onoff[17:19], stack(df_onoff[4:6]), df_onoff[7],unlist(df_onoff[20]))
names(df_stack_gld2)[8] <- "z_confidence"

p <- df_stack_gld2 %>%
  ggplot(aes(x = values, y =z_confidence, color = ses:accept, group = ses:accept)) +
  stat_summary_bin(fun = mean, binwidth = 10, geom = "point", size = 0.5) +
  stat_summary_bin(fun.data = mean_ci, binwidth = 10, geom = "linerange",aes(width = 0.01)) +
  labs(x = "value", y = "z_confidence") +
  scale_color_manual(values = c("#94bcff", "#004ac8", "#f98d85", "#dc180a")) +
 xlim(10,50)+
  coord_cartesian(ylim = c(-0.7, 0.7))+
  facet_wrap(~ind)+
  geom_hline(yintercept = 0, color = "black") +
  theme_bw()+
     guides(color = FALSE, fill = FALSE, alpha = FALSE)
p
```


```{r, fig.height=4, fig.width=8}
p+  geom_hline(yintercept = 0, color = "black")+
      theme(axis.title.x=element_blank(),
            axis.title.y=element_blank(),
        axis.text.x=element_blank(),
        axis.text.y=element_blank(),
         strip.text.x = element_blank())+
  labs(title = NULL) +
  theme(panel.spacing = unit(2, "lines"))
```

## Prediction plot zconf

* Predict each trial result with model
* Stack the dataframe on the G, L, D parameters
* plot 

```{r fig.height=4, fig.width=6}
# predict values from the model
df_pred <- df_onoff
df_pred$predicted_zconfidence <- predict(m_zconfidence, newdata = df_pred)

pred_stack <- data.frame(df_pred[1], df_pred[17:19], stack(df_pred[4:6]), df_pred[7], df_pred[21])

p <- pred_stack %>%
  ggplot(aes(x = values, y =predicted_zconfidence , color = ses:accept, group = ses:accept, fill = ses:accept)) +
 stat_summary_bin(fun = mean, binwidth = 10, geom = "line") +
    scale_color_manual(values = c("#94bcff", "#004ac8", "#f98d85", "#dc180a")) +
    scale_fill_manual(values = c("#94bcff", "#004ac8", "#f98d85", "#dc180a")) +
  xlim(10,50)+
  coord_cartesian(ylim = c(-0.7, 0.7))+
  facet_wrap(~ind) +
  geom_hline(yintercept = 0, color = "black") +
    theme(panel.grid.major = element_blank(), panel.grid.minor = element_blank())+
  guides(color = FALSE, fill = FALSE, alpha = FALSE)+
  theme_bw()

p

```

```{r, fig.height=4, fig.width=8}
p+  geom_hline(yintercept = 0, color = "black")+
      theme(axis.title.x=element_blank(),
            axis.title.y=element_blank(),
        axis.text.x=element_blank(),
        axis.text.y=element_blank(),
         strip.text.x = element_blank())+
  labs(title = NULL) +
  theme(panel.spacing = unit(2, "lines"))
```


## plot all effects of stimulation
Chunk deactivated (see after)
```{r, fig.width=4, fig.height=4}
# plot_model(m_zconfidence, type = "pred", terms = c("ses"))
# plot_model(m_zconfidence, type = "pred", terms = c("diffic", "ses"))
# plot_model(m_zconfidence, type = "pred", terms = c("loss", "ses"))
# plot_model(m_zconfidence, type = "pred", terms = c("accept", "ses"))
```

## Effect of stimulation
-
### plot

```{r}
myplot <- plot_model(m_zconfidence, type = "pred", terms = c("ses"), 
           # aes(color = c("ses")), 
           colors = off_on_colors )+
  ylim(-0.5, 0)
myplot

```
```{r, fig.width = 1.5, fig.height = 4}
myplot + 
  theme(legend.position = "none") +
  geom_hline(yintercept = 0, color = "black")+
      theme(axis.title.x=element_blank(),
            axis.title.y=element_blank(),
        axis.text.x=element_blank(),
        axis.text.y=element_blank())+
  labs(title = NULL) 
```

```{r }
mydf <- df_onoff %>% group_by(subj,ses) %>% 
  summarise(zconfidence_mean = mean(as.numeric(z_confidence)))

zconf_plot_offon  <- ggplot(mydf, aes(x = ses, y = zconfidence_mean))+
  geom_bar(aes(x = ses,
               y = zconfidence_mean, color = ses, fill = ses),
            stat = "summary",
            fun.y = "mean_se",
            size = 0.8,
            width = 0.6,
           alpha = 0.4)+
  geom_errorbar(aes(x = ses,
                    y = zconfidence_mean, 
                    color = ses),
                size = 1.2, width = 0.4, 
                stat = "summary", fun.y = "mean_se")+
  scale_color_manual(values = off_on_colors)+
  scale_fill_manual(values = off_on_colors)+
   geom_point( aes(group = subj, color = ses)) +
 geom_line(aes(group = subj), color = "black")
zconf_plot_offon



```
```{r, fig.width=2.5, fig.height=4}
zconf_plot_offon +
      theme(axis.title.x=element_blank(),
            axis.title.y=element_blank(),
        axis.text.x=element_blank(),
        axis.text.y=element_blank())+
  labs(title = NULL) 
```

### test 

seson                 3.319e-01  1.593e-01  6.832e+03   2.084 0.037177 *  

```{r}
my_emm = emmeans(m_zconfidence, pairwise~ses)
kable(my_emm$emmeans,digits = 2, caption = "Post-hoc effect of group on confidence - means" )
kable(my_emm$contrasts,digits = 2, caption = "Post-hoc effect of group on confidence - contrasts" )
```


## Effect stimulation x loss

### Marginal effects plot 

```{r, fig.width=4, fig.height=4}
myplot<- plot_model(m_zconfidence, type = "pred", terms = c("loss", "ses"))+
  scale_color_manual(values = off_on_colors)+
  scale_fill_manual(values = off_on_colors)
myplot
```
```{r, fig.width = 1.5, fig.height = 4}
myplot + 
  theme(legend.position = "none") +
  geom_hline(yintercept = 0, color = "black")+
      theme(axis.title.x=element_blank(),
            axis.title.y=element_blank(),
        axis.text.x=element_blank(),
        axis.text.y=element_blank())+
  labs(title = NULL) 
```


### test

loss:seson           -6.477e-03  2.823e-03  6.832e+03  -2.295 0.021775 *  

```{r}
my_emm = emtrends(m_zconfidence, pairwise~ses, var="loss")
kable(my_emm$emtrends,digits = 2, caption = "Post-hoc effect of dbs*loss on confidence - trends" )
kable(my_emm$contrasts,digits = 2, caption = "Post-hoc effect of dbs*loss on confidence - contrasts" )
```


## Effect stimulation x difficulty

### Marginal effects plot

```{r, fig.width=4, fig.height=4}
myplot<- plot_model(m_zconfidence, type = "pred", terms = c("diffic", "ses"))+
  scale_color_manual(values = off_on_colors)+
  scale_fill_manual(values = off_on_colors)
myplot
```

```{r, fig.width = 1.5, fig.height = 4}
myplot + 
  theme(legend.position = "none") +
  geom_hline(yintercept = 0, color = "black")+
      theme(axis.title.x=element_blank(),
            axis.title.y=element_blank(),
        axis.text.x=element_blank(),
        axis.text.y=element_blank())+
  labs(title = NULL) 
```

### test

seson:diffic           -0.16397    0.05404 6820.11542  -3.034  0.00242 ** 

```{r}
my_emm = emtrends(m_zconfidence, pairwise~ses, var="diffic")

kable(my_emm$emtrends,digits = 2, caption = "Post-hoc effect of stim*diffic  on confidence - trends" )
kable(my_emm$contrasts,digits = 2, caption = "Post-hoc effect of stim*diffic on confidence - contrasts" )

```





## Effect choice * dbs



### plot


```{r }
myplot <- plot_model(m_zconfidence, type = "pred", terms = c("accept", "ses")) +
    scale_color_manual(values = off_on_colors)+
  scale_fill_manual(values = off_on_colors)

myplot

```

```{r, fig.width = 1.5, fig.height = 4}
myplot + 
  theme(legend.position = "none") +
  geom_hline(yintercept = 0, color = "black")+
      theme(axis.title.x=element_blank(),
            axis.title.y=element_blank(),
        axis.text.x=element_blank(),
        axis.text.y=element_blank())+
  labs(title = NULL) 
```


### tests
seson:accept1        -7.236e-01  2.274e-01  6.832e+03  -3.181 0.001472 ** 

```{r}
my_emm2 = emmeans(m_zconfidence, ~ses * accept)
pairs(my_emm2, simple = "each")
```




```{r}
my_emm = emmeans(m_zconfidence, pairwise~ses|accept, adjust = "bonferroni")
kable(my_emm$emmeans,digits = 2, caption = "Post-hoc effect of group*choice on confidence - means" )
kable(my_emm$contrasts,digits = 2, caption = "Post-hoc effect of group*choice on confidence - contrasts" )
```

# Individual models

```{r}
subj_mdl_df <- df_onoff %>%
  group_by(subj) %>%
  do(tidy(lm(z_confidence ~ (gain+loss+diffic) * accept * ses, data = .)))
subj_mdl_df
```


