% Analyses comportement CONFIDENCE-DECID COGNITION task

% clearvars
% close all

% TO DO AUDREY
% nombre de sessions variable par patient
% ajouter une boucle on-off
% Mood model not available because of TML_ConfiDecid_VBA not transmitted
% plot p(accept) = f(utility)
% v�rifier les plots liss�s : est-ce bien une r�gression lin�aire ? �a
% fitte pas du tout. => r�gression incorrecte sur GAIN ?
% v�rifier les intervalles de confiance sur les plots bin�s, �a ressemble
% pas aux donn�es
% mettre des titres sur les figures. 
% renommer figure corr�lation en r�gression
% comment r�cup�rer le t-test ? 
%
% faire des figures on et off de la m�me taille ! 
%


%% load conf file
% confidecid_beh_conf;

%% Process only 1 subj
% input parameters
subj.name = conf.subj;
subj.ses = conf.ses;

fprintf('[subj-analysis] START sub-%s ses-%s ======================= \n', subj.name, subj.ses)

% create file name for choice model
subj.choicefile = fullfile(conf.derivpath, append('sub-',subj.name,'_ses-', subj.ses, ...
        '_task-confidecid_choicedata_beh.mat'));

    
    %%
if (conf.computechoice == 1)
    fprintf('[subj-analysis] start processing choice sub-%s ses-%s ======================= \n', subj.name, subj.ses)

    % concatenate beh raw data for subject
    subj = confidecid_single_subjses_rawdata(subj,conf);

    % process choice model
    subj = confidecid_single_subjses_computechoice(subj,conf);

    % Save choice data
    save(subj.choicefile, 'subj');
    fprintf('[subj-analysis] saving choice data to %s \n', subj.choicefile)
    fprintf('[subj-analysis] END PROCESSING sub-%s ses-%s CHOICE ======================= \n', subj.name, subj.ses)
else
    % load data from previously processed file
	fprintf('[subj-analysis] loading choice data from existing file \n');
	x = load(subj.choicefile);
	subj = x.subj; 
	clear x;
end
    
%% MOOD MODEL
% create file name for mood model
subj.moodfile = fullfile(conf.derivpath, append('sub-',subj.name,'_ses-', subj.ses, ...
        '_task-confidecid_mooddata_beh.mat'));

if (conf.computemood == 1)
    fprintf('[subj-analysis] START PROCESSING sub-%s SES-%s MOOD ======================= \n', subj.name, subj.ses)
    subj = confidecid_single_subjses_computemood(subj, conf);
    % Save mood data
    fprintf('[subj-analysis] saving mood data to %s \n', subj.moodfile)
    save(subj.moodfile, 'subj');    
    fprintf('[subj-analysis] END PROCESSING sub-%s SES %s MOOD ======================= \n', subj.name, subj.ses)
else
    % load data from previously processed file
% 	fprintf('[subj-analysis] loading mood data from existing file \n');
% 	x = load(subj.moodfile);
% 	subj = x.subj; 
% 	clear x;
end


%% Mood models comparison
% does not work yet, needs to be cleaned up
% it seems that agent was taken as an argument, but never changed : took
% only the last value of agentivity, therefore processsed everything
% altogether ?? 
% 
if (conf.comparemoodmodel == 1)

    if ~isfield(subj.mood_data, 'best_model')

        p = VBA_groupBMC(subj.mood_data.inversionResult.FF); close all;
        best_model = find(p.a == max(p.a));
        
        % ajouter ici agentivit�
        null_model = find(ismember({subj.mood_data.inversionResult.param.modelSpace.moodType}, 'null'));

        subj.mood_data.best_model = best_model;
        subj.mood_data.null_model = null_model;
        save(mood_data_fullname, 'mood_data');

        % Plot best mood model
        plot_best_mood_model(init, subj.mood_data)

    end
end
%% ANALYSE 1 : Probabilit� d'accepter en fonction des dimensions de la t�che et de l'agentivit�

if (conf.analyzechoice ==1) 
    % the dependant variable of interest is choice (1 = accept ; 0 = reject)
    opt.depdt_variable_col = 7; 
    opt.ylabel = 'P(accept)';

    % ANALYSE 1.A : probabilit� d'accepter le d�fi en fonction de chaque dimension de la t�che pour chaque sujet
    opt.YTick = (0:.2:1);
    opt.axis = [0 5 0 1];
    opt.subj_fig_name = '_gld_effect_on_accept.tiff';
    %task_dim_effect_on_var_1subjses(subj, conf, opt);
    confidecid_single_subjses_kgt_effectonvar(subj, conf, opt);


    % ANALYSE 1.B : corr�lation entre la probabilit� d'accepter le d�fi et chaque dimension de la t�che pour chaque sujets
    opt.subj_fig_name = '_correl_gld_accept.tiff';
    opt.ylim = [0 1];
    % the beta is (should be?) the slope of the regression 
    % but is the regression correct ? and is the t-test paired ?
    beta_corr = corr_task_dim_effect_subjses(subj, conf, opt);

    %% ANALYSE 1.C : ttest sur les corr�lations obtenues dans l'analyse B
    % attention � remplacer init 
%     opt.ylabel = 'choice';
%     opt.DataAspectRatio = [1 0.2 1];
%     opt.all_fig_name = 'slope_correl_task_dim_and_choice.tiff';
%     
%     ttest_beta_corr(beta_corr, init, opt)
end

%% ANALYSE 2 : Note de confiance en fonction des dimensions de la t�che et de l'agentivit�
if (conf.analyzeconfidence ==1) 
    opt.depdt_variable_col = 9; % confidence rating -  1 to 100
    opt.ylabel = 'Confidence';

    % ANALYSE 2.A : confiance en fonction des dimensions de la t�che
    opt.YTick = (0:25:100);
    opt.axis = [0 5 0 100];
    opt.subj_fig_name = '_gld_effect_on_confidence.tiff';
    opt.all_fig_name = 'confidence_according2dim.tiff';

    %task_dim_effect_on_var(init, opt);

    % opt.subj_fig_name = '_gld_effect_on_accept.tiff';
    % task_dim_effect_on_var_1subjses(subj, conf, opt);
    confidecid_single_subjses_kgt_effectonvar(subj, conf, opt);


    %
    % % ANALYSE 1.B : corr�lation entre la probabilit� d'accepter le d�fi et chaque dimension de la t�che pour chaque sujets
    % opt.subj_fig_name = '_correl_task_dim_and_confidence.tiff';
    % opt.ylim = [0 100];
    %
    % beta_corr = corr_task_dim_effect(init, opt);
    %%
    opt.subj_fig_name = '_correl_gld_confidence.tiff';
    opt.ylim = [0 100];
    % the beta is (should be?) the slope of the regression 
    % but is the regression correct ? and is the t-test paired ?
    beta_corr = corr_task_dim_effect_subjses(subj, conf, opt);
    %%
    %
    % % ANALYSE 2.C : ttest sur les corr�lations obtenues dans l'analyse B
    % opt.ylabel = 'confidence';
    % opt.DataAspectRatio = [1 1 1];
    % opt.all_fig_name = 'slope_correl_task_dim_and_confidence.tiff';
    %
    % ttest_beta_corr(beta_corr, init, opt)
end
%% ANALYSE 3 : Performance according to task dimension and agentivity

% opt.depdt_variable_col = 12; % Challenge result (1 = success ; 0 = failure)
% opt.ylabel = 'P(success)';
%
% % ANALYSE 3.A : confiance en fonction des dimensions de la t�che
% opt.YTick = (0:.2:1);
% opt.axis = [0 5 0 1];
% opt.subj_fig_name = '_task_dim_effect_on_success.tiff';
% opt.all_fig_name = 'p_success_according2dim.tiff';
%
% task_dim_effect_on_var(init, opt);
%
% % ANALYSE 3.B : corr�lation entre la probabilit� d'accepter le d�fi et chaque dimension de la t�che pour chaque sujets
% opt.subj_fig_name = '_correl_task_dim_and_perf.tiff';
% opt.ylim = [0 1];
%
% beta_corr = corr_task_dim_effect(init, opt);
%
% % ANALYSE 3.C : ttest sur les corr�lations obtenues dans l'analyse B
% opt.ylabel = 'perf';
% opt.DataAspectRatio = [1 0.2 1];
% opt.all_fig_name = 'slope_correl_task_dim_and_perf.tiff';
%
% ttest_beta_corr(beta_corr, init, opt)
%
% % ANALYSE 3.D : Perfomance according to agentivity
% perf_acc2agent(init)

%% ANALYSE 4 : Choice model analysis
if (conf.analyzechoice ==1) 
    %choice_model_analysis(init, choice_data) % Plot of the model
   % choice_model_analysis(init, choice_data) % Plot of the model %"init" does not work yet

    % confidecid_single_subjses_kgt_effectonvar(subj, conf, opt);
end 
%% FIN
%return;
fprintf('[subj-analysis] END sub-%s ses-%s ======================= \n', subj.name, subj.ses)

