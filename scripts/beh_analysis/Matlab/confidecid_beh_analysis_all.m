%% Analyses comportement CONFIDENCE-DECID COGNITION task
% for ALL subjects

fprintf('[allsubj-analysis] START analyzing all on-off subjeects and sessions ======================= \n')

%% Loop on subjects

for i_subj = 1:height(conf.subjects_table)
    if ismissing(conf.subjects_table(i_subj,'subj_name'))
        % if subject is empty, skip line
        continue
    else
        conf.subj = conf.subjects_table{i_subj,'subj_name'};

        % loop on session conf.seslist = {'off','on'}; 
        for i_ses = 1:length(conf.seslist)
            % starts with off, then on
            % off runs value is in column number 2, on in col 3 
            conf.ses = conf.seslist{i_ses};
            
            conf.nbruns = conf.subjects_table{i_subj,i_ses+1}; 
            conf.computechoice = conf.subjects_table{i_subj, 'computechoice'}; % if 1 then compute mood; else load choice data from file 
            conf.computemood = conf.subjects_table{i_subj, 'computemood'};  % if 1 then compute mood; else skip mood completely
            conf.comparemoodmodel = conf.subjects_table{i_subj, 'comparemoodmodel'}; 
            conf.analyzechoice = conf.subjects_table{i_subj, 'analyzechoice'}; % if 1 then plot acceptance data, else do nothing 
            conf.analyzeconfidence = conf.subjects_table{i_subj, 'analyzeconfidence'};  % if 1 then plot confidence data, else do nothing 
            
            % perform analysis
            fprintf('[subj-analysis-%s-%s] START ======================= \n', conf.subj, conf.ses)
            confidecid_beh_analysis;
            fprintf('[subj-analysis-%s-%s] END ======================= \n', conf.subj, conf.ses)

            % go to next ses
        end
    end
end
fprintf('[allsubj-analysis] END All done. Have a nice day! ======================= \n')
