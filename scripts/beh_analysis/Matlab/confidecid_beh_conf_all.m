%% confidecid_beh_conf script 
% options for using perithr_behav_onoff

clearvars
close all

fprintf('[conf] START CONF ======================== \n');

addpath functions

%% create a table with all subjects options to know where to loop
sz = [15 8];
varTypes = ["string",   "int8",        "int8",       "logical",          "logical",        "logical","logical","logical"];
varNames = ["subj_name","nbruns_off", "nbruns_on", "computechoice", "computemood", "comparemoodmodel", "analyzechoice", "analyzeconfidence"];
conf.subjects_table = table('Size',sz,'VariableTypes',varTypes,'VariableNames',varNames);

conf.subjects_table(1,:) = {'bedn', 4,4,1,0,0,0,0};
conf.subjects_table(2,:) = {'bons', 4,4,1,0,0,0,0};
conf.subjects_table(3,:) = {'chae', 4,4,1,0,0,0,0};
conf.subjects_table(4,:) = {'dals', 4,4,1,0,0,0,0};
conf.subjects_table(5,:) = {'dosd', 5,4,1,0,0,0,0};
conf.subjects_table(6,:) = {'roba', 4,4,1,0,0,0,0};
conf.subjects_table(7,:) = {'sacn', 5,5,1,0,0,0,0};
conf.subjects_table(8,:) = {'seyc', 4,4,1,0,0,0,0};
conf.subjects_table(9,:) = {'sire', 4,4,1,0,0,0,0};
conf.subjects_table(10,:) = {'szcr', 4,4,1,0,0,0,0};

% conf.subjects_table(1,:) = {'bedn', 4,4,1,1,1,1,1};
% conf.subjects_table(2,:) = {'bons', 4,4,1,1,1,1,1};
% conf.subjects_table(3,:) = {'chae', 4,4,1,1,1,1,1};
% conf.subjects_table(4,:) = {'dals', 4,4,1,1,1,1,1};
% conf.subjects_table(5,:) = {'dosd', 5,4,1,1,1,1,1};
% conf.subjects_table(6,:) = {'roba', 4,4,1,1,1,1,1};
% conf.subjects_table(7,:) = {'sacn', 5,5,1,1,1,1,1};
% conf.subjects_table(8,:) = {'seyc', 4,4,1,1,1,1,1};
% conf.subjects_table(9,:) = {'sire', 4,4,1,1,1,1,1};
% conf.subjects_table(10,:) = {'szcr', 4,4,1,1,1,1,1};
% conf.subjects_table(11,:) = {'', 4,4,1,1,1,1,1};
% conf.subjects_table(12,:) = {'gaue', 4,4,1,1,1,1,1};
% conf.subjects_table(13,:) = {'gaue', 4,4,1,1,1,1,1};
% conf.subjects_table(14,:) = {'gaue', 4,4,1,1,1,1,1};
% conf.subjects_table(15,:) = {'gaue', 4,4,1,1,1,1,1};

%% group-specific attributes 
conf.seslist = {'off','on'}; 
% 
% if strcmp('dbs',conf.subjtype) % On-off patients 
%     conf.subjlist = {'roba','dosd','bons','seyc','sacn',...
%         'sire','dals','szcr'}; % exclusions: 'gaue' did 1 ses; 'sima' did not
%     % do confidecid task. 
% elseif strcmp('lfp',conf.subjtype) % lfp patients
%     conf.subjlist = {'szcr','dals','pagh','main'}; 
% elseif strcmp('str',conf.subjtype) % healthy subjects
%     conf.subjlist = {'kisa','chos','lenj','siga','roum'}; 
% end

%% Other parameters
% Colors definition

conf.color.blue = [0.305, 0.656, 0.867];
conf.color.purple = [0.590, 0.430, 0.676];
conf.color.darkblue = [0.02, 0.447, 0.467];
conf.color.agent = conf.color.blue;
conf.color.non_agent = conf.color.purple;

% list of agentivity
conf.agentlist = {'agent', 'non_agent'};

% gain, loss, diff columns numbers 
conf.gld_columns = [4 5 6]; % Gains = colonne 4 ; Pertes = colonne 5 ; Diff = colonne 6 dans la matrice 'group_data'

%% create directories that don't exist
% checkdirs(conf);

% %% graphical options
% conf.red = [0.6350 0.0780 0.1840];
% conf.blue = [0 0.4470 0.7410];
% conf.lightblue = [0.3010 0.7450 0.9330];
% conf.green = [0.4660 0.6740 0.1880];
% conf.yellow = [0.9290 0.6940 0.1250];
% conf.orange = [0.8500 0.3250 0.0980];
% conf.lightorange = [0.996 0.655 0.302]; 
% conf.silver = [0.8 0.8 0.8]; 
% conf.linewidth = 2.3;
% conf.linethin = 1;
% conf.markersize = 15;

%% checking user and setting paths
if ismac
    user = 'michael';
else
    user = 'audrey';
end
fprintf('[conf] setting user to %s\n',user);
% user-dependent paths
if strcmp(user,'audrey')
    % path to VBA toolbox from https://mbb-team.github.io/VBA-toolbox/ : 
    conf.vbapath = 'D:/Documents/MATLAB/VBA-toolbox';
    % path to the army-knife-master + other tools that have been
    % used for development by Romane : 
    conf.vbaaddonpath = 'D:/Documents/MATLAB/VBA-addon';
    addpath(genpath(conf.vbapath)) %recursive addpath
    addpath(genpath(conf.vbaaddonpath))
    
    fprintf('[conf] adding vba path %s \n and add-on path %s\n',conf.vbapath,conf.vbaaddonpath);
    
    % BIDS database path
    conf.bidspath = 'D:/OneDrive - PhD/OneDrive/Documents/01 data-bids';
    fprintf('[conf] setting BIDS path to %s\n',conf.bidspath);
    
    % path to derivative files (results of analysis)    
    conf.derivpath = 'D:/OneDrive - PhD/OneDrive/Documents/01 data-bids/derivatives/beh_prep/confidecid';
    fprintf('[conf] setting derivatives path to %s\n',conf.derivpath);
    
    % path to figure files (results of analysis)
    conf.figpath = fullfile(conf.derivpath, 'Figures'); 
    fprintf('[conf] setting figures path to %s\n',conf.figpath);
    
elseif strcmp(user,'michael')
    conf.bids = '/Volumes/MichaelData/data/BIDS/perithr_faces/';
end

% check that all necessary directory exist
checkdirs(conf);

% End
fprintf('[conf] END CONF ======================== \n');

% load analysis script
confidecid_beh_analysis_all; 
