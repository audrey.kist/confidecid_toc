%% Script Name: confidecid_compare_kGLD_onoff
%
% Description: ...
%
% Assumptions: All individual subjects have been processed with choiceModel
%
% Inputs:
%
% Outputs:
%       beta_corr
%
% Revision: 
% $Author: Audrey Kist
% Date: 
%---------------------------------------------------------

clearvars
close all


%% load conf file
confidecid_beh_conf;

% set list of subjects to DBS
conf.subjtype = 'dbs'; 

fprintf('[on-off-analysis] START ======================== \n');

%% load files for all subjects in a single structure
fprintf('[on-off-analysis] load kG, kL, kD in tables \n');

% create 3 tables
% size of table is number of subject * 2 (for on-off)
table_kG_agent = table('Size',[numel(conf.subjlist),1+numel(conf.seslist)],...
    'VariableTypes', {'string','double','double'},...
    'VariableNames',{'name','off','on'});
table_kG_nonagent = table('Size',[numel(conf.subjlist),1+numel(conf.seslist)],...
    'VariableTypes', {'string','double','double'},...
    'VariableNames',{'name','off','on'});

table_kL_agent = table('Size',[numel(conf.subjlist),1+numel(conf.seslist)],...
    'VariableTypes', {'string','double','double'},...
    'VariableNames',{'name','off','on'});
table_kL_nonagent = table('Size',[numel(conf.subjlist),1+numel(conf.seslist)],...
    'VariableTypes', {'string','double','double'},...
    'VariableNames',{'name','off','on'});

table_kT_agent = table('Size',[numel(conf.subjlist),1+numel(conf.seslist)],...
    'VariableTypes', {'string','double','double'},...
    'VariableNames',{'name','off','on'});
table_kT_nonagent = table('Size',[numel(conf.subjlist),1+numel(conf.seslist)],...
    'VariableTypes', {'string','double','double'},...
    'VariableNames',{'name','off','on'});

% to get acceptance coefficients in the tables,
% loop over subjects

% for subj_num = 1:numel(conf.subjlist) 
%     % get current subject name
%     mysubj = conf.subjlist{subj_num};    
%         
%     % loop over on-off
%     for ses_num = 1:numel(conf.seslist) 
%         % get current session name
%         myses = conf.seslist{ses_num};
%        % message = ['[on-off-analysis] loading sub-' string(myses) 'blabla' string(mysubj)]; 
%         fprintf('[on-off-analysis] loading sub-%s ses-%s \n', mysubj, myses);
%    
%         % name choice data file
%         subj_filename = fullfile(conf.derivpath, ['sub-' mysubj '_ses-' myses '_task-confidecid_choicedata_beh']); 
%         % (if exists) load choice_data file
%         load(subj_filename);
%                   
%         % get gain coefficient in this condition, for this subject
%         table_kG_agent(subj_num,'name') = {string(mysubj)};
%         table_kG_agent(subj_num, {myses}) = {subj.choice_data.agent.inversionResult.freeParameter.model.kG} ;
%         table_kG_nonagent(subj_num, {myses}) = {subj.choice_data.non_agent.inversionResult.freeParameter.model.kG} ;
%         
%         % get loss coefficient 
%         table_kL_agent(subj_num,'name') = {string(mysubj)};
%         table_kL_agent(subj_num, {myses}) = {subj.choice_data.agent.inversionResult.freeParameter.model.kL} ;
%         table_kL_nonagent(subj_num, {myses}) = {subj.choice_data.non_agent.inversionResult.freeParameter.model.kL} ;
%         
%         % get difficulty coefficient 
%         table_kT_agent(subj_num,'name') = {string(mysubj)};
%         table_kT_agent(subj_num, {myses}) = {subj.choice_data.agent.inversionResult.freeParameter.model.kT} ;
%         table_kT_nonagent(subj_num, {myses}) = {subj.choice_data.non_agent.inversionResult.freeParameter.model.kT} ;  
%          
%     end 
% end
% fprintf('[on-off-analysis] loading sub-%s ses-%s \n', mysubj, myses);
%%
%%

% create a table to store G, L, T ; one line on one line off
table_kGLT_agent = table('Size',[2*numel(conf.subjlist),5],...
    'VariableTypes', {'string','string','double','double','double'},...
    'VariableNames',{'name','ses','kG','kL','kT'});

table_kGLT_nonagent = table('Size',[2*numel(conf.subjlist),5],...
    'VariableTypes', {'string','string','double','double','double'},...
    'VariableNames',{'name','ses','kG','kL','kT'});

%%
for subj_num = 1:numel(conf.subjlist) 
    % get current subject name
    mysubj = conf.subjlist{subj_num};
        
    % loop over on-off
    for ses_num = 1: numel(conf.seslist) 
        % compute line number in table
        myline_nb = (subj_num * 2) + ses_num -2;
        % get current session name
        myses = conf.seslist{ses_num};
        
       % message = ['[on-off-analysis] loading sub-' string(myses) 'blabla' string(mysubj)]; 
        fprintf('[on-off-analysis] loading sub-%s ses-%s \n', mysubj, myses);
   
        % name choice data file
        subj_filename = fullfile(conf.derivpath, ['sub-' mysubj '_ses-' myses '_task-confidecid_choicedata_beh']); 
        % (if exists) load choice_data file
        load(subj_filename);
                  
        % put coefficients in the tables
        table_kGLT_agent(myline_nb,'name') = {string(mysubj)};
        table_kGLT_agent(myline_nb,'ses') = {string(myses)};
        table_kGLT_agent(myline_nb, 'kG') = {subj.choice_data.agent.inversionResult.freeParameter.model.kG} ;
        table_kGLT_agent(myline_nb, 'kL') = {subj.choice_data.agent.inversionResult.freeParameter.model.kL} ;
        table_kGLT_agent(myline_nb, 'kT') = {subj.choice_data.agent.inversionResult.freeParameter.model.kT} ;   
        
        table_kGLT_nonagent(myline_nb,'name') = {string(mysubj)};
        table_kGLT_nonagent(myline_nb,'ses') = {string(myses)};
        table_kGLT_nonagent(myline_nb, 'kG') = {subj.choice_data.non_agent.inversionResult.freeParameter.model.kG} ;
        table_kGLT_nonagent(myline_nb, 'kL') = {subj.choice_data.non_agent.inversionResult.freeParameter.model.kL} ;
        table_kGLT_nonagent(myline_nb, 'kT') = {subj.choice_data.non_agent.inversionResult.freeParameter.model.kT} ;   
     
    end 
end
fprintf('[on-off-analysis] data has been loaded in agent and on-agent tables \n');
%%
% on-off paired t-test and plot
% parameters should be: coeff_of_interest(kG,kL,kT) and agentivity (0,1)

%coeff_of_interest = kL;
%agentivity = 1;
% 
% table_of_interest = table_kL_agent;
% 
% [h,p,ci,stats] = ttest(table_of_interest.off,table_of_interest.on); 

%% export table for further use in R for plots
table_klt_agent_filename = fullfile(conf.derivpath,"onoff-allsubj-agent-kglt_coeffs.csv");
writetable(table_kGLT_agent,table_klt_agent_filename);
fprintf('[on-off-analysis] agent data has been exported to %s \n',table_klt_agent_filename);

table_klt_nonagent_filename = fullfile(conf.derivpath,"onoff-allsubj-nonagent-kglt_coeffs.csv");
writetable(table_kGLT_nonagent,table_klt_nonagent_filename);
fprintf('[on-off-analysis] non-agent data has been exported to %s \n',table_klt_nonagent_filename);


%% plots
% � externaliser en fonction
confidecid_plot_myK_onoff('kG', 'agent', table_kGLT_agent, conf)
confidecid_plot_myK_onoff('kL', 'agent', table_kGLT_agent, conf)
confidecid_plot_myK_onoff('kT', 'agent', table_kGLT_agent, conf)
confidecid_plot_myK_onoff('kG', 'non-agent', table_kGLT_nonagent, conf)
confidecid_plot_myK_onoff('kL', 'non-agent', table_kGLT_nonagent, conf)
confidecid_plot_myK_onoff('kT', 'non-agent', table_kGLT_nonagent, conf)

