%% Function Name: perf_acc2agent
%
% Description: ANALYSE 3 : performance en fonction de l'agentivit�
%
% Assumptions: BIDSified files and folders 
%
% Inputs:
%...
%
% Outputs:
%       ...
%
% Revision: 
% $Author: Audrey Kist
% Date: 
%---------------------------------------------------------
% 
function perf_acc2agent(init)

for subj_idx = 1:length(conf.subjlist) % Boucle � travers les sujets
    for agentivity = 1:2 % Boucle � travers l'agentivit�
        
        agent = agentivity*(-1)+2; % 1 = agent ; 2 = non agent
        
        subj_data = init.group_data(init.group_data(:,1) == subj_idx,:); % All data for one subject
        agent_data = subj_data(subj_data(:,11) == agent,:); % All data for one condition (agent/non agent)
        
        % Performance all trials
        perf(subj_idx, agentivity) = mean(agent_data(:,12)); % nSubj * Agentivity
        err_perf(subj_idx, agentivity) = std(agent_data(:,12))/sqrt(length(agent_data(:,12)));
        
        % Proba accept
        pAccept(subj_idx, agentivity) = mean(agent_data(:,7)); % nSubj * Agentivity
        
        % Confidence
        z_confi = zscore(subj_data(:,9));
        confi(subj_idx, agentivity) = mean(z_confi(subj_data(:,11) == agent)); % nSubj * Agentivity
        
        % Loop over sessions
        sess_id = unique(subj_data(:,2));
        for sess = 1:length(sess_id) % Loop over sessions
            perf_sess(subj_idx, agentivity, sess) = mean(agent_data(agent_data(:,2) == sess_id(sess), 12));
        end % End of the loop over sessions
        
    end % Fin de la boucle � travers l'agentivit�
end % Fin de la boucle � travers les sujets

% FIGURE (for all subjects)

fig = figure;
hold on;

subgrid = [2, 3];

% All subjects
subplot(subgrid(1), subgrid(2), 1:3);
hold on;

bar(perf)

ngroups = size(perf, 1);
nbars = size(perf, 2);
% Calculating the width for each bar group
groupwidth = min(0.8, nbars/(nbars + 1.5));
for i = 1:nbars
    x = (1:ngroups) - groupwidth/2 + (2*i-1) * groupwidth / (2*nbars);
    errorbar(x, perf(:,i), err_perf(:,i), 'k', 'LineStyle', 'none');
end

legend('agent', 'non agent', 'Location', 'best')

xlabel('Subjects')
ylabel('% success')

% Mean
subplot(subgrid(1), subgrid(2), 4);
hold on;

b = bar(mean(perf), 'FaceColor', 'flat');
b.CData(2,:) = [0.85,0.33,0.10];
errorbar(mean(perf), std(perf)/sqrt(length(perf)), 'k', 'LineStyle', 'none')

ylim([0 1])
[~,p] = ttest(perf(:,1),perf(:,2));
TextLocation(sprintf('p = %.2f', p), 'Location', 'best');

xticks([1,2])
xticklabels({'agent','non agent'})
ylabel('% success')
title('Across subjects')

% Correlation with p(accept)
subplot(subgrid(1), subgrid(2), 5);
hold on;

x = perf(:,1)-perf(:,2);
y = pAccept(:,1)-pAccept(:,2);
n = length(y);

scatter(x, y, 'filled', 'MarkerEdgeColor', [0 0 0])
line([-1 1], [0 0], 'Color', 'k', 'LineStyle', '--')
line([0 0], [-1 1], 'Color', 'k', 'LineStyle', '--')

[beta, ~, stats] = glmfit(x, y); % Correlation pAccept according to performance
yfit = glmval(beta, x, 'identity', 'size', n);
plot(x, yfit./n, '-', 'LineWidth', 2)

TextLocation(sprintf('p = %.2f', stats.p(end)), 'Location', 'best');

xlabel('perf_{agent} - perf_{proba}')
ylabel('p(accept)_{agent} - p(accept)_{proba}')
title('Choice')

% Correlation with confidence
subplot(subgrid(1), subgrid(2), 6);
hold on;

x = perf(:,1)-perf(:,2);
y = confi(:,1)-confi(:,2);
n = length(y);

scatter(x, y, 'filled', 'MarkerEdgeColor', [0 0 0])
line([-1 1], [0 0], 'Color', 'k', 'LineStyle', '--')
line([0 0], [-1.5 1.5], 'Color', 'k', 'LineStyle', '--')

[beta, ~, stats] = glmfit(x, y); % Correlation confidence according to performance
yfit = glmval(beta, x, 'identity', 'size', n);
plot(x, yfit./n, '-', 'LineWidth', 2)

TextLocation(sprintf('p = %.2f', stats.p(end)), 'Location', 'best');

xlabel('perf_{agent} - perf_{proba}')
ylabel('confidence_{agent} - confidence_{proba}')
title('Confidence')

% Save
fig_name = fullfile(init.pathtofig, 'perf_acc2agent');
% print(fig, [fig_name '.tiff'], '-dtiff', '-r200');
savefig(fig, fig_name,'compact'); % Matlab format

close(fig);

end