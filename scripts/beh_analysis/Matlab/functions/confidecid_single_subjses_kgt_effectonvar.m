%% Function Name: task_dim_effect_on_var_1subj-ses
%
% Description: ANALYSE A : variation d'une variable en fonction de chaque
% dimension de la t�che pour chaque sujet
%
% Assumptions: BIDSified files and folders 
%
% Inputs:
%       subj
%           .choice_data
%       conf
%       opt
%           .opt.depdt_variable_col to select the col of var of interest
%           .ylabel 
%           .YTick
%           .axis 
%           .subj_fig_name 
%           .opt.all_fig_name 
%
% Outputs:
%       no var
%
% Revision: 
% $Author: Romane
% Date: 
%---------------------------------------------------------
% 

function confidecid_single_subjses_kgt_effectonvar(subj, conf, opt)

    % create a n-dim table var_p of row= 4, and col = gain loss diff
    depdt_variable_p = cell(4,3,2); % p is for ?? 
    % create a vector with the id. of col of interest
    task_dim_column = conf.gld_columns; 
    % gain = colonne 4 ; 
    % loss = colonne 5 ; 
    % diff = colonne 6 in matrix 'group_data'
  
    % get subject data
    subj_data = subj.raw_data; %(init.group_data(:,1) == subj_idx,:); % All data for one subject
    
    % var is the columns of values of the var of interest
    % (gain loss or diff)
    depdt_variable_values = subj_data(:,opt.depdt_variable_col); % Offre accept�e (1) ou rejet�e (0) // Notes de confiance entre 1 et 100
    
    fig = figure;
    hold on;
    
    for agentivity = 1:2 % Boucle � travers l'agentivit�
        agent = agentivity*(-1)+2; % 1 = agent ; 2 = non agent
        
        for task_dim = 1:length(task_dim_column) % task_dim : 1 = gain, 2 = loss, 3 = diff
            
            % bin the values of the variable (G,L,D)
            prospect = subj_data(:,task_dim_column(task_dim)); % prospect de toutes les sessions pour 1 sujet
            id_q1 = prospect < 20 & subj_data(:,11) == agent;
            id_q2 = prospect < 30 & prospect >= 20 & subj_data(:,11) == agent;
            id_q3 = prospect < 40 & prospect >= 30 & subj_data(:,11) == agent;
            id_q4 = prospect < 50 & prospect >= 40 & subj_data(:,11) == agent;
            
            % fill the value of the table
             depdt_variable_p(:,task_dim,agentivity) = ...
                 {depdt_variable_values(id_q1), depdt_variable_values(id_q2), ...
                 depdt_variable_values(id_q3), depdt_variable_values(id_q4)}; % choice_p dim : subject by task_levels (4 levels) by task dim (gain, loss, challenge_diff)
            if agentivity == 1
                plot_color(agentivity,:) = conf.color.agent;
            elseif agentivity == 2
                plot_color(agentivity,:) = conf.color.non_agent;
            end
            
            %FIGURE : proba d'accepter pour chaque dimension (gain, loss, diff) et chaque sujet
            subplot(3,1,task_dim);
            hold on;
            pbaspect([1,0.5,1]);
            errorbar(cellfun(@mean, depdt_variable_p(:,task_dim,agentivity)),...
                cellfun(@std, depdt_variable_p(:,task_dim,agentivity))./sqrt(cellfun(@length, ...
                depdt_variable_p(:,task_dim,agentivity))),...
                'o-', 'Color', plot_color(agentivity,:), 'MarkerSize', 3);
            ylabel(opt.ylabel)
            ax = gca;
            ax.XTick = [1 2 3 4];
            ax.YTick = opt.YTick;
            axis(opt.axis)
            set(gca,'XTickLabel',{'[1-2]',']2-3]',']3-4]',']4-5]'})
            
            switch task_dim
                case 1
                    xlabel('Gain prospect ()')
                case 2
                    xlabel('Loss prospect ()')
                case 3
                    xlabel('Challenge difficulty')
            end          
        end % Fin de la boucle � travers les dimensions
    end % Fin de la boucle � travers l'agentivit�
    
    %% legend figure, save and close  
    title(['subj-' subj.name '_ses-' subj.ses])
    legend('agent','non agent','Location','best');   
    set(gcf, 'Units', 'centimeters');
    set(gcf, 'PaperUnits', 'centimeters');
    fig.PaperPosition = [0 0 7 14]; % this is for printing
    fig.Position = [0 0 7 14]; % this is for figure size
    fig_name = fullfile(conf.figpath, ['subj-' subj.name '_ses-' subj.ses opt.subj_fig_name]);   
    print(fig, fig_name, '-dtiff', '-r200');
    fprintf('[subj-analysis] figure created for gain-loss-diff on %s for sub-%s ses-%s \n', opt.ylabel, subj.name, subj.ses)
   % close(fig);

end