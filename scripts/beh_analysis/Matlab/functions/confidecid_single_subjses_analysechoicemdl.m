%% Function Name: confidecid_single_subjses_analysechoicemdl 
%
% Description: ANALYSE 4 : choice model analysis
%
% Assumptions: BIDSified files and folders 
%
% Inputs:
%...
%
% Outputs:
%       ...
%
% Revision: 
% $Author: Audrey Kist
% Date: 
%---------------------------------------------------------
% 
function confidecid_single_subjses_analysechoicemdl(subj, conf)

%%

%%

    task_dim_column = [4 5 6]; % Gains = colonne 4 ; Pertes = colonne 5 ; Diff = colonne 6 dans la matrice 'group_data'

   % for subj_idx = 1:length(conf.subjlist) % Boucle � travers les sujets

       % subj_data = init.group_data(init.group_data(:,1) == subj_idx,:); % All data for one subject
       subj_data = subj.raw_data;

        cond = conf.agentlist;

        fig = figure;
        hold on;
%%
        for agentivity = 1:2 % Boucle � travers l'agentivit�

            ag = agentivity*(-1)+2; % 1 = agent ; 2 = non agent
            agent_data = subj_data(subj_data(:,11) == ag,:); % All data for one subject and one condition (agent/non agent)

            choices = agent_data(:,7);
            proba_accept = subj.choice_data.(cond{agentivity}).inversionResult.model.(conf.subj).posterior.muX(2,:)'; % Also in out.suffStat.gx
            utility_accept = subj.choice_data.(cond{agentivity}).inversionResult.model.(conf.subj).posterior.muX(1,:);

            for task_dim = 1:length(task_dim_column) % task_dim : 1 = gain, 2 = loss, 3 = diff

                prospect = agent_data(:,task_dim_column(task_dim)); % prospect de toutes les sessions pour 1 sujet
                id_q1 = prospect < 20;
                id_q2 = prospect < 30 & prospect >= 20;
                id_q3 = prospect < 40 & prospect >= 30;
                id_q4 = prospect < 50 & prospect >= 40;

                choice_bin(:, task_dim, agentivity) = {choices(id_q1), choices(id_q2), choices(id_q3), choices(id_q4)}; % choice_bin dim : subject by task_levels (4 levels) by task dim (gain, loss, challenge_diff)
                model_bin(:, task_dim, agentivity) = {proba_accept(id_q1), proba_accept(id_q2), proba_accept(id_q3), proba_accept(id_q4)};

                %FIGURE : proba d'accepter pour chaque dimension (gain, loss, diff) 
                subplot(2, 2, task_dim);
                hold on;
                pbaspect([1, 0.5, 1]);

                % Model
                mean_model = cellfun(@mean, model_bin(:, task_dim, agentivity));
                err_model = cellfun(@std, model_bin(:, task_dim, agentivity))./sqrt(cellfun(@length, model_bin( :, task_dim, agentivity)));
                patch([1:length(mean_model) length(mean_model):-1:1], mean_model([1:end end:-1:1]) + cat(2, err_model(1:end), -err_model(end:-1:1)),...
                    init.color.(cond{agentivity})-0.1, 'EdgeColor', 'none', 'FaceAlpha', .2);
                l(agentivity) = plot(1:length(mean_model), mean_model, '--', 'LineWidth', 1, 'Color', init.color.(cond{agentivity})-0.1);

                % Data
                e(agentivity) = errorbar(cellfun(@mean, choice_bin(subj_idx,:,task_dim,agentivity)), cellfun(@std, choice_bin(subj_idx,:,task_dim,agentivity))./sqrt(cellfun(@length, choice_bin(subj_idx,:,task_dim,agentivity))),...
                    'o', 'Color', init.color.(cond{agentivity}), 'MarkerFaceColor', init.color.(cond{agentivity}), 'MarkerSize', 3); % Choices

                ylabel('P(accept)')
                ax = gca;
                ax.XTick = [1 2 3 4];
                ax.YTick = (0:.2:1);
                axis([0 5 0 1])
                set(gca,'XTickLabel',{'[1-2]',']2-3]',']3-4]',']4-5]'})

                switch task_dim
                    case 1
                        xlabel('Gain prospect ()')
                    case 2
                        xlabel('Loss prospect ()')
                    case 3
                        xlabel('Challenge difficulty')
                end

            end % Fin de la boucle � travers les dimensions

            % Plot also utility
            subplot(2, 2, 4);
            hold on;
            pbaspect([1, 0.5, 1]);

            group = linspace(-8,7,10); % A modifier si on veut regrouper plus ou moins d'utilit�

            % Make bins
            for ig = 1:length(group)-1
                if ig == 1 % First bin
                    itmp = utility_accept < group(ig+1);
                elseif ig == length(group)-1 % Last bin
                    itmp = utility_accept >= group(ig);
                else
                    itmp = utility_accept >= group(ig) & utility_accept < group(ig+1);
                end
                choice_utility(subj_idx, ig, agentivity) = {choices(itmp)}; % Data
                model_utility(subj_idx, ig, agentivity) = {proba_accept(itmp)}; % Model
            end

            % Model
            mean_model = cellfun(@mean, model_utility(subj_idx, :, agentivity));
            err_model = cellfun(@std, model_utility(subj_idx, :, agentivity))./sqrt(cellfun(@length, model_utility(subj_idx, :, agentivity)));
            patch([1:length(mean_model) length(mean_model):-1:1], mean_model([1:end end:-1:1]) + cat(2, err_model(1:end), -err_model(end:-1:1)),...
                init.color.(cond{agentivity})-0.1, 'EdgeColor', 'none', 'FaceAlpha', .2);
            l(agentivity) = plot(1:length(mean_model), mean_model, '--', 'LineWidth', 1, 'Color', init.color.(cond{agentivity})-0.1);

            % Data
            e(agentivity) = errorbar(cellfun(@mean, choice_utility(subj_idx,:,agentivity)), cellfun(@std, choice_utility(subj_idx,:,agentivity))./sqrt(cellfun(@length, choice_utility(subj_idx,:,agentivity))),...
                'o', 'Color', init.color.(cond{agentivity}), 'MarkerFaceColor', init.color.(cond{agentivity}), 'MarkerSize', 3); % Choices

            ylabel('P(accept)')
            xlabel('Utility(accept)')

            xticks(1:length(group)-1)
            for i = 1:length(group)-1
                Xtick{i} = sprintf('(n = %d) [%.1f : %.1f[',length(choice_utility{subj_idx,i,agentivity}),group(i),group(i+1));
            end
            xticklabels(Xtick);
            xtickangle(45);
            xlim([1 length(group)-1])
            ylim([0 1])

        end % Fin de la boucle � travers l'agentivit�

        leg = legend([e,l], 'agent', 'non agent', 'model agent', 'model non agent', 'Location','best');
        leg.Position = [.5-(leg.Position(3)/2) .5-(leg.Position(4)/2) leg.Position(3:4)]; % [left bottom width height]

        fig.PaperPosition = [0, 0, 15 13]; % [left bottom width height]
        fig_name = fullfile(init.pathtofigsubj{subj_idx}, [conf.subjlist{subj_idx} '_task_dim_effect_on_choice_+model.tiff']);
        print(fig, fig_name, '-dtiff', '-r200');

        close(fig);
    
   % end % Fin de la boucle � travers les sujets

    % Figure pour tous les sujets
    if subj_idx == length(conf.subjlist)

        fig = figure;
        hold on;

        for agentivity = 1:2 % Boucle � travers l'agentivit�
            for task_dim = 1:3 % task_dim : 1 = gain, 2 = loss, 3 = diff

                %FIGURE : proba d'accepter pour chaque dimension (gain, loss, diff) tous les sujets
                subplot(2,2,task_dim);
                hold on;

                % Model
                mean_model_all = mean(cellfun(@mean, model_bin(:, :, task_dim, agentivity)));
                err_model_all = std(cellfun(@mean, model_bin(:, :, task_dim, agentivity)))./sqrt(length(cellfun(@mean, model_bin(:, :, task_dim, agentivity))));
                patch([1:length(mean_model_all) length(mean_model_all):-1:1], mean_model_all([1:end end:-1:1]) + cat(2, err_model_all(1:end), -err_model_all(end:-1:1)),...
                    init.color.(cond{agentivity})-0.1, 'EdgeColor', 'none', 'FaceAlpha', .2);
                l(agentivity) = plot(1:length(mean_model_all), mean_model_all, '--', 'LineWidth', 1, 'Color', init.color.(cond{agentivity})-0.1);

                % Data
                e(agentivity) = errorbar(mean(cellfun(@mean, choice_bin(:,:,task_dim,agentivity))), std(cellfun(@mean, choice_bin(:,:,task_dim,agentivity)))./sqrt(length(cellfun(@mean, choice_bin(:,:,task_dim,agentivity)))),...
                    'o', 'Color', init.color.(cond{agentivity}), 'MarkerFaceColor', init.color.(cond{agentivity}), 'MarkerSize', 3); % Choices

                ylabel('P(accept)')
                ax = gca;
                ax.XTick = [1 2 3 4];
                ax.YTick = (0:.2:1);
                axis([0 5 0 1])
                set(gca,'XTickLabel',{'[1-2]',']2-3]',']3-4]',']4-5]'})

                switch task_dim
                    case 1
                        xlabel('Gain prospect ()')
                    case 2
                        xlabel('Loss prospect ()')
                    case 3
                        xlabel('Challenge difficulty')
                end

            end % Fin de la boucle � travers les dimensions

            % Plot also utility
            subplot(2, 2, 4);
            hold on;

            % Model
            mean_model_all = nanmean(cellfun(@mean, model_utility(:, :, agentivity)));
            err_model_all = nanstd(cellfun(@mean, model_utility(:, :, agentivity)))./sqrt(length(cellfun(@mean, model_utility(:, :, agentivity))));
            patch([1:length(mean_model_all) length(mean_model_all):-1:1], mean_model_all([1:end end:-1:1]) + cat(2, err_model_all(1:end), -err_model_all(end:-1:1)),...
                init.color.(cond{agentivity})-0.1, 'EdgeColor', 'none', 'FaceAlpha', .2);
            l(agentivity) = plot(1:length(mean_model_all), mean_model_all, '--', 'LineWidth', 1, 'Color', init.color.(cond{agentivity})-0.1);

            % Data
            e(agentivity) = errorbar(nanmean(cellfun(@mean, choice_utility(:,:,agentivity))), nanstd(cellfun(@mean, choice_utility(:,:,agentivity)))./sqrt(length(cellfun(@mean, choice_utility(:,:,agentivity)))),...
                'o', 'Color', init.color.(cond{agentivity}), 'MarkerFaceColor', init.color.(cond{agentivity}), 'MarkerSize', 3); % Choices

            ylabel('P(accept)')
            xlabel('Utility(accept)')

            xticks(1:length(group)-1)
            for i = 1:length(group)-1
                Xtick{i} = sprintf('(n = %d) [%.1f : %.1f[',sum(cellfun(@length, choice_utility(:,i,agentivity))),group(i),group(i+1));
            end
            xticklabels(Xtick);
            xtickangle(45);
            xlim([1 length(group)-1])
            ylim([0 1])

        end % Fin de la boucle � travers l'agentivit�

        leg = legend([e,l], 'agent', 'non agent', 'model agent', 'model non agent');
        leg.Position = [.5-(leg.Position(3)/2) .5-(leg.Position(4)/2) leg.Position(3:4)]; % [left bottom width height]

        % Save
        fig.PaperPosition = [0, 0, 15 13]; % [left bottom width height];
        fig_name = fullfile(init.pathtofig, 'p_accept_according2dim_+model.tiff');
        print(fig, fig_name, '-dtiff', '-r200');
        % savefig(fig, [fig_name(1:end-5), '.fig'], 'compact');

        close(fig);

    end

end