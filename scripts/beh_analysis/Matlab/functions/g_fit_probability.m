function  [ gx ] = g_fit_probability( x_t,P,u_t,in )

%% Fit mood from quizz 

[x,~,~] = getStateParamInput(x_t,P, u_t,in);

%% Prediction of probability

gx = x.probability;

