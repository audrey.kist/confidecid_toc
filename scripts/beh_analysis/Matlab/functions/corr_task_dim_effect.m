%% Function Name: corr_task_dim_effect
%
% Description: ANALYSE B : corr�lation entre une variable et chaque 
% dimension de la t�che pour chaque sujet
%
% Assumptions: BIDSified files and folders 
%
% Inputs:
%...
%
% Outputs:
%       ...
%
% Revision: 
% $Author: Audrey Kist
% Date: 
%---------------------------------------------------------

% 
function beta_corr = corr_task_dim_effect(init, opt)

task_dim_column = [4 5 6]; % Gains = colonne 4 ; Pertes = colonne 5 ; Diff = colonne 6 dans la matrice 'group_data'

for subj_idx = 1:length(conf.subjlist) % Boucle � travers les sujets
    
    subj_data = init.group_data(init.group_data(:,1) == subj_idx,:); % All data for one subject
    
    for agentivity = 1:2 % Boucle � travers l'agentivit�
        
        voi(subj_idx,1,agentivity) = {subj_data(subj_data(:,11) == agentivity*(-1)+2, task_dim_column)}; % Col 1 = Prospects
        voi(subj_idx,2,agentivity) = {subj_data(subj_data(:,11) == agentivity*(-1)+2, opt.var_column)}; % Col 2 = Variable d'int�r�t (choix, confiance...)
        
        % Corr�lation entre la variable d'int�r�t et toutes les dimensions (gain, loss, diff) pour chaque sujet
        
        x = voi{subj_idx,1,agentivity}; % Predictors
        y = voi{subj_idx,2,agentivity}; % Responses
        
        if VBA_isBinary(y) == true
            b = glmfit(x,y,'binomial');
            blink = 'logit';
        else
            b = glmfit(x,y);
            blink = 'identity';
        end
        
        intercept(subj_idx, agentivity) = b(1); % Intercept (utile pour le plot)
        beta_corr(subj_idx, agentivity, :) = b(2:end); % Pente de la corr�lation
        
    end % Fin de la boucle � travers l'agentivit�
    
    fig = figure;
    hold on;
    
    for task_dim = 1:length(task_dim_column) % task_dim : 1 = gain, 2 = loss, 3 = diff
        for agentivity = 1:2 % Boucle � travers l'agentivit�
            
            prdct = voi{subj_idx,1,agentivity}(:,task_dim);
            resp = voi{subj_idx,2,agentivity};
            beta = [intercept(subj_idx, agentivity); beta_corr(subj_idx, agentivity, task_dim)];
            
            if agentivity == 1
                plot_color = init.color.agent;
            elseif agentivity == 2
                plot_color = init.color.non_agent;
            end
            
            % FIGURE : Plot de la corr�lation entre la variable d'int�r�t et l'une des dimensions
            subplot(3,1,task_dim);
            hold on;
            pbaspect([1,0.5,1]);
            
            yfit = glmval(beta, prdct, blink);
            st = sortrows([prdct, yfit]);
            p(agentivity) = plot(st(:,1), st(:,2), 'LineWidth', 2, 'Color', plot_color);
            scatter(prdct, resp, 10, 'filled', 'MarkerFaceColor', p(agentivity).Color, 'MarkerFaceAlpha', 0.3);
            
            ylabel(opt.ylabel)
            ylim(opt.ylim)
            
            switch task_dim
                case 1
                    xlabel('Gain prospect ()')
                case 2
                    xlabel('Loss prospect ()')
                case 3
                    xlabel('Challenge difficulty')
            end
            
        end % Fin de la boucle � travers l'agentivit�
    end % Fin de la boucle � travers les dimensions de la t�che
    
    legend(p, 'agent','non agent','Location','best');
    
    fig.PaperPosition = [0, 0, 7 14];
    fig_name = fullfile(init.pathtofigsubj{subj_idx}, [conf.subjlist{subj_idx} opt.subj_fig_name]);
    print(fig, fig_name, '-dtiff', '-r200');
    
    close(fig);
    
end % Fin de la boucle � travers les sujets

% T-test agent vs. non agent (identique pour chaque dimensions de la t�che)
[h,p,~,stats] = ttest(cellfun(@mean, voi(:,2,1)), cellfun(@mean, voi(:,2,2)));

end