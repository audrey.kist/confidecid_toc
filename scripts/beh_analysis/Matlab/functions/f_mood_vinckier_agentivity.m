function  [updatedX] = f_mood_vinckier_agentivity(x_t, P, u_t, in)

%% Parameters
[x,theta,u] = getStateParamInput(x_t, P, u_t, in); % Get the hidden states, the free parameters and the inputs (using labels stored in options.in[FG]

switch in.modelSpace.agentivityInclusion
    case {'none', '2R'}
        kF = theta.kF;       % Weight of feedbacks in mood
    case '2kF'
        if u.agentivity == 1 % Agent trials
            kF = theta.kF;  % Weight of feedbacks in mood when agent
        else                 % Proba trials
            kF = theta.kFp;  % Weight of feedbacks in mood when non agent
        end
    case '+kF'
        if u.agentivity == 1 % Agent trials
            kF = theta.kF;  % Weight of feedbacks in mood when agent
        else                 % Proba trials
            kF = theta.kF + theta.kFp;  % Weight of feedbacks in mood when non agent
        end
    case '4kF'
        if u.agentivity == 1 && u.choice == 1 % Agent trials & resp = YES
            kF = theta.kFay;
        elseif u.agentivity == 1 && u.choice == 0 % Agent trials & resp = NO
            kF = theta.kFan;
        elseif u.agentivity == 0 && u.choice == 1 % Proba trials & resp = YES
            kF = theta.kFpy;
        elseif u.agentivity == 0 && u.choice == 0 % Agent trials & resp = NO
            kF = theta.kFpn;
        end
    case '2kFChoice'
        if u.choice == 1 % Choice = YES
            kF = theta.kF;
        elseif u.choice == 0 % Choice = NO
            kF = theta.kFp;
        end
end

kP = theta.kP; % Weight of prospect in mood
kEasyness = 0; % Weight of mean correct response rate in mood : used as a proxy for subjective probability to be correct

switch in.modelSpace.reinforcementType
     case 'R' 
           kEasyness = 0; % Feedbacks are directly used
end

switch in.modelSpace.gainLossAssymetry
    case 'no'
        R = 1;            % Same weight for positive and negative events
    case 'yes'
        if ismember(in.modelSpace.agentivityInclusion, {'2R'}) && u.agentivity == 0 % If multiplicative factor option and proba (non agent) trial
            R = exp(theta.Rp); % Multiplicative factor when non agent
        else
            R = exp(theta.R); % Separate weights for positive and negative events (it's a multiplicative factor so it should be positive: exponential is used to map R --> R+)
        end
end

gamma = 1./(1+exp(-theta.gamma)); % Forgetting factor (should be between 0 and 1, sigmoid is used to map R --> [0 - 1 ]

% time effect
switch in.modelSpace.time
    case 'no'
        kTime = 0;
    case 'outDeltaRule'
        kTime = theta.kTime;
end

% kMood (delta)
kMood = theta.kM;

% k0 (constant in mood)
switch in.modelSpace.k0
    case 'no'
        k0 = 0;
    case 'yes'
        k0 = theta.k0;
end

% Mood components
isQ = 0;
isP = 0;
switch in.modelSpace.moodType % Which task is used to fit mood
    case 'quizz'
        isQ = 1;
end

%% Update mood from previous prospect (last trial) - It should be done before feedback modulation by mood !

switch in.modelSpace.choosenOption
    case 'no'
       EV = u.EV;
end    

newH2 =  kP  .* EV + gamma * x.h2;                      % h2 tracks component of mood linked to prospect
mood = k0 + isQ * x.h1 + isP * newH2 + kTime * u.time;  % Recompute mood (in order to take into account new value of h2)

%% Compute modified feedback used to update mood
switch in.modelSpace.biasedReward
    case 'no'
        r = u.feedback;
    case 'yes'
        r = u.feedback  * (1 + u.feedback * kMood * mood); % Perception of feedback is modulated by mood
end

if u.feedback > 0
    r = r * R;  % If separate weights are used for positive and negative events, R ~= 1
end

%% Update mood from current feedback (should be done after current feedback modulation)

newH1 =  kF  .* r    + kEasyness * u.quizzEasyness  + gamma * x.h1; % h1 tracks component of mood linked to quizz
newMood = k0 + isQ * newH1 + isP * newH2  + kTime * u.time;         % Recompute mood (in order to take into account new value of h1)

%% Update

updatedX = setNewX(x_t, in, 'h1', newH1);           % Update for newt trial
updatedX = setNewX(updatedX, in, 'h2', newH2);      % Update for newt trial
updatedX = setNewX(updatedX, in, 'mood', newMood);  % Update for newt trial

