%% Function Name: task_dim_effect_on_var
%
% Description: ANALYSE A : variation d'une variable en fonction de chaque
% dimension de la t�che pour chaque sujet
%
% Assumptions: BIDSified files and folders 
%
% Inputs:
%       subj
%       opt
%           .opt.var_column to select the col of var of interest
%           .ylabel 
%           .YTick
%           .axis 
%           .subj_fig_name 
%           .opt.all_fig_name 
%
% Outputs:
%       no var
%
% Revision: 
% $Author: Romane
% Date: 
%---------------------------------------------------------
% 

function task_dim_effect_on_var(subj, opt)

var_p = cell(length(conf.subjlist),4,3,2);
task_dim_column = [4 5 6]; % Gains = colonne 4 ; Pertes = colonne 5 ; Diff = colonne 6 dans la matrice 'group_data'

for subj_idx = 1:length(conf.subjlist) % Boucle � travers les sujets
    
    subj_data = subj.group_data(subj.group_data(:,1) == subj_idx,:); % All data for one subject
    var = subj_data(:,opt.var_column); % Offre accept�e (1) ou rejet�e (0) // Notes de confiance entre 1 et 100
    
    fig = figure;
    hold on;
    
    for agentivity = 1:2 % Boucle � travers l'agentivit�
        agent = agentivity*(-1)+2; % 1 = agent ; 2 = non agent
        
        for task_dim = 1:length(task_dim_column) % task_dim : 1 = gain, 2 = loss, 3 = diff
            
            prospect = subj_data(:,task_dim_column(task_dim)); % prospect de toutes les sessions pour 1 sujet
            id_q1 = prospect < 20 & subj_data(:,11) == agent;
            id_q2 = prospect < 30 & prospect >= 20 & subj_data(:,11) == agent;
            id_q3 = prospect < 40 & prospect >= 30 & subj_data(:,11) == agent;
            id_q4 = prospect < 50 & prospect >= 40 & subj_data(:,11) == agent;
            
            var_p(subj_idx,:,task_dim,agentivity) = {var(id_q1), var(id_q2), var(id_q3), var(id_q4)}; % choice_p dim : subject by task_levels (4 levels) by task dim (gain, loss, challenge_diff)
            
            if agentivity == 1
                plot_color(agentivity,:) = subj.color.agent;
            elseif agentivity == 2
                plot_color(agentivity,:) = subj.color.non_agent;
            end
            
            %FIGURE : proba d'accepter pour chaque dimension (gain, loss, diff) et chaque sujet
            subplot(3,1,task_dim);
            hold on;
            pbaspect([1,0.5,1]);
            errorbar(cellfun(@mean, var_p(subj_idx,:,task_dim,agentivity)), cellfun(@std, var_p(subj_idx,:,task_dim,agentivity))./sqrt(cellfun(@length, var_p(subj_idx,:,task_dim,agentivity))),...
                'o-', 'Color', plot_color(agentivity,:), 'MarkerSize', 3);
            ylabel(opt.ylabel)
            ax = gca;
            ax.XTick = [1 2 3 4];
            ax.YTick = opt.YTick;
            axis(opt.axis)
            set(gca,'XTickLabel',{'[1-2]',']2-3]',']3-4]',']4-5]'})
            
            switch task_dim
                case 1
                    xlabel('Gain prospect ()')
                case 2
                    xlabel('Loss prospect ()')
                case 3
                    xlabel('Challenge difficulty')
            end
            
        end % Fin de la boucle � travers les dimensions
    end % Fin de la boucle � travers l'agentivit�
    
    legend('agent','non agent','Location','best');
    
    fig.PaperPosition = [0, 0, 7 14];
    fig_name = fullfile(subj.pathtofigsubj{subj_idx}, [conf.subjlist{subj_idx} opt.subj_fig_name]);
    print(fig, fig_name, '-dtiff', '-r200');
    
    close(fig);
    
end % Fin de la boucle � travers les sujets

% Figure pour tous les sujets
if subj_idx == length(conf.subjlist)
    
    fig = figure;
    hold on;
    
    for agentivity = 1:2 % Boucle � travers l'agentivit�
        for task_dim = 1:3 % task_dim : 1 = gain, 2 = loss, 3 = diff
            
            %FIGURE : proba d'accepter pour chaque dimension (gain, loss, diff) tous les sujets
            subplot(3,1,task_dim);
            hold on;
            errorbar(nanmean(cellfun(@nanmean, var_p(:,:,task_dim,agentivity))), nanstd(cellfun(@nanmean, var_p(:,:,task_dim,agentivity)))/sqrt(size(var_p,1)),...
                'o-', 'Color', plot_color(agentivity,:), 'MarkerSize', 3);
            ylabel(opt.ylabel)
            ax = gca;
            ax.XTick = [1 2 3 4];
            ax.YTick = opt.YTick;
            axis(opt.axis)
            set(gca,'XTickLabel',{'[1-2]',']2-3]',']3-4]',']4-5]'})
            
            switch task_dim
                case 1
                    xlabel('Gain prospect ()')
                case 2
                    xlabel('Loss prospect ()')
                case 3
                    xlabel('Challenge difficulty')
            end
            
        end
    end % Fin de la boucle � travers l'agentivit�
    
    legend('agent','non agent','Location','best');
    
    fig.PaperPosition = [0, 0, 7 14];
    fig_name = fullfile(subj.pathtofig, opt.all_fig_name);
    print(fig, fig_name, '-dtiff', '-r200');
    
    close(fig);
end

end