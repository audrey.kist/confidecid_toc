% Fit choices in the DECID task with possible modulation of free parameters
%
% INPUT : Structure
%   - S.subjname  = Subject name
%   - S.data      = 'data' matrix for the subject
%   - S.moy_estim
%   - S.std_estim
%   - S.trial_idx = Index of trials used
%   - S.trial_nb = Total trials number
%
% uses https://mbb-team.github.io/VBA-toolbox/wiki/

function choice_data = choiceModel_ConfiDecid(S, choice_data) % choice_data peut �tre une structure vide au d�but

if ~isempty(fieldnames(choice_data))
    inversionResult = choice_data.inversionResult;
    % inversionResult is... 
end

%% Define model space : produced a factorial combination of all possible options
% Warning = mood modulation options should be at the end (see below)

tableModel = struct2table(VBA_factorial_struct(...
    'isProspect', {1},...        % [0|1]   (is Prospect Theory) - Weird option name to define if utility is just a linear combination of the 3 informations (0) or expected outcome (p*G - (1-p)*L)
    'isPwin', {1},...            % [0|1]   Do you transform target size in subjective probability of success (using pWin_precision). If not p ~ targetSize
    'isSeparateWeight', {1},...  % [0|1]   Do you want to use different weight for expected reward and expected loss (Kg*p*G - Kl*(1-p)*L) or the same weight (== inverse temperature)
    'isConstant', {1},...        % [0|1]   Do you want to include a constant in the softmax
    'pDeformation', {0},...      % [0|1|2] pSuccess deformation (as in prospect theory ; with 1 or 2 free parameters) : 0 --> no deformation; 1 --> p= p^?/(((p^?) + (1-p)^?)^(1/?)); 2 --> p=(?*(p^?))/((?*(p^?))+(1-p)^?)
    'isCurvature', {0},...       % [0|1]   Do you want to deform gain and loss as in prospect theory : G = G ^ e with e < 1
    'isLowOption', {0},...       % [0|1]   Do you want to compute the value of low option (when participant reject the prospect). G and P are fixes (==0.5) but targetSize still varies from trial to trial
    'isMoodProbability', {0},... % [0|1]   Do you want mood to modulate pSuccess computation (modulation of sigma, subjective precision)
    'isMoodGain', {0},...        % [0|1]   Do you want mood to modulate weight of gain (modulation of Kg)
    'isMoodLoss', {0},...        % [0|1]   Do you want mood to modulate weight of loss (modulation of Kl)
    'isMoodConstant', {0}));     % [0|1]   Do you want mood to modulate the constant in the softmax (modulation of C)

%% Small sample of models to test

% tableModel=struct2table(factorial_struct(...
%                 'isProspect', {1},...          % [0|1]   (is Prospect Theory) - Weird option name to define if utility is just a linear combination of the 3 informations (0) or expected outcome (p*G - (1-p)*L)
%                 'isPwin', {1},...              % [0|1]   Do you transform target size in subjective probability of success (using pWin_precision). If not p ~ targetSize
%                 'isSeparateWeight', {1},...    % [0|1]   Do you want to use different weight for expected reward and expected loss (Kg*p*G - Kl*(1-p)*L) or the same weight (== inverse temperature)
%                 'isConstant', {1},...          % [0|1]   Do you want to include a constant in the softmax
%                 'pDeformation', {0},...        % [0|1|2] pSuccess deformation (as in prospect theory ; with 1 or 2 free parameters) : 0 --> no deformation; 1 --> p= p^?/(((p^?) + (1-p)^?)^(1/?)); 2 --> p=(?*(p^?))/((?*(p^?))+(1-p)^?)
%                 'isCurvature', {0},...         % [0|1]   Do you want to deform gain and loss as in prospect theory : G = G ^ e with e < 1
%                 'isLowOption', {0},...       % [0|1]   Do you want to compute the value of low option (when participant reject the prospect). G and P are fixes (==0.5) but targetSize still varies from trial to trial
%                 'isMoodProbability', {0},... % [0|1]   Do you want mood to modulate pSuccess computation (modulation of sigma, subjective precision)
%                 'isMoodGain', {0,1},...        % [0|1]   Do you want mood to modulate weight of gain (modulation of Kg)
%                 'isMoodLoss', {0},...        % [0|1]   Do you want mood to modulate weight of loss (modulation of Kl)
%                 'isMoodConstant', {0}));      % [0|1]   Do you want mood to modulate the constant in the softmax (modulation of C)

tableModel.isMoodEffect = (tableModel.isMoodProbability + tableModel.isMoodGain + tableModel.isMoodLoss + tableModel.isMoodConstant)>0;
nModel = height(tableModel);
tableModel.iModel = (1:nModel)';

% When there is a mood effect, priors from the same model without mood modulation will be required
% This is why mood modulation should be at the end

for iModel = 1:nModel
    if tableModel.isMoodEffect(iModel) == 0
        tableModel.iModelForPrior(iModel, 1) = NaN;
        modelToUse = iModel;
    else
        tableModel.iModelForPrior(iModel, 1) = modelToUse;
    end
end

%% Define priors

options = struct;
dim = struct('n',0, 'n_theta', 0, 'n_phi', 0);

[dim, options] = setPriors(...
    options, dim, 'X0',...
    'prospectValue', 0, 0,...  % == utility
    'probability', 0, 0,...);  % == pChoice
    'p_success', 0, 0);        % == pSuccess (added by Romane)

if ~isfield(S,'priors') % Priors d�finis par Fabien
    
    [dim, options] = setPriors(...
        options, dim, 'theta',...
        'sigma',-3, 0.5,...   % Subjective precision
        'gamma', 0, 0.5,...   % Parameter used to deform pSuccess (if pDeformation > 0)
        'delta', 0, 0.5,...   % Parameter used to deform pSuccess (if pDeformation == 2)
        'kE',    0, 5,...     % Weight of sizeTarget (used only if isProspect == 0)
        'kG',    0, 125,...5,...     % Weight of gain (125 if gains range = 0.2 to 1 ; 5 if range = 1 to 5)
        'kL',    0, 125,...5,...     % Weight of loss
        'kT',    0, 5,...     % Weight of time
        'C',     0, 5,...     % Constant (used only if isConstant == 1)
        'e1',    0, 1,...     % Gain deformation (used only if isCurvature == 1)
        'e2',    0, 1,...     % Loss deformation (used only if isCurvature == 1)
        'kMP',   0, 5,...     % Size of the mood effect on sigma (used only if isMoodProbability == 1)
        'kMG',   0, 5,...     % Size of the mood effect on Kg (used only if isMoodGain == 1)
        'kML',   0, 5,...     % Size of the mood effect on Kl (used only if isMoodLoss == 1)
        'kMC',   0, 5);       % Size of the mood effect on C (used only if isMoodConstant == 1)
    
else % On fixe certains param�tres
    
    [dim, options] = setPriors(...
        options, dim, 'theta',...
        'sigma', S.priors.sigma.mean, S.priors.sigma.var,...   % Subjective precision
        'gamma', S.priors.gamma.mean, S.priors.gamma.var,...   % Parameter used to deform pSuccess (if pDeformation > 0)
        'delta', S.priors.delta.mean, S.priors.delta.var,...   % Parameter used to deform pSuccess (if pDeformation == 2)
        'kE',    S.priors.kE.mean, S.priors.kE.var,...     % Weight of sizeTarget (used only if isProspect == 0)
        'kG',    S.priors.kG.mean, S.priors.kG.var,...     % Weight of gain
        'kL',    S.priors.kL.mean, S.priors.kL.var,...     % Weight of loss
        'kT',    S.priors.kT.mean, S.priors.kT.var,...     % Weight of time
        'C',     S.priors.C.mean, S.priors.C.var,...     % Constant (used only if isConstant == 1)
        'e1',    S.priors.e1.mean, S.priors.e1.var,...     % Gain deformation (used only if isCurvature == 1)
        'e2',    S.priors.e2.mean, S.priors.e2.var,...     % Loss deformation (used only if isCurvature == 1)
        'kMP',   S.priors.kMP.mean, S.priors.kMP.var,...     % Size of the mood effect on sigma (used only if isMoodProbability == 1)
        'kMG',   S.priors.kMG.mean, S.priors.kMG.var,...     % Size of the mood effect on Kg (used only if isMoodGain == 1)
        'kML',   S.priors.kML.mean, S.priors.kML.var,...     % Size of the mood effect on Kl (used only if isMoodLoss == 1)
        'kMC',   S.priors.kMC.mean, S.priors.kMC.var);       % Size of the mood effect on C (used only if isMoodConstant == 1)
end

inversionResult.tableModel = tableModel;
inversionResult.options = options;

%% Define f and g functions

f_fname = @f_brian_normativeChoice_prospectTheory_withModulation;
g_fname = @g_fit_probability;

%% Run
tic

% Display elapsed time
nh=floor(toc/3600);
nm=floor((toc-nh*3600)/60);
ns=floor(toc-nh*3600-nm*60);
disp([S.subjname ' : Total elapsed time = ' num2str(nh) 'h, ' num2str(nm) 'mn, ' num2str(ns) 's.'])

% y = observations
y = S.data(:,7); % Choices (0 = reject ; 1 = accept)
if size(y, 1) > 1; y = y'; end
options.isYout = (S.data(:,8) > 10)'; % RT to choose > 10 sec % isnan(y); % Edit isYout if you want to delete some trials
y(options.isYout == 1) = 0;
nTrial = length(y);
options.sources.type = 1;

% u (= utility)
gain = (S.data(:,4)'/10)/5; % Perspective of gain (/!\ from 0 to 1)
loss = (S.data(:,5)'/10)/5; % Perspective of loss (/!\ from 0 to 1)
% diff = S.data(:,6); % Difficulty
diff = S.data(:,6)'; % Difficulty - modified AK

z_score = [0.68, 0.65, 0.62, 0.59, 0.56, 0.53, 0.50, 0.47, 0.44, 0.42, 0.39, 0.36, 0.34, 0.31, 0.28,...
    0.26, 0.23, 0.21, 0.18, 0.16, 0.13, 0.11, 0.08, 0.06, 0.03, 0.00, -0.03, -0.06, -0.08, -0.11, -0.13,...
    -0.16, -0.18, -0.21, -0.23, -0.26, -0.28, -0.31, -0.34, -0.36, -0.39];
sizeTarget = 2*(S.moy_estim + z_score(diff-9) * S.std_estim); % In sec
% sizeTarget = 2.5./diff'; % Normalized Target Size

% time = 1:nTrial;
% time = S.trial_idx;
time = S.trial_idx'; %modif AK

% Loop across models
for iModel = 1:nModel
    clear posterior
    clear out
    disp(['Model number ' num2str(iModel)]);
    disp(tableModel(iModel,:));
    
    [u, dim, options] = setInput(options, dim,...
        'gain', gain,...
        'loss', loss,...
        'sizeTarget', sizeTarget,...
        'time', (time/S.trial_nb),... (time/nTrial),...
        'zGain', zscore(gain),...
        'zLoss', zscore(loss),...
        'zSizeTarget', zscore(sizeTarget));
    
    % Def Model
    dim.p = 1; % Only one output to predict == Choice
    dim.n_t = nTrial;
    options.dim = dim;
    
    % Misc
    options.inF.tableModel = tableModel(iModel,:);
    options.inG.modelSpace = tableModel(iModel,:);
    options.skipf = zeros(nTrial, 1);
    options.dim = dim;
    options.priors.a_alpha = Inf; % No state noise / deterministic
    options.priors.b_alpha = 0;   % No state noise / deterministic
    
    if tableModel.isMoodEffect(iModel) == 1
        options.inF.basicPosterior = inversionResult.model(tableModel.iModelForPrior(iModel)).(S.subjname).posterior;
    end
    
    % Define running options
    options.verbose         = 1; % Command windows outputs ?
    options.DisplayWin      = 1; % Show the results ?
    
    [posterior, out] = VBA_NLStateSpaceModel(y, u, f_fname, g_fname, dim, options);
    assert(isfield(out,'F'), 'Empty model');
    inversionResult.model(iModel).(S.subjname).posterior = posterior;
    inversionResult.model(iModel).(S.subjname).out = out;
end

% Find index of the subject
subj_idx = find(strcmp(fieldnames(inversionResult.model), S.subjname));

for iModel=1:nModel
    inversionResult.freeParameter.model(iModel).FF(subj_idx) = inversionResult.model(iModel).(S.subjname).out.F;
    inversionResult.FF(iModel, subj_idx)= inversionResult.model(iModel).(S.subjname).out.F;
end

for iModel = 1:nModel
    [~,theta,~]=getStateParamInput(inversionResult.model(iModel).(S.subjname).posterior.muX0,inversionResult.model(iModel).(S.subjname).posterior.muTheta, [],inversionResult.options.inF);
    theta.sigma = exp(theta.sigma);
    theta.gamma = exp(theta.gamma);
    theta.delta = exp(theta.delta);
    theta.kT = theta.kT;
    listField=fields(theta);
    nField = length(listField);
    for iField=1:nField
        inversionResult.freeParameter.model(iModel).(listField{iField})(subj_idx) = theta.(listField{iField});
    end
end

% Save data
choice_data.inversionResult = inversionResult;
choice_data.nModel = nModel;
% save([homedir filesep 'choiceModel' filesep 'result' filesep 'Brian_all_normativeChoice_prospectTheory_withModulation.mat'], 'inversionResult', 'nModel', 'nSubject');

end
