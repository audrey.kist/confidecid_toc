function  [ gx ] = g_fitMood_0003( x_t,P,u_t,in )

%% Get hidden states

[x,~,~] = getStateParamInput(x_t,P, u_t,in);

%% Minimal observation function !

gx = x.mood;

