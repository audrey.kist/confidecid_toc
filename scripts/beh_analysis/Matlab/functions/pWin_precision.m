function [p] = pWin_precision(df, sig)

% df = difficult�
% p = subjective probability of success
% Centr� sur 0.25 dans la t�che de Fabien

p = normcdf(.25+df/2,.25,sig) - normcdf(.25-df/2,.25,sig);
%p = normcdf(1+df/2,1,sig) - normcdf(1-df/2,1,sig);

end

