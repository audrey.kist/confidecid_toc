%% Function Name: ttest_beta_corr
%
% Description: ANALYSE C : ttest de la pente des corr�lations obtenues 
% dans l'analyse B
%
% Assumptions: BIDSified files and folders 
%
% Inputs:
%...
%
% Outputs:
%       ...
%
% Revision: 
% $Author: Audrey Kist
% Date: 
%---------------------------------------------------------

% 
function ttest_beta_corr(beta_corr, init, opt)

    fig = figure;
    hold on;

    for task_dim = 1:size(beta_corr,3) % task_dim : 1 = gain, 2 = loss, 3 = diff
        for agentivity = 1:2 % Boucle � travers l'agentivit�

            if agentivity == 1
                plot_color = init.color.agent;
            elseif agentivity == 2
                plot_color = init.color.non_agent;
            end

            % FIGURE
            subplot(3,1,task_dim);
            hold on;
            pbaspect([1,0.5,1]);
            xlim([0 3])
            xticks([1 2])

            bar(agentivity, mean(beta_corr(:, agentivity, task_dim)), 'FaceColor', plot_color);
            errorbar(agentivity, mean(beta_corr(:, agentivity, task_dim)), std(beta_corr(:, agentivity, task_dim))/sqrt(length(beta_corr)), 'Color', 'black')
            scatter(repmat(agentivity,1,size(beta_corr,1)), beta_corr(:, agentivity, task_dim), 15, 'MarkerFaceColor', [0.5 0.5 0.5], 'MarkerFaceAlpha', 0.3, 'MarkerEdgeColor', 'none')

            % T-test
            [h,p,~,stats] = ttest(beta_corr(:, agentivity, task_dim));
            if h == 1 % Mettre une �toile au dessus de la bar si c'est significaivement diff�rent de z�ro
                y = mean(beta_corr(:, agentivity, task_dim)) + std(beta_corr(:, agentivity, task_dim))/sqrt(length(beta_corr))*sign(mean(beta_corr(:, agentivity, task_dim)))*4;
                scatter(agentivity, y, '*k', 'sizeData',50,'linewidth',0.7)
            end

        end % Fin de la boucle � travers l'agentivit�

        xticklabels({'agent','non agent'})

        switch task_dim
            case 1
                xlabel('Gain prospect')
            case 2
                xlabel('Loss prospect')
            case 3
                xlabel('Challenge difficulty')
        end

        set(gca,'DataAspectRatio',opt.DataAspectRatio)
        ylabel(sprintf('Slope of correl\nw/ %s', opt.ylabel));

    end % Fin de la boucle � travers les dimensions de la t�che

    fig.PaperPosition = [0, 0, 7 14];
    fig_name = fullfile(init.pathtofig, opt.all_fig_name);
    print(fig, fig_name, '-dtiff', '-r200');

    close(fig);

end