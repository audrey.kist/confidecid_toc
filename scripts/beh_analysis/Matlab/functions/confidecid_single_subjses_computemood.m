%% Function Name: confidecid_single_subjses_computemood
%
% Description: ...
%
% Assumptions: BIDSified files and folders 
%
% Inputs:
%...
%
% Outputs:
%       ...
%
% Revision: 
% $Author: Audrey Kist
% Date: 
%---------------------------------------------------------

%% WORK IN PROGRESS
% cette fonction doit �tre reprise enti�rement une fois que j'ai les
% scripts pour la faire tourner !

function myoutput = confidecid_single_subjses_computemood(mysubj, conf)
    % THIS FUNCTION : still a work in progress !!

    % A AJOUTER:
    % v�rifier si le fichier n'existe pas d�j�. s'il existe, le charger.
    % sinon, le cr�er.   
%  % Load mood data of all subjects
%     mood_data_fullname = fullfile(init.pathtofig, 'all_mood_data.mat');
%     if exist(mood_data_fullname, 'file') && ~exist('mood_data', 'var')
%         load(mood_data_fullname, 'mood_data'); % Load 'mood_data' structure
%     elseif ~exist(mood_data_fullname, 'file') % Create an empty 'mood_data' structure
%         mood_data = struct();
%     end

%%
fprintf('[subj-analysis] entering computemood \n');

    mysubj.mood_data = struct();
    % If subject's name is not a field of 'mood_data' : compute TML for this subject
    
    % TO DO : loop over agentivity
    
    %if isempty(fieldnames(mood_data)) || ~isfield(mood_data.inversionResult.model(1), subj.name)
   % if isempty(mysubj.mood_data) 
        for agentivity = 1:length(conf.agentlist) 
            fprintf('[subj-analysis] getting mood data, agentivity = "%s" \n', conf.agentlist{agentivity});

            % get agentivity as a number
            agent = agentivity*(-1)+2; % 1 = agent ; 2 = non agent
            % Parameters
            S = [];
            S.subjname = mysubj.name;
            % get all data for this condition of agentivity
            S.data = mysubj.raw_data(mysubj.raw_data(:,11) == agent,:);
            % parameters to call the TML_ConfiDecid_VBA function
            S.meanTML = 0;
            S.pathtofig = conf.figpath;
            S.agent = agent;
            
            % Get model data for the subject
            % mysubj.mood_data = TML_ConfiDecid_VBA(S, mysubj.mood_data); 

            % Save mood data
            % save(mood_data_fullname, 'mood_data');
            
            % Get model datas for the subject
            mysubj.mood_data.(conf.agentlist{agentivity}) = TML_ConfiDecid_VBA(S, mysubj.mood_data); % Get model datas for the subject
            % does this really work ? because in the function there are
            % still different subjects ? 
            
                        % Save mood data
            % save(mood_data_fullname, 'mood_data');
            
        end % done for agent and non agent
        % then I need to plot 
        
        % WORK IN PROGRESS STARTS HERE : plot does not work yet
        
        for agentivity = 1:length(conf.agentlist) 
            fprintf('[subj-analysis] comparing mood models, agentivity = "%s" \n', conf.agentlist{agentivity});
            
            if ~isfield(mysubj.mood_data, 'best_model')

                sprintf('computing VBM group BMC');
                % BMC = best mood ? model ? comparison ? 
                p = VBA_groupBMC(mysubj.mood_data.(conf.agentlist{agentivity}).inversionResult.FF); 
                
                close all;
                
                best_model = find(p.a == max(p.a));

                % ajouter ici agentivit�
                null_model = find(ismember({mysubj.mood_data.(conf.agentlist{agentivity}).inversionResult.param.modelSpace.moodType}, 'null'));

                mysubj.mood_data.(conf.agentlist{agentivity}).best_model = best_model;
                mysubj.mood_data.(conf.agentlist{agentivity}).null_model = null_model;
                
                % save(mood_data_fullname, 'mood_data');

                % Plot best mood model
               % plot_best_mood_model(init, mysubj.mood_data)
                confidecid_plot_best_model(mysubj, conf)
                
                % to be replaced by confidecid_plot_best_mood_model

                % save plot somewhere ?? 
            end
        end
        % END OF WORK IN PROGRESS
        
    %end
    myoutput = mysubj;
end