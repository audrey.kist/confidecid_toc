function [updatedX] = f_brian_normativeChoice_prospectTheory_withModulation(x_t,P,u_t,in)


 
%% Get Parameters and inputs
[~,theta,u] = getStateParamInput(x_t,P, u_t,in); 


%% Transform Parameters (from R to the space in which they live)
sigma = exp(theta.sigma);
gamma = exp(theta.gamma);
delta = exp(theta.delta);
if in.tableModel.isSeparateWeight
    kG = theta.kG;
    kL = theta.kL;
else
    kG = theta.kG;
    kL = theta.kG; % same parameter for both weight
end
kT = theta.kT;
C = in.tableModel.isConstant * theta.C;

if in.tableModel.isCurvature
    e1 = 1./(1+exp(-theta.e1));
    e2 = 1./(1+exp(-theta.e2));
else
    e1 = 1;
    e2 = 1;
end

if in.tableModel.isProspect ==0
    kE= theta.kE;
    kG = theta.kG;
    kL = theta.kL;  
end

%Mood parameters
kMP=theta.kMP;
kMG=theta.kMG;
kML=theta.kML;
kMC=theta.kMC;
if in.tableModel.isMoodProbability == 0
    kMP = 0;
end
if in.tableModel.isMoodGain == 0
    kMG = 0;
end
if in.tableModel.isMoodLoss == 0
    kML = 0;
end
if in.tableModel.isMoodConstant == 0
    kMC = 0;
end
isMoodEffect= in.tableModel.isMoodEffect;


%% Specify mood effect

if isMoodEffect == 1
    [~,basicPrior,~] = getStateParamInput(x_t,in.basicPosterior.muTheta, u_t,in); 
    basicPrior.sigma = exp(basicPrior.sigma);
    basicPrior.gamma = exp(basicPrior.gamma);
    basicPrior.delta = exp(basicPrior.delta);
    basicPrior.e1=exp(basicPrior.e1);
    basicPrior.e2=exp(basicPrior.e2);
    sigma = sigma +  basicPrior.sigma * kMP * u.mood;
    kG = kG +  abs(basicPrior.kG) * kMG * u.mood;
    kL = kL +  abs(basicPrior.kL) * kML * u.mood;
    C = C + 3 * kMC * u.mood;
end

%% Compute pSuccess
if in.tableModel.isPwin == 1
    p=pWin_precision(u.sizeTarget, sigma);
else 
    p=2*u.sizeTarget;
end

if (p<0 | p>1)
   error('p should be <1');
end

% Probability distorsion
switch in.tableModel.pDeformation
    case 0
        p=p;
    case 1;
        p=(p^gamma)/(((p^gamma) + (1-p)^gamma)^(1/gamma));
    case 2
        p=(delta*(p^gamma))/((delta*(p^gamma))+(1-p)^gamma);
end

if in.tableModel.isLowOption == 1
    lowOptionUtility = kG *  p * (0.5 ^ e1) - kL * (1-p) * (0.5 ^ e2);
else
    lowOptionUtility = 0;
end

%% Compute prospectValue and utility
if in.tableModel.isProspect == 1
    prospectValue = kG * p * (u.gain^e1) - kL *(1-p) * (u.loss^e2);
    utility= prospectValue  + kT*u.time + C - lowOptionUtility;
elseif in.tableModel.isProspect ==0
    prospectValue = kG * u.zGain - kL * u.zLoss + kE * u.zSizeTarget;
    utility= prospectValue  + kT*u.time + C ;
else
    error('no model defined')
end

probability= 1/(1+exp(-utility));

%% Update X
updatedX = setNewX(x_t, in, 'prospectValue', prospectValue);
updatedX = setNewX(updatedX, in, 'probability', probability);
updatedX = setNewX(updatedX, in, 'p_success', p); % Added by Romane


