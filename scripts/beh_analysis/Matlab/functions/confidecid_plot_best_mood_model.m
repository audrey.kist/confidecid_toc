%%confidecid_plot_best_model
% PLOT BEST MOOD MODEL
% based on script of Romane, adapted for standalone packaging by Audrey

function confidecid_plot_best_mood_model(mysubj, conf)

    % for subj_idx = 1:length(init.subj_name)

        % S.data = init.group_data(init.group_data(:,1) == subj_idx,:); % All data for one subject
        S.data = mysubj.mood_data ; % ou choice data ou raw ?? 
        S.subjname = mysubj.name ;
        S.pathtofig = conf.figpath ;
        
        % WORK IN PROGRESS STARTS HERE
        % how to process agentivity ?? 

        TML_best = mood_data.inversionResult.model(mood_data.best_model).(S.subjname).out.suffStat.gx;

        % Define colors
        color.feed.positive = [0.322, 0.808, 0.573];
        color.feed.negative = [1, 0.416, 0.40];
        color.grey = [0.5 0.5 0.5];

        fig = figure;
        hold on;

        % Plot the color of real data according to feedback
        posFeedIdx = find(S.data(:,12) == 1);
        negFeedIdx = find(S.data(:,12) == 0);
        YoutIdx    = find(mood_data.inversionResult.model(mood_data.best_model).(S.subjname).out.options.isYout);
        interp_mood = mood_data.inversionResult.model(mood_data.best_model).(S.subjname).out.y;
        mood_pos = find(~isnan(S.data(:,14))); % Mood position
        mood_val = zscore(S.data(mood_pos, 14)); % Mood value (zscored)
        time = (1:length(interp_mood));

        plot(interp_mood, '--', 'LineWidth', 1, 'Color', color.grey, 'HandleVisibility', 'off') % Real data extrapolated
        scatter(posFeedIdx, interp_mood(posFeedIdx), 'o', 'LineWidth', 1, 'MarkerEdgeColor', color.feed.positive) % Positive feedbacks
        scatter(negFeedIdx, interp_mood(negFeedIdx), 'o', 'LineWidth', 1, 'MarkerEdgeColor', color.feed.negative) % Negative feedbacks
        scatter(YoutIdx, interp_mood(YoutIdx), 'o', 'LineWidth', 1, 'MarkerEdgeColor', color.grey) % Non fitted trials

        scatter(mood_pos(ismember(mood_pos, posFeedIdx)), mood_val(ismember(mood_pos, posFeedIdx)), 'filled', 'MarkerFaceColor', color.feed.positive); % Color positive feedbacks at actual mood ratings
        scatter(mood_pos(ismember(mood_pos, negFeedIdx)), mood_val(ismember(mood_pos, negFeedIdx)), 'filled', 'MarkerFaceColor', color.feed.negative); % Color negative feedbacks at actual mood ratings

        % Plot TML
        plot(TML_best, 'linewidth', 1, 'Color', color.grey) % TML best
        patch([time fliplr(time)], [mood_data.inversionResult.model(mood_data.best_model).(S.subjname).out.suffStat.gx - mood_data.inversionResult.model(mood_data.best_model).(S.subjname).out.suffStat.vy fliplr(mood_data.inversionResult.model(mood_data.best_model).(S.subjname).out.suffStat.gx + mood_data.inversionResult.model(mood_data.best_model).(S.subjname).out.suffStat.vy)],...
            color.grey, 'EdgeColor', 'none', 'FaceAlpha', .2, 'HandleVisibility', 'off'); % Confidence interval

        % Define ylim
        Ylim = [min([interp_mood, mood_data.inversionResult.model(mood_data.best_model).(S.subjname).out.suffStat.gx - mood_data.inversionResult.model(mood_data.best_model).(S.subjname).out.suffStat.vy]) - 0.5,...
            max([interp_mood, mood_data.inversionResult.model(mood_data.best_model).(S.subjname).out.suffStat.gx + mood_data.inversionResult.model(mood_data.best_model).(S.subjname).out.suffStat.vy]) + 0.5];

        ylim(Ylim);

        legend('Interpolated mood ratings (positive feedback)','Interpolated mood ratings (negative feedback)','Non fitted trials',...
            'Real mood ratings (positive feedback)','Real mood ratings (negative feedback)','TML (vba)','Location','bestoutside');
        xlabel('Trials');
        ylabel('Mood');
        title(S.subjname,'Interpreter', 'none');

        % Save figure
        fig_name = fullfile(S.pathtofig, sprintf('%s_mood_data_vs_model.tiff', S.subjname));
        fig.PaperPosition = [0 0 40 15]; % Fig size [left bottom width height]
        print(fig, fig_name, '-dtiff', '-r200');

        fprintf('[subj-analysis] saving mood data as  %s \n', fig_name);

end


