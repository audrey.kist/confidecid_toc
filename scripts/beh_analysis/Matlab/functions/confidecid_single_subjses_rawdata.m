%% Function Name: confidecid_single_subjses_rawdata
%
% Description: Based on subject name and session,
% this function concatenate behavioral files and returns a
% raw data mat + saves the raw data as .mat in the beh
% derivatives folder. 
%
% Assumptions: BIDSified files and folders 
%
% Inputs:
%           conf
%           mysubj
%       	mysubj.name
%           mysubj.ses
%
% Outputs:
%       mysubj
%       	mysubj.allruns_filename 
%           mysubj.raw_data
%
% Revision: 
% $Author: Audrey Kist
% Date: 
%---------------------------------------------------------

function myoutput = confidecid_single_subjses_rawdata(mysubj,conf)
    %create structure for output raw data
    mysubj.raw_data = [];

    % load task parameters file
    mysubj.paramfile = fullfile(conf.bidspath, append('sub-',mysubj.name), ...
        'ses-train','beh', append('sub-',mysubj.name,'_ses-train_task-confitrain_estimparam.mat'));
    load(mysubj.paramfile);
    fprintf('[subj-analysis] loading parameters from %s \n', mysubj.paramfile)

%     % create file name for choice model
%     mysubj.choicefile = fullfile(conf.derivpath, append('sub-',mysubj.name,'_ses-', mysubj.ses, ...
%         '_task-confidecid_choicedata_beh.mat'));

    % Loop across runs of subj-ses to create raw data matrix  
    nbruns = conf.nbruns ; 
    for run = 1:nbruns
        % build behavior file name
        beh_filename = fullfile(conf.bidspath, append('sub-',mysubj.name), ...
            append('ses-',mysubj.ses),'beh', sprintf('sub-%s_ses-%s_task-confidecid_run-%02d_beh.mat',...
            mysubj.name, mysubj.ses,run));
        % load behavior file name and append to subject data
        if isfile(beh_filename)
            fprintf('[subj-analysis] appending file %s  \n', beh_filename)
            load(beh_filename, 'data'); 
            mysubj.raw_data = [mysubj.raw_data ; data]; % this is the matrix with all data for this subject and ses
        else
            fprintf('[subj-analysis] file %s does not exist \n', beh_filename)
        end
    end 

    % save raw data as .mat
    mysubj.allruns_filename = fullfile(conf.derivpath, ...
        append('sub-',mysubj.name,'_ses-', mysubj.ses, '_task-confidecid_runconfidecid_allruns_beh.mat'));
    save(mysubj.allruns_filename,'mysubj');
    fprintf('[subj-analysis] raw data saved to %s  \n', mysubj.allruns_filename);
    
    % return output data
    myoutput = mysubj;
end