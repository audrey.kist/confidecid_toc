%% Function Name: confidecid_single_subjses_computechoice
%
% Description: Based on subject name and session,
% this function concatenate behavioral files and returns a
% raw data mat + saves the raw data as .mat in the beh
% derivatives folder. 
%
% Assumptions: BIDSified files and folders 
%
% Inputs:
%       conf
%       mysubj
%       	mysubj.name
%           mysubj.ses
%
% Outputs:
%       mysubj
%       ...
%
% Revision: 
% $Author: Audrey Kist
% Date: 
%---------------------------------------------------------

function myoutput = confidecid_single_subjses_computechoice(mysubj,conf)
    fprintf('[subj-analysis] processing choice data for subject %s session %s \n', ...
        mysubj.name, mysubj.ses);
    
    % create a structure containing choice data for each agentivity
    % condition separately
    mysubj.choice_data = struct(conf.agentlist{1}, struct(), conf.agentlist{2}, struct());

    % load param file
    load(mysubj.paramfile);
    
    % loop over agentivity
    for agentivity = 1:length(conf.agentlist) 
        fprintf('[subj-analysis] processing agentivity = "%s" \n', conf.agentlist{agentivity});
        % get agentivity as a number
        agent = agentivity*(-1)+2; % 1 = agent ; 2 = non agent
        % get all data for this condition of agentivity
        cond_data = mysubj.raw_data(mysubj.raw_data(:,11) == agent,:); 
        % get the trial numbers for this condition of agentivity
        agent_trials = find(mysubj.raw_data(:,11) == agent);

        % parameters to call the choiceModel_ConfiDecid function
        S = [];
        S.subjname = mysubj.name;
        S.data = cond_data;
        S.moy_estim = moy_estim;
        S.std_estim = std_estim;
        S.trial_idx = agent_trials;
        S.trial_nb = length(mysubj.raw_data);
        
        % call choice model function
        mysubj.choice_data.(conf.agentlist{agentivity}) = choiceModel_ConfiDecid(S, mysubj.choice_data.(conf.agentlist{agentivity})); % Get choice model for the subject

        mysubj.choice_data.nSubject = length(fieldnames(mysubj.choice_data.(conf.agentlist{agentivity}).inversionResult.model));

        % close all figures
        close all;
        
    end % End of the loop over agentivity
    myoutput = mysubj;
    
%     %% Mood models comparison
% 
% if ~isfield(mood_data, 'best_model')
%     
%     p = VBA_groupBMC(mood_data.inversionResult.FF); close all;
%     best_model = find(p.a == max(p.a));
%     null_model = find(ismember({mood_data.inversionResult.param.modelSpace.moodType}, 'null'));
%     
%     mood_data.best_model = best_model;
%     mood_data.null_model = null_model;
%     save(mood_data_fullname, 'mood_data');
%     
%     % Plot best mood model
%     plot_best_mood_model(init, mood_data)
%     
% end
    
end