%% Function Name: corr_task_dim_effect_subjses
%
% Description: ANALYSE B : corr�lation entre une variable et chaque 
% dimension de la t�che pour 1 subj 1 ses
%
% Assumptions: BIDSified files and folders 
%
% Inputs:
%       subj
%       conf
%       options
%
% Outputs:
%       beta_corr
%
% Revision: 
% $Author: Audrey Kist
% Date: 
%---------------------------------------------------------

% 
function beta_corr = corr_task_dim_effect_subjses(subj, conf, opt)

% opt.depdt_variable_col

    % get columns numbers for gain loss diff
    gld_columns = conf.gld_columns; 
    
    % get subject data
    subj_data = subj.raw_data;
    
    % create table for variable of interest
    % var_of_interest
%%
    for agentivity = 1:2 % Boucle � travers l'agentivit�

        % Col 1 = Prospects of GLD
        var_of_interest(1,agentivity) = {subj_data(subj_data(:,11) == agentivity*(-1)+2, gld_columns)}; 
        % Col 2 = variable of interest (accept, confidence...)
        var_of_interest(2,agentivity) = {subj_data(subj_data(:,11) == agentivity*(-1)+2, opt.depdt_variable_col)}; 

        % Corr�lation entre la variable d'int�r�t et toutes les dimensions (gain, loss, diff) pour chaque sujet
        x = var_of_interest{1,agentivity}; % Predictors
        y = var_of_interest{2,agentivity}; % Responses

        if VBA_isBinary(y) == true
            b = glmfit(x,y,'binomial');
            blink = 'logit';
        else
            b = glmfit(x,y);
            blink = 'identity';
        end

        intercept(agentivity) = b(1); % Intercept (utile pour le plot)
        beta_corr(agentivity, :) = b(2:end); % Pente de la corr�lation

    end % Fin de la boucle � travers l'agentivit�

    fig = figure;
    hold on;

    for task_dim = 1:length(gld_columns) % task_dim : 1 = gain, 2 = loss, 3 = diff
        for agentivity = 1:2 % Boucle � travers l'agentivit�
            prdct = var_of_interest{1,agentivity}(:,task_dim);
            resp = var_of_interest{2,agentivity};
            beta = [intercept( agentivity); beta_corr(agentivity, task_dim)];

            if agentivity == 1
                plot_color = conf.color.agent;
            elseif agentivity == 2
                plot_color = conf.color.non_agent;
            end

            % FIGURE : Plot de la corr�lation entre la variable d'int�r�t et l'une des dimensions
            subplot(3,1,task_dim);
            hold on;
            pbaspect([1,0.5,1]);

            yfit = glmval(beta, prdct, blink);
            st = sortrows([prdct, yfit]);
            p(agentivity) = plot(st(:,1), st(:,2), 'LineWidth', 2, 'Color', plot_color);
            scatter(prdct, resp, 10, 'filled', 'MarkerFaceColor', p(agentivity).Color, 'MarkerFaceAlpha', 0.3);

            ylabel(opt.ylabel)
            ylim(opt.ylim)

            switch task_dim
                case 1
                    xlabel('Gain prospect ()')
                case 2
                    xlabel('Loss prospect ()')
                case 3
                    xlabel('Challenge difficulty')
            end

        end % Fin de la boucle � travers l'agentivit�
    end % Fin de la boucle � travers les dimensions de la t�che

    legend(p, 'agent','non agent','Location','best');
    title(['subj-' subj.name '_ses-' subj.ses])
    
    set(gcf, 'Units', 'centimeters');
    set(gcf, 'PaperUnits', 'centimeters');
    fig.PaperPosition = [0 0 7 14]; % this is for printing
    fig.Position = [0 0 7 14]; % this is for figure size
    %fig_name = fullfile(conf.pathtofig, [conf.subjlist{subj_idx} opt.subj_fig_name]);

    fig_name = fullfile(conf.figpath, ['subj-' subj.name '_ses-' subj.ses opt.subj_fig_name]);   

    print(fig, fig_name, '-dtiff', '-r200');

    close(fig);
%% THIS PART DOES NOT WORK YET
% and should be used as output variable
    % T-test agent vs. non agent (identique pour chaque dimension de la t�che)
    % is it paired? c'est quoi h,p, stats ?? 
    %[h,p,~,stats] = ttest(cellfun(@mean, var_of_interest(:,2,1)), cellfun(@mean, var_of_interest(:,2,2)));

end