function confidecid_plot_myK_onoff(myK, myagentivity, table_kGLT_agent, conf)
%UNTITLED4 Summary of this function goes here
%   Detailed explanation goes here

    % plot data points for this coefficient, on compared to off 
    figure
    hold on
    yA = table2array(table_kGLT_agent(table_kGLT_agent.ses == 'off',{myK}))';
    xA = zeros(size(yA));
    scatter(xA,yA);

    yB = table2array(table_kGLT_agent(table_kGLT_agent.ses == 'on',{myK}))';
    xB = ones(size(yB));
    scatter(xB,yB);

    set(gca, 'XTick',[0,1])
    set(gca,'XTickLabel',{'off','on'})
    xlim([-0.5,1.5])
    mytitle = [myK ' comparison - ' myagentivity ' condition'];
    title(mytitle);

    myname = ['onoff_allsubj-points_agent_' myK '.png'];    
    figure_filename = fullfile(conf.figpath,myname);
    saveas(gcf,figure_filename)
    % close(figure)
    %%
    % Plot box for this coeff, on compared to off 
    figure
    boxplot([yA; yB],[ones(size(xA)), zeros(size(xB))])
    set(gca,'XTickLabel',{'off','on'})
    mytitle = [myK ' comparison - ' myagentivity ' condition'];
    title(mytitle);
    myname = ['onoff_allsubj-boxplot_' myagentivity '_' myK '.png'];   
    
    figure_filename = fullfile(conf.figpath, myname);

    saveas(gcf,figure_filename)
    %close(figure)
end